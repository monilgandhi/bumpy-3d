using UnityEngine;
using System;

public class PlayerStats
{
    public long CoinCount { get; private set; }
    public string CarId { get; private set; }
    public string WeaponId { get; private set; }
    public string CurrentCity {get; private set;}
    public long CurrentHighScore { get; private set; }
    public int CurrentHighCombo
    {
        get { return currentHighCombo; }
        set
        {
            if(value <= currentHighCombo)
            {
                return;
            }

            currentHighCombo = PersistenceManager.HighCombo = value;
        }
    }

    private DateTime installTimeUtc;
    public DateTime InstallTimeUTC => installTimeUtc;
    public bool ReviewProvided 
    {
        get 
        {
            return PersistenceManager.ReviewProvided;
        }

        set
        {
            PersistenceManager.ReviewProvided = true;
        }
    }
    
    public DateTime? DailyRewardDate
    {
        get
        {
            return PersistenceManager.LastDailyRewardDate;
        }
    }

    public int DailyRewardDayCount
    {
        get
        {
            return PersistenceManager.DailyRewardDayCount;
        }
    }

    public int DailyRewardConsecutiveCount
    {
        get
        {
            return PersistenceManager.DailyRewardConsecutiveCount;
        }
    }

    public CollectibleInventory CollectibleInventory { get; private set; }
    private bool? isSoundOn;
    private int currentHighCombo;
    private static PlayerStats instance;

    /// <summary>
    /// Call this to get the instance of current city or load a new city or load a new instance.null
    /// At a given point only 1 city can be active
    /// </summary>
    /// <returns>PlayerStats</returns>
    public static PlayerStats Instance
    {
        get
        {
            if(instance == null)
            {
                instance = LoadInternal();
            }

            return instance;
        }
    }

    public void LoadCity(string cityName)
    {
        if(string.IsNullOrEmpty(cityName))
        {
            throw new ArgumentNullException(nameof(cityName));
        }

        if(cityName != null && instance != null && cityName.Equals(instance.CurrentCity))
        {
            return;
        }

        LoadNewCity(cityName);
    }

    private PlayerStats(long coinCount) 
    {
        this.CoinCount = coinCount;
        this.CollectibleInventory = CollectibleInventory.Instance;
        this.installTimeUtc = PersistenceManager.InstallDateUTC;
    }

    private void LoadNewCity(string cityName)
    {
        instance.CurrentCity = cityName;
        instance.CarId = PersistenceManager.GetCurrentCarModelName(instance.CurrentCity);
        instance.WeaponId = PersistenceManager.GetCurrentWeaponModelName(instance.CurrentCity);
        // BumperCopsDebug.Log($"Current model id is {instance.CarId}");
        instance.CurrentHighScore = PersistenceManager.GetHighScore();
        instance.currentHighCombo = PersistenceManager.HighCombo;
        MissionStats.Instance.LoadCity();
    }

    private static PlayerStats LoadInternal()
    {
        long coinCount = Convert.ToInt64(PersistenceManager.GetTotalCoinCount());
        return new PlayerStats(coinCount);
    }

    public void UnlockAndSelectModel(string itemId, string city, ShopItemType itemType)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }

        if(itemType == ShopItemType.CAR)
        {
            PersistenceManager.HasPurchasedCar = true;
        } else if (itemType == ShopItemType.WEAPON)
        {
            PersistenceManager.HasPurchasedWeapon = true;
        }


        PersistenceManager.UnlockItem(itemId);
        SelectNewModel(itemId, itemType, city);
    }

    public void SelectNewModel(string itemId, ShopItemType shopItemType, string city)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }

        if(string.IsNullOrEmpty(city))
        {
            throw new ArgumentNullException(nameof(city));
        }

        switch(shopItemType)
        {
            case ShopItemType.CAR:
                BumperCopsDebug.Log($"Selecting new car with car id {itemId}");
                PersistenceManager.SetCurrentCarModelName(city, itemId);
                CarId = itemId;
                break;
            
            case ShopItemType.WEAPON:
                BumperCopsDebug.Log($"Selecting new weapon with weapon id {itemId}");
                PersistenceManager.SetCurrentWeaponName(city, itemId);
                WeaponId = itemId;
                break;

            default:
                throw new NotSupportedException(nameof(shopItemType));
        }
    }

    public void DailyRewardGiven(int maxNumberOfRewards, bool consecutiveDays)
    {
        // record time and record count
        PersistenceManager.LastDailyRewardDate = DateTime.Now.Date;

        int newCount = PersistenceManager.DailyRewardDayCount = PersistenceManager.DailyRewardDayCount + 1;
        if(consecutiveDays || PersistenceManager.DailyRewardConsecutiveCount == 0)
        {
            PersistenceManager.DailyRewardConsecutiveCount = PersistenceManager.DailyRewardConsecutiveCount + 1;
        }
        else
        {
            PersistenceManager.DailyRewardConsecutiveCount = 1;
        }

        BumperCopsDebug.Log($"maxNumberofreward {maxNumberOfRewards} and dailyrewardcount {newCount}");
        if(maxNumberOfRewards <= newCount)
        {
            PersistenceManager.DailyRewardDayCount = 0;
            PersistenceManager.DailyRewardConsecutiveCount = 0;
        }
    }

    public void IncreaseCoins(long coinsToGive)
    {
        if(coinsToGive < 0)
        {
            throw new FormatException("coinsToGive has to be positive");
        }
        
        if(coinsToGive == 0)
        {
            return;
        }
        
        CoinCount = Convert.ToInt64(PersistenceManager.AddOrReduce(coinsToGive));
        //BumperCopsDebug.Log("PlayerStats: New coin count is " + CoinCount);
    }

    public void ReduceCoins(int coinsToReduce)
    {
        if(coinsToReduce > CoinCount)
        {
            throw new ArgumentException("coin balance cannot be negative");
        }

        CoinCount = Convert.ToInt64(PersistenceManager.AddOrReduce(coinsToReduce * -1));
        //BumperCopsDebug.Log("PlayerStats: New coin count is " + CoinCount);
    }

    public bool SaveHighScore(long currentScore)
    {
        long previousHighScore = PersistenceManager.GetHighScore();
        if(currentScore <= previousHighScore)
        {
            return false;
        }

        PersistenceManager.SetHighScore(currentScore);
        CurrentHighScore = currentScore;
        return true;
    }

    public long GetHighScore()
    {
        return PersistenceManager.GetHighScore();
    }

    public int GetMaxEnemiesCaught()
    {
        return PersistenceManager.GetMaxEnemiesCaught();
    }

    public bool SetMaxEnemiesCaught(int count)
    {
        if(count <= PersistenceManager.GetMaxEnemiesCaught())
        {
            return false;
        }

        PersistenceManager.SetMaxEnemiesCaught(count);
        return true;
    }

    public void AddEnemiesCaughtCount(int count)
    {
        if(count <= 0)
        {
            return;
        }
        
        PersistenceManager.AddEnemiesCaughtCount(count);
    }
    

    public bool IsItemUnlocked(string carId)
    {
        if(string.IsNullOrEmpty(carId))
        {
            throw new ArgumentNullException(nameof(carId));
        }

        return PersistenceManager.IsItemUnlocked(carId);
    }

    public string GetSelectedSkin(string carId)
    {
        if(string.IsNullOrEmpty(carId))
        {
            return null;
        }

        return PersistenceManager.GetSelectedSkin(carId);
    }

    public void SetSelectedSkin(string carId, string skinId)
    {
        BumperCopsDebug.Log($"Saving the skin for {carId} and skin {skinId}");
        if(string.IsNullOrEmpty(carId) || string.IsNullOrEmpty(skinId))
        {
            return;
        }

        PersistenceManager.SetSelectedSkin(carId, skinId);
    }

    public void UnlockSkin(string carId, string skinId)
    {
        if(string.IsNullOrEmpty(carId) || string.IsNullOrEmpty(skinId))
        {
            return;
        }

        PersistenceManager.UnlockSkin(carId, skinId);
    }

    public bool IsSkinUnlocked(string carId, string skinId)
    {
        if(string.IsNullOrEmpty(carId))
        {
            return false;
        }

        return PersistenceManager.CheckUnlockSkin(carId, skinId);
    }

    public bool IsTutorialComplete()
    {
        return PersistenceManager.GetTutorialComplete();
    }

    public bool IsCurrentTutorialVersionComplete()
    {
        return PersistenceManager.CurrentTutorialVersion;
    }

    public void MarkTutorialCompleted()
    {
        PersistenceManager.SetTutorialComplete();
    }

    public int GetSkillUpgradeId(Skill skill, string modelName)
    {
        if(string.IsNullOrEmpty(modelName))
        {
            throw new ArgumentNullException(nameof(modelName));
        }

        return PersistenceManager.GetSkillUpgradeId(modelName, skill);
    }

    public bool IsSoundTurnedOn()
    {
        if(!isSoundOn.HasValue)
        {
            isSoundOn = PersistenceManager.GetSoundStatus();
        } 

        return isSoundOn.Value;
    }

    public void ToggleSound()
    {
        if(!isSoundOn.HasValue)
        {
            isSoundOn = PersistenceManager.GetSoundStatus();
        }

        PersistenceManager.SetSoundStatus(!isSoundOn.Value);
        isSoundOn = !isSoundOn.Value;
    }

    public void SetFirstPurchase()
    {
        PersistenceManager.RecordFirstPurchase();
    }

    public bool HaveShownFeedbackForm(string version)
    {
        return false;
    }

    public long CurrentSessionNumber => PersistenceManager.GetCurrentSessionNumber();
    public long IncrementSessionNumber()
    {
        PersistenceManager.IncrementSessionNumber();
        return PersistenceManager.GetCurrentSessionNumber();
    }

    public long LastReviewTime()
    {
        return PersistenceManager.GetLastReviewTime();
    }

    public void UpdateLastReviewTime(long time)
    {
        PersistenceManager.RecordLastReviewTime(time);
    }
    
    public int GetCurrentBaseCombo()
    {
        return PersistenceManager.BaseCombo;
    }

    public void IncreaseCurrentBaseCombo(uint value)
    {
        if(value == 0)
        {
            BumperCopsDebug.LogWarning("Invalid value for base combo");
            return;
        }

        int currentCombo = GetCurrentBaseCombo();
        PersistenceManager.BaseCombo = (int)value + currentCombo;
    }

    public bool HasPurchasedCar()
    {
        return PersistenceManager.HasPurchasedCar;
    }

    public bool HasPurchasedWeapon()
    {
        return PersistenceManager.HasPurchasedWeapon;
    }

    public bool HasPurchasedSkin()
    {
        return PersistenceManager.HasPurchasedSkin;
    }
    
    private static class PersistenceManager
    {
        private const string MODEL_NAME_SEPARATOR = "_";
        private const int DEFAULT_UPGRADE = 0;
        private const string COIN_COUNT = "coins";
        private const string HIGH_SCORE = "score";
        private const string SINGLE_RUN_CRIMINALS_CAUGHT = "single_run_criminal_caught";
        private const string TOTAL_CRIMINALS_CAUGHT = "total_criminal_caught";
        private const string CITY_MODEL_IN_USE = "model_{0}";
        private const string CITY_WEAPON_IN_USE = "weapon_{0}";
        private const string CITY_DEFAULT_MODEL_ID = "{0}_mini";
        private const string TUTORIAL = "game_tutorial";
        private const string TUTORIAL_V3 = "game_tutorial_v5";
        private const string SOUND = "SOUND";
        private const string FIRST_PURCHASE = "first_purchase";
        private const string SESSION_NUMBER = "game_session";
        private const string LAST_REVIEW_TIME = "last_review_secs";
        private const string INSTALL_DATE = "install_date";
        private const string DAILY_REWARD_DATE = "daily_reward_date";
        private const string DAILY_REWARD_DAY_COUNT = "daily_reward_day";
        private const string DAILY_REWARD_CONSECUTIVE_COUNT = "daily_reward_consecutive";
        private const string HIGH_COMBO = "high_combo";
        private const string CAR_SELECTED_SKIN = "{0}_skin";
        private const string CAR_UNLOCKED_SKIN = "{0}_skin_unlocked";
        private const string DEFAULT_CAR_SKIN = "{0}_skin_default";
        private const string PURCHASED_CAR = "purchased_car";
        private const string PURCHASED_SKIN = "purchased_skin";
        private const string PURCHASED_WEAPON = "purchased_weapon";
        private const string BASE_COMBO = "base_combo";
        private const string REVIEW_PROVIDED = "review_provided";

        public static bool ReviewProvided
        {
            get
            {
                return Convert.ToBoolean(PlayerPrefs.GetInt(REVIEW_PROVIDED, 0));
            }

            set
            {
                PlayerPrefs.SetInt(REVIEW_PROVIDED, 1);
            }
        }

        public static int BaseCombo 
        {
            get
            {
                return PlayerPrefs.GetInt(BASE_COMBO, 1);
            }

            set
            {
                PlayerPrefs.SetInt(BASE_COMBO, value);
                Save();
            }
        }

        public static bool HasPurchasedCar
        {
            get
            {
                return Convert.ToBoolean(PlayerPrefs.GetInt(PURCHASED_CAR, 0));
            }
            set
            {
                int intValue = Convert.ToInt32(value);
                PlayerPrefs.GetInt(PURCHASED_CAR, intValue);
                Save();
            }
        }

        public static bool HasPurchasedSkin
        {
            get
            {
                return Convert.ToBoolean(PlayerPrefs.GetInt(PURCHASED_SKIN, 0));
            }
            set
            {
                int intValue = Convert.ToInt32(value);
                PlayerPrefs.GetInt(PURCHASED_SKIN, intValue);
                Save();
            }
        }

        public static bool HasPurchasedWeapon
        {
            get
            {
                return Convert.ToBoolean(PlayerPrefs.GetInt(PURCHASED_WEAPON, 0));
            }
            set
            {
                int intValue = Convert.ToInt32(value);
                PlayerPrefs.GetInt(PURCHASED_WEAPON, intValue);
                Save();
            }
        }

        public static string GetSelectedSkin(string carId)
        {
            return PlayerPrefs.GetString(string.Format(CAR_SELECTED_SKIN, carId), 
                string.Format(DEFAULT_CAR_SKIN, carId));
        }

        public static void SetSelectedSkin(string carId, string skinId)
        {
            BumperCopsDebug.Log($"Skin for {carId} is {skinId}");
            PlayerPrefs.SetString(string.Format(CAR_SELECTED_SKIN, carId), skinId);
            Save();
        }

        public static bool CheckUnlockSkin(string carId, string skinId)
        {
            if(skinId.Equals(string.Format(DEFAULT_CAR_SKIN, carId)))
            {
                return true;
            }
            
            return Convert.ToBoolean(PlayerPrefs.GetInt(string.Format(CAR_UNLOCKED_SKIN, carId), 0));
        }

        public static void UnlockSkin(string carId, string skinId)
        {
            PersistenceManager.HasPurchasedSkin = true;
            PlayerPrefs.SetInt(string.Format(CAR_UNLOCKED_SKIN, carId), 1);
            Save();
        }

        public static bool CurrentTutorialVersion
        {
            get 
            {
                BumperCopsDebug.Log($"Curent version tutorial {PlayerPrefs.GetInt(TUTORIAL_V3, 0)}");
                return Convert.ToBoolean(PlayerPrefs.GetInt(TUTORIAL_V3, 0));
            }

            set
            {
                PlayerPrefs.SetInt(TUTORIAL_V3, Convert.ToInt32(value));
                Save();
            }
        }
        
        public static int HighCombo
        {
            get { return PlayerPrefs.GetInt(HIGH_COMBO); }
            set
            {
                PlayerPrefs.SetInt(HIGH_COMBO, value);
                Save();
            }
        }

        public static DateTime InstallDateUTC
        {
            get 
            {
                float secsSinceEpoch = PlayerPrefs.GetFloat(INSTALL_DATE, 0);

                // set if not set
                if(secsSinceEpoch == 0)
                {
                    DateTime utcNow = DateTime.UtcNow;
                    InstallDateUTC = utcNow.AddSeconds(-utcNow.Second).AddMilliseconds(-utcNow.Millisecond);
                    //BumperCopsDebug.Log($"Utc install time is {utcNow}");
                    secsSinceEpoch = PlayerPrefs.GetFloat(INSTALL_DATE, 0);
                }

                return DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(secsSinceEpoch)).UtcDateTime;
            }

            private set
            {
                DateTimeOffset valueUtc = value;
                //BumperCopsDebug.Log($"Utcoffset install time is {valueUtc.LocalDateTime}");
                PlayerPrefs.SetFloat(INSTALL_DATE, Convert.ToInt64(valueUtc.ToUnixTimeSeconds()));
                Save();
            }
        }

        public static DateTime? LastDailyRewardDate
        {
            get 
            {
                float secsSinceEpoch = PlayerPrefs.GetFloat(DAILY_REWARD_DATE, 0);
                if(secsSinceEpoch == 0)
                {
                    return null;
                }

                return DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(secsSinceEpoch)).UtcDateTime;
            }

            set
            {
                if(!value.HasValue)
                {
                    return;
                }

                DateTimeOffset dtOffset = value.Value;
                //BumperCopsDebug.Log($"Utcoffset install time is {valueUtc.LocalDateTime}");
                PlayerPrefs.SetFloat(DAILY_REWARD_DATE, Convert.ToInt64(dtOffset.ToUnixTimeSeconds()));
                Save();
            }
        }

        public static int DailyRewardDayCount
        {
            get 
            {
                return PlayerPrefs.GetInt(DAILY_REWARD_DAY_COUNT, 0);
            }
            set
            {
                BumperCopsDebug.Log($"Daily reward count is {value}");
                PlayerPrefs.SetInt(DAILY_REWARD_DAY_COUNT, value);
                Save();
            }
        }

        public static int DailyRewardConsecutiveCount
        {
            get 
            {
                return PlayerPrefs.GetInt(DAILY_REWARD_CONSECUTIVE_COUNT, 0);
            }
            set
            {
                //BumperCopsDebug.Log($"Utcoffset install time is {valueUtc.LocalDateTime}");
                PlayerPrefs.SetInt(DAILY_REWARD_CONSECUTIVE_COUNT, value);
                Save();
            }
        }

        public static void RecordLastReviewTime(long secSinceEpoch)
        {
            PlayerPrefs.SetFloat(LAST_REVIEW_TIME, secSinceEpoch);
            Save();
        }

        public static long GetLastReviewTime()
        {
            return Convert.ToInt64(PlayerPrefs.GetFloat(LAST_REVIEW_TIME, 0));
        }

        public static long GetCurrentSessionNumber()
        {
            return Convert.ToInt64(PlayerPrefs.GetFloat(SESSION_NUMBER, 0));
        }

        public static void IncrementSessionNumber()
        {
            long currentSessionNumber = GetCurrentSessionNumber();
            PlayerPrefs.SetFloat(SESSION_NUMBER, ++currentSessionNumber);
            Save();
        }

        public static int GetMaxEnemiesCaught()
        {
            return PlayerPrefs.GetInt(SINGLE_RUN_CRIMINALS_CAUGHT, 0);
        }

        public static void SetMaxEnemiesCaught(int enemiesCaught)
        {
            PlayerPrefs.SetInt(SINGLE_RUN_CRIMINALS_CAUGHT, enemiesCaught);
            Save();
        }

        public static void AddEnemiesCaughtCount(int enemiesCaught)
        {
            if(enemiesCaught <= 0)
            {
                return;
            }

            long totalCountSoFar = (long)PlayerPrefs.GetFloat(TOTAL_CRIMINALS_CAUGHT, 0);
            PlayerPrefs.GetFloat(TOTAL_CRIMINALS_CAUGHT, totalCountSoFar + enemiesCaught);
            Save();
        }

        public static void SetSoundStatus(bool soundStats)
        {
            PlayerPrefs.SetInt(SOUND, Convert.ToInt16(soundStats));
            Save();
        }
        
        public static bool GetSoundStatus()
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(SOUND, 1));
        }

        public static bool GetTutorialComplete()
        {
            return Convert.ToBoolean(PlayerPrefs.GetInt(TUTORIAL, 0));
        }

        public static void SetTutorialComplete()
        {
            PlayerPrefs.SetInt(TUTORIAL, 1);
            PersistenceManager.CurrentTutorialVersion = true;
            Save();
        }

        public static void RecordFirstPurchase()
        {
            PlayerPrefs.SetInt(FIRST_PURCHASE, 1);
            Save();
        }

        public static void SetSkillUpgradeId(string modelName, int upgradeId, Skill skill)
        {
            PlayerPrefs.SetInt(GetSkillKey(modelName, skill), upgradeId);
            Save();

            //BumperCopsDebug.Log("Upgrade id after save is " + upgradeId + " for model name " + modelName);
        }

        public static int GetSkillUpgradeId(string modelName, Skill skill)
        {
            return PlayerPrefs.GetInt(GetSkillKey(modelName, skill), DEFAULT_UPGRADE);
        }

        public static bool IsItemUnlocked(string carId)
        {
            int isUnlocked = PlayerPrefs.GetInt(carId, 0);
            return Convert.ToBoolean(isUnlocked);
        }

        public static void UnlockItem(string itemId)
        {
            PlayerPrefs.SetInt(itemId, 1);
            Save();
        }

        public static string GetCurrentCarModelName(string cityName)
        {
            String modelName = PlayerPrefs.GetString(GetCityModelKey(cityName));
            // save the defaultModel
            if(string.IsNullOrEmpty(modelName))
            {
                modelName = string.Format(CITY_DEFAULT_MODEL_ID, cityName);
                SetCurrentCarModelName(cityName, modelName);
                UnlockItem(modelName);
            }
            
            return modelName;
        }

        public static string GetCurrentWeaponModelName(string cityName)
        {
            return PlayerPrefs.GetString(GetWeaponModelKey(cityName));
        }

        public static void SetCurrentCarModelName(string cityName, string currentCar)
        {
            PlayerPrefs.SetString(GetCityModelKey(cityName), currentCar);
            Save();
        }

    
        public static void SetCurrentWeaponName(string cityName, string currentWeapon)
        {
            PlayerPrefs.SetString(GetWeaponModelKey(cityName), currentWeapon);
            Save();
        }

        public static long GetTotalCoinCount()
        {
            return Convert.ToInt64(PlayerPrefs.GetFloat(COIN_COUNT, 100));
        }

        public static void SetHighScore(long currentScore)
        {
            PlayerPrefs.SetFloat(HIGH_SCORE, currentScore);
            Save();
        }

        public static long GetHighScore()
        {
            return (long)PlayerPrefs.GetFloat(HIGH_SCORE, 0);
        }

        public static float AddOrReduce(float delta)
        {
            float currentCoinCount = GetTotalCoinCount();
            if((currentCoinCount + delta) < 0)
            {
                throw new ArithmeticException("Coins cannot be negative");
            }

            float newCoinCount = currentCoinCount + delta;
            BumperCopsDebug.Log("New coin count is " + newCoinCount);
            PlayerPrefs.SetFloat(COIN_COUNT, newCoinCount);

            // save right away;
            Save();

            return newCoinCount;
        }

        private static void Save()
        {
            PlayerPrefs.Save();
        }

        private static string GetSkillKey(string modelName, Skill skill)
        {
            return modelName + MODEL_NAME_SEPARATOR + Enum.GetName(typeof(Skill), skill).ToLower();
        }

        private static string GetCityModelKey(string cityName)
        {
            return string.Format(CITY_MODEL_IN_USE, cityName);
        }

        private static string GetWeaponModelKey(string cityName)
        {
            return string.Format(CITY_WEAPON_IN_USE, cityName);
        }
    }
}