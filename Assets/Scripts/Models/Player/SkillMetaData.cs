using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class SkillMetaData
{
    public int AttackMaxPoints;
    public int MovementMaxPoints;
    public float BoostMaxPoints;
}