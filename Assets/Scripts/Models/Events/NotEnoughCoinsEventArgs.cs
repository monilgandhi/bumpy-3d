public class NotEnoughCoinsEventArgs
{
    public readonly int Deficit;

    public NotEnoughCoinsEventArgs(int deficit)
    {
        this.Deficit = deficit;
    }
}