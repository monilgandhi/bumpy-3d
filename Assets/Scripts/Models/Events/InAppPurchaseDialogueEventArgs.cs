public class InAppPurchaseDialogueEventArgs
{
    public readonly bool IsShown;
    public InAppPurchaseDialogueEventArgs(bool shown)
    {
        this.IsShown = shown;
    }
}