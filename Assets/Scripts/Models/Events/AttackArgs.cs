using System;
using UnityEngine;
public class AttackArgs
{
    public readonly int RemainingHealth; 
    public readonly float AttackStrength;
    public readonly Vector3 AttackNormal;
    public readonly bool AttackedbyPlayer;

    public AttackArgs(int health, float attackStrength, Vector3 attackNormal, bool attackedbyPlayer)
    {
        this.RemainingHealth = health;
        this.AttackStrength = attackStrength;
        this.AttackNormal = attackNormal;
        this.AttackedbyPlayer = attackedbyPlayer;
    }
}
