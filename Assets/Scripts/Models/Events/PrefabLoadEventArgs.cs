using System;
using UnityEngine;
public class PrefabLoadEventArgs
{
    public readonly GameObject GObject;
    public readonly string PrefabId;
    public readonly bool Success;
    public Type CallingClass;
    public PrefabLoadEventArgs(GameObject c, Type callingClass, string prefabId)
    {
        if(c == null)
        {
            this.Success = false;
            return;
        }

        this.Success = true;
        this.PrefabId = prefabId;
        this.CallingClass = callingClass;
        this.GObject = c;
    }
}