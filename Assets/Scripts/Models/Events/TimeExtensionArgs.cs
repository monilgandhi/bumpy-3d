public class TimeExtensionArgs
{
    public readonly bool TimeExtended;
    public readonly int Cost = -1;
    public readonly int TimeExtension = -1;
    public readonly bool IsAds;
    public TimeExtensionArgs(int cost, bool timeExtended, int timeExtension, bool isAds)
    {
        if(timeExtended)
        {
            this.TimeExtended = true;
            this.Cost = cost;
            this.TimeExtension = timeExtension;
            this.IsAds = isAds;
        }
    }

    public TimeExtensionArgs()
    {
    }
}