public class PlayerObstacleCollisionEventArgs
{
    public readonly string NPCName;
    public readonly bool IsHitCounted;

    public PlayerObstacleCollisionEventArgs(string npcName, bool isHitCounted = true)
    {
        this.NPCName = npcName;

        // we send this because when we are running in backup we do not want to count the hit or get the timer down
        this.IsHitCounted = isHitCounted;
    }
}