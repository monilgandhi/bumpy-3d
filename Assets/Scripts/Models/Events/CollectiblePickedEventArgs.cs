public class CollectiblePickedEventArgs
{
    public readonly int Value;
    public readonly PowerupType Type;
    public CollectiblePickedEventArgs(int extensionValue, PowerupType type)
    {
        this.Value = extensionValue;
        this.Type = type;
    }
}