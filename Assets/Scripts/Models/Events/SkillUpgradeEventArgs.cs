public class SkillUpgradeEventArgs
{
    public readonly int CoinsSpent;

    /// <summary>
    /// Model id
    /// </summary>
    public readonly string ItemId;
    public readonly Skill skill;
    public SkillUpgradeEventArgs(int coinsSpent, string itemId, Skill skill)
    {
        this.skill = skill;
        this.CoinsSpent = coinsSpent;
        this.ItemId = itemId;
    }
}