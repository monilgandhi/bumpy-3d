public class TapGestureArgs
{
    public readonly float FocusXPoint;
    public readonly float FocusYPoint;

    public TapGestureArgs(float focusXPoint, float focusYPoint)
    {
        this.FocusXPoint = focusXPoint;
        this.FocusYPoint = focusYPoint;
    }
}