﻿
public class GameEventArgs
{
    public readonly bool NewSessionStarted;
    public readonly bool IsTutorial;
    public readonly long CurrentSessionNumber;

    public GameEventArgs(bool newSession, long currentSessionNumber)
    {
        this.NewSessionStarted = newSession;
        this.CurrentSessionNumber = currentSessionNumber;
    }

    public GameEventArgs(bool isTutorial)
    {
        this.IsTutorial = isTutorial;
    }
    
    public GameEventArgs() {}
}