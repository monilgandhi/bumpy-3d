public class CoinSpentEventArgs
{
    public readonly long AmountSpent;
    public readonly string SpendReason;
    public readonly string ItemId;
    public CoinSpentEventArgs(long amountSpent, string reason, string itemId)
    {
        this.AmountSpent = amountSpent;
        this.SpendReason = reason;
        this.ItemId = itemId;
    }
}