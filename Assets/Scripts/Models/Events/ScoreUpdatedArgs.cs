public class ScoreUpdatedEventArgs
{
    public readonly long NewScore;
    public readonly string Reason;
    public readonly int Combo;
    public readonly bool IsHighCombo;
    public readonly long PreviousScore;
    public readonly bool IsHighScore;
    public readonly int Multiplicand;
    public readonly long CoinsCollected;
    public ScoreUpdatedEventArgs(long newScore, string reason, int combo, long previousScore, bool isHighScore, int multiplicand)
    {
        this.NewScore = newScore;
        this.Reason = reason;
        this.Combo = combo;
        this.PreviousScore = previousScore;
        this.IsHighScore = isHighScore;
        this.Multiplicand = multiplicand;
    }

    public ScoreUpdatedEventArgs(long newScore, bool isHighScore, int combo, bool isHighCombo, long coinsCollected)
    {
        this.NewScore = newScore;
        this.IsHighScore = isHighScore;
        this.IsHighCombo = isHighCombo;
        this.Combo = combo;
        this.CoinsCollected = coinsCollected;
    }
}