public class PowerupSpawnedEventArgs
{
    public readonly PowerupReward Reward;
    public readonly int NumberOfLanesToPlace;
    public PowerupSpawnedEventArgs(PowerupReward reward)
    {
        this.Reward = reward;
        this.NumberOfLanesToPlace = 1;
    }

    public PowerupSpawnedEventArgs(PowerupReward reward, int numberOfLanesToPlace)
    {
        this.Reward = reward;
        this.NumberOfLanesToPlace = numberOfLanesToPlace;
    }
}