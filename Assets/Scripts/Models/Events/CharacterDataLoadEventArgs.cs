public class CharacterDataLoadEventArgs
{
    public readonly Character Character;
    public readonly bool Success;

    public CharacterDataLoadEventArgs(Character character)
    {
        this.Character = character;
    }
}