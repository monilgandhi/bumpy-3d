public class RoadtypeChangedEventArgs
{
    public readonly RoadManager.RoadType From;
    public readonly RoadManager.RoadType To;
    public readonly RoadController LastRoadCurrentType;
    public RoadtypeChangedEventArgs(RoadManager.RoadType from, RoadManager.RoadType to, RoadController lastRoadCurrentType)
    {
        this.From = from;
        this.To = to;
        this.LastRoadCurrentType = lastRoadCurrentType;
    }
}