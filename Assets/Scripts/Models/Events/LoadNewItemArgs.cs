public class LoadNewItemArgs
{
    public readonly string ItemId;
    public readonly string CarId;
    public readonly ShopItemType ItemType;
    public LoadNewItemArgs(string itemId, ShopItemType itemType, string carId)
    {
        this.ItemId = itemId;
        this.ItemType = itemType;
        this.CarId = carId;
    }
}