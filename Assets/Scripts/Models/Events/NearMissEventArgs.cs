public class NearMissEventArgs
{
    public readonly ObstacleType ObstacleType;
    public readonly float NearMissTime;
    public readonly string NPCName;
    public readonly PowerupReward Reward;
    public readonly LaneDirection LaneDirection;
    public NearMissEventArgs(ObstacleType type, float timeOfNearMiss, string npcName, PowerupReward reward, LaneDirection laneDirection)
    {
        this.ObstacleType = type;
        this.NearMissTime = timeOfNearMiss;
        this.NPCName = npcName;
        this.Reward = reward;
        this.LaneDirection = laneDirection;
    }
}