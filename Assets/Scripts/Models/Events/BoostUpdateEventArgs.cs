public class BoostUpdateEventArgs
{
    public readonly bool IsTurnedOn;
    public readonly float BoostLength;
    public readonly bool IsPowerup;

    public BoostUpdateEventArgs(bool isTurnedOn, float boostLength, bool isPowerup)
    {
        this.IsTurnedOn = isTurnedOn;
        this.BoostLength = boostLength;
        this.IsPowerup = isPowerup;
    }
}