using UnityEngine;
public class PlayerObstacleEventArgs
{
    public readonly GameObject Obstacle;

    public PlayerObstacleEventArgs(GameObject obstacle)
    {
        this.Obstacle = obstacle;
    }
}