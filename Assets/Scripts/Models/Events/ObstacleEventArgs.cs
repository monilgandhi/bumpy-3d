public class ObstacleEventArgs
{
    public readonly ObstacleController Obstacle;

    public ObstacleEventArgs(ObstacleController obstacle)
    {
        this.Obstacle = obstacle;
    }
}