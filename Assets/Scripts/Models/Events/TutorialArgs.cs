public class TutorialArgs
{
    public readonly bool IsTutorialActive;
    public readonly bool IsTutorialStepActive;
    public readonly bool IsTutorialStepComplete;
    public readonly TutorialStep TutorialStep;
    public readonly bool PauseGame;
    public TutorialArgs(bool isTutorialActive, TutorialStep step)
    {
        this.IsTutorialActive = isTutorialActive;
        this.TutorialStep = step;
    }

    public TutorialArgs(bool isTutorialActive, bool isTutorialStepComplete, TutorialStep step)
    {
        this.IsTutorialActive = isTutorialActive;
        this.TutorialStep = step;
        this.IsTutorialStepComplete = isTutorialStepComplete;
    }

    public TutorialArgs(bool isTutorialActive, TutorialStep step, bool pauseGame)
    {
        this.IsTutorialActive = isTutorialActive;
        this.TutorialStep = step;
        this.IsTutorialStepActive = true;
        this.PauseGame = pauseGame;
    }

    public override string ToString()
    {
        return $"Step: {this.TutorialStep}, IsTutorialStepActive: {this.IsTutorialStepActive}, IsTutorialActive: {this.IsTutorialActive}";
    }
    
}