public class BackUpCarPowerupEventArgs
{
    public readonly PlayerCharacter BackupCharacter;
    public readonly string BackupCharacterId;
    public BackUpCarPowerupEventArgs(PlayerCharacter newCharacter)
    {
        this.BackupCharacter = newCharacter;
        this.BackupCharacterId = newCharacter.Id;
    }

    public BackUpCarPowerupEventArgs(string backupCharacterId)
    {
        this.BackupCharacterId = backupCharacterId;
    }
}