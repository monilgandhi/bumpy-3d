public class FeedbackEventArgs
{
    public readonly int AnswersCompleted;

    public FeedbackEventArgs(int answersCompleted)
    {
        this.AnswersCompleted = answersCompleted;
    }
}