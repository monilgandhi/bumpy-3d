using System;
using UnityEngine;
//#undef DEBUG_ENABLED
public class BumperCopsDebug
{
    public static void Log(object message)
    {
#if DEBUG_ENABLED
        Debug.Log(message);
#endif
    }

public static void LogWarning(object message, UnityEngine.Object context = null)
    {
#if WARNING_ENABLED
        Debug.LogWarning(message, context);
#endif
    }

public static void LogError(object message)
    {
#if ERROR_ENABLED
        Debug.LogError(message);
#endif
    }

public static void LogFormat(string message, params object[] p)
    {
#if DEBUG_ENABLED
        Debug.LogFormat(message, p);
#endif
    }

public static void LogException(Exception exception)
    {
#if ERROR_ENABLED
        Debug.LogException(exception);
#endif
    }
}