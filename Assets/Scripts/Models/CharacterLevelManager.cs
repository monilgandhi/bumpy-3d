using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
public class CharacterLevelManager
{
    public static CharacterLevelManager Instance
    {
        get 
        {
            if(instance == null)
            {
                instance = new CharacterLevelManager();
            }

            return instance;
        }
    }

    private static CharacterLevelManager instance;

    private List<CharacterLevel> levels;
    private CharacterLevelManager()
    {
        string filePath = "characterLevel" + Path.DirectorySeparatorChar + "characterLevel";
        TextAsset aJson = Resources.Load<TextAsset>(filePath);

        if(aJson == null || string.IsNullOrEmpty(aJson.text))
        {
            BumperCopsDebug.LogWarning("Unable to find level mapping");
            return;
        }

        levels = JsonConvert.DeserializeObject<List<CharacterLevel>>(aJson.text);
    }
    
    public void Load()
    {
        return;
    }
    
    public int GetLevelId(float hitPoints)
    {
        int level = 0;
        for(int i = 0; i < levels.Count; i++)
        {
            if( hitPoints <= levels[i].MaxHitPoints)
            {
                level = i;
                break;
            }
        }

        return level + 1;
    }

    public int GetBounty(int levelId)
    {
        if(levelId > levels.Count)
        {
            BumperCopsDebug.LogWarning($"Invalid levelId max is {levels.Count} asked is {levelId}");
            return 0;
        }

        if(levelId <= 0)
        {
            BumperCopsDebug.LogWarning("Level id is less than or eq to zero. resetting to level 1");
            levelId = 1;
        }

        return levels[levelId - 1].Bounty;
    }

    public float GetHitPoints(int levelId)
    {
        if(levelId > levels.Count)
        {
            BumperCopsDebug.LogWarning($"Invalid levelId max is {levels.Count} asked is {levelId}");
            return 0;
        }

        if(levelId <= 0)
        {
            BumperCopsDebug.LogWarning("Level id is less than or eq to zero. resetting to level 1");
            levelId = 1;
        }

        return levels[levelId - 1].MaxHitPoints;
    }
}