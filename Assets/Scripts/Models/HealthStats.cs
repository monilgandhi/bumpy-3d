﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Text;

/// <summary>
/// This is the health stats for each of the 3Dmodel
/// </summary>

[RequireComponent(typeof(Character))]
public class HealthStats : MonoBehaviour
{
    public int Health 
    { 
        get
        { return this.RemainingHealth; }
    }

    public int StartHealth 
    { 
        get
        { return this.BaseHealth + this.HealthIncrement; }
    }

    [SerializeField]
    private int BaseHealth;

    private int RemainingHealth;
    private int HealthIncrement;
    private Character character;
    private Dictionary<string, HealthUpgrade> upgrades;
    private HealthUpgrade currentUpgrade;
    private const string HEALTH_STATS_RESOURCE_FOLDER = "player";

    public bool HasNextUpgrade { get { return !string.IsNullOrEmpty(this.currentUpgrade.NextUpgradeId); }}
    private void Awake() 
    {
        this.RemainingHealth = this.BaseHealth + this.HealthIncrement;
        this.character = this.GetComponent<Character>();
    }

    public void Init(int health)
    {
        this.BaseHealth = health;
        this.RemainingHealth = this.BaseHealth + this.HealthIncrement;
    }
    
    public void OnUpgrade()
    {
        if(!this.character.IsPlayer)
        {
            throw new InvalidProgramException("Cannot upgrade non player asset");
        }

        RetrieveCurrentUpgrade(this.currentUpgrade.NextUpgradeId);
    }

    private void RetrieveCurrentUpgrade(string upgradeId)
    {
        if(!this.upgrades.TryGetValue(upgradeId, out currentUpgrade))
        {
            throw new KeyNotFoundException("next upgrade not found");
        }

        this.HealthIncrement = currentUpgrade.HealthIncrement;
    }

    /// <summary>
    /// Call this function to inititalize the prefab health stats.
    /// </summary>
    private void LoadUpgrades()
    {
        string filePath = HEALTH_STATS_RESOURCE_FOLDER + Path.DirectorySeparatorChar + PlayerStats.Instance.CurrentCity + Path.DirectorySeparatorChar +
         this.character.Id + Path.DirectorySeparatorChar + "health";

        BumperCopsDebug.Log("File path is " + filePath);
        TextAsset prefabHealthFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabHealthFile.bytes, 0, prefabHealthFile.bytes.Length);
        upgrades = JsonConvert.DeserializeObject<List<HealthUpgrade>>(json).ToDictionary(k => k.Id, v => v);
    }

    public void ReduceHealthOfNPC(float delta)
    {
        this.RemainingHealth -= Mathf.RoundToInt(delta);
        BumperCopsDebug.Log($"HealthStats: Reducing the health by {delta} from {this.RemainingHealth}");
        if(this.RemainingHealth < 0)
        {
            this.RemainingHealth = 0;
        }
    }
}
