using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class ObjectiveCondition
{
    [JsonProperty("type")]
    [JsonConverter(typeof(StringEnumConverter))]
    public Conditions Type;

    [JsonProperty("min_required_value")]
    public int MinValue = int.MaxValue;

    [JsonProperty("max_required_value")]
    public int MaxValue = int.MinValue;

    [JsonProperty("vehicle")]
    [JsonConverter(typeof(StringEnumConverter))]
    public ObstacleType VehicleType;

    [JsonProperty("powerup")]
    [JsonConverter(typeof(StringEnumConverter))]
    public PowerupType PowerupType;
    public enum Conditions
    {
        NONE = 0,
        TIME_REMAINING,
        CRIMINAL,
        VEHICLE,
        CIVILIAN_HIT,
        WEAPON,
        ENEMY_HEALTH_BAR,
        POWERUP,
        COMBO,
        SCORE
    }
}