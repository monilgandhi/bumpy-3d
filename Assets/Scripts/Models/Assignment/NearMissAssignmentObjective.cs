using UnityEngine;
public class NearMissAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnNearMiss(NearMissEventArgs npc)
    {
        if(this.AssignmentObjective.ObjectiveCondition == null)
        {
            BumperCopsDebug.Log("Near miss increment");
            ++this.CompletedValue;
            base.OnGenericHandlerRun();
        }
        else
        {
            int conditionsFullFilled = 0;
            if(npc.ObstacleType == ObstacleType.NONE)
            {
                BumperCopsDebug.LogError("Obstacle type not set");
                return;
            }

            foreach(ObjectiveCondition c in this.AssignmentObjective.ObjectiveCondition)
            {
                switch(c.Type)
                {
                    case ObjectiveCondition.Conditions.VEHICLE:
                        if( c.VehicleType != ObstacleType.NONE)
                        {
                            // if obstacle type is same as what is described or if it is truck and include construction and garbage as well
                            if(c.VehicleType == npc.ObstacleType || 
                                (c.VehicleType == ObstacleType.TRUCK && (npc.ObstacleType == ObstacleType.CONSTRUCTION || npc.ObstacleType == ObstacleType.GARBAGE)))
                            {
                                ++conditionsFullFilled;
                            }
                        }

                        break;

                    // near miss when criminals are active or chase is on
                    case ObjectiveCondition.Conditions.CRIMINAL:
                        if(this.enemyController == null)
                        {
                            continue;
                        }

                        ++conditionsFullFilled;
                        break;

                    default:
                        break;
                }
            }

            if(conditionsFullFilled == this.AssignmentObjective.ObjectiveCondition.Count)
            {
                ++this.CompletedValue;
                base.OnGenericHandlerRun();
            }
        }
    }
}