using UnityEngine;
public class PowerupCollectedObjective : AbstractAssignmentObjective
{
    protected override void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Args for poweurp collected are null");
            return;
        }

        if(this.AssignmentObjective.ObjectiveCondition == null)
        {
            BumperCopsDebug.LogWarning("Powerup collected needs atleast one type of condition");
            return;
        }

        int conditionsCompleted = 0;

        BumperCopsDebug.Log($"condition size is {AssignmentObjective.ObjectiveCondition.Count}");
        foreach(ObjectiveCondition c in this.AssignmentObjective.ObjectiveCondition)
        {
            if(c.Type != ObjectiveCondition.Conditions.POWERUP)
            {
                continue;
            }

            BumperCopsDebug.Log($"Objective condition is {c.Type} poweruptype {c.PowerupType} annd powerup type of args is {args.Type} and condition type is {c.PowerupType}");
            if(c.PowerupType == args.Type)
            {
                ++conditionsCompleted;
            }
        }

        if(conditionsCompleted >= this.AssignmentObjective.ObjectiveCondition.Count)
        {
            BumperCopsDebug.Log("Powerup objective compelted");
            ++this.CompletedValue;
        }

        base.OnGenericHandlerRun();
    }
}