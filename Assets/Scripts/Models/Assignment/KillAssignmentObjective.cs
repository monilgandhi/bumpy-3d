using System;
using UnityEngine;
public class KillAssignmentObjective : AbstractAssignmentObjective
{
    private int currentEnemyHits;
    private int currentEnemyNumber = -1;
    private int currentEnemyObstaclesCollided;

    private bool weaponInUse;
    private int enemyHealthBarCount;
    protected override void OnEnemyHit(BumpEventArgs args)
    {
        if(this.enemyController == null || this.enemyController.EnemyCharacteristics == null)
        {
            return;
        }

        if(this.enemyController.EnemyCharacteristics.Number != currentEnemyNumber)
        {
            currentEnemyNumber = this.enemyController.EnemyCharacteristics.Number;
            currentEnemyHits = 0;
        }

        if(!weaponInUse)
        {
            weaponInUse = args.WeaponUsed;
        }

        ++currentEnemyHits;
    }

    protected override void OnPlayercollidedWithObstacle(PlayerObstacleCollisionEventArgs args)
    {
        if(!args.IsHitCounted)
        {
            return;
        }
        
        ++currentEnemyObstaclesCollided;
    }

    protected override void OnEnemySpawned(EnemyArgs args)
    {
        weaponInUse = false;
        enemyHealthBarCount = args.enemy.Health.RemainingHits;
        base.OnEnemySpawned(args);
    }

    protected override void OnEnemyDestroyed(EnemyArgs args)
    {
        if(this.AssignmentObjective.ObjectiveCondition == null)
        {
            //BumperCopsDebug.Log($"KillAssignmentObjective: No condition");
            ++this.CompletedValue;
            base.OnGenericHandlerRun();
        }
        else
        {
            int conditionsCompleted = 0;
            foreach(ObjectiveCondition c in this.AssignmentObjective.ObjectiveCondition)
            {
                switch(c.Type)
                {
                    case ObjectiveCondition.Conditions.TIME_REMAINING:
                        
                        if(args.TimeRemaining >= c.MinValue || args.TimeRemaining <= c.MaxValue)
                        {
                            BumperCopsDebug.Log($"KillAssignmentObjective: Time remaining is {args.TimeRemaining} and max value is {c.MaxValue}");
                            ++conditionsCompleted;
                        }

                        break;

                    case ObjectiveCondition.Conditions.CIVILIAN_HIT:
                    {
                        BumperCopsDebug.Log($"KillAssignmentObjective: civlians hit are {this.currentEnemyObstaclesCollided}");
                        if(c.MaxValue >= this.currentEnemyObstaclesCollided)
                        {
                            BumperCopsDebug.Log("KillAssignmentObjective: civilian assignment complete");
                            ++conditionsCompleted;
                        }

                        break;
                    }

                    case ObjectiveCondition.Conditions.WEAPON:
                        BumperCopsDebug.Log("checking if weapons were used");
                        if(weaponInUse)
                        {
                            BumperCopsDebug.Log("weapon used to arrest the enemy");
                            ++conditionsCompleted;
                        }

                        break;

                    case ObjectiveCondition.Conditions.ENEMY_HEALTH_BAR:
                        BumperCopsDebug.Log("checking if weapon was used");
                        if(enemyHealthBarCount >= c.MinValue)
                        {
                            BumperCopsDebug.Log("weapon used to arrest the enemy");
                            ++conditionsCompleted;
                        }

                        break;
                        
                    default:
                        break;
                }
            }

            BumperCopsDebug.Log($"KillAssignmentObjective: Conditions completed {conditionsCompleted} and requried {this.AssignmentObjective.ObjectiveCondition.Count}");
            // all conditions are AND. So they need to be complete before we report all the values;
            if(conditionsCompleted >= this.AssignmentObjective.ObjectiveCondition.Count)
            {
                BumperCopsDebug.Log($"KillAssignmentObjective: Increasing completed value");
                ++this.CompletedValue;
                base.OnGenericHandlerRun();
            }
        }

        // null the enemy controller
        this.enemyController = null;
        this.currentEnemyObstaclesCollided = 0;
    }
}