using System;
using System.Collections.Generic;
public abstract class AbstractAssignmentObjective
{
    public event Action<AbstractAssignmentObjective> ObjectiveComplete;
    public event Action<AbstractAssignmentObjective> ObjectiveUpdated;
    protected EnemyManager enemyManager;
    protected EnemyController enemyController;
    protected PlayerController player;
    protected ScoreBehaviour scoreBehaviour;
    protected CoinCollectibleBehaviour coinCollectibleBehaviour;
    protected GameManager gameManager;
    public int CompletedValue { get; protected set; }
    public AssignmentObjectives AssignmentObjective { get; private set; }
    protected int PreviousCompletedValue;
    public bool IsObjectiveComplete { get; protected set; }
    public bool IsObjectiveSkippedForCoins { get; private set; }
    private IList<NearMissBehaviour> currentCharacterNearMiss;
    public virtual void Init(PlayerController player, 
                            EnemyManager enemy, 
                            ScoreBehaviour scoreBehaviour, 
                            CoinCollectibleBehaviour coinBehaviour, 
                            GameManager gameManager,
                            AssignmentObjectives objectives)
    {
        if(player == null)
        {
            throw new ArgumentNullException(nameof(player));
        }

        if(enemy == null)
        {
            throw new ArgumentNullException(nameof(enemy));
        }

        if(scoreBehaviour == null)
        {
            throw new ArgumentNullException(nameof(scoreBehaviour));
        }

        if(coinBehaviour == null)
        {
            throw new ArgumentNullException(nameof(coinBehaviour));
        }

        if(gameManager == null)
        {
            throw new ArgumentNullException(nameof(gameManager));
        }

        if(objectives == null || 
            string.IsNullOrEmpty(objectives.Id) || 
            objectives.Count <= 0)
        {
            throw new ArgumentException(nameof(objectives));
        }

        this.player = player;
        this.enemyManager = enemy;
        this.AssignmentObjective = objectives;
        this.CompletedValue = MissionStats.Instance.GetObjectiveScore(objectives.Id);
        this.scoreBehaviour = scoreBehaviour;
        this.coinCollectibleBehaviour = coinBehaviour;
        this.gameManager = gameManager;

        this.gameManager.GameOver += (r) => RemoveAllTheListeners();
        this.gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
        this.gameManager.TutorialManager.TutorialEnd += OnTutorialEnded;
        if(this.CompletedValue < objectives.Count)
        {
            AddAllTheListeneres();
        }
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        RemoveAllTheListeners();
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        if(this.CompletedValue < AssignmentObjective.Count)
        {
            AddAllTheListeneres();
        }
    }

    protected virtual void OnSkillUpgraded(SkillUpgradeEventArgs args)
    {
        return;
    }

    protected virtual void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        return;
    }

    protected virtual void OnNearMiss(NearMissEventArgs npcName)
    {
        return;
    }

    protected virtual void OnCoinAwarded(int coinAmount, string reason)
    {
        return;
    }

    protected virtual void OnEnemySpawned(EnemyArgs args)
    {
        this.enemyController = args.enemy;
    }

    protected virtual void OnEnemyHit(BumpEventArgs args)
    {
       return;
    }

    protected virtual void OnEnemyDestroyed(EnemyArgs args)
    {
       return;
    }

    protected virtual void OnScoreUpdated(ScoreUpdatedEventArgs args)
    {
       return;
    }

    protected virtual void OnTutorialStepUpdate(TutorialArgs args)
    {
        return;
    }

    protected virtual void OnGameStarted(GameEventArgs args)
    {
        return;
    }

    protected virtual void OnPlayercollidedWithObstacle(PlayerObstacleCollisionEventArgs args)
    {
        return;
    }

    protected virtual void OnComboChanged(ComboEventArgs args)
    {
        return;
    }
    
    public void ForceCompleteForCoins()
    {
        BumperCopsDebug.Log($"Force completing objective {this.AssignmentObjective.Id}");
        this.CompletedValue = AssignmentObjective.Count;
        OnGenericHandlerRun(false);
        IsObjectiveSkippedForCoins = true;
    }

    protected void OnGenericHandlerRun(bool sendEvent = true)
    {
        if(this.CompletedValue > this.PreviousCompletedValue &&
             !IsObjectiveComplete && 
             !this.AssignmentObjective.SingleSession)
        {
            MissionStats.Instance.UpdateObjectiveCompletionCount(this.CompletedValue, this.AssignmentObjective.Id);
        }

        if(this.CompletedValue >= AssignmentObjective.Count)
        {
            this.CompletedValue = AssignmentObjective.Count;
            MissionStats.Instance.UpdateObjectiveCompletionCount(this.CompletedValue, this.AssignmentObjective.Id);
            
            BumperCopsDebug.Log($"objective with id completed {this.AssignmentObjective.Id}");
            OnObjectiveComplete();
        }
        else
        {
            OnObjectiveUpdated();
        }

        this.PreviousCompletedValue = this.CompletedValue;
    }

    protected void OnObjectiveComplete(bool sendEvent = true)
    {
        IsObjectiveComplete = true;
        RemoveAllTheListeners();

        if(sendEvent)
        {
            ObjectiveComplete?.Invoke(this);
        }
    }

    protected void OnObjectiveUpdated(bool sendEvent = true)
    {
        if(sendEvent)
        {
            ObjectiveUpdated?.Invoke(this);
        }
    }

    private void RemoveAllTheListeners()
    {
        if(this.player.NearMiss != null)
        {
            foreach(NearMissBehaviour n in this.player.NearMiss)
            {
                n.NearMiss -= OnNearMiss;
            }
        }
        
        // game listeners
        this.gameManager.GameStarted -= OnGameStarted;

        // player listeners
        this.player.AttackController.Bumped -= OnEnemyHit;
        this.player.PlayerCharacterChanged -= OnPlayerCharacterChanged;
        this.player.healthController.PlayerObstacleCollided -= OnPlayercollidedWithObstacle;

        // powerup listeners
        this.gameManager.PowerupManager.PowerupCollected -= OnPowerupCollected;

        // enemy listeners
        this.enemyManager.EnemySpawned -= OnEnemySpawned;
        this.enemyManager.EnemyHealthOver -= OnEnemyDestroyed;
        
        // score listeners
        this.scoreBehaviour.ScoreUpdated -= OnScoreUpdated;
        this.scoreBehaviour.ComboBehaviour.ComboChanged -= OnComboChanged;

        // coin listener
        this.coinCollectibleBehaviour.CoinAwarded -= OnCoinAwarded;

        // tutorial listener
        this.gameManager.TutorialManager.TutorialStepUpdate -= OnTutorialStepUpdate;
    }

    private void OnPlayerCharacterChanged()
    {
        BumperCopsDebug.Log("REgistering near miss characters");
        if(currentCharacterNearMiss != null)
        {
            foreach(NearMissBehaviour b in currentCharacterNearMiss)
            {
                b.NearMiss -= OnNearMiss;
            }
        }

        currentCharacterNearMiss = this.player.NearMiss;
        foreach(NearMissBehaviour b in currentCharacterNearMiss)
        {
            b.NearMiss += OnNearMiss;
        }
    }

    private void AddAllTheListeneres()
    {
        // player listeners
        if(this.player.character != null)
        {
            OnPlayerCharacterChanged();
        }

        // game listeners
        this.gameManager.GameStarted += OnGameStarted;

        // player listeners
        this.player.PlayerCharacterChanged += OnPlayerCharacterChanged;
        this.player.healthController.PlayerObstacleCollided += OnPlayercollidedWithObstacle;
        this.player.AttackController.Bumped += OnEnemyHit;
        
        // powerup behaviour
        this.gameManager.PowerupManager.PowerupCollected += OnPowerupCollected;

        // enemy listeners
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        this.enemyManager.EnemyHealthOver += OnEnemyDestroyed;

        // score listener
        this.scoreBehaviour.ScoreUpdated += OnScoreUpdated;
        this.scoreBehaviour.ComboBehaviour.ComboChanged += OnComboChanged;

        // coin listener
        this.coinCollectibleBehaviour.CoinAwarded += OnCoinAwarded;

        // tutorial listener
        this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
    }
}