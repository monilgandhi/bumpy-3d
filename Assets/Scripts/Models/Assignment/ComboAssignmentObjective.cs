using UnityEngine;
public class ComboAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnComboChanged(ComboEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Args for combo changed are null");
            return;
        }

        if(this.AssignmentObjective.ObjectiveCondition == null)
        {
            BumperCopsDebug.LogWarning("Combo objective needs atleast one type of condition");
            return;
        }

        int conditionsCompleted = 0;
        
        foreach(ObjectiveCondition c in this.AssignmentObjective.ObjectiveCondition)
        {
            if(c.Type != ObjectiveCondition.Conditions.COMBO)
            {
                continue;
            }

            BumperCopsDebug.Log($"New combo is {args.NewCombo}");
            if(c.MinValue < args.NewCombo)
            {
                ++conditionsCompleted;
            }
        }

        if(conditionsCompleted >= this.AssignmentObjective.ObjectiveCondition.Count)
        {
            BumperCopsDebug.Log("Powerup objective compelted");
            ++this.CompletedValue;
            base.OnGenericHandlerRun();
        }
    }
}