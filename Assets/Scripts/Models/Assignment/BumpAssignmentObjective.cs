using UnityEngine;
public class BumpAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnEnemyHit(BumpEventArgs args)
    {
        ++this.CompletedValue;
        base.OnGenericHandlerRun();
    }
}