using UnityEngine;
public class TutorialCompleteAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnTutorialStepUpdate(TutorialArgs args)
    {
        if(args.TutorialStep == TutorialStep.OVER)
        {
            //BumperCopsDebug.Log("TutorialCompleteAssignmentObjective");
            ++this.CompletedValue;
            this.PreviousCompletedValue = this.CompletedValue;
            OnObjectiveComplete();
        }
    }
}