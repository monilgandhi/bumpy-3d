using System;
using UnityEngine;
public class CoinAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnCoinAwarded(int coinAmountGiven, string reason)
    {
        this.CompletedValue += coinAmountGiven;
        base.OnGenericHandlerRun();
    }
}