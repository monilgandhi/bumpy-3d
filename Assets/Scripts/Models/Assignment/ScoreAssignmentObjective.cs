using System;
using UnityEngine;
public class ScoreAssignmentObjective : AbstractAssignmentObjective
{
    protected override void OnScoreUpdated(ScoreUpdatedEventArgs args)
    {
        //BumperCopsDebug.Log("ScoreAssignmentObjective: score updated");
        if(args == null)
        {
            BumperCopsDebug.Log("args for score update is null");
            return;
        }

        int completedConditions = 0;
        foreach(ObjectiveCondition c in AssignmentObjective.ObjectiveCondition)
        {
            if(c.Type != ObjectiveCondition.Conditions.SCORE)
            {
                continue;
            }

            if(c.MinValue <= args.NewScore)
            {
                ++completedConditions;
            }
        }

        if(completedConditions == AssignmentObjective.ObjectiveCondition.Count)
        {
            ++this.CompletedValue;
            base.OnGenericHandlerRun();
        }
    }
}