using UnityEngine;

public class ChaseAssignmentObjective : AbstractAssignmentObjective
{
    private bool isChaseOn;
    protected override void OnEnemySpawned(EnemyArgs args)
    {
        base.OnEnemySpawned(args);
        isChaseOn = true;
    }

    protected override void OnEnemyDestroyed(EnemyArgs args)
    {
        isChaseOn = false;
    }

    protected override void OnPlayercollidedWithObstacle(PlayerObstacleCollisionEventArgs args)
    {
        if(!isChaseOn || !args.IsHitCounted)
        {
            return;
        }

        int completedConditions = 0;

        foreach(ObjectiveCondition c in  this.AssignmentObjective.ObjectiveCondition)
        {
            switch(c.Type)
            {
                case ObjectiveCondition.Conditions.CIVILIAN_HIT:
                    ++completedConditions;
                    break;
            }
        }

        if(completedConditions >= this.AssignmentObjective.ObjectiveCondition.Count)
        {
            ++this.CompletedValue;
            base.OnGenericHandlerRun();
        }
    }
}