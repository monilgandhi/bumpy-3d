using System;
public class AssignmentObjectiveFactory
{
    public static AbstractAssignmentObjective GetClass(AssignmentObjectives objectives, 
                                                        PlayerController player, 
                                                        EnemyManager enemy,
                                                        ScoreBehaviour scoreBehaviour,
                                                        CoinCollectibleBehaviour coinCollectibleBehaviour,
                                                        GameManager gameManager)
    {
        return GetClassInternal(objectives, 
                                player: player, 
                                enemy: enemy, 
                                score: scoreBehaviour, 
                                coinCollectibleBehaviour: coinCollectibleBehaviour,
                                gameManager : gameManager);
    }

    public static AbstractAssignmentObjective GetClass(AssignmentObjectives objectives, InGameShopManager shop)
    {
        return GetClassInternal(objectives, shop: shop);
    }

    private static AbstractAssignmentObjective GetClassInternal(AssignmentObjectives objectives,
                                                                GameManager gameManager = null,
                                                                PlayerController player = null,
                                                                EnemyManager enemy = null,
                                                                ScoreBehaviour score = null,
                                                                CoinCollectibleBehaviour coinCollectibleBehaviour = null,
                                                                InGameShopManager shop = null)
    {
        AbstractAssignmentObjective resultClass;
        switch(objectives.Objective)
        {
            case AssignmentObjectives.ObjectiveType.KILL:
                resultClass = new KillAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.TOTAL_BUMP_COUNT:
                resultClass = new BumpAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.POWERUP_COLLECTED:
                resultClass = new PowerupCollectedObjective();
                break;

            case AssignmentObjectives.ObjectiveType.NEAR_MISS:
                resultClass = new NearMissAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.SCORE:
                resultClass = new ScoreAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.TUTORIAL:
                resultClass = new TutorialCompleteAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.COIN_COLLECT:
                resultClass = new CoinAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.CHASE:
                resultClass = new ChaseAssignmentObjective();
                break;

            case AssignmentObjectives.ObjectiveType.COMBO:
                resultClass = new ComboAssignmentObjective();
                break;

            default:
                throw new NotImplementedException(nameof(AssignmentObjectives.ObjectiveType));
        }

        resultClass.Init(player, enemy, score, coinCollectibleBehaviour, gameManager, objectives);
        return resultClass;
    }
}