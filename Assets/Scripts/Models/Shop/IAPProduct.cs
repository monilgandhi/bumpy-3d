using UnityEngine.Purchasing;
public class IAPProduct
{
    public readonly string ProductId;
    public readonly ProductMetadata Metadata;
    public readonly ProductDefinition Definition;

    public IAPProduct(string id, ProductMetadata metadata, ProductDefinition definition)
    {
        this.ProductId = id;
        this.Metadata = metadata;
        this.Definition = definition;
    }
}