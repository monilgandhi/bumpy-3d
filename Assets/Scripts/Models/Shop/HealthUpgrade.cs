﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the health upgrade class which is derived from json file of each object.
/// </summary>
public class HealthUpgrade
{
    public string Id;
    public int HealthIncrement;
    public int IncrementCost;
    public string NextUpgradeId;
}