using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
public class ShopInventory
{
    public event Action ShopLoaded;
    private const string SHOP_FOLDER = "shop";
    private static Dictionary<string, ShopItem> itemsDictionary = new Dictionary<string, ShopItem>();
    public IReadOnlyList<ShopItem> AllCars { get; private set; }
    public IReadOnlyList<ShopItem> AllWeapons { get; private set; }
    public IReadOnlyList<ShopItem> AllPowerups { get; private set; }
    private string currentCity;

    public bool IsLoaded { get; private set; }
    public int TotalItems
    {
        get 
        {
            return itemsDictionary.Count; 
        }
    }    

    public string CurrentCity
    {
        get
        {
            return currentCity;
        }
    }

    public ReadOnlyCollection<string> BackupCars { get; private set; }
    private static ShopInventory instance;

    public static ShopInventory Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new ShopInventory();
            }

            return instance;
        }
    }

    private ShopInventory(){}

    public bool LoadCityInventory(string city)
    {
        if(string.IsNullOrEmpty(city))
        {
            throw new ArgumentNullException(nameof(city));
        }

        if(city.Equals(currentCity))
        {
            return true;
        }

        this.currentCity = city;
        LoadCityInventory(city, ShopItemType.CAR);
        LoadCityInventory(city, ShopItemType.WEAPON);
        LoadCityInventory(city, ShopItemType.POWERUP);

        return true;
    }

    private bool LoadCityInventory(string city, ShopItemType itemType)
    {   
        if(!PlayerStats.Instance.CurrentCity.Equals(city))
        {
            throw new InvalidOperationException("Player stats city does not match current city");
        }

        string filePath = SHOP_FOLDER + Path.DirectorySeparatorChar + currentCity + "_" + itemType.ToString().ToLower();

        BumperCopsDebug.Log($"Loading shop inventory with filepath {filePath}");
        TextAsset prefabHealthFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabHealthFile.bytes, 0, prefabHealthFile.bytes.Length);
        List<ShopItem> shopItems = JsonConvert.DeserializeObject<List<ShopItem>>(json);

        FillMetaFieldsForCurrentInventory(shopItems, itemType);
    
        IsLoaded = true;
        return true;
    }

    private void FillMetaFieldsForCurrentInventory(List<ShopItem> shopItems, ShopItemType itemType)
    {
        if(itemsDictionary == null)
        {
            itemsDictionary = new Dictionary<string, ShopItem>();
        }

        string playersCurrentCar = PlayerStats.Instance.CarId;

        List<ShopItem> currentItems = new List<ShopItem>();
        for(int i = 0 ; i < shopItems.Count; i++)
        {
            ShopItem item = shopItems[i];

            // first check if the user has unlocked the car. Since we are adding this feature after launching,
            // do not screw players who have already unlocked. However seems like no one has but still
            item.IsUnlocked = PlayerStats.Instance.IsItemUnlocked(item.Id) || itemType == ShopItemType.POWERUP;

            // iif not and if the completed assignment is less than whatever the item requires, lock it
            if(!item.IsUnlocked)
            {
                item.IsUnlocked = false;
            }

            //BumperCopsDebug.Log("item id " + item.Id + " unlocked " + item.IsUnlocked);
            item.IsSelectedItem = playersCurrentCar.Equals(item.Id);
            item.ShopPosition = i;
            item.Skins = FillMetaForSkins(item.Skins, item.Id);
            itemsDictionary.Add(item.Id, item);
            currentItems.Add(item);
        }

        switch(itemType)
        {
            case ShopItemType.CAR:
                AllCars = new List<ShopItem>(currentItems);
                break;

            case ShopItemType.POWERUP:
                AllPowerups = new List<ShopItem>(currentItems);
                break;

            case ShopItemType.WEAPON:
                AllWeapons = new List<ShopItem>(currentItems);
                break;
        }

        ShopLoaded?.Invoke();
    }

    private List<ShopItem> FillMetaForSkins(List<ShopItem> skins, string carId)
    {
        if(skins == null || skins.Count == 0)
        {
            return skins;
        }

        string currentSelectedSkin = PlayerStats.Instance.GetSelectedSkin(carId);
        foreach(ShopItem s in skins)
        {
            if(s.Id.Equals(currentSelectedSkin))
            {
                s.IsSelectedItem = true;
            }

            s.IsUnlocked = PlayerStats.Instance.IsSkinUnlocked(carId, s.Id);
        }

        return skins;
    }

    public void UnlockItem(string itemId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }
        
        ShopItem listItem = null;

        if(!itemsDictionary.TryGetValue(itemId, out listItem))
        {
            throw new KeyNotFoundException(itemId);
        }

        PlayerStats.Instance.UnlockAndSelectModel(itemId, ShopInventory.Instance.CurrentCity, listItem.ItemType);
        listItem.IsUnlocked = true;
        itemsDictionary[itemId] = listItem;
    }

    public void UnlockSkin(string skinId, string carId)
    {
        if(string.IsNullOrEmpty(carId))
        {
            throw new ArgumentNullException(nameof(carId));
        }
        
        ShopItem listItem = null;

        if(!itemsDictionary.TryGetValue(carId, out listItem))
        {
            throw new KeyNotFoundException(carId);
        }

        foreach(ShopItem s in listItem.Skins)
        {
            if(s.Id.Equals(skinId))
            {
                BumperCopsDebug.Log($"unlocking skin {skinId} for car {carId}");
                s.IsUnlocked = true;
                PlayerStats.Instance.UnlockSkin(carId, skinId);
                break;
            }
        }
    }

    public void SelectShopCar(string itemId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            BumperCopsDebug.LogError("Loading null or invalid item for shop");
            return;
        }

        ShopItem newItem;
        if(!itemsDictionary.TryGetValue(itemId, out newItem))
        {
            BumperCopsDebug.LogError($"Item not found {itemId}");
            return;
        }
    }

    public CollectibleType GetItemCurrency(string itemId, string carId, ShopItemType itemType)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            BumperCopsDebug.LogWarning("itemId is null for skins");
            return CollectibleType.NONE;
        }
        
        string itemLookupKey = itemId;
        if(itemType == ShopItemType.SKIN)
        {
            itemLookupKey = carId;
        }

        ShopItem item;
        if(!itemsDictionary.TryGetValue(itemLookupKey, out item))
        {
            BumperCopsDebug.LogWarning($"itemId not found in dictionary {itemId}");
            return CollectibleType.NONE;
        }

        if(itemType != ShopItemType.SKIN)
        {
            return item.Currency == CollectibleType.NONE ? CollectibleType.COIN : item.Currency;
        }
        else
        {
            foreach(ShopItem s in item.Skins)
            {
                if(s.Id.Equals(itemId))
                {
                    item = s;
                    break;
                }
            }

            if(item == null)
            {
                BumperCopsDebug.LogWarning("Item is null");
                return CollectibleType.NONE;
            }

            return item.Currency == CollectibleType.NONE ? CollectibleType.COIN : item.Currency;
        }
    }

    public IReadOnlyList<ShopItem> GetItemSkins(string itemId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            BumperCopsDebug.LogWarning("itemId is null for skins");
            return null;
        }

        ShopItem item;
        if(!itemsDictionary.TryGetValue(itemId, out item))
        {
            BumperCopsDebug.LogWarning($"itemId not found in dictionary {itemId}");
            return null;
        }

        return item.Skins;
    }

    public bool SelectNewItemForPlay(string itemId, string carId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }

        ShopItemType itemType = GetItemType(itemId);
        if(itemType == ShopItemType.SKIN && string.IsNullOrEmpty(carId))
        {
            throw new ArgumentNullException(nameof(carId));
        }

        if(itemType == ShopItemType.NONE)
        {
            throw new ArgumentException(itemId);
        }

        string oldItemId = itemType == ShopItemType.CAR ? PlayerStats.Instance.CarId : PlayerStats.Instance.WeaponId;
        switch(itemType)
        {
            case ShopItemType.SKIN:
                return SelectSkinForPlay(itemId, carId);
            
            case ShopItemType.CAR:
            case ShopItemType.WEAPON:
                oldItemId = PlayerStats.Instance.WeaponId;
                if(SelectNewItemForPlayInternal(itemId, oldItemId))
                {
                    PlayerStats.Instance.SelectNewModel(itemId, itemType, CurrentCity);
                    return true;
                }

                break;
        }

        return false;
    }

    private ShopItemType GetItemType(string itemId)
    {
        ShopItem requiredItem = null;
        if(!TryGetItem(itemId, out requiredItem))
        {
            return ShopItemType.NONE;
        }

        return requiredItem.ItemType;
    }

    private bool SelectNewItemForPlayInternal(string newItemId, string oldItemId)
    {
                //first deselect the current item
        ShopItem currentSelectedItem = null;
        ShopItem newSelectedItem = null;

        // now select the new item
        if(!TryGetItem(newItemId, out currentSelectedItem))
        {
            BumperCopsDebug.LogWarning($"ItemId not found {newItemId}");
            return false;
        }

        if(!TryGetItem(oldItemId, out newSelectedItem))
        {
            BumperCopsDebug.LogWarning($"ItemId not found {oldItemId}");
            return false;
        }

        if(!newSelectedItem.IsUnlocked)
        {
            BumperCopsDebug.LogWarning($"item not unlocked {newSelectedItem.Id}");
            return false;
        }

        BumperCopsDebug.Log("Car Swithced");
        currentSelectedItem.IsSelectedItem = false;

        newSelectedItem.IsSelectedItem = true;
        return true;
    }

    private bool TryGetItem(string itemId, out ShopItem car)
    {
        if(!itemsDictionary.TryGetValue(PlayerStats.Instance.CarId, out car))
        {
            BumperCopsDebug.LogWarning($"ItemId not found {PlayerStats.Instance.CarId}");
            return false;
        }

        return true;
    }

    public bool SelectSkinForPlay(string skinId, string carId)
    {
        if(string.IsNullOrEmpty(skinId) || string.IsNullOrEmpty(carId))
        {
            BumperCopsDebug.LogWarning("Invalid skinId or carId");
            return false;
        }

        SelectNewItemForPlay(carId, carId);

        ShopItem curentSelectedItem = null;
        if(!TryGetItem(PlayerStats.Instance.CarId, out curentSelectedItem))
        {
            BumperCopsDebug.LogWarning($"ItemId not found {PlayerStats.Instance.CarId}");
            return false;
        }

        // check if the skin is unlock
        foreach(ShopItem i in curentSelectedItem.Skins)
        {
            if(i.Id.Equals(skinId))
            {
                if(!i.IsUnlocked)
                {
                    BumperCopsDebug.LogError($"Skins {skinId} is not locked for car {curentSelectedItem.Id}");
                    return false;
                }

                i.IsSelectedItem = true;
                BumperCopsDebug.Log($"Selecting skin for play {i.Id}");
                PlayerStats.Instance.SetSelectedSkin(curentSelectedItem.Id, i.Id);
                return true;
            }
        }

        return false;
    }

    public int ItemCost(string itemId)
    {
        if(!itemsDictionary.ContainsKey(itemId))
        {
            BumperCopsDebug.LogWarning("item not found in inventory");
            return 0;
        }

        return itemsDictionary[itemId].Cost;
    }

    public string ItemName(string itemId)
    {
        if(!itemsDictionary.ContainsKey(itemId))
        {
            BumperCopsDebug.LogWarning("item not found in inventory");
            return null;
        }

        return itemsDictionary[itemId].Name;
    }

    public CollectibleType PowerupCollectibleType(string itemId)
    {
        if(!itemsDictionary.ContainsKey(itemId))
        {
            BumperCopsDebug.LogWarning("item not found in inventory");
            return CollectibleType.NONE;
        }

        return itemsDictionary[itemId].CollectibleType;
    }

    public string ItemDescription(string itemId)
    {
        if(!itemsDictionary.ContainsKey(itemId))
        {
            BumperCopsDebug.LogWarning("item not found in inventory");
            return null;
        }

        return itemsDictionary[itemId].Description;
    }

    public bool IsItemContentLocked(string itemId)
    {
        if(String.IsNullOrEmpty(itemId))
        {
            BumperCopsDebug.LogWarning("item id is null");
            return false;
        }

        if(!itemsDictionary.ContainsKey(itemId))
        {
            BumperCopsDebug.LogWarning("item not found in inventory");
            return false;
        }

        return itemsDictionary[itemId].IsContentLocked;
    }

    public int ItemMissionRequirement(string itemId)
    {
        if(itemsDictionary == null || itemsDictionary.Count == 0)
        {
            throw new InvalidProgramException("Inventory not loaded");
        }

        return itemsDictionary[itemId].MissionUnlock;
    }
    
    public bool IsItemUnlocked(string itemId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }

        //BumperCopsDebug.LogWarning("IsItemUnlocked for non current item");
        ShopItem item = null;

        if(!itemsDictionary.TryGetValue(itemId, out item))
        {
            return false;
        }

        return item.IsUnlocked;
    }
}