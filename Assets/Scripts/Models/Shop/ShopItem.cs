using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
public class ShopItem
{
    public string Id;
    public int Cost;
    [JsonProperty("currency")]
    [JsonConverter(typeof(StringEnumConverter))]
    public CollectibleType Currency;

    [JsonProperty("unlock_mission")]    
    public int MissionUnlock;

    [JsonProperty("is_content_locked")]
    public bool IsContentLocked;

    [JsonProperty("name")]
    public string Name;

    [JsonProperty("description")]
    public string Description;

    [JsonProperty("collectibleType")]
    [JsonConverter(typeof(StringEnumConverter))]
    public CollectibleType CollectibleType;

    [JsonProperty("skins")]
    public List<ShopItem> Skins;

    [JsonProperty("item_type")]
    [JsonConverter(typeof(StringEnumConverter))]
    public ShopItemType ItemType;

    public bool IsUnlocked;
    public bool IsSelectedItem;

    public int ShopPosition;
}