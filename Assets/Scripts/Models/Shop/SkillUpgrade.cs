public class SkillUpgrade
{
    public int Id;
    public float Increment;
    public int Cost;
    public int RemainingUpgrade;
    public int NextUpgradeId;

    public SkillUpgrade()
    {}

    public SkillUpgrade(float increment)
    {
        this.Increment = increment;
    }
}