using System.IO;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
public static class ConfigCache
{
    private static Dictionary<string, string> rawConfigCache = new Dictionary<string, string>();

    public static bool TryGetConfig<ConfigType>(string fileName, string filePath, out ConfigType config) where ConfigType : class
    {
        config = default(ConfigType);
        if(string.IsNullOrEmpty(fileName))
        {
            BumperCopsDebug.LogError("filename is null");
            return false;
        }

        string rawJson;
        bool result = rawConfigCache.TryGetValue(fileName, out rawJson);

        if(!result)
        {
            rawJson = LoadFile(filePath + Path.DirectorySeparatorChar + fileName);
            if(string.IsNullOrEmpty(rawJson))
            {
                //BumperCopsDebug.LogError("config is null .Please check the path");
                return false;
            }
            
            rawConfigCache.Add(fileName, rawJson);
        }

        // load the file
        config = JsonConvert.DeserializeObject<ConfigType>(rawJson);
        if(config == null)
        {
            BumperCopsDebug.LogError($"config is null");
            return false;
        }
        
        return true;
    }

    private static string LoadFile(string filePath)
    {
        TextAsset assignmentJson = Resources.Load<TextAsset>(filePath);

        if(assignmentJson == null)
        {
            throw new FileNotFoundException(filePath);
        }

        return Encoding.UTF8.GetString(assignmentJson.bytes, 0, assignmentJson.bytes.Length);
    }
}