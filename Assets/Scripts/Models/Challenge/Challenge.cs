using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;
public class Challenge
{
    public readonly Guid Id;
    public List<ChallengeUser> Users { get; private set; }
    public readonly DateTime StartTime;
    public readonly DateTime EndTime;
    public readonly int BuyIn;
    public bool HasUserSubmittedScore
    {
        get
        {
            if(User != null && User.ChallengeHighScore > 0)
            {
                return true;
            }

            return false;
        }
    }

    private ChallengeUser user;
    public ChallengeUser User
    {
        get
        {
            if(user == null)
            {
                foreach(ChallengeUser u in Users)
                {
                    if(u.IsUser)
                    {
                        user = u;
                    }
                }
            }
            
            return user;
        }
    }

    public int NumberOftimesScoreSubmitted {get; private set; }
    public bool WinnerDeclared { get; private set; }

    [JsonConstructor]
    public Challenge(
        Guid newId, 
        List<ChallengeUser> users, 
        int buyIn, 
        DateTime startTime, 
        int numberOftimesScoreSubmitted,
        bool winnerDeclared) : this(newId, users, buyIn, startTime, false)
    {
        this.NumberOftimesScoreSubmitted = numberOftimesScoreSubmitted;
        this.WinnerDeclared = winnerDeclared;
    }

    public Challenge(Guid newId, List<ChallengeUser> users, int buyIn, DateTime startTime, bool store = true)
    {
        Id = newId;
        Users = users;
        SortScores();
        StartTime = startTime;
        EndTime = startTime.AddMinutes(15);
        BuyIn = buyIn;

        if(store)
        {
            ChallengeStats.Instance.StoreOrUpdateChallenge(this);
        }
    }

    public void OnSubmitNewHighScore(long highScore)
    {
        if(this.WinnerDeclared)
        {
            return;
        }
        
        foreach(ChallengeUser u in Users)
        {
            if(u.IsUser && u.ChallengeHighScore < highScore)
            {
                BumperCopsDebug.Log($"Updating user score {highScore}");
                u.ChallengeHighScore = highScore;
                ++NumberOftimesScoreSubmitted;
                BumperCopsDebug.Log($"number of times score submitted {NumberOftimesScoreSubmitted}");
                break;
            }
        }

        SortScores();
        Save();
    }

    private void SortScores()
    {
        if(Users == null || Users.Count <= 1)
        {
            return;
        }

        Users.
            Sort(delegate(ChallengeUser u1, ChallengeUser u2) 
                { 
                    return u2.ChallengeHighScore.CompareTo(u1.ChallengeHighScore); 
                }
            );
    }

    public bool BotsNewScore(long newScore)
    {
        if(WinnerDeclared)
        {
            return false;
        }

        bool scoreChanged = false;
        foreach(ChallengeUser u in Users)
        {
            if(u.ChallengeHighScore > 0)
            {
                continue;
            }

            u.ChallengeHighScore = newScore;
            scoreChanged = true;
            Save();
            break;
        }

        SortScores();
        return scoreChanged;
    }

    public void MarkChallengeComplete()
    {
        this.WinnerDeclared = true;
        Save();
    }

    public int GetCountBotsSubmittedScores()
    {
        return this.Users.Count(x => x.ChallengeHighScore > 0 && !x.IsUser);
    }
    private void Save()
    {
        ChallengeStats.Instance.StoreOrUpdateChallenge(this);
    }
}