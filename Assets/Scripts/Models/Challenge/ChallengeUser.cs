using System;
using UnityEngine;

public class ChallengeUser
{
    public readonly string Id;
    public readonly string Name;
    public long HighScore;
    public long ChallengeHighScore;
    public bool IsSelected;
    public bool IsUser;

    public readonly Device Device;

    public ChallengeUser(string id, string name, Device device, bool isUser)
    {
        if(string.IsNullOrEmpty(id))
        {
            throw new ArgumentException(nameof(id));
        }
        this.Id = id;

        if(string.IsNullOrEmpty(name))
        {
            throw new ArgumentException(nameof(name));
        }
        this.Name = name;

        if(device.Equals(Device.NONE))
        {
            throw new ArgumentException(nameof(device));
        }
        this.Device = device;
        this.IsUser = isUser;
    }
}

public enum Device
{
    NONE,
    iOS,
    ANDROID
}