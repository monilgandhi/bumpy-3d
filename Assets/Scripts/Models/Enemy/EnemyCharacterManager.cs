using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BumperCops.Models.Leveling;
public class EnemyCharacterManager
{
    private PlayerController player;

    // how fast the enemy was caught
    private int enemyCount = 0;
    private GameLevelManager gameLevelManager;
    private List<string> namesList = new List<string>();
    private int namesIndex;
    private const string NAMES_FILE = "names";
    public EnemyCharacterManager(PlayerController player, GameLevelManager gameLevelManager, TimerExtendBehaviour timerExtendBehaviour)
    {
        this.player = player;
        this.gameLevelManager = gameLevelManager;
        //BumperCopsDebug.Log($"speed rnage: {speedRange.Value} mmap value : {EnemyLevelingConfig.Instance.SpeedLevelMap.ElementAt((int)speedLevel).Value}");

        LoadEnemyNames();
    }

    public EnemySequence GetNextEnemy()
    {
        EnemySequence enemyCharacter = new EnemySequence();
        enemyCharacter.Health = Mathf.RoundToInt(this.player.character.AttackStats * gameLevelManager.EnemyHealth);

        float speedIncrement = this.player.character.SpeedStats * gameLevelManager.EnemySpeed;

        enemyCharacter.Movement = Mathf.RoundToInt(this.player.character.SpeedStats + speedIncrement);
        BumperCopsDebug.Log($"Speed increment is {speedIncrement} with gameLevelManager enemy speed {gameLevelManager.EnemySpeed}");
        enemyCharacter.Time = 30;
        enemyCharacter.Bounty = (int)gameLevelManager.EnemyBounty;
        //BumperCopsDebug.Log($"New enemy health range is {healthRange.Value} and level is {enemyCharacter.Level}");
        enemyCharacter.Name = NextName();
        enemyCharacter.Number = ++enemyCount;
        return enemyCharacter;
    }

    private string NextName()
    {   
        if(namesIndex >= namesList.Count)
        {
            namesList.Shuffle();
            namesIndex = 0;
        }

        return namesList[namesIndex++];
    }

    private void LoadEnemyNames()
    {
        if(!ConfigCache.TryGetConfig<List<String>>(NAMES_FILE, Constants.ENEMY_SEQUENCE_JSON_LOCATION, out namesList))
        {
            BumperCopsDebug.Log("Loading crrirmes list");
            LoadCrimeList();
        }

        namesList.Shuffle();
        namesIndex = 0;
    }

    private void LoadCrimeList()
    {
        namesList = Enum.GetNames(typeof(EnemyCrime)).ToList();
        //remove none
        namesList.RemoveAt(0);
    }
}