namespace BumperCops.Models
{
    public class Reward
    {
        public uint Amount { get; private set; }
        public CollectibleType Type;
        public RewardReason Reason;

        public Reward(uint amount, CollectibleType type, RewardReason reason)
        {
            this.Amount = amount;
            this.Type = type;
            this.Reason = reason;
        }

        public void IncrementAmount(uint amount)
        {
            if(amount <= 0)
            {
                return;
            }

            this.Amount += amount;
        }
    }
}