using System.Collections.Generic;
using UnityEngine;
public class Score
{
    public bool IsHighScore;
    public bool DidArrestMaxEnemies;
    public int NearMissCount { get; private set; }
    public long NearMissTotalScore { get; private set; }

    public int EnemyCount { get; private set; }
    public long EnemyArrestedTotalScore { get; private set; }

    private int perfectAttackCount;
    public long PerfectAttackTotalScore { get; private set; }

    private int timeCount;
    public long TimeBonusTotalScore { get; private set; }
    public long BonusTotalScore { get; private set; }
    public long CoinsCollected { get; private set; }

    public long TotalScore { get; private set; }    
    public long PreviousScore { get; private set; }

    private const int ENEMY_DESTROYED_BONUS = 500;
    private const int ENEMY_HIT_BONUS = 10;
    private const int NEAR_MISS_BONUS = 50;
    private const int NEAR_MISS_OPPOSITE_BONUS = 75;
    private const int TIME_REMAINING_BONUS = 5;
    private const int PERFECT_ATTACK_BONUS = 100;
    private const int POWERUP_PICKUP_BONUS = 30;
    private const int OBJECTIVE_COMPLETE = 1000;
    private const int MISSION_COMPLETE = 3000;
    private const string ENEMY_DESTROYED_STRING = "Criminals Busted";
    private const string NEAR_MISS_STRING = "Near Miss";
    private const string COINS_STRING = "Coins collected";
    private const string TIME_REMAINING_STRING = "Time Bonus (seconds)";
    private const string PERFECT_ATTACK_STRING = "Perfect Attack";
    private const string TOTAL_SCORE_STRING = "Total";

    public int RecordNearMissAndGetMultiplicand(LaneDirection direction, int comboValue)
    {
        int multiplicand = direction == LaneDirection.OPPOSITE ? NEAR_MISS_OPPOSITE_BONUS : NEAR_MISS_BONUS;
        long scoreToAdd = multiplicand * comboValue;
        this.NearMissTotalScore += scoreToAdd;
        ++this.NearMissCount;

        UpdateTotalScore(scoreToAdd);
        return multiplicand;
    }

    public void AddCoins(long coinReward)
    {
        this.CoinsCollected += coinReward;
        //BumperCopsDebug.Log($"Score: Adding coins {coinReward} total coins is {this.CoinsCollected}");
    }

    public int AddEnemyHitScoreAndGetMultiplicand(int comboValue)
    {
        long scoreToAdd = ENEMY_DESTROYED_BONUS * comboValue;
        UpdateTotalScore(scoreToAdd);

        return ENEMY_DESTROYED_BONUS;
    }

    public int AddObjectiveCompleteAndGetMultiplicand(int comboValue)
    {
        long scoreToAdd = OBJECTIVE_COMPLETE * comboValue;
        UpdateTotalScore(scoreToAdd);

        return OBJECTIVE_COMPLETE;
    }

    public int AddMissionCompleteAndGetMultiplicand(int comboValue)
    {
        long scoreToAdd = MISSION_COMPLETE * comboValue;
        UpdateTotalScore(scoreToAdd);

        return MISSION_COMPLETE;
    }

    public int AddPowerupPickupAndGetMuiltiplicand(int comboValue)
    {
        long scoreToAdd = POWERUP_PICKUP_BONUS * comboValue;
        UpdateTotalScore(scoreToAdd);

        return POWERUP_PICKUP_BONUS;
    }

    public int AddEnemyArrestedScoreAndGetMultiplicand(int comboValue)
    {
        long scoreToAdd = ENEMY_HIT_BONUS * comboValue;
        this.EnemyArrestedTotalScore += scoreToAdd;
        ++this.EnemyCount;
        UpdateTotalScore(scoreToAdd);

        return ENEMY_HIT_BONUS;
    }

    public int AddPerfectAttackScoreAndGetMultiplicand(int comboValue)
    {
        long scoreToAdd = PERFECT_ATTACK_BONUS * comboValue;
        this.PerfectAttackTotalScore += scoreToAdd;
        ++this.perfectAttackCount;
        UpdateTotalScore(PERFECT_ATTACK_BONUS);
        return PERFECT_ATTACK_BONUS;
    }

    public int AddTimeScoreAndGetMultiplicand(int timeRemaining, int comboValue)
    {
        int multiplicand = timeRemaining * TIME_REMAINING_BONUS;
        long scoreToAdd = multiplicand * comboValue;
        this.TimeBonusTotalScore += scoreToAdd;
        this.timeCount += timeRemaining;
        UpdateTotalScore(scoreToAdd);
        return multiplicand;
    }

    private void UpdateTotalScore(long scoreToAdd)
    {
        this.PreviousScore = this.TotalScore;
        // BumperCopsDebug.Log($"Score: Updating score from {this.TotalScore} to {this.TotalScore + scoreToAdd}");
        this.TotalScore += scoreToAdd;
    }

    // this flag acts as a lock to control the bonus scenario
    // below we reset the score if the current score goes beyond the existing multiplier score
    // additionally we also realign the score in case it is too less than required number of near misses

    /*public void ToggleTutorialLock(bool lockBonus)
    {
        // this lock is needed because when the step changes this function is called. It will force the multiplier to equal value as defined by
        // RequiredMultiplierScoreForTutorialBonus
        if(this.HasTutorialScoreLock == lockBonus)
        {
            return;
        }

        this.HasTutorialScoreLock = lockBonus;
        if(!HasTutorialScoreLock && this.CurrentMultiplierScore < RequiredMultiplierScoreForTutorialBonus())
        {
            this.CurrentMultiplierScore = RequiredMultiplierScoreForTutorialBonus();
        }
    }

    private long RequiredMultiplierScoreForTutorialBonus()
    {
        return this.requiredMultiplierScore[currentRequiredMultiplierScoreIndex].RequiredScore - (2 * NEAR_MISS_BONUS);
    }

    private void CheckForMultiplierUpdate()
    {
        if(this.CurrentMultiplierScore < this.requiredMultiplierScore[currentRequiredMultiplierScoreIndex].RequiredScore)
        {
            return;
        }

        if(HasTutorialScoreLock)
        {
            this.CurrentMultiplierScore = RequiredMultiplierScoreForTutorialBonus();
            return;
        }

        this.CurrentMultiplierScore = 0;
        //this.CurrentMultiplierScore -= this.requiredMultiplierScore[currentRequiredMultiplierScoreIndex].RequiredScore;
        this.currentRequiredMultiplierScoreIndex = this.currentRequiredMultiplierScoreIndex >= this.requiredMultiplierScore.Count ? 
            this.requiredMultiplierScore.Count : 
            this.currentRequiredMultiplierScoreIndex + 1;

        ++this.ScoreMultiplier;

        if(MultiplierChanged != null)
        {
            BumperCopsDebug.Log($"Score: Multiplier changed to {ScoreMultiplier}");
            MultiplierChanged(new MultiplierChangedEventArgs(false, 
                this.ScoreMultiplier, 
                this.ScoreMultiplier - 1, 
                this.requiredMultiplierScore[currentRequiredMultiplierScoreIndex].RequiredScore));
        }
    }

    private void LoadMultiplierScoreMap()
    {
        string filePath = BONUS_CONFIG_FOLDER_NAME + Path.DirectorySeparatorChar + "bonus";
        TextAsset assignmentJson = Resources.Load<TextAsset>(filePath);

        if(assignmentJson == null)
        {
            throw new FileNotFoundException(filePath);
        }

        string json = Encoding.UTF8.GetString(assignmentJson.bytes, 0, assignmentJson.bytes.Length);
        requiredMultiplierScore = JsonConvert.DeserializeObject<List<BonusConfig>>(json);
    }

    public Score(bool isTutorial = false)
    {
        LoadMultiplierScoreMap();
        this.HasTutorialScoreLock = isTutorial;
    }*/
}