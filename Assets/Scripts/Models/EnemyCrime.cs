
public enum EnemyCrime
{
    NONE = 0,
    BRAWLER,
    FRAUDSTER,
    CAR_THIEF,
    SCAMMER,
    RASH_DRIVER,
    ROBBER,
    HACKER,
    PRISONER,
    MAFIA,
    SMUGGLER
}