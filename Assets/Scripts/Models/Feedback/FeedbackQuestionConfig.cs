using System.Collections.Generic;
using Newtonsoft.Json;
public class FeedbackQuestionConfig
{
    [JsonProperty("id")]
    public int Id;    
    [JsonProperty("question")]
    public string Question;

    [JsonProperty("toolTips")]
    public List<string> ToolTips;

    [JsonProperty("choices")]
    public List<string> Choices;

    [JsonProperty("isChoice")]
    public bool IsChoice;

    [JsonProperty("version")]
    public string Version;
    public string Response;
}