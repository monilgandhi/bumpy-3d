public class CollectibleReward
{
    public readonly CollectibleType Collectible;
    public uint Amount;

    public CollectibleReward(CollectibleType type, uint amount)
    {
        this.Amount = amount;
        this.Collectible = type;
    }

    public void IncrementReward(uint incremental)
    {
        this.Amount += incremental;
    }
}