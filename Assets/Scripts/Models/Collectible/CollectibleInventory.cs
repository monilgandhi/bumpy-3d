using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
public class CollectibleInventory
{
    private Dictionary<CollectibleType, Sprite> spriteDictionary = new Dictionary<CollectibleType, Sprite>();
    private Dictionary<CollectibleType, int> collectibleCount = new Dictionary<CollectibleType, int>();
    private static CollectibleInventory instance;
    public static CollectibleInventory Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new CollectibleInventory();
            }

            return instance;
        }
    }

    private CollectibleInventory()
    {
        foreach(CollectibleType i in Enum.GetValues(typeof(CollectibleType)))
        {
            LoadInventory(i);
        }
    }

    public static string GetDescription(CollectibleType type)
    {
        switch(type)
        {
            case CollectibleType.AIR_STRIKE:
                return "Call the big bird for help.";
            
            case CollectibleType.BAG_COINS:
                return "Bag of good ol' coins.";

            case CollectibleType.COMBO_INCREMENT:
                return "Your combo increases by 5. Now thats a high score waiting to happen";

            case CollectibleType.EXTRA_TIME:
                return "Extra time when you really need it.";

            case CollectibleType.WEAPON_TOKEN:
                return "Buy crazy weapons.";
        }

        return "";
    }

    public int GetCollectibleAmountInventory(CollectibleType type)
    {
        int amount = 0;
        collectibleCount.TryGetValue(type, out amount);
        
        return amount;
    }

    public bool AddCollectible(CollectibleType type, uint increment = 1)
    {
        if(type == CollectibleType.BAG_COINS)
        {
            // add it to player stats
            PlayerStats.Instance.IncreaseCoins(increment);
            return true;
        }

        int currentAmount = GetCollectibleAmountInventory(type);
        int newValue = currentAmount + (int)increment;
        PersistenceManager.SetCollectibleAmount(type, newValue);
        collectibleCount[type] = newValue;

        return true;
    }

    public bool UseCollectible(CollectibleType type, int delta = 1)
    {
        int currentAmount = GetCollectibleAmountInventory(type);
        if(currentAmount <= 0)
        {
            return false;
        }

        int newValue = currentAmount - delta;
        PersistenceManager.SetCollectibleAmount(type, newValue);
        collectibleCount[type] = newValue;

        return true;
    }

    private void LoadInventory(CollectibleType type)
    {
        // load each for the collectible data 
        if(!collectibleCount.ContainsKey(type))
        {
            collectibleCount.Add(type, 0);
        }

        collectibleCount[type] = PersistenceManager.GetCollectibleAmount(type);
    }

    public long GetLastRewardTime()
    {
        return PersistenceManager.LastRewardTime;
    }

    public void RecordLastRewardTime(long utcDateTime)
    {
        PersistenceManager.LastRewardTime = utcDateTime;
    }

    private class PersistenceManager
    {
        private const string LAST_REWARD_TIME = "reward_time";
        private const string COLLECTIBLE_INVENTORY = "collectible_{0}";
        private const int DEFAULT_COLLECTIBLE_AMOUNT = 0;
        public static int GetCollectibleAmount(CollectibleType type)
        {
            return PlayerPrefs.GetInt(String.Format(COLLECTIBLE_INVENTORY, type), DEFAULT_COLLECTIBLE_AMOUNT);
        }

        public static void SetCollectibleAmount(CollectibleType type, int value)
        {
            PlayerPrefs.SetInt(String.Format(COLLECTIBLE_INVENTORY, type), value);
            Save();
        } 

        public static long LastRewardTime
        {
            get 
            { 
                return Convert.ToInt64(PlayerPrefs.GetFloat(LAST_REWARD_TIME, 0));
            }

            set
            {
                if(value <= 0)
                {
                    return;
                }

                PlayerPrefs.SetFloat(LAST_REWARD_TIME, value);
                Save();
            }
        }

        private static void Save()
        {
            PlayerPrefs.Save();
        }
    }
}