using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
/// <summary>
/// This is the health stats for each of the 3Dmodel
/// </summary>
public class Character : MonoBehaviour
{
    /// <summary>
    /// This is required because we read stats from file system which may take time
    /// Dependencies need to know when the data load is complete
    /// For instance in shop, the model is loaded after other scripts which depend on this
    /// </summary>
    public event DataLoadCompleteEventHandler DataLoaded;
    public delegate void DataLoadCompleteEventHandler(CharacterDataLoadEventArgs args);
    [Header("All Cars")]
    [SerializeField]
    private string id;

    [SerializeField]
    private float mass;

    [SerializeField]
    private GameObject centerOfMass;
    public GameObject CenterOfMass { get { return centerOfMass; }}

    [SerializeField]
    private GameObject rearEndPosition;
    public Vector3 RearEndPosition { get { return this.rearEndPosition.transform.position; }}

    [SerializeField]
    private WheelCollider[] wheels;
    public WheelCollider[] Wheels { get { return this.wheels; }}

    [Header("NPCs")]
    [SerializeField]
    private ObstacleType type;
    public ObstacleType ObstacleType { get {return this.type; }}

    [SerializeField]
    private TurnIndicatorBehaviour turnLights;
    public TurnIndicatorBehaviour TurnLights { get {return this.turnLights; }}

    [SerializeField]
    private LightBehaviour headLights;
    public LightBehaviour HeadLights { get {return this.headLights; }}

    [Header("Enemy")]
    [SerializeField]
    private ParticleSystem smokeOnLowHealth;
    public ParticleSystem SmokeParticleEffect { get {return this.smokeOnLowHealth; }}

    [SerializeField]
    private GameObject frontEndPosition;
    public Vector3 FrontEndPosition { get { return this.frontEndPosition.transform.position; }}

    [Header("roadblock")]
    [SerializeField]
    private PoliceLightBehaviour policeLight;
    
    [Header("Player")]
    [SerializeField]
    private bool isPlayer;
    public bool IsPlayer { get { return isPlayer; }}
    public SkillStats AttackStats { get; private set; }
    public float MovementStats { get; private set; }
    public string Id { get { return id; }}
    public float Mass { get { return mass; }}
    public PoliceLightBehaviour PoliceLight { get { return policeLight; }}

    public bool IsVisible { get; private set; }
    public void InitAndStartEnemy(float speed)
    {
        this.MovementStats = speed;
        this.gameObject.SetActive(true);
    }

    private void OnBecameInvisible() 
    {
        IsVisible = false;    
    }

    private void OnBecameVisible() 
    {
        IsVisible = true;    
    }
    
    public void Destroyed()
    {
        this.gameObject.SetActive(false);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
