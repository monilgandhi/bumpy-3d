using UnityEngine;
using System.Collections.Generic;

public class PowerupGenerator
{
    private int donutGeneratedCount;
    private float donutLastSpawnCheckTime;
    private Dictionary<GameLeverType, List<float>> donutConfig;
    private string lastEnemyIdDonut;

    // we want boost to start immediately
    private float boostStartTimer = 0f;
    private float boostCoolOff = BOOST_COOLOFF_TIME_SECONDS;
    private const float DONUT_COOLOFF_TIME_SECONDS = 8f;
    private const float COOLOFF_BETWEEN_COLLECTIBLES = 0.5f;
    private const float BOOST_COOLOFF_TIME_SECONDS = 5f;
    private const float MAX_DONUT_PER_ENEMY = 3;

    private const string DONUT_POWERUP_CHANCE_CONFIG = "donut_powerup";
    private const string CONFIG_FOLDER = "collectible";
    private bool startGeneration = false;
    public PowerupGenerator(long sessionNumber)
    {
        string fileName = sessionNumber <= 1 ? $"{DONUT_POWERUP_CHANCE_CONFIG}_1" : DONUT_POWERUP_CHANCE_CONFIG;
        BumperCopsDebug.Log($"loading donut config {fileName}");
        if(!ConfigCache.TryGetConfig<Dictionary<GameLeverType, List<float>>>(fileName, CONFIG_FOLDER, out donutConfig))
        {
            BumperCopsDebug.LogWarning($"unable to load config {fileName}");
            donutConfig = null;
            return;
        }

        startGeneration = true;
    }

    public void UpdateBoostCoolOff(float boostCoolOff)
    {
        if(boostCoolOff <= 0)
        {
            BumperCopsDebug.LogWarning("Boost cooloff has to be non-zero or non-negative");
            return;
        }
        
        this.boostCoolOff = boostCoolOff;
    }

    public void Reset()
    {
        donutLastSpawnCheckTime = 0;
        donutGeneratedCount = 0;
    }

    public bool CanSpawnBoost(EnemyController enemy)
    {
        if(!startGeneration)
        {
            return false;
        }

        bool result = enemy != null && (Time.time - boostStartTimer) > boostCoolOff;
        if(result)
        {
            // reset
            boostStartTimer = Time.time;
            // delay generation of clock by 2 seconds
            donutLastSpawnCheckTime += COOLOFF_BETWEEN_COLLECTIBLES;
        }

        return result;
    }

    public bool CanSpawnDonut(
            string enemyId,
            GameLevel enemyLevel, 
            GameLevel gameSpeedLevel, 
            GameLevel trafficDensityLevel)
    {
        if(!startGeneration)
        {
            return false;
        }

        if(string.IsNullOrEmpty(enemyId))
        {
            BumperCopsDebug.LogWarning("Enemy id needs to be provided");
            return false;
        }

        if(enemyId.Equals(lastEnemyIdDonut))
        {
            return false;
        }

        if(donutConfig == null)
        {
            BumperCopsDebug.LogError("Donut config is null");
            return false;
        }

        if(Time.time - donutLastSpawnCheckTime < DONUT_COOLOFF_TIME_SECONDS)
        {
            return false;
        }

        donutLastSpawnCheckTime = Time.time;
        float random = UnityEngine.Random.value;
        List<float> enemyConfig = donutConfig[GameLeverType.ENEMY];
        List<float> gameSpeedConfig = donutConfig[GameLeverType.GAME_SPEED];
        List<float> gameLevelConfig = donutConfig[GameLeverType.TRAFFIC_DENSITY];

        float calculatedChance = enemyConfig[(int)enemyLevel] * gameSpeedConfig[(int)gameSpeedLevel] * enemyConfig[(int)trafficDensityLevel];

        BumperCopsDebug.Log($"random is {random} and calculated is {calculatedChance}");
        bool result = random < calculatedChance;

        if(result)
        {
            lastEnemyIdDonut = enemyId;
        }

        return result;
    }

    public bool CanSpawnDonutNonConfig(
        GameLevel enemysCurrentLevel, 
        float percentTimeRemaining)
    {
        if(percentTimeRemaining <= 0.03)
        {
            BumperCopsDebug.Log("time remaining percent is less than 3");
            return false;
        }

        if(donutGeneratedCount >= MAX_DONUT_PER_ENEMY || Time.time - donutLastSpawnCheckTime < DONUT_COOLOFF_TIME_SECONDS)
        {
            // if the players speed is greater than enemy speed no need to generate a clock
            return false;
        }
        
        //BumperCopsDebug.Log("PowerupManager: trying to generate now");
        donutLastSpawnCheckTime = Time.time;

        //BumperCopsDebug.Log($"PowerupManager: Time given {gameManager.TimerBehaviour.TimeGivenToCatchEnemy},, remaining {gameManager.TimerBehaviour.RemainingTime}, percent {percentTimeRemaining}");
        if(percentTimeRemaining <= 0.6)
        {
            //force generate at 40% time remaining and if time remaining is 15 percent or less.
            if(percentTimeRemaining <= 0.2 && donutGeneratedCount < 1 || 
                UnityEngine.Random.value <= 1 - percentTimeRemaining || 
                percentTimeRemaining <= 0.15)
            {
                // force generate 
                BumperCopsDebug.Log("PowerupManager: Generating clock");
                ++donutGeneratedCount;

                // we delay the boost generation else both will be generated together causing frustration in player since they can only pick up one
                boostStartTimer += COOLOFF_BETWEEN_COLLECTIBLES;
                return true;
            }
            if(UnityEngine.Random.value <= 1 - percentTimeRemaining)
            {
                BumperCopsDebug.Log("PowerupManager: Generating clock");
                ++donutGeneratedCount;

                // we delay the boost generation else both will be generated together causing frustration in player since they can only pick up one
                boostStartTimer += COOLOFF_BETWEEN_COLLECTIBLES;
                return true;
            }
        }

        return false;
    }

    public void GameEnd()
    {
        startGeneration = false;
    }

}