public enum PowerupType
{
    INSTANT_BONUS,
    BIG_BOOST,
    AIR_SUPPORT,
    SIREN,
    WEAPON,
    BOOST,
    CLOCK,
    COINS,
    DONUT,
    BACKUP,
    SHIELD,
    NONE
}