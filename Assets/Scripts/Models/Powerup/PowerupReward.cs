public class PowerupReward
{
    public readonly PowerupType Type;
    public int Amount { get; private set; }
    public readonly bool IsDefaultReward;
    public PowerupReward(PowerupType type, int amount, bool isDefaultReward)
    {
        this.Type = type;
        this.Amount = amount;
        this.IsDefaultReward = isDefaultReward;
    }

    public void UpdateAmount(int multiplier)
    {
        this.Amount *= multiplier;
    }
}