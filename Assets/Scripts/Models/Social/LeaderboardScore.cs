public class LeaderboardScore
{
    public readonly long Score;
    public readonly string UserId;
    public int Rank { get; private set; }
    public string Name { get; private set; }
    public readonly bool IsUser;
    private bool isNPCScore;
    public bool IsNonNpcUser { get { return !isNPCScore; }}

    public LeaderboardScore(long score, string userId, int rank, bool isUser, string name, bool isNPCScore = false)
    {
        this.Score = score;
        this.UserId = userId;
        this.Rank = rank;
        this.IsUser = isUser;
        
        this.Name = isUser ? "You" : name;

        // Todo get random players
        if(string.IsNullOrEmpty(this.Name))
        {
            this.Name = "player";
        }

        this.isNPCScore = isNPCScore;
    }

    public void UpdateRank(int newRank)
    {
        this.Rank = newRank;
    }

    public void UpdateName(string name)
    {
        if(this.IsUser)
        {   
            return;
        }

        this.Name = name;
    }
}