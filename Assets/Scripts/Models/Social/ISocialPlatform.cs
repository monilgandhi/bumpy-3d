using System;
interface ISocialPlatform
{
    event Action<SocialPlatformEventArgs> AuthenticationComplete;
    event Action<SocialPlatformEventArgs> LeaderboardScoresLoadComplete;
    void Authenticate(bool forceSignIn, bool showDialogue);
    void FetchUserCentricHighScoreLeaderboard(int scoresToFetch, long userHighScore);
    void PostScoreToLeaderboard(string leaderboardId, long score, bool retry = false);
    string HighScoreLeaderBoardId { get; }
    bool IsAuthenticated { get; }
    bool IsPermissionGiven();
    void RecordPermission(bool permission);
    bool ConfirmationDialogueRequired();
}