// #if UNITY_ANDROID && !UNITY_EDITOR
#if UNITY_ANDROID

using System;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
    
class AndroidSocialPlatform : SocialPlatformAbstract, ISocialPlatform
{
    public string HighScoreLeaderBoardId { get { return GPGSIds.leaderboard_high_score; } }
    private int scoresToFetch;
    private long usersCurrentHighScore;
    private bool fetchAllTimeHighScores;
    public void Authenticate(bool forceSignIn, bool showDialogue)
    {
        if(IsAuthenticated)
        {
            RaiseAuthenticationComplete();
            return;
        }

        SignInInteractivity interactivity = forceSignIn ? 
            SignInInteractivity.CanPromptAlways : SignInInteractivity.NoPrompt;

        PlayGamesPlatform.DebugLogEnabled = CarUtils.IsUnityDevelopmentBuild();

        Social.Active = PlayGamesPlatform.Instance;
        PlayGamesPlatform.Instance.Authenticate(interactivity, (result) => {
            // BumperCopsDebug.Log($"result is {result}");
            switch(result)
            {
                case SignInStatus.Success:
                    RaiseAuthenticationComplete();
                    break;
                
                default:
                    BumperCopsDebug.Log("Something went wrong");
                    RaiseAuthenticationComplete();
                    break;
            }
        });
    }

    public bool IsPermissionGiven()
    {
        if(IsAuthenticated)
        {
            return false;
        }

        BumperCopsDebug.Log($"Can ask for signing {PersistanceManager.GooglePlayPermission}");
        bool? value = PersistanceManager.GameCenterPermission;
        return value.HasValue && value.Value;
    }

    public void RecordPermission(bool permission)
    {
        BumperCopsDebug.Log($"Recording leaderboard permission {permission}");
        PersistanceManager.GooglePlayPermission = permission;
    }

    public bool ConfirmationDialogueRequired()
    {
        if(IsAuthenticated)
        {
            return false;
        }

        BumperCopsDebug.Log($"Can signin directly {PersistanceManager.GooglePlayPermission}");
        bool? value = PersistanceManager.GooglePlayPermission;
        return !value.HasValue; 
    }

    public void FetchUserCentricHighScoreLeaderboard(int scoresToFetch, long userHighScore)
    {
        if(!IsAuthenticated)
        {
            RaiseLeaderboardScoresLoaded(false, null);
            BumperCopsDebug.Log("User not authenticated. Cannot Fetching scores");
            return;
        }

        fetchAllTimeHighScores = false;
        this.scoresToFetch = scoresToFetch;
        this.usersCurrentHighScore = userHighScore;
        FetchUserCentricHighScoreLeaderboardInternal();
    }

    private void FetchUserCentricHighScoreLeaderboardInternal()
    {
        if(!IsAuthenticated)
        {
            RaiseLeaderboardScoresLoaded(false, null);
            BumperCopsDebug.Log("User not authenticated. Cannot Fetching scores");
            return;
        }

        BumperCopsDebug.Log("User authenticated. Fetching scores");
        PlayGamesPlatform.Instance.LoadScores(
            GPGSIds.leaderboard_high_score,
            LeaderboardStart.PlayerCentered,
            scoresToFetch,
            LeaderboardCollection.Public,
            LeaderboardTimeSpan.AllTime,
            OnLeaderBoardDataReceivedInternal);
    }

    private void OnLeaderBoardDataReceivedInternal(LeaderboardScoreData data)
    {
        if(!data.Valid)
        {
            BumperCopsDebug.LogWarning("Data is not valid");
            return;
        }

        if(!data.Valid || data.Scores.Length < scoresToFetch)
        {
            // fetch all time high scorer if weekly scores are not available
            if(!fetchAllTimeHighScores)
            {
                BumperCopsDebug.Log("Fetching all time high score");
                fetchAllTimeHighScores = true;
                FetchUserCentricHighScoreLeaderboardInternal();
                return;
            }

            BumperCopsDebug.Log($"Less score {data.Scores.Length}");
            RaiseLeaderboardScoresLoaded(false, null);
            return;
        }
        
        List<LeaderboardScore> userCentricScores = new List<LeaderboardScore>();

        LeaderboardScore userLeaderboardScore = new LeaderboardScore(
            this.usersCurrentHighScore, 
            Social.Active.localUser.id, -1, 
            true, 
            null, 
            false);

        BumperCopsDebug.Log($"User id is {userLeaderboardScore.UserId}");

        bool userFound = false;
        for(int i = 0; i < data.Scores.Length; i++)
        {
            LeaderboardScore score = new LeaderboardScore(data.Scores[i].value, 
                data.Scores[i].userID, 
                data.Scores[i].rank, 
                data.Scores[i].userID.Equals(Social.Active.localUser.id),
                null);

            if(score.IsUser)
            {
                BumperCopsDebug.Log("User found on the leaderboard from play");
                userFound = true;
            }

            userCentricScores.Add(score);
        }

        if(!userFound)
        {
            BumperCopsDebug.Log("User not found merging artificially");
            List<LeaderboardScore> mergedUserScore = new List<LeaderboardScore>();
            // merge the user
            for(int i = 0; i < userCentricScores.Count - 2; i++)
            {
                if(userLeaderboardScore.Score >= userCentricScores[i].Score && !userFound)
                {
                    BumperCopsDebug.Log($"Adding user at positiong number {i} wit id {userLeaderboardScore.UserId}");
                    mergedUserScore.Add(userLeaderboardScore);
                    userFound = true;
                }

                mergedUserScore.Add(userCentricScores[i]);
            }

            mergedUserScore.Add(userCentricScores[userCentricScores.Count - 1]);

            if(!userFound)
            {
                // if the user is still not found add to the end
                mergedUserScore.Add(userLeaderboardScore);
            }

            userCentricScores = mergedUserScore;
        }

        BumperCopsDebug.Log($"Loading user profile with length {userCentricScores.Count}");
        LoadScoreProfiles(userCentricScores);
    }
}

#endif