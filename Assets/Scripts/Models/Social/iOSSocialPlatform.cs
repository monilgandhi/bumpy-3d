//#if UNITY_IOS && !UNITY_EDITOR
#if UNITY_IOS

using System.Collections.Generic;
using UnityEngine.SocialPlatforms;
using UnityEngine;
    
class iOSSocialPlatform : SocialPlatformAbstract, ISocialPlatform
{
    public string HighScoreLeaderBoardId { get { return GPGSIds.leaderboard_high_score; } }
    private int scoresToFetch;
    private long usersCurrentHighScore;
    private bool fetchAllTimeHighScores;
    private ILeaderboard highScoreLeaderboard;
    public void Authenticate(bool forceSignIn, bool showDialogue)
    {
        // either we are already authenticated or we cannot signin directly since we have not shown the dialogue.
        if(IsAuthenticated || ConfirmationDialogueRequired() || !IsPermissionGiven())
        {
            RaiseAuthenticationComplete();
            return;
        }

        Social.localUser.Authenticate((success, errors) => {
            BumperCopsDebug.Log($"Success for authentication is {success}");
            RaiseAuthenticationComplete();
        });
    }

    public bool IsPermissionGiven()
    {
        if(IsAuthenticated)
        {
            return false;
        }

        BumperCopsDebug.Log($"Can ask for signing {PersistanceManager.GameCenterPermission}");
        bool? value = PersistanceManager.GameCenterPermission;
        return value.HasValue && value.Value;
    }

    public void RecordPermission(bool permission)
    {
        BumperCopsDebug.Log($"Recording leaderboard permission {permission}");
        PersistanceManager.GameCenterPermission = permission;
    }

    public bool ConfirmationDialogueRequired()
    {
        if(IsAuthenticated)
        {
            return false;
        }

        BumperCopsDebug.Log($"Can signin directly {PersistanceManager.GameCenterPermission}");
        bool? value = PersistanceManager.GameCenterPermission;
        return !value.HasValue; 
    }

    public void FetchUserCentricHighScoreLeaderboard(int scoresToFetch, long userHighScore)
    {
        if(!IsAuthenticated)
        {
            RaiseLeaderboardScoresLoaded(false, null);
            BumperCopsDebug.Log("User not authenticated. Cannot Fetching scores");
            return;
        }

        fetchAllTimeHighScores = false;
        this.scoresToFetch = scoresToFetch;
        this.usersCurrentHighScore = userHighScore;
        FetchUserCentricHighScoreLeaderboardInternal();
    }

    private void FetchUserCentricHighScoreLeaderboardInternal()
    {
        if(!IsAuthenticated)
        {
            RaiseLeaderboardScoresLoaded(false, null);
            BumperCopsDebug.Log("User not authenticated. Cannot Fetching scores");
            return;
        }

        BumperCopsDebug.Log("User authenticated. Fetching scores");
        highScoreLeaderboard = Social.CreateLeaderboard();
        highScoreLeaderboard.id = HighScoreLeaderBoardId;
        highScoreLeaderboard.timeScope = fetchAllTimeHighScores ? TimeScope.AllTime : TimeScope.Week;
        highScoreLeaderboard.userScope = UserScope.Global;
        highScoreLeaderboard.range = new Range(1, 10);
        highScoreLeaderboard.LoadScores(OnLeaderBoardDataReceivedInternal);
    }

    private void OnLeaderBoardDataReceivedInternal(bool success)
    {
        if(highScoreLeaderboard.scores == null || highScoreLeaderboard.scores.LongLength <= scoresToFetch)
        {
            // fetch all time high scorer if weekly scores are not available
            if(!fetchAllTimeHighScores)
            {
                fetchAllTimeHighScores = true;
                FetchUserCentricHighScoreLeaderboardInternal();
                return;
            }

            RaiseLeaderboardScoresLoaded(false, null);
            return;
        }
        
        List<LeaderboardScore> userCentricScores = new List<LeaderboardScore>();

        LeaderboardScore userLeaderboardScore = new LeaderboardScore(
            this.usersCurrentHighScore, 
            Social.Active.localUser.id, -1, 
            true, 
            null, 
            false);

        BumperCopsDebug.Log($"User id is {userLeaderboardScore.UserId}");

        bool userFound = false;
        for(int i = 0; i < highScoreLeaderboard.scores.Length; i++)
        {
            LeaderboardScore score = new LeaderboardScore(highScoreLeaderboard.scores[i].value, 
                highScoreLeaderboard.scores[i].userID, 
                highScoreLeaderboard.scores[i].rank, 
                highScoreLeaderboard.scores[i].userID.Equals(Social.Active.localUser.id),
                null);

            if(score.IsUser)
            {
                BumperCopsDebug.Log("User found on the leaderboard from play");
                userFound = true;
            }

            userCentricScores.Add(score);
        }

        if(!userFound)
        {
            BumperCopsDebug.Log("User not found merging artificially");
            List<LeaderboardScore> mergedUserScore = new List<LeaderboardScore>();
            // merge the user
            for(int i = 0; i < userCentricScores.Count - 2; i++)
            {
                if(userLeaderboardScore.Score >= userCentricScores[i].Score && !userFound)
                {
                    BumperCopsDebug.Log($"Adding user at positiong number {i} wit id {userLeaderboardScore.UserId}");
                    mergedUserScore.Add(userLeaderboardScore);
                    userFound = true;
                }

                mergedUserScore.Add(userCentricScores[i]);
            }

            mergedUserScore.Add(userCentricScores[userCentricScores.Count - 1]);
            if(!userFound)
            {
                // if the user is still not found add to the end
                mergedUserScore.Add(userLeaderboardScore);
            }

            userCentricScores = mergedUserScore;
        }

        BumperCopsDebug.Log($"Loading user profile with length {userCentricScores.Count}");
        LoadScoreProfiles(userCentricScores);
    }
}

#endif