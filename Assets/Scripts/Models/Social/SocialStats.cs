using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class SocialStats
{
    public Action<SocialPlatformEventArgs> AuthenticationComplete;
    public Action LeaderboardScoresLoaded;
    public event Action SocialScoresLoaded;
    private static SocialStats instance;
    public IReadOnlyList<LeaderboardScore> UserCentricScores { get; private set; }
    public ReadOnlyCollection<LeaderboardScore> TopScores { get; private set; }
    private ISocialPlatform socialPlatform;
    private int scoresToFetch;

    private static readonly List<LeaderboardScore> genericScores = new List<LeaderboardScore>()
    {
        { new LeaderboardScore(20000000, "Chief", 1, false, "Chief", true)},
        { new LeaderboardScore(10000000, "Deputy", 2, false, "Deputy", true)},
        { new LeaderboardScore(1000000, "Commander", 3, false, "Commander", true)},
        { new LeaderboardScore(500000, "Captain", 4, false, "Captain", true)},
        { new LeaderboardScore(250000, "Lieutenant", 5, false, "Lieutenant", true)},
        { new LeaderboardScore(100000, "Detective", 6, false, "Detective", true)},
        { new LeaderboardScore(50000, "Sergeant", 7, false, "Sergeant", true)},
        { new LeaderboardScore(30000, "Officer", 8, false, "Officer", true)}
    };

    public static SocialStats Instance
    {
        get
        {
            if(instance == null)
            {
                Load();
            }

            return instance;
        }
    }

    public SocialStats()
    {
        UserCentricScores = new List<LeaderboardScore>();
    }
    
    public static void Load()
    {
        instance = new SocialStats();
        instance.scoresToFetch = genericScores.Count;
#if UNITY_ANDROID && !UNITY_EDITOR
        instance.socialPlatform = new AndroidSocialPlatform();
        instance.socialPlatform.Authenticate(false, false);
#elif UNITY_IOS && !UNITY_EDITOR
        instance.socialPlatform = new iOSSocialPlatform();
        instance.socialPlatform.Authenticate(false, false);
#endif
        if(instance.socialPlatform == null)
        {
            return;
        }

        instance.socialPlatform.AuthenticationComplete += instance.OnAuthenticationComplete;
        instance.socialPlatform.LeaderboardScoresLoadComplete += instance.OnLeaderboardScoreLoadComplete;
    }

    public bool IsAuthenticated()
    {
        return socialPlatform != null && socialPlatform.IsAuthenticated;
    }

    public bool ConfirmationDialogueRequired()
    {
        return socialPlatform != null && socialPlatform.ConfirmationDialogueRequired();
    }

    public void RecordPermission(bool permissionGiven)
    {
        if(socialPlatform == null)
        {
            return;
        }

        // record the permission
        socialPlatform.RecordPermission(permissionGiven);
        
        if(permissionGiven)
        {
            ForceSignin();
        }
    }

    public bool IsPermissionGranted()
    {
        return socialPlatform != null && socialPlatform.IsPermissionGiven();
    }

    public void PreFetchLeaderboard(int scoresToFetch, long userHighScore)
    {
        LeaderboardScore userScore = new LeaderboardScore(userHighScore, null, -1, true, null);
        MergeAndUpdateUserCentricScore(userScore, genericScores);

        if(this.socialPlatform == null || !IsAuthenticated() || !RemoteConfig.Instance.Config.UseStoreLeaderboard)
        {
            OnLeaderboardScoreLoadComplete(null);
            return;
        }

        BumperCopsDebug.Log($"Positing user score of {userHighScore}");
        socialPlatform.PostScoreToLeaderboard(socialPlatform.HighScoreLeaderBoardId, userHighScore, true);
        socialPlatform.FetchUserCentricHighScoreLeaderboard(scoresToFetch, userHighScore);
    }

    private void OnAuthenticationComplete(SocialPlatformEventArgs args)
    {
        AuthenticationComplete?.Invoke(args);
    }

    private void OnLeaderboardScoreLoadComplete(SocialPlatformEventArgs args)
    {   
        if(args != null && args.Scores != null && args.Scores.Count > 0)
        {
            UserCentricScores = args.Scores;
        }

        BumperCopsDebug.Log("Sending leaderboard scores loaded event");
        LeaderboardScoresLoaded?.Invoke();
    }
    
    private void ForceSignin()
    {
        BumperCopsDebug.Log("Force signing in");
        BumperCopsDebug.Log("Force signing");
        socialPlatform.Authenticate(false, true);
    }

    private void MergeAndUpdateUserCentricScore(LeaderboardScore userScore, List<LeaderboardScore> score)
    {
        List<LeaderboardScore> mergedScore = new List<LeaderboardScore>();
        int rankOffset = 0;
        bool userAdded = false;
        for(int i = 0; i < score.Count; i++)
        {
            if(score[i].Score <= userScore.Score && !userAdded)
            {
                userScore.UpdateRank(score[i].Rank);
                mergedScore.Add(userScore);
                userAdded = true;
                ++rankOffset;
            }

            if(rankOffset > 0)
            {
                score[i].UpdateRank(score[i].Rank + rankOffset);
            }

            mergedScore.Add(score[i]);
        }

        BumperCopsDebug.Log($"User added {userAdded}");
        if(!userAdded)
        {
            BumperCopsDebug.Log($"User rank is {score.Count + 1}");
            userScore.UpdateRank(score.Count + 1);
            // user is last
            mergedScore.Add(userScore);
            userAdded = true;
        }

        UserCentricScores = mergedScore;
        BumperCopsDebug.Log($"Size is {UserCentricScores.Count}");
    }

    public static Sprite GetProfileImage(string rank)
    {
        if(string.IsNullOrEmpty(rank))
        {
            BumperCopsDebug.LogError("invalid rank");
            return null;
        }

        string filePath = "profilePics" + Path.DirectorySeparatorChar + rank;
        BumperCopsDebug.Log($"Loading profile pic with path {filePath}");
        return Resources.Load<Sprite>(filePath);
    }

    public static Tuple<string, long> GetNextTargetScore(long score)
    {
        for(int i = genericScores.Count - 1; i >= 0; i--)
        {
            LeaderboardScore currentScore = genericScores[i];
            if(currentScore.Score > score)
            {
                return new Tuple<string, long>(currentScore.Name, currentScore.Score);
            }
        }

        return null;
    }

    public static string GetGenericScoreDimension(long score)
    {
        string value = "None";
        for(int i = genericScores.Count - 1; i >= 0; i--)
        {
            if(score > genericScores[i].Score)
            {
                value = genericScores[i].Name;
            }
        }

        BumperCopsDebug.Log($"Score dimension is {value}");
        return value;
    }
}

public class SocialPlatformEventArgs : EventArgs
{
    public readonly bool Authenticated;
    public readonly bool ActionSuccess;
    public readonly IReadOnlyList<LeaderboardScore> Scores;
    public SocialPlatformEventArgs(bool authenticated)
    {
        this.Authenticated = authenticated;
    }

    public SocialPlatformEventArgs(bool authenticated, bool success)
    {
        this.Authenticated = authenticated;
        this.ActionSuccess = success;
    }

    public SocialPlatformEventArgs(bool authenticated, bool success, List<LeaderboardScore> scores) 
        : this(authenticated, success)
    {
        this.Scores = scores;
    }
}