using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
public class SocialPlatformAbstract
{
    public event Action<SocialPlatformEventArgs> AuthenticationComplete;
    public event Action<SocialPlatformEventArgs> LeaderboardScoresLoadComplete;

    public bool IsAuthenticated 
    {
        get 
        {
            return Social.Active != null && 
                    Social.Active.localUser != null && 
                    Social.Active.localUser.authenticated;
        }
    }

    public void PostScoreToLeaderboard(string leaderboardId, long score, bool retry = false)
    {
        if(!this.IsAuthenticated)
        {
            return;
        }
        
        BumperCopsDebug.Log("Posting score to leaderboard");
        Social.ReportScore(score, leaderboardId, (success) => {
            if(!success && !retry)
            {
                // retry once more
                PostScoreToLeaderboard(leaderboardId, score, true);
            }
        });
    }

    protected void RaiseAuthenticationComplete()
    {
        AuthenticationComplete?.Invoke(new SocialPlatformEventArgs(IsAuthenticated));
    }

    protected void RaiseLeaderboardScoresLoaded(bool success, List<LeaderboardScore> scores)
    {
        if(scores == null || scores.Count == 0)
        {
            LeaderboardScoresLoadComplete?.Invoke(new SocialPlatformEventArgs(IsAuthenticated, success));
            return;
        }

        LeaderboardScoresLoadComplete?.Invoke(new SocialPlatformEventArgs(IsAuthenticated, success, scores));
    }

    protected void LoadScoreProfiles(List<LeaderboardScore> leaderboardScoreData)
    {
        List<string> userNames = new List<string>();
        string[] userIds = new string[leaderboardScoreData.Count];

        bool hasValidIds = false;
        for(int i = 0; i < leaderboardScoreData.Count; i++)
        {
            if(string.IsNullOrEmpty(leaderboardScoreData[i].UserId))
            {
                continue;
            }

            hasValidIds = true;
            userIds[i] = leaderboardScoreData[i].UserId;
        }

        // no valid ids
        if(!hasValidIds)
        {
            return;
        }

        BumperCopsDebug.Log("Loading user profile now");
        Social.LoadUsers(userIds, (users) => {
            if(users.Length == 0)
            {
                BumperCopsDebug.LogWarning("No users found in profile info");
                RaiseLeaderboardScoresLoaded(false, null);

                return;
            }

            BumperCopsDebug.Log($"Users found for load users {users.Length}");
            Dictionary<string, IUserProfile> userIdToProfileMap = new Dictionary<string, IUserProfile>();
            foreach(IUserProfile u in users)
            {
                if(u == null)
                {
                    BumperCopsDebug.LogWarning("Null userprofile");
                    continue;
                }

                BumperCopsDebug.Log($"user id {u.id} with name {u.userName}");
                userIdToProfileMap.Add(u.id, u);
            }

            foreach(LeaderboardScore d in leaderboardScoreData)
            {
                IUserProfile requiredProfile;
                if(!userIdToProfileMap.TryGetValue(d.UserId, out requiredProfile))
                {
                    BumperCopsDebug.Log($"User not found in the returned profile map {d.UserId}");
                    continue;
                }

                BumperCopsDebug.Log($"updating user id {d.UserId} with name {requiredProfile.userName} and is user {d.IsUser}");
                d.UpdateName(requiredProfile.userName);
            }

            //BumperCopsDebug.LogWarning($"profile updated. sending event. Any subscribers {SocialScoresLoaded == null}");
            RaiseLeaderboardScoresLoaded(true, leaderboardScoreData);
        });
    }

    protected class PersistanceManager
    {
        private const string GOOGLE_PLAY_PERMISSION = "play_permission";
        private const string GAME_CENTER_PERMISSION = "game_center_permission";

        public static bool? GooglePlayPermission
        {
            get
            {
                int val = PlayerPrefs.GetInt(GOOGLE_PLAY_PERMISSION, -1);
                BumperCopsDebug.Log($"retrieving google play permission is {val}");
                if(val < 0)
                {
                    // we have not asked for the permission
                    return null;
                }

                return Convert.ToBoolean(val);
            }
            set
            {
                BumperCopsDebug.Log($"Google play permission is {value}");
                PlayerPrefs.SetInt(GOOGLE_PLAY_PERMISSION, Convert.ToInt32(value));
                PlayerPrefs.Save();
            }
        }

        public static bool? GameCenterPermission
        {
            get
            {
                int val = PlayerPrefs.GetInt(GAME_CENTER_PERMISSION, -1);
                BumperCopsDebug.Log($"retrieving Game center permission is {val}");
                if(val < 0)
                {
                    // we have not asked for the permission
                    return null;
                }

                return Convert.ToBoolean(val);
            }
            set
            {
                BumperCopsDebug.Log($"Game center permission is {value}");
                PlayerPrefs.SetInt(GAME_CENTER_PERMISSION, Convert.ToInt32(value));
                PlayerPrefs.Save();
            }
        }
    }
}