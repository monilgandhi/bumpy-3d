using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
public class EnemySequence
{
    [JsonProperty("id")]
    public int Number;
    public string Id { get { return $"enemy_{Number}"; }}

    // TODO this needs to be more system controlled
    [JsonProperty("traffic_probability")]
    public float TrafficGenerationProbability;
    [JsonProperty("traffic_generation_time")]
    public float TrafficGenerationTime;

    [JsonProperty("name")]
    public string Name;

    [JsonProperty("bounty")]
    public int Bounty;

    [JsonProperty("health")]
    public int Health;
    
    [JsonProperty("movement")]
    public float Movement;

    [JsonProperty("time")]
    public int Time;

    [JsonProperty("chase_lane_change")]
    public bool LaneChangeAllowedDuringChase = true;
}