using UnityEngine;
namespace BumperCops.Notifications
{
    public class NotificationContentItem
    {
        public string Id;
        public string Version;
        public string Title;
        public string Description;

        public static string GetMetadata(string id, string version)
        {
            if(string.IsNullOrEmpty(id) || string.IsNullOrEmpty(version))
            {
                BumperCopsDebug.LogWarning("id or version is null");
                return null;
            }

            return $"{id}_{version}";
        }

        public static bool TryGetIdAndVersion(string metadata, out string id, out string version)
        {
            id = null;
            version = null;

            if(string.IsNullOrEmpty(metadata))
            {
                BumperCopsDebug.LogWarning("metadata is null");
                return false;
            }

            string[] split = metadata.Split('_');

            if(split.Length < 2)
            {
                BumperCopsDebug.LogWarning("invalid metadata found");
                return false;
            }

            id = split[0];
            version = split[1];
            return true;
        }
    }
}