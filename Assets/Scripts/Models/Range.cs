using Newtonsoft.Json;

namespace BumperCops.Models
{
    public struct Range
    {
        /// <summary>Minimum value of the range.</summary>
        public float Minimum { get; private set; }

        /// <summary>Maximum value of the range.</summary>
        public float Maximum { get; private set; }

        public float Value { get { return this.Minimum; }}

        public float InverseValue { get { return 1/ Value; }}

        public float StepSize { get; private set; }

        /// <summary>Presents the Range in readable format.</summary>
        /// <returns>String representation of the Range</returns>
        public override string ToString()
        {
            return $"[{this.Minimum} - {this.Maximum}]";
        }

        private bool inverse;

        [JsonConstructor]
        public Range(float minimum, float maximum, float step, bool inverse = false)
        {
            this.inverse = inverse;

            if(inverse)
            {
                this.Minimum = 1/minimum;
                this.Maximum = 1/maximum;
            }
            else
            {
                this.Minimum = minimum;
                this.Maximum = maximum;
            }

            this.StepSize = step;
            this.inverse = inverse;
        }

        public void StepUp()
        {
            if(this.Minimum.Equals(this.Maximum))
            {
                BumperCopsDebug.Log("Cannot step up");
                return;
            }
            
            this.Minimum += this.StepSize;
            //BumperCopsDebug.Log("Stepping up " + this.ToString());

            if(this.Minimum > this.Maximum)
            {
                this.Minimum = this.Maximum;
            }
        }

        public bool RangeContains(float number)
        {
            return number >= this.Minimum && number < this.Maximum;
        }

        public bool CanStepUp()
        {
            return this.Minimum < this.Maximum && StepSize > 0;
        }
    }
}