using Newtonsoft.Json;
public class BonusConfig
{
    [JsonProperty("required_score")]
    public long RequiredScore;
}