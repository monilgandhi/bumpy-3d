using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;
public class CarConfig
{
    [JsonProperty("speed")]
    public float Speed;

    [JsonProperty("attack")]
    public float Attack;

    [JsonProperty("boost")]
    public float Boost;

    [JsonProperty("handling")]
    public float Handling;

}