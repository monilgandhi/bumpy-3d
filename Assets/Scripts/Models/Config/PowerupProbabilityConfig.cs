using Newtonsoft.Json;

public class PowerupProbabilityConfig
{
    public PowerupType Type;

    [JsonProperty("minValue")]
    public float MinimumValue;

    [JsonProperty("maxValue")]
    public float MaximumValue;
}