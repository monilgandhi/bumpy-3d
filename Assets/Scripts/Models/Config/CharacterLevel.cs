using Newtonsoft.Json;
public class CharacterLevel
{
    [JsonProperty("max_hit_points")]
    public float MaxHitPoints;

    [JsonProperty("bounty")]
    public int Bounty;
}