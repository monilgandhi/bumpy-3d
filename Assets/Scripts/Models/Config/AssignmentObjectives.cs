using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class AssignmentObjectives
{
    [JsonProperty("id")]
    public string Id;
    [JsonProperty("type")]
    [JsonConverter(typeof(StringEnumConverter))]
    public ObjectiveType Objective;

    [JsonProperty("count")]
    public int Count;

    [JsonProperty("description")]
    public string UnformatedDescription;

    private string formatedDescription;
    public string Description 
    {
        get 
        {
            if(string.IsNullOrEmpty(formatedDescription))
            {
                formatedDescription = string.Format(UnformatedDescription, Count);
            }

            return formatedDescription;
        }
    }

    [JsonProperty("condition")]
    public List<ObjectiveCondition> ObjectiveCondition;

    [JsonProperty("single_session")]
    public bool SingleSession;
    [JsonProperty("skip_cost")]
    public int SkipCost;
    public int PlayerCount;
    public bool IsComplete { get { return this.PlayerCount >= this.Count; } }
    public bool CompletedInThisSession;
    public enum ObjectiveType
    {
        KILL,
        NEAR_MISS,
        POWERUP_COLLECTED,
        TOTAL_BUMP_COUNT,
        COMBO,
        SCORE,
        COIN_COLLECT,
        TUTORIAL,
        CHASE
    }
}