using System.Collections.Generic;
using Newtonsoft.Json;

public class Assignment
{
    [JsonProperty("id")]
    public string Id;

    [JsonProperty("next_id")]
    public string NextAssignmentId;

    [JsonProperty("assignment_number")]
    public int AssignmentNumber;
    
    [JsonProperty("objectives")]
    public List<AssignmentObjectives> Objectives;

    [JsonProperty("reward")]
    public RewardsConfig Reward;

    [JsonProperty("isTutorial")]
    public bool IsTutorial;
    public bool IsAssignmentComplete;
}