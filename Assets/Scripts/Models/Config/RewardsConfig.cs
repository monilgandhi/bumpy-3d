using System.Collections.Generic;
using Newtonsoft.Json;

public class RewardsConfig
{
    public RewardReason Reason;

    [JsonProperty("base_chance")]
    public float ChanceForGivingReward;

    [JsonProperty("reward_chance")]
    public List<RewardChanceConfig> RewardChance;    
}