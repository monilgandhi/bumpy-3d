using Newtonsoft.Json;
public class RewardChanceConfig
{
    public CollectibleType Type;
    [JsonProperty("minimum_amount")]
    public int MinimumAmount;
    [JsonProperty("maximum_amount")]
    public int MaximumAmount;
    public int Multiple = 1;
    public float Chance; 
}