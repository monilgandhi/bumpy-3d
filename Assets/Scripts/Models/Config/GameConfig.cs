using Newtonsoft.Json;
public class GameConfig
{

    [JsonProperty("nm_bounds")]
    public float NearMissBounds;

    [JsonProperty("ads_running")]
    public bool AdsRunning;

    [JsonProperty("extension_rewarded_ads")]
    public bool TimeExtensionRewardedAds;

    [JsonProperty("extension_rewarded_adunit_android")]
    public string TimeExtensionRewardedAdUnitAndroid;

    [JsonProperty("extension_rewarded_adunit_ios")]
    public string TimeExtensionRewardedAdUnitIOS;

    [JsonProperty("video_ad_rewarded_adunit_android")]
    public string VideoRewardedAdUnitAndroid;

    [JsonProperty("video_ad_rewarded_adunit_ios")]
    public string VideoRewardedAdUnitIOS;

    [JsonProperty("reward_dialogue_ad_rewarded_adunit_android")]
    public string RewardDialogRewardedAdUnitAndroid;


    [JsonProperty("reward_dialogue_ad_rewarded_adunit_ios")]
    public string RewardDialogRewardedAdUnitIOS;

    [JsonProperty("r_dialogue")]
    // -1 show only custom
    // 0 show both
    // 1 show only in built
    public int RatingDialogue;

    [JsonProperty("s_lb")]
    public bool UseStoreLeaderboard;
}