using System;
using System.Text;
using System.Collections;
using GameAnalyticsSDK;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
public class RemoteConfig
{
    private static RemoteConfig instance;
    private GameConfig gameConfig;
    public GameConfig Config { get { return gameConfig; }}

    private bool hasConfigChanged;

    public static RemoteConfig Instance
    {
        get
        {
            if(instance == null)
            {
                BumperCopsDebug.LogWarning("Config is not loaded. Call will fail");
            }

            return instance;
        }
    }

    private const string GAME_CONFIG_FOLDER = "game";
    private RemoteConfig()
    {
        gameConfig = LoadLocalConfig();
    }

    private void LoadRemoteConfig()
    {
        string r_dialogue = GameAnalytics.GetRemoteConfigsValueAsString("r_dialogue");
        BumperCopsDebug.Log($"remote value {r_dialogue}");
        // near miss bounds
        gameConfig.UseStoreLeaderboard = ParseBoolean("s_lb");
        gameConfig.NearMissBounds = ParseFloat("nm_bounds", gameConfig.NearMissBounds);
        gameConfig.RatingDialogue = ParseInt("r_dialogue", gameConfig.RatingDialogue);
    }

    private Boolean ParseBoolean(string key, Boolean defaultValue = false)
    {
        Boolean result = defaultValue;
        if(!Boolean.TryParse(GameAnalytics.GetRemoteConfigsValueAsString(key, defaultValue.ToString()), out result))
        {
            result = defaultValue;
        }

        return result;
    }
    private float ParseFloat(string key, float defaultValue = 0.0f)
    {
        float result = defaultValue;
        if(!float.TryParse(GameAnalytics.GetRemoteConfigsValueAsString(key, defaultValue.ToString()), out result))
        {
            result = defaultValue;
        }

        return result;
    }

    private int ParseInt(string key, int defaultValue = 0)
    {
        int result = defaultValue;
        if(!int.TryParse(GameAnalytics.GetRemoteConfigsValueAsString(key, defaultValue.ToString()), out result))
        {
            result = defaultValue;
        }

        return result;
    }

    private GameConfig LoadLocalConfig()
    {
        string filePath = GAME_CONFIG_FOLDER + Path.DirectorySeparatorChar + "game";

        TextAsset assignmentJson = Resources.Load<TextAsset>(filePath);

        if(assignmentJson == null)
        {
            throw new FileNotFoundException(filePath);
        }

        string json = Encoding.UTF8.GetString(assignmentJson.bytes, 0, assignmentJson.bytes.Length);

        return JsonConvert.DeserializeObject<GameConfig>(json);
    }

    public static void ReloadConfig(MonoBehaviour behaviour)
    {
        if(instance == null || instance.hasConfigChanged)
        {
            instance = new RemoteConfig();
            instance.hasConfigChanged = false;
            behaviour.StartCoroutine(instance.LoadAfterRemoteConfig());
        }
    }

    private IEnumerator LoadAfterRemoteConfig()
    {
        BumperCopsDebug.Log("loading remote config");
        float startTime = Time.time;
        while(Time.time - startTime < 10 && !GameAnalytics.IsRemoteConfigsReady())
        {
            yield return new WaitForSecondsRealtime(1);
        }

        if(GameAnalytics.IsRemoteConfigsReady())
        {
            BumperCopsDebug.Log("remote config loaded");
            GameAnalytics.OnRemoteConfigsUpdatedEvent += () => hasConfigChanged = true;
            LoadRemoteConfig();
        }
    }
}