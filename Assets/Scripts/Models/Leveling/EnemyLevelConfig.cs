using System;
using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Models.Leveling
{
    public class EnemyLevelConfig : ILevelConfig
    {
        public GameLevel CurrentLevel { get { return speedGameLevel > healthGameLevel ? speedGameLevel : healthGameLevel; } }
        public GameLeverType GameLeverType { get { return GameLeverType.ENEMY; }}

        public float Speed { get { return currentSpeedRange.Value; }}
        public float Health { get { return currentHealthRange.Value; }}
        public IReadOnlyList<float> Values { get { return enemyStats; }}

        public List<float> enemyStats;
        private List<Range> speedLevelMap;
        private List<Range> healthLevelMap;
        private List<Range> bountyLevelMap;
        private List<Range> distanceLevelMap;

        private GameLevel speedGameLevel;
        private GameLevel healthGameLevel;
        private Range currentSpeedRange;
        private Range currentHealthRange;
        private Range currentBountyRange;
        private Range currentDistanceRange;

        private const int ENEMY_BOUNTY_START = 50;
        private const string ENEMY_RESOURCE_FOLDER = "level-config";
        public bool Load()
        {
            if(!ConfigCache.TryGetConfig<List<Range>>("speed-leveling", ENEMY_RESOURCE_FOLDER, out speedLevelMap))
            {
                BumperCopsDebug.LogError("Could not load speed config for enemy");
                return false;
            }

            if(!ConfigCache.TryGetConfig<List<Range>>("health-leveling", ENEMY_RESOURCE_FOLDER, out healthLevelMap))
            {
                BumperCopsDebug.LogError("Could not load health config for enemy");
                return false;
            }

            if(!ConfigCache.TryGetConfig<List<Range>>("bounty-increment-leveling", ENEMY_RESOURCE_FOLDER, out bountyLevelMap))
            {
                BumperCopsDebug.LogError("Could not load health config for enemy");
                return false;
            }

            if(!ConfigCache.TryGetConfig<List<Range>>("distance-leveling", ENEMY_RESOURCE_FOLDER, out distanceLevelMap))
            {
                BumperCopsDebug.LogError("Could not load health config for enemy");
                return false;
            }

            speedGameLevel = GameLevel.EASY;
            currentSpeedRange = speedLevelMap[(int)speedGameLevel];            
            healthGameLevel = GameLevel.EASY;
            currentHealthRange = healthLevelMap[(int)healthGameLevel];
            currentDistanceRange = distanceLevelMap[(int)healthGameLevel];
            AssignBountyRange();

            enemyStats = new List<float>();
            enemyStats.Add(currentSpeedRange.Value);
            enemyStats.Add(currentHealthRange.Value);
            enemyStats.Add(GetCurrentBounty());
            enemyStats.Add(currentDistanceRange.Value);

            //BumperCopsDebug.Log($"Speed range values is {currentSpeedRange.Value} and array {enemyStats[0]}");

            return true;
        }

        private void AssignBountyRange()
        {
            GameLevel level = speedGameLevel >= healthGameLevel ? speedGameLevel : healthGameLevel;
            currentBountyRange = bountyLevelMap[(int)level];
        }

        private void AssignDistanceRange()
        {
            GameLevel level = speedGameLevel >= healthGameLevel ? speedGameLevel : healthGameLevel;
            currentDistanceRange = distanceLevelMap[(int)level];
        }

        public bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null)
        {
            return CanIncreaseHealth() || CanIncreaseSpeed();
        }

        public void ChangeLevel(GameLevel recommendedLevel)
        {
            int rLevel = (int)recommendedLevel;
            BumperCopsDebug.Log($"Leveling: Changing enemy stats to {recommendedLevel}");
            // change health iff health is less that recommended level and can step up
            // we change health before changing speed. iff speed is less than recommended level OR
            // if health is same as current level
            bool changeSpeed = (healthGameLevel >= recommendedLevel) && CanIncreaseSpeed();
            bool justChangedLevel = false;

            if(changeSpeed)
            {
                if(speedGameLevel < recommendedLevel)
                {
                    speedGameLevel = recommendedLevel;
                    currentSpeedRange = speedLevelMap[rLevel];
                    justChangedLevel = true;
                }

                BumperCopsDebug.Log("Leveling: Changing speed");
                if(!currentSpeedRange.CanStepUp() && !justChangedLevel)
                {
                    currentSpeedRange = speedLevelMap[(int)++speedGameLevel];
                    justChangedLevel = true;
                    BumperCopsDebug.Log($"Leveling: New speed Level is {speedGameLevel}");
                }

                // step up only if we have not switched the level
                if(!justChangedLevel)
                {
                    currentSpeedRange.StepUp();
                }
            }
            else if(CanIncreaseHealth())
            {
                if(healthGameLevel < recommendedLevel)
                {
                    healthGameLevel = recommendedLevel;
                    currentHealthRange = healthLevelMap[rLevel];
                    justChangedLevel = true;
                }

                BumperCopsDebug.Log("Leveling: Changing health");
                if(!currentHealthRange.CanStepUp() && !justChangedLevel)
                {
                    currentHealthRange = speedLevelMap[++rLevel];
                    justChangedLevel = true;
                    BumperCopsDebug.Log($"Leveling: New health Level is {healthGameLevel}");
                }

                // step up only if we have not switched the level
                if(!justChangedLevel)
                {
                    currentHealthRange.StepUp();
                }
            }

            if(!justChangedLevel && currentBountyRange.CanStepUp())
            {
                // step up if we have not changed the level
                currentBountyRange.StepUp();
            }
            else
            {
                // switch the level
                AssignBountyRange();
                AssignDistanceRange();
            }

            enemyStats[(int)EnemyStats.SPEED] = currentSpeedRange.Value;
            enemyStats[(int)EnemyStats.HEALTH] = currentHealthRange.Value;
            enemyStats[(int)EnemyStats.BOUNTY] =  GetCurrentBounty();
            enemyStats[(int)EnemyStats.DISTANCE] =  currentDistanceRange.Value;
            
            BumperCopsDebug.Log($"Leveling: After step up health: {healthGameLevel} and speed: {speedGameLevel}");
        }

        private bool CanIncreaseSpeed()
        {   
            return currentSpeedRange.CanStepUp() || (int)CurrentLevel < speedLevelMap.Count - 1;
        }

        private bool CanIncreaseHealth()
        {   
            return currentHealthRange.CanStepUp() || (int)CurrentLevel < healthLevelMap.Count - 1;
        }

        private float GetCurrentBounty()
        {
            int currentBounty = Mathf.RoundToInt(ENEMY_BOUNTY_START + currentBountyRange.Value);
            // round to nearest 5
            return ((currentBounty + 4) / 5 * 5);
        }
    }

    public enum EnemyStats
    {
        SPEED = 0,
        HEALTH,
        BOUNTY,
        DISTANCE
    }
}