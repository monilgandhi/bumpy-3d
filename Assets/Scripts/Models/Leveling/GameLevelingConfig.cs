using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Models.Leveling
{
    public class GameLevelingConfig : ILevelConfig
    {
        public GameLevel CurrentLevel { get { throw new NotSupportedException("Please call DetermineNextLevel"); } }

        public IReadOnlyList<float> Values { get { throw new NotSupportedException("Please call DetermineNextLevel"); }} 
        public GameLeverType GameLeverType { get { return GameLeverType.GAME_LEVEL; }}
        private List<Range> gameLevelMap;

        private const string CONFIG_FOLDER = "level-config";
        public bool Load()
        {
            if(!ConfigCache.TryGetConfig<List<Range>>("game-leveling", CONFIG_FOLDER, out gameLevelMap))
            {
                BumperCopsDebug.LogError("Could not load speed config for enemy");
                return false;
            }

            return true;
        }

        public bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null)
        {
            return true;
        }

        public void ChangeLevel(GameLevel recommendedLevel)
        {
            throw new NotImplementedException();
        }

        public GameLevel DetermineNextLevel(float currentPlayTimeInSeconds)
        {
            float currentPlayTimeInMinutes = currentPlayTimeInSeconds / 40;
            for(int i = 0; i < gameLevelMap.Count; i++)
            {
                Range currentRange = gameLevelMap[i];
                if(currentPlayTimeInMinutes >= currentRange.Minimum && currentPlayTimeInMinutes < currentRange.Maximum)
                {
                    return (GameLevel)i;
                }
            }

            throw new ArgumentException("unsupported game level");
        }
        
    }
}