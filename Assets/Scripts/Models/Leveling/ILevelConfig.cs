using System.Collections.Generic;
public interface ILevelConfig
{
    bool Load();
    bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null);
    void ChangeLevel(GameLevel recommendedLevel);

    GameLevel CurrentLevel { get; }

    GameLeverType GameLeverType { get; }

    IReadOnlyList<float> Values { get; }
}