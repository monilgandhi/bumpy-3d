using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Models.Leveling
{
    public class BonusBarLevelConfig : ILevelConfig
    {
        public GameLevel CurrentLevel { get; private set; }
        // TODO, change this in case bonus bar is active
        public GameLeverType GameLeverType { get { return GameLeverType.ENEMY; }}
        public IReadOnlyList<float> Values { get { return requiredActionsCount; }}
        private List<Range> bonusComboMap;
        private List<float> requiredActionsCount;
        private Range currentRange;

        private const string CONFIG_FOLDER = "level-config";
        public bool Load()
        {
            if(!ConfigCache.TryGetConfig<List<Range>>("bonus-bar-leveling", CONFIG_FOLDER, out bonusComboMap))
            {
                BumperCopsDebug.LogError("Could not load speed config for enemy");
                return false;
            }

            requiredActionsCount = new List<float>();
            CurrentLevel = GameLevel.EASY;
            currentRange = bonusComboMap[(int)CurrentLevel];
            requiredActionsCount.Add(currentRange.Value);
            return true;
        }

        public bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null)
        {
            return currentRange.CanStepUp() || (int)CurrentLevel < bonusComboMap.Count - 1;
        }

        public void ChangeLevel(GameLevel recommendedLevel)
        {
            int rLevel = (int)recommendedLevel;
            bool justChangedLevel = false;
            if(CurrentLevel < recommendedLevel)
            {
                BumperCopsDebug.Log($"Bonus Leveling: Moved from {CurrentLevel} to {recommendedLevel}");
                CurrentLevel = recommendedLevel;
                justChangedLevel = true;
            }

            if(!currentRange.CanStepUp() && rLevel > bonusComboMap.Count - 1)
            {
                BumperCopsDebug.Log($"Bonus Leveling LeveLing: Reached max level in game speed {CurrentLevel}");
                return;
            }

            if(!currentRange.CanStepUp() && !justChangedLevel)
            {
                currentRange = bonusComboMap[(int)++CurrentLevel];
                BumperCopsDebug.Log($"Bonus Leveling Leveling: New speed Level is {CurrentLevel}");
            }

                // change speed
            currentRange.StepUp();
            requiredActionsCount.Clear();
            requiredActionsCount.Add(currentRange.Value);
            BumperCopsDebug.Log($"Bonus Leveling: After step up game speed: {CurrentLevel} with value {currentRange.Value}");
        }
    }
}