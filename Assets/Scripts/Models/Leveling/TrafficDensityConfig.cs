using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Models.Leveling
{
    public class TrafficDensityConfig : ILevelConfig
    {
        public GameLevel CurrentLevel { get; private set; }

        public IReadOnlyList<float> Values { get { return trafficDensity; }}

        public List<float> trafficDensity;
        public GameLeverType GameLeverType { get { return GameLeverType.TRAFFIC_DENSITY; }}
        private List<Range> trafficDensityMap;
        private Range currentRange;

        private const string CONFIG_FOLDER = "level-config";
        public bool Load()
        {
            if(!ConfigCache.TryGetConfig<List<Range>>("traffic-leveling", CONFIG_FOLDER, out trafficDensityMap))
            {
                BumperCopsDebug.LogError("Could not load speed config for enemy");
                return false;
            }

            CurrentLevel = GameLevel.EASY;
            currentRange = trafficDensityMap[(int)CurrentLevel];
            trafficDensity = new List<float>();
            trafficDensity.Add(1/currentRange.Value);
            return true;
        }

        public bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null)
        {
            return currentRange.CanStepUp() || (int)CurrentLevel < trafficDensityMap.Count - 1;
        }

        public void ChangeLevel(GameLevel recommendedLevel)
        {
            int rLevel = (int)recommendedLevel;
            bool justChangedLevel = false;
            if(CurrentLevel < recommendedLevel)
            {
                BumperCopsDebug.Log($"Switching the level from {CurrentLevel} to {recommendedLevel}");
                CurrentLevel = recommendedLevel;
                currentRange = trafficDensityMap[rLevel];
                justChangedLevel = true;
            }

            if(!currentRange.CanStepUp() && rLevel > trafficDensityMap.Count - 1)
            {
                BumperCopsDebug.Log($"Traffic Leveling: Reached max level in game speed {CurrentLevel}");
                return;
            }

            if(!currentRange.CanStepUp() && !justChangedLevel)
            {
                currentRange = trafficDensityMap[++rLevel];
                CurrentLevel = (GameLevel)rLevel;
                BumperCopsDebug.Log($"Traffic Leveling: New speed Level is {CurrentLevel}");
            }

                // change speed
            currentRange.StepUp();
            trafficDensity.Clear();
            trafficDensity.Add(1/currentRange.Value);

            BumperCopsDebug.Log($"Traffic Leveling : After step up game speed: {CurrentLevel} with value {currentRange.Value}");
        }
    }
}