using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Models.Leveling
{
    public class GameSpeedConfig : ILevelConfig
    {
        public GameLevel CurrentLevel { get; private set; }
        public GameLeverType GameLeverType { get { return GameLeverType.GAME_SPEED; }}
        public IReadOnlyList<float> Values { get { return currentSpeed; }}
        private List<Range> gameSpeedMap;
        private List<float> currentSpeed;
        private Range currentRange;

        private const string CONFIG_FOLDER = "level-config";
        public bool Load()
        {
            if(!ConfigCache.TryGetConfig<List<Range>>("game-speed-leveling", CONFIG_FOLDER, out gameSpeedMap))
            {
                BumperCopsDebug.LogError("Could not load speed config for enemy");
                return false;
            }

            currentSpeed = new List<float>();
            CurrentLevel = GameLevel.EASY;
            currentRange = gameSpeedMap[(int)CurrentLevel];
            currentSpeed.Add(currentRange.Value);
            return true;
        }

        public bool CanChangeLevel(IList<ILevelConfig> dependentConfigs = null)
        {
            return currentRange.CanStepUp() || (int)CurrentLevel < gameSpeedMap.Count - 1;
        }

        public void ChangeLevel(GameLevel recommendedLevel)
        {
            int rLevel = (int)recommendedLevel;
            bool justChangedLevel = false;
            if(CurrentLevel < recommendedLevel)
            {
                BumperCopsDebug.Log($"Game Speed Leveling: Moved from {CurrentLevel} to {recommendedLevel}");
                CurrentLevel = recommendedLevel;
                currentRange = gameSpeedMap[rLevel];
                justChangedLevel = true;
            }

            if(!currentRange.CanStepUp() && rLevel > gameSpeedMap.Count - 1)
            {
                BumperCopsDebug.Log($"Game Speed LeveLing: Reached max level in game speed {CurrentLevel}");
                return;
            }

            if(!currentRange.CanStepUp() && !justChangedLevel)
            {
                currentRange = gameSpeedMap[(int)++CurrentLevel];
                BumperCopsDebug.Log($"Game speed Leveling: New speed Level is {CurrentLevel}");
            }

                // change speed
            currentRange.StepUp();
            currentSpeed.Clear();
            currentSpeed.Add(currentRange.Value);   
            BumperCopsDebug.Log($"Game speed Leveling: After step up game speed: {CurrentLevel} with value {currentRange.Value}");
        }
    }
}