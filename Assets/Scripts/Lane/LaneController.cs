﻿using System;
using System.Collections;
using UnityEngine;

public class LaneController : MonoBehaviour
{
    [SerializeField]
    private int id;
    public int Id { get {return id; }}

    [SerializeField]
    private GameObject nextLane;
    public GameObject NextLane { get {return nextLane; }}

    [SerializeField]
    private GameObject previousLane;
    public GameObject PreviousLane { get {return previousLane; }}

    [SerializeField]
    private LaneDirection laneDirection;
    public LaneDirection LaneDirection { get {return laneDirection; }}

    [SerializeField]
    private Sprite laneBlockSprite;

    public RoadController Road { get; private set; }
    public void Init(RoadController road) 
    {
        this.Road = road;
        //BumperCopsDebug.Log("Road number is " + road.MyPathNumber);
    }
}
