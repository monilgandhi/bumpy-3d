using System.Collections;
using UnityEngine;

public class PowerupPickedUpBehaviour : MonoBehaviour 
{
    public delegate void PowerupPickedupEventHandler(PowerupPickedUpBehaviour c, CollectiblePickedEventArgs args);
    public event PowerupPickedupEventHandler PowerupCollected;

    [SerializeField]
    private int value;
    public int OriginalValue { get { return value; }}

    [Header("Particle Effects")]
    [SerializeField]
    private ParticleSystem glowEffect;

    [SerializeField]
    private ParticleSystem powerupParticeOnCollect;

    [Header("Sound Effects")]
    [SerializeField]
    private AudioSource powerupPickupSound;

    [SerializeField]
    private PowerupType type;

    public PowerupType Type { get { return type; }}

    private int finalRewardValue;
    public int Value { get { return finalRewardValue; }}

    private Renderer modelRenderer;
    private float yPosition;
    private void Awake() 
    {
        modelRenderer = this.GetComponent<Renderer>();    
        yPosition = this.transform.position.y;
    }
    
    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag.Equals(Constants.PLAYER_COLLECTIBLE_TAG))
        {
            //BumperCopsDebug.Log($"On trigger for type {this.Type} for name {this.transform.root.name}");
            PowerupCollected?.Invoke(this, new CollectiblePickedEventArgs(Value, this.Type));
            StartCoroutine(PlayCollectEffect());
        }
    }

    private void ToggleParticleSystem(ParticleSystem system, bool enable)
    {
        if(system == null)
        {
            return;
        }

        if(enable)
        {
            system.Play();
        }
        else
        {
            system.Stop();
        }
    }

    private IEnumerator PlayCollectEffect()
    {
        ToggleParticleSystem(glowEffect, false);
        this.modelRenderer.enabled = false;

        ToggleParticleSystem(powerupParticeOnCollect, true);
        
        if(this.powerupPickupSound != null)
        {
            this.powerupPickupSound.Play();
            yield return new WaitWhile(() => this.powerupPickupSound.isPlaying);
        } else if (powerupParticeOnCollect != null)
        {
            yield return new WaitWhile(() => this.powerupParticeOnCollect.isPlaying);
        }

        ToggleParticleSystem(powerupParticeOnCollect, false);
        Disable();
    }

    public void Disable() 
    {
        this.transform.root.gameObject.SetActive(false);
        this.modelRenderer.enabled = false;
    }

    public void Enable() 
    {
        // reset the yPosition
        this.transform.position = new Vector3(this.transform.position.x, yPosition, this.transform.position.z);
        this.transform.root.gameObject.SetActive(true);
        this.modelRenderer.enabled = true;

        ToggleParticleSystem(glowEffect, true);
    }

    public void UpdateValue(int newValue)
    {   
        this.finalRewardValue = newValue;
    }
}