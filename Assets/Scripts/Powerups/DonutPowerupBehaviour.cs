using System;
using System.IO;
using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(DonutPowerupFactory))]
public class DonutPowerupBehaviour : MonoBehaviour
{
    public event Action<DonutPowerupEventArgs> PowerupSelected;
    public event Action<DonutPowerupEventArgs> PowerupReady;
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupOver;
    public event Action<DonutPowerupEventArgs> PowerupWasted;

    [Header("Game Elements")]
    [SerializeField]
    private TouchManager touchManager;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private PowerupManager powerupManager;

    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private PlayerController playerController;

    [SerializeField]
    private BonusBarBehaviour bonusBarBehaviour;

    [Header("UI Elements")]
    [SerializeField]
    private PowerupImageScrollBehaviour imageScrollBehaviour;

    [Header("Admin")]
    [SerializeField]
    private bool useOnTap = true;

    [SerializeField]
    private PowerupType adminType = PowerupType.NONE;

    public IReadOnlyDictionary<PowerupType, Sprite> PowerupImageMap { get { return donutPowerupFactory.DonutPowerupImageMap; }}
    private DonutPowerupFactory donutPowerupFactory;
    private IDonutPowerup nextSelectedDonutPowerup;

    private bool isPowerupPocketActive;
    private bool isPowerupActive;
    private bool isTutorialActive;
    private bool canActivatePowerup;
    
    private List<PowerupProbabilityConfig> activeConfig;
    private const string POWERUP_CONFIG_PATH = "powerup";
    private void Awake()
    {
        donutPowerupFactory = this.GetComponent<DonutPowerupFactory>();
    }

    private void Start()
    {
        this.gameManager.GamePaused += OnGamePaused;
        this.gameManager.GameOver += OnGameOver;
        this.gameManager.GameStarted += OnGameStarted;
        this.gameManager.GameTutorialSessionInitialized += OnGameTutorialStarted;
        //this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;

        powerupManager.PowerupCollected += OnPowerupCollected;
        imageScrollBehaviour.PowerupScrollComplete += OnPowerupScrollCompleted;

        // this.playerController.AttackController.PlayerEnteredAttackZone += OnPlayerEnteredAttackZone;
        // this.playerController.AttackController.PlayerExitAttackZone += OnPlayerExitAttackZone;

        string fileName = this.playerController.HasWeaponUnlocked ? "powerup-weapon" : "powerup";

        ConfigCache.TryGetConfig<List<PowerupProbabilityConfig>>(fileName, POWERUP_CONFIG_PATH, out activeConfig);
    }

    private void OnGameTutorialStarted(GameEventArgs args)
    {
        isTutorialActive = true;
    }

    private void OnGameStarted(GameEventArgs args)
    {
        if(isTutorialActive)
        {
            // this is turned on in the tutorual step update
            return;
        }
        this.touchManager.Tapped += OnTap;
    }

    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        isTutorialActive = args.TutorialStep < TutorialStep.OVER;
        
        /*if(args.TutorialStep >= TutorialStep.POWERUP_USE_OBJECTIVE)
        {
            this.touchManager.Tapped += OnTap;
        }*/
    }

    private void OnPlayerEnteredAttackZone()
    {
        canActivatePowerup = false;
        imageScrollBehaviour.TogglePowerup(canActivatePowerup);
    }

    private void OnPlayerExitAttackZone()
    {
        canActivatePowerup = true;
        imageScrollBehaviour.TogglePowerup(canActivatePowerup);
    }

    private void OnPowerupScrollCompleted()
    {
        canActivatePowerup = true;
        PowerupReady?.Invoke(new DonutPowerupEventArgs(nextSelectedDonutPowerup.Type));
        if(!useOnTap)
        {
            OnTap(null);
        }
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.DONUT || isPowerupPocketActive)
        {
            return;
        }

        isPowerupPocketActive = true;
        //BumperCopsDebug.Log("Donut powerup connected");
        
        SelectNextPowerup();
        PowerupSelected?.Invoke(new DonutPowerupEventArgs(nextSelectedDonutPowerup.Type));
        return;
    }

    private void OnGamePaused()
    {
        DeregisterTapEvent();
    }

    private void OnTap(TapGestureArgs args)
    {
        if(!isPowerupPocketActive || !canActivatePowerup)
        {
            imageScrollBehaviour.PlayUnavailbleAnimation();
            return;
        }

        nextSelectedDonutPowerup.Activate(enemyManager, playerController, bonusBarBehaviour);
        BumperCopsDebug.Log("Tapped");
    }

    private void OnGameOver(GameOverReason reason)
    {
        DeregisterTapEvent();
    }

    private void DeregisterTapEvent()
    {
        this.touchManager.Tapped -= OnTap;
    }

    private void SelectNextPowerup()
    {
        float value = UnityEngine.Random.value;

        PowerupType nextType = PowerupType.BIG_BOOST;

        BumperCopsDebug.Log($"active config size is {activeConfig.Count}");
        if(!isTutorialActive)
        {
            foreach(PowerupProbabilityConfig p in activeConfig)
            {
                BumperCopsDebug.Log($"Probability config {p.Type}, min value: {p.MinimumValue}, max value: {p.MaximumValue}");
                if(value >= p.MinimumValue && value < p.MaximumValue)
                {
                    nextType = p.Type;
                    break;
                }
            }
        }
        else
        {
            // generate once and turn off the tutorial
            isTutorialActive = false;
        }

        if(adminType != PowerupType.NONE)
        {
            nextType = adminType;
        }

        BumperCopsDebug.Log($"Next powerup is {nextType} with random value {value}");

        if(!donutPowerupFactory.TryGetDonutPowerup(nextType, out nextSelectedDonutPowerup))
        {
            BumperCopsDebug.LogWarning($"Could not find powerup of type {nextType}");
            return;
        }

        nextSelectedDonutPowerup.PowerupStarted += OnDonutPowerupStarted;
        nextSelectedDonutPowerup.PowerupComplete += OnDonutPowerupEnded;
    }

    private void OnDonutPowerupStarted(DonutPowerupEventArgs args)
    {
        //BumperCopsDebug.Log($"Powerup of type {args.Type} started");
        PowerupStarted?.Invoke(args);
        isPowerupActive = true;
        nextSelectedDonutPowerup.PowerupStarted -= OnDonutPowerupStarted;
    }

    private void OnDonutPowerupEnded(DonutPowerupEventArgs args)
    {
        isPowerupPocketActive = false;
        canActivatePowerup = false;

        if(!isPowerupActive)
        {
            // wasted powerrup
            PowerupWasted?.Invoke(args);
        }
        else
        {
            isPowerupActive = false;
            PowerupOver?.Invoke(args);
        }
        
        BumperCopsDebug.Log($"Powerup of type {args.Type} ended");
        nextSelectedDonutPowerup.PowerupComplete -= OnDonutPowerupEnded; 
    }
}