using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour 
{
    public event Action<CollectiblePickedEventArgs> PowerupReady;
    public event Action<CollectiblePickedEventArgs> PowerupCollected;
    public event Action<CollectiblePickedEventArgs> TimedPowerupExpired;
    public event Action<PowerupSpawnedEventArgs> PowerupSpawned;
    public event Action<PowerupSpawnedEventArgs> PowerupGenerated;

    public PowerupReward DefaultReward 
    {
        get { return new PowerupReward(PowerupType.COINS, DefaultRewardAmount, true); }
    }    

    public int DefaultRewardAmount
    {
        get { return DEFAULT_REWARD_AMOUNT; }
    }

    [SerializeField]
    private DonutPowerupBehaviour donutPowerupBehaviour;

    [Header("Powerup Prefabs")]
    [SerializeField]
    private GameObject coinPrefab;

    [SerializeField]
    private GameObject boostPrefab;

    [SerializeField]
    private GameObject donutPrefab;

    [SerializeField]
    private UtilityBeltBehaviour utilityBeltBehaviour;

    private GameManager gameManager;
    private EnemyController enemy;

    private float boostGenerationTimer = 3;

    private Dictionary<PowerupType, Queue<PowerupPickedUpBehaviour>> pool = new Dictionary<PowerupType, Queue<PowerupPickedUpBehaviour>>();
    private HashSet<PowerupPickedUpBehaviour> activeCollectiblePool = new HashSet<PowerupPickedUpBehaviour>();
    private Queue<PowerupSpawnedEventArgs> pendingCollectible = new Queue<PowerupSpawnedEventArgs>();
    private int clockPoolActiveIndex = 0;
    private int boostPoolActiveIndex = 0;
    private PowerupGenerator PowerupGenerator;
    private Dictionary<PowerupType, Sprite> powerupSprites = new Dictionary<PowerupType, Sprite>();
    private const int DEFAULT_REWARD_AMOUNT = 5;
    private int previousPowerupLaneId;
    private PowerupTutorialManager powerupTutorialManager;
    private bool isPowerupActive;
    private void Awake() 
    {
        gameManager = this.GetComponentInParent<GameManager>();
        this.gameManager.GameStarted += OnGameStarted;
        LoadPool();
        LoadSprites();
    }

    private void OnGameStarted(GameEventArgs args)
    {
        PowerupGenerator = new PowerupGenerator(args.CurrentSessionNumber);
        PowerupGenerator.UpdateBoostCoolOff(1.5f);
    }

    private void LoadPool()
    {
        Queue<PowerupPickedUpBehaviour> coinPool = new Queue<PowerupPickedUpBehaviour>();
        GameObject pooledObject = null;
        for(int i = 0; i < 100; i++)
        {
            pooledObject = Instantiate(coinPrefab);
            pooledObject.name = $"coin_{i}";
            
            PowerupPickedUpBehaviour coinBehaviour = pooledObject.GetComponentInChildren<PowerupPickedUpBehaviour>();
            coinBehaviour.PowerupCollected += OnCollectibleCollected;
            coinPool.Enqueue(coinBehaviour);
            coinBehaviour.Disable();
        }

        Queue<PowerupPickedUpBehaviour> boostPool = new Queue<PowerupPickedUpBehaviour>();
        Queue<PowerupPickedUpBehaviour> donutPool = new Queue<PowerupPickedUpBehaviour>();

        for(int i = 0; i < 50; i++)
        {
            pooledObject = Instantiate(boostPrefab);
            pooledObject.name = $"boost_{i}";
            
            PowerupPickedUpBehaviour boostBehaviour = pooledObject.GetComponentInChildren<PowerupPickedUpBehaviour>();
            boostBehaviour.PowerupCollected += OnCollectibleCollected;
            boostPool.Enqueue(boostBehaviour);
            boostBehaviour.Disable();
        }

        for(int i = 0; i < 5; i++)
        {
            // generate backup
            pooledObject = Instantiate(donutPrefab);
            pooledObject.name = $"donut_{i}";
            
            PowerupPickedUpBehaviour  donutCollectibleBehaviour = pooledObject.GetComponentInChildren<PowerupPickedUpBehaviour>();
            donutCollectibleBehaviour.PowerupCollected += OnCollectibleCollected;
            donutPool.Enqueue(donutCollectibleBehaviour);
            donutCollectibleBehaviour.Disable();
        }
        
        pool.Add(PowerupType.COINS, coinPool);
        pool.Add(PowerupType.BOOST, boostPool);
        pool.Add(PowerupType.DONUT, donutPool);
    }

    private void OnCollectibleCollected(PowerupPickedUpBehaviour c, CollectiblePickedEventArgs args)
    {
        if(c == null || args == null)
        {
            BumperCopsDebug.LogWarning("PowerupPickedUpBehaviour is null");
            return;
        }

        if(enemy == null && args.Type != PowerupType.COINS)
        {
            return;
        }

        BumperCopsDebug.Log($"Powerup collected {args.Type}");
        PowerupCollected?.Invoke(args);
        // move to the pool bucket
    }

    private void LoadSprites()
    {
        powerupSprites = new Dictionary<PowerupType, Sprite>();
        StringBuilder basePath = new StringBuilder(Constants.COLLECTIBLE_FOLDER + Path.DirectorySeparatorChar);
        powerupSprites.Add(PowerupType.BOOST, Resources.Load<Sprite>(basePath + "boost"));
    }

    private void Start() 
    {
        this.gameManager.EnemyManager.EnemySpawned += OnEnemySpawned;
        this.gameManager.EnemyManager.EnemyHealthOver += OnEnemyHealthOver;
        this.gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
        this.gameManager.TutorialManager.TutorialEnd += OnTutorialEnd;
        this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
        // game manager events
        // game over
        this.gameManager.GameOver += OnGameOver;

        if(gameManager.GameMode == GameMode.NORMAL)
        {
            BumperCopsDebug.Log("PowerupManager: Game mode is normal");
            this.PowerupSpawned += OnSpawnNewCollectible;
        }

        donutPowerupBehaviour.PowerupReady += OnDonutPowerupReady;
        donutPowerupBehaviour.PowerupOver += OnDonutPowerupComplete;
        donutPowerupBehaviour.PowerupWasted += OnDonutPowerupComplete;
        donutPowerupBehaviour.PowerupStarted += OnDonutPowerupTimerStart;
        utilityBeltBehaviour.PowerupActivated += OnUtilityPowerupActivated;
    }
    
    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        powerupTutorialManager.OnTutorialUpdate(args.TutorialStep);
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        powerupTutorialManager = new PowerupTutorialManager();
    }

    private void OnTutorialEnd(TutorialArgs args)
    {
        powerupTutorialManager.EndTutorial();
    }

    private void OnGameOver(GameOverReason reason)
    {
        PowerupGenerator.GameEnd();
        foreach(PowerupPickedUpBehaviour b in activeCollectiblePool)
        {
            if(b != null && b.gameObject.activeInHierarchy)
            {
                b.gameObject.SetActive(false);
            }
        }
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        //BumperCopsDebug.Log("Enemy spawned. Will start boost generation");
        pendingCollectible.Clear();
        this.enemy = this.gameManager.EnemyManager.currentEnemy;
        PowerupGenerator.Reset();
    }

    private void OnEnemyHealthOver(EnemyArgs args)
    {
        this.enemy = null;
        ReplenishPool();
    }

    private void OnUtilityPowerupActivated(UtilityBeltEventArgs args)
    {
        BumperCopsDebug.Log($"Utility belt active {args.Type}");
        PowerupCollected?.Invoke(new CollectiblePickedEventArgs(1, args.Type));
    }

    private void OnDonutPowerupTimerStart(DonutPowerupEventArgs args)
    {   
        BumperCopsDebug.Log($"Powerup active {args.Type}");
        PowerupCollected?.Invoke(new CollectiblePickedEventArgs(1, args.Type));
    }

    private void OnDonutPowerupReady(DonutPowerupEventArgs args)
    {
        if(isPowerupActive)
        {
            BumperCopsDebug.LogError("Powerup is already active");
            return;
        }
        
        PowerupReady?.Invoke(new CollectiblePickedEventArgs(1, args.Type));
        isPowerupActive =true;
    }

    private void OnDonutPowerupComplete(DonutPowerupEventArgs args)
    {
        if(!isPowerupActive)
        {
            BumperCopsDebug.LogError("Powerup is not active");
            return;
        }
        
        isPowerupActive = false;
        BumperCopsDebug.Log($"Powerup inactive {args.Type}");
        TimedPowerupExpired?.Invoke(new CollectiblePickedEventArgs(1, args.Type));
    }

    private void LateUpdate() 
    {
        SpawnNewCollectible();
        GeneratePowerupFromTheQueue();
        ReplenishPool();
        // generate the boosts every X seconds
        // TODO figure out when is the lane null
    }

    private void ReplenishPool()
    {
        List<PowerupPickedUpBehaviour> itemsToRemove = new List<PowerupPickedUpBehaviour>();
        foreach(PowerupPickedUpBehaviour c in activeCollectiblePool)
        {
            if(c.transform.root.position.z < this.gameManager.Player.transform.position.z - 200)
            {
                //BumperCopsDebug.Log($"Removing the item of type {c.transform.root.name} with position {c.transform.root.position.z} and player position {this.gameManager.Player.transform.position.z}");
                itemsToRemove.Add(c);
            }
        }

        itemsToRemove.ForEach(i => 
        {
            i.Disable();
            activeCollectiblePool.Remove(i);
            pool[i.Type].Enqueue(i);
        });
    }

    public bool GeneratePowerup(PowerupType type, Vector3 position, bool isBonus)
    {
        Queue<PowerupPickedUpBehaviour> pooledCollectible;
        if(!pool.TryGetValue(type, out pooledCollectible))
        {
            return false;
        }

        int multiplier = 1;
        BumperCopsDebug.Log($"Generating power of type {type}");

        GameObject prefabToGenerate = null;
        switch(type)
        {
            case PowerupType.DONUT:
                prefabToGenerate = donutPrefab;
                break;
            
            case PowerupType.COINS:
                prefabToGenerate = coinPrefab;
                break;

            case PowerupType.BOOST:
                prefabToGenerate = boostPrefab;
                break;
        }

        GeneratePowerup(position, pool[type], ref clockPoolActiveIndex, prefabToGenerate, multiplier);

        return true;
    }

    private void GeneratePowerupFromTheQueue()
    {
        if(enemy == null || pendingCollectible.Count == 0)
        {
           // BumperCopsDebug.LogWarning("No Enemy found");
            return;
        }

        LaneController enemyLane = enemy.Lane;
        LaneController playerLane = this.gameManager.Player.MoveController.Lane;
        if(enemyLane == null)
        {
            // add it to the queue
            //BumperCopsDebug.LogWarning("Enemy lane not found for generation of powerup");
            return;
        }

        // get position
        int laneToAvoid = UnityEngine.Random.value < 0.25 ? -1 : previousPowerupLaneId;

        PowerupSpawnedEventArgs args = pendingCollectible.Dequeue();
        int requestedLaneCount = args.NumberOfLanesToPlace;

        requestedLaneCount = powerupTutorialManager != null && 
                            powerupTutorialManager.IsTutorialActive ? 
                            enemyLane.Road.LaneCount : 
                            requestedLaneCount;

        BumperCopsDebug.Log($"requested lane count for {args.Reward.Type} is {requestedLaneCount}");
        HashSet<int> laneIds = new HashSet<int>();
        if(!TryGetGenerationLanesIndex(enemyLane.Road, playerLane, laneToAvoid, requestedLaneCount, out laneIds))
        {
            BumperCopsDebug.LogWarning("Unable to find lane for powerup. Will retry");
            return;
        }

        if(powerupTutorialManager != null && powerupTutorialManager.IsTutorialActive)
        {
            BumperCopsDebug.Log($"requested lane count is {requestedLaneCount}, lane counts :{laneIds.Count}");
        }

        // we want to count each generation as only 1 even if it is generated on multiple lanes
        bool powerupGenerated = false;
        foreach(int laneIdx in laneIds)
        {
            Vector3? nextLanePosition = enemyLane.Road.GetLanePosition(laneIdx);
            if(!nextLanePosition.HasValue)
            {
                BumperCopsDebug.LogWarning("Next lane position for boost generation is zero");
                return;
            }

            Vector3 position = new Vector3(nextLanePosition.Value.x, 1, this.gameManager.Player.transform.position.z + 200);

            // BumperCopsDebug.Log($"PowerupManager: Generating reward of type {args.Reward.Type}");
            bool success = GeneratePowerup(args.Reward.Type, position, false); 

            if(!powerupGenerated)
            {
                powerupGenerated = success;
            }
        }

        if(powerupGenerated)
        {
            // this is to track on the analytics side
            PowerupGenerated?.Invoke(args);
        }
    }

    private void OnSpawnNewCollectible(PowerupSpawnedEventArgs args)
    {
        if(args == null || args.Reward == null || args.Reward.IsDefaultReward)
        {
            return;
        }

        BumperCopsDebug.Log($"Enqueing collectible of type {args.Reward.Type}");
        pendingCollectible.Enqueue(args);
    }

    // this way of sending an event and then queueing is useful if we wanted to powerups with near miss not otherwise
    private void SpawnNewCollectible()
    {
        if(enemy == null)
        {
            return;
        }

        LaneController playerLane = this.gameManager.Player.MoveController.Lane;
        int requestedCount = 1;
        GameLevel currentLevel = this.gameManager.GameLevelManager.GetCurrentLevel(GameLeverType.ENEMY);
        // spawn any one
        if(Input.GetKeyDown(KeyCode.D))
        {
            //BumperCopsDebug.Log("Generating Donut");
            PowerupSpawned(new PowerupSpawnedEventArgs(new PowerupReward(PowerupType.DONUT, 1, false)));
        }        
        else if(CanGenerateDonuts())
        {   
            requestedCount = currentLevel == GameLevel.EASY ? 2 : 1;
            BumperCopsDebug.Log("Generating donuts");
            PowerupSpawned(new PowerupSpawnedEventArgs(new PowerupReward(PowerupType.DONUT, 1, false), requestedCount));
        } 
        else if(CanGenerateBoost())
        {
            requestedCount = currentLevel == GameLevel.EASY ? 2 : 1;
            PowerupSpawned(new PowerupSpawnedEventArgs(new PowerupReward(PowerupType.BOOST, 1, false), requestedCount));
        }
    }

    private bool CanGenerateDonuts()
    {
        if(powerupTutorialManager != null && powerupTutorialManager.IsTutorialActive && this.enemy != null)
        {
            if(powerupTutorialManager.GenerateDonut)
            {
                BumperCopsDebug.Log("Generating donuts now for tutorial");
                powerupTutorialManager.DonutsGenerated();
                return true;
            }

            return false;
        }

        return PowerupGenerator.CanSpawnDonut(
                        enemy.EnemyCharacteristics.Id,
                        enemyLevel: this.gameManager.GameLevelManager.GetCurrentLevel(GameLeverType.ENEMY),
                        gameSpeedLevel: this.gameManager.GameLevelManager.GetCurrentLevel(GameLeverType.GAME_SPEED),
                        trafficDensityLevel: this.gameManager.GameLevelManager.GetCurrentLevel(GameLeverType.TRAFFIC_DENSITY));
    }

    private bool CanGenerateBoost()
    {
        bool isBoostGenerationTimeup = PowerupGenerator.CanSpawnBoost(this.enemy);        
        if(powerupTutorialManager != null && powerupTutorialManager.IsTutorialActive)
        {
            return powerupTutorialManager.GenerateBoost && isBoostGenerationTimeup;
        }

        return isBoostGenerationTimeup;
    }

    private void GeneratePowerup(Vector3 position, Queue<PowerupPickedUpBehaviour> powerupPool, ref int currentActiveIdx, GameObject prefabOfCollectible, int multiplier)
    {
        // boost needs to have passed the player
        PowerupPickedUpBehaviour newCollectible = null;

        if(powerupPool.Count == 0)
        {
            BumperCopsDebug.LogError("Ran out of pooled objects. Adding a new item to the pool");

            if(prefabOfCollectible == null)
            {
                BumperCopsDebug.LogError("Cannot generate prefab since it is null");
            }

            GameObject b = Instantiate(prefabOfCollectible);
            newCollectible = b.GetComponentInChildren<PowerupPickedUpBehaviour>();
        }
        else
        {
            newCollectible = powerupPool.Dequeue();
            BumperCopsDebug.Log($"Getting a new powerr up from pool for type {newCollectible.Type}");
        }

        newCollectible.UpdateValue(newCollectible.OriginalValue * multiplier);
        activeCollectiblePool.Add(newCollectible);
        newCollectible.transform.root.position = position;
        //BumperCopsDebug.Log($"Generating collectible {newCollectible.Type} with name {newCollectible.transform.root.name} at position {newCollectible.transform.root.position}");
        newCollectible.Enable();
    }

    public static bool TryGetGenerationLanesIndex(
        RoadController roadToGenerate, 
        LaneController playerLane, 
        int laneToAvoid,
        int requestedLaneCount,
        out HashSet<int> generationLaneIds)
    {
        BumperCopsDebug.Log($"requested lane count is {requestedLaneCount}");
        generationLaneIds = new HashSet<int>();
        if(roadToGenerate == null || (playerLane == null))
        {
            BumperCopsDebug.LogWarning("Road to generate powerup on is null");
            return false;
        }

        if(roadToGenerate.LaneCount.Equals(requestedLaneCount))
        {
            for(int i = 0; i < requestedLaneCount; i++)
            {
                generationLaneIds.Add(i);
            }

            return true;
        }

        List<int> excludeLaneIds = new List<int>();
        excludeLaneIds.Add(laneToAvoid);

        // if we are on double or single generate based on enemy.
        // if we are on fork && if player and enemy are on the same side, generate per enemy, else generate per player
        // or if enemy lane is null

        for(int i = 0; i < requestedLaneCount; i++)
        {
            int selectedLane = -1;
            if (roadToGenerate.RoadType == RoadManager.RoadType.FORK)
            {
                selectedLane = CarUtils.GetRandomLane(roadToGenerate, 
                    playerLane.LaneDirection, 
                    excludeLaneIds);
            }
            else
            {
                selectedLane = CarUtils.GetRandomLane(roadToGenerate, excludeLaneIds: excludeLaneIds);

            }

            generationLaneIds.Add(selectedLane);
        }
        
        return true;
    }
}