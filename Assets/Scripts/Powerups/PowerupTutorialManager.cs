public class PowerupTutorialManager
{
    public bool IsTutorialActive { get; private set; }
    private bool? canGenerateDonut;

    public bool GenerateDonut
    {
        get 
        {
             return canGenerateDonut.HasValue && canGenerateDonut.Value; 
        }
    }
    
    public bool GenerateBoost { get; private set; }

    public PowerupTutorialManager()
    {
        this.IsTutorialActive = true;
        GenerateBoost = true;
    }

    public void OnTutorialUpdate(TutorialStep step)
    {
        if(!IsTutorialActive)
        {
            return;
        }
        
        if(step >= TutorialStep.DONUT_GENERATION_INIT)
        {
            BumperCopsDebug.Log($"tutorial step is {step} and can generate bonus has value {canGenerateDonut.HasValue}");
            if(!canGenerateDonut.HasValue)
            {
                BumperCopsDebug.Log($"Value is {canGenerateDonut}");
                canGenerateDonut = true;
            }

            GenerateBoost = false;
            BumperCopsDebug.Log($"Can generate boost {GenerateBoost} with step {step}");
        }
    }

    public void DonutsGenerated()
    {
        canGenerateDonut = false;
    }
    
    public void EndTutorial()
    {
        BumperCopsDebug.Log("Tutorial ended");
        this.IsTutorialActive = false;
        GenerateBoost = true;
        canGenerateDonut = true;
    }
}