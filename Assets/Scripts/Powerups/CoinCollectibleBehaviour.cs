using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameManager))]
public class CoinCollectibleBehaviour : MonoBehaviour 
{
    public delegate void CoinAwardedEventHandler(int amountOfCoinsAwarded, string reason);
    public event CoinAwardedEventHandler CoinAwarded;

    [SerializeField]
    private GameObject coinEffectsPrefab;

    [SerializeField]
    private TimerBehaviour timer;

    [SerializeField]
    private AudioSource coinBlastAudio;

    [Header("UI Elements")]
    [SerializeField]
    private CoinStatsBehaviour coinStatsBehaviour;

    private GameManager gameManager;
    private const float yPosDeltaWrtPlayer = 5;
    private const int DEFAULT_COIN_COUNT = 10;

    public long CoinsCollected { get { return BountiesCollected + NearMissCollected + BonusCoinCollected; }}
    public int BountiesCollected { get; private set; }
    public int NearMissCollected { get; private set; }
    public int BonusCoinCollected { get ; private set; }
    private ParticleSystem coinEffects;

    private void Start() 
    {
        if(coinEffects == null)
        {
            GameObject go = Instantiate(coinEffectsPrefab, this.transform);
            coinEffects = go.GetComponent<ParticleSystem>();
        }

        this.gameManager = this.GetComponent<GameManager>();
        this.gameManager.EnemyManager.EnemyHealthOver += OnEnemyDestroyed;
        this.gameManager.PowerupManager.PowerupCollected += OnPowerupCollected;
        //this.gameManager.GameStarted += (args) => coinStats.Switch("Coins", CoinsCollected.ToString());
        coinStatsBehaviour.ChangeAmount(CoinsCollected.ToString());
    }    

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.COINS)
        {
            return;
        }

        BonusCoinCollected += args.Value;
        BumperCopsDebug.Log($"Awarding coins with value {args.Value}");
        AwardCoins(args.Value, "coin");
    }

    private void OnEnemyDestroyed(EnemyArgs args)
    {
        if(args.Bounty < 0)
        {
            BumperCopsDebug.LogError("Bounty is less than 0");
            return;
        }

        StartCoroutine(GiveRewardOnPlayerFace(args.Bounty));
    }

    private IEnumerator GiveRewardOnPlayerFace(int coinCount)
    {
        coinBlastAudio.Play();
        if(!coinEffects.isPlaying)
        {
            coinEffects.Play();
        }

        IncreaseCurrentEnemyCoins(coinCount);

        int i = 0;
        while(coinEffects.isPlaying || i < coinCount)
        {
            this.transform.position = new Vector3(this.gameManager.Player.transform.position.x,
                 this.gameManager.Player.transform.position.y + yPosDeltaWrtPlayer, 
                 this.gameManager.Player.transform.position.z); 

            yield return new WaitForEndOfFrame();
        }
    }

    public void IncreaseNearMissCoins(int coinAmount)
    {
        NearMissCollected += coinAmount;
        BumperCopsDebug.Log($"Nearmisscollected {NearMissCollected} with new coinamount {coinAmount}");
        AwardCoins(coinAmount, "near_miss");
    }
    
    private void IncreaseCurrentEnemyCoins(int bounty)
    {
        BountiesCollected += bounty;
        BumperCopsDebug.Log($"bounties {BountiesCollected} with new coinamount {bounty}");
        AwardCoins(bounty, "enemy");
    }
    
    private void AwardCoins(int coinAmount, string reason)
    {
        coinStatsBehaviour.ChangeAmount(CoinsCollected.ToString());
        //PlayerStats.Instance.IncreaseCoins(coinAmount);
        if(CoinAwarded != null)
        {
            CoinAwarded(coinAmount, reason);
        }
    }
}