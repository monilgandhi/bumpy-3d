using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupImageScrollBehaviour : MonoBehaviour 
{
    public event Action PowerupScrollComplete;

    [Header("Game Elements")]
    [SerializeField]
    private DonutPowerupBehaviour donutPowerupBehaviour;

    [Header("UI elements")]
    [SerializeField]
    private Image powerupImage;

    [SerializeField]
    private Color deactivatedColor;

    private Image outlineImage;

    private PowerupType nextPowerupType;
    private List<Sprite> powerupImages;
    private bool isPowerupPresent;
    private Animator animator;

    private void Awake() 
    {
        animator = this.GetComponent<Animator>();
        outlineImage = this.GetComponent<Image>();
    }
    
    private void Start()
    {
        RegisterEventHandlers();
        powerupImages = new List<Sprite>(donutPowerupBehaviour.PowerupImageMap.Values);
        powerupImages.Shuffle();
        this.gameObject.SetActive(false);
    }

    public void OnPowerupStarted(DonutPowerupEventArgs args)
    {
        if(!isPowerupPresent)
        {
            BumperCopsDebug.LogWarning("power is not present");
            return;
        }

        TogglePowerup(true);
        this.animator.SetTrigger("selected");
    }

    public void TogglePowerup(bool activated)
    {
        Color newColor = activated ? Color.white : deactivatedColor;
        this.outlineImage.color = newColor;
        this.powerupImage.color = newColor;
    }

    public void PlayUnavailbleAnimation()
    {
        this.animator.SetTrigger("unavailable");
    }

    public void OnPowerupOver(DonutPowerupEventArgs args)
    {
        BumperCopsDebug.Log("Powerup over called");
        if(!isPowerupPresent)
        {
            BumperCopsDebug.LogWarning("power is not present");
            return;
        }

        StopPowerup("exit");
    }
    public void OnPowerupWasted(DonutPowerupEventArgs args)
    {
        BumperCopsDebug.Log("Powerup over called");
        if(!isPowerupPresent)
        {
            BumperCopsDebug.LogWarning("power is not present");
            return;
        }

        StopPowerup("wasted");
    }

    private void StopPowerup(string animationState)
    {
        BumperCopsDebug.Log("Power up over");
        isPowerupPresent = false;
        TogglePowerup(false);
        this.animator.SetTrigger(animationState); 
    }

    public void PowerupImageAnimationStateComplete(string state)
    {
        switch(state)
        {
            case "enter":
                BumperCopsDebug.Log("enter");
                StartCoroutine(RotateImages());
                break;
                
            case "rotation":
                BumperCopsDebug.Log("rotation");
                PowerupScrollComplete?.Invoke();
                break;
            case "exit":
                this.gameObject.SetActive(false);
                break; 

            default:
                return;
        }
    }

    private void OnPowerupSelected(DonutPowerupEventArgs args)
    {
        if(args == null)
        {
            return;
        }

        if(isPowerupPresent)
        {
            BumperCopsDebug.LogWarning("power is already present");
            return;
        }

        isPowerupPresent = true;
        this.gameObject.SetActive(true);
        TogglePowerup(true);
        float value = UnityEngine.Random.value;

        nextPowerupType = args.Type;
    }

    private void RegisterEventHandlers()
    {
        donutPowerupBehaviour.PowerupSelected += OnPowerupSelected;
        donutPowerupBehaviour.PowerupStarted += OnPowerupStarted;
        donutPowerupBehaviour.PowerupOver += OnPowerupOver;
        donutPowerupBehaviour.PowerupWasted += OnPowerupWasted;
    }
    
    private IEnumerator RotateImages()
    {
        BumperCopsDebug.Log("starting rotation");

        int rotationCount = 0;
        int idx = 0;

        Sprite finalSpriteToShow = null;
        donutPowerupBehaviour.PowerupImageMap.TryGetValue(nextPowerupType, out finalSpriteToShow);
        while(rotationCount < 2)
        {
            powerupImage.sprite = powerupImages[idx++];;
            animator.SetTrigger("newImage");
            if(idx >= powerupImages.Count)
            {
                idx = 0;
                powerupImages.Shuffle();
                ++rotationCount;
            }

            yield return new WaitForSeconds(0.1f);
        }

        // show the final image
        powerupImage.sprite = finalSpriteToShow;
        animator.SetTrigger("newImage");

        BumperCopsDebug.Log($"Ending rotation with count {rotationCount} and length of {powerupImages.Count}");
        PowerupImageAnimationStateComplete("rotation");
    }
}