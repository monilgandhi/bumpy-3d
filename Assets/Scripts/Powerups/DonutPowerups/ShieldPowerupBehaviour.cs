using System;
using System.Collections;
using UnityEngine;

public class ShieldPowerupBehaviour : MonoBehaviour, IDonutPowerup
{    
    [SerializeField]
    private Sprite powerupSpriteImage;
    public Sprite PowerupSpriteImage { get { return powerupSpriteImage; }}
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;
    public PowerupType Type { get { return PowerupType.SHIELD; } }
    private DonutPowerupEventArgs defaultArgs;

    private PlayerController playerController;
    private void Awake()
    {
        defaultArgs = new DonutPowerupEventArgs(Type);
    }

    public void Activate(EnemyManager enemyManager, 
                    PlayerController player, 
                    BonusBarBehaviour bonusBarBehaviour)
    {
        this.playerController = player;
        PowerupStarted?.Invoke(defaultArgs);
        StartCoroutine(KeepShieldAnimationActiveForMoments());
    }

    private IEnumerator KeepShieldAnimationActiveForMoments()
    {
        yield return new WaitForSeconds(3);
        Deactivate();
    }

    private void Deactivate()
    {
        PowerupComplete?.Invoke(defaultArgs);
    }
}