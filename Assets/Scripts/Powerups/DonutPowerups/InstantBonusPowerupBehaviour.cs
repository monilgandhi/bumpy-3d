using System;
using System.Collections;
using UnityEngine;

public class InstantBonusPowerupBehaviour : MonoBehaviour, IDonutPowerup 
{
    [SerializeField]
    private Sprite powerupImage;
    public Sprite PowerupSpriteImage { get { return powerupImage; }}
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;

    private DonutPowerupEventArgs defaultArgs;
    private BonusBarBehaviour bonusBar;
    public PowerupType Type { get { return PowerupType.INSTANT_BONUS; }}
    public void Activate(EnemyManager enemyManager, 
                            PlayerController playerController,
                            BonusBarBehaviour bonusBarBehaviour)
    {
        if(bonusBarBehaviour == null)
        {
            BumperCopsDebug.LogWarning("bonus bar behaviour is null");
            return;
        }

        if(defaultArgs == null)
        {
            defaultArgs = new DonutPowerupEventArgs(Type);
        }

        this.bonusBar = bonusBarBehaviour;
        this.gameObject.SetActive(true);
        this.bonusBar.MegaSirenTurnedOff += OnMegaSirenTurnedOff;
        PowerupStarted?.Invoke(defaultArgs);
    }

    private void OnMegaSirenTurnedOff()
    {
        this.bonusBar.MegaSirenTurnedOff -= OnMegaSirenTurnedOff;
        PowerupComplete?.Invoke(defaultArgs);
        this.gameObject.SetActive(false);
    }
}