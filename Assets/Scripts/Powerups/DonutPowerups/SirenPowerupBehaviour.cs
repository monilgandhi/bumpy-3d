using System;
using System.Collections;
using UnityEngine;

public class SirenPowerupBehaviour : MonoBehaviour, IDonutPowerup 
{
    [SerializeField]
    private int timer;

    [SerializeField]
    private Sprite powerupImage;
    public Sprite PowerupSpriteImage { get { return powerupImage; }}
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;

    private DonutPowerupEventArgs defaultArgs;
    public PowerupType Type { get { return PowerupType.SIREN; }}
    public void Activate(EnemyManager enemyManager, 
                            PlayerController playerController,
                            BonusBarBehaviour bonusBarBehaviour)
    {
        if(defaultArgs == null)
        {
            defaultArgs = new DonutPowerupEventArgs(Type);
        }

        this.gameObject.SetActive(true);
        StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer()
    {
        PowerupStarted?.Invoke(defaultArgs);
        float startTime = Time.time;
        yield return new WaitWhile(() => Time.time - startTime < timer);
        BumperCopsDebug.Log($"Stopping timer of {timer}");
        PowerupComplete?.Invoke(defaultArgs);
        this.gameObject.SetActive(false);
    }
}