using System;
using System.Collections;
using UnityEngine;

public class BackupMovementBehaviour : NPCMovementController 
{
    public event Action BackupBumped;
    private EnemyController enemy;
    private PlayerController playerController;
    private bool bumped;
    private void Awake() 
    {
        this.rigidBody = this.GetComponent<Rigidbody>();    
    }
    public bool Init(EnemyController enemy, PlayerController player)
    {
        if(enemy == null)
        {
            throw new ArgumentNullException(nameof(enemy));
        }

        this.playerController = player;
        this.enemy = enemy;

        LaneController lane = enemy.movementController.Lane;

        if(lane == null)
        {
            return false;
        }
        
        LaneController myLane = CarUtils.GetNextLane(lane);

        this.transform.position = 
            new Vector3(lane.transform.position.x, 
                        player.transform.position.y, 
                        player.transform.position.z - 10);

        this.isActive = true;
        bumped = false;

        return true;
    }    

    private void FixedUpdate() 
    {
        if(!isActive || bumped || this.enemy == null || !this.enemy.IsActive)
        {
            return;
        }

        if(this.transform.position.z > playerController.transform.position.z)
        {
            LaneController enemyLane = this.enemy.Lane;
            if(enemyLane != null && enemyLane.Id != this.Lane.Id && !changingLanes)
            {
                BumperCopsDebug.Log("Changing lanes to match that of enemy");
                StartCoroutine(MakeTurn(enemyLane, 0.8f, true));
            }    
        }
        
        int enemySpeed = Mathf.RoundToInt(this.enemy.Speed);

        if(this.enemy.transform.position.z - this.transform.position.z < 20)
        {
            StartCoroutine(Bump());
        }
        else
        {
            MoveWithSpeed(enemySpeed + 50);
        }
    }

    private IEnumerator Bump()
    {
        bumped = true;
        Vector3 target = new Vector3(enemy.transform.position.x, this.transform.position.y, this.transform.position.z + 1);
        Vector3 direction = target - this.transform.position;

        int count = 0;
        while(count < 2)
        {
            this.rigidBody.AddForce(direction.normalized * (this.rigidBody.mass * 2), ForceMode.Impulse);
            ++count;
            yield return new WaitForFixedUpdate();
        }
        
        yield return new WaitForSeconds(2);
        MoveWithSpeed(0);
        isActive = false;
    }

    private void MoveWithSpeed(int speed)
    {
        this.Speed = speed;
        velocityVector = transform.TransformDirection(Vector3.forward);
        base.Move();
    }

    private void OnCollisionExit(Collision other) 
    {
        if(other.gameObject.layer == Constants.ENEMY_LAYER && this.isActive)
        {
            this.isActive = false;
            this.Deactivate();
            BumperCopsDebug.Log("Deactivating: name " + this.name);
            BackupBumped?.Invoke();
        }    
    }
}