using System;
using UnityEngine;
using System.Collections;
public class BackupCarPowerupBehaviour : MonoBehaviour, IDonutPowerup 
{
    [SerializeField]
    private Sprite powerupSpriteImage;
    public Sprite PowerupSpriteImage { get { return powerupSpriteImage; } }
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;
    public PowerupType Type { get { return PowerupType.BACKUP; }}
    private BackupMovementBehaviour backupMovement;
    private PlayerController playerController;
    private EnemyManager enemyManager;

    private DonutPowerupEventArgs defaultEventArgs;

    private void Awake() 
    {    
        this.backupMovement = this.GetComponent<BackupMovementBehaviour>() ?? throw new MissingReferenceException(nameof(BackupMovementBehaviour));
        this.backupMovement.BackupBumped += OnBumped;
        defaultEventArgs = new DonutPowerupEventArgs(Type);
        BumperCopsDebug.Log("backup inited");
        this.gameObject.SetActive(false);
    }

    private void OnBumped()
    {
        Stop();
        PowerupComplete?.Invoke(defaultEventArgs);
    }

    public void Activate(EnemyManager enemyManager, 
                            PlayerController player, 
                            BonusBarBehaviour bonusBarBehaviour)
    {
        if(enemyManager.currentEnemy == null)
        {
            OnBumped();
            return;
        }

        this.playerController = player;
        this.enemyManager = enemyManager;

        InitCars();
    }

    private void InitCars()
    {
        bool success = backupMovement.Init(enemyManager.currentEnemy, this.playerController);
        
        if(!success)
        {
            OnBumped();
            return;
        }

        this.enemyManager.EnemyHealthOver += OnEnemyDestroyed;
        this.gameObject.SetActive(true);
        PowerupStarted?.Invoke(defaultEventArgs);
    }

    private void OnEnemyDestroyed(EnemyArgs args)
    {
        Stop();
    }

    private void FixedUpdate() 
    {
        if(this.enemyManager.currentEnemy == null)
        {
            OnBumped();
        }
    }

    private void Stop()
    {
        if(this.enemyManager != null)
        {
            this.enemyManager.EnemyHealthOver -= OnEnemyDestroyed;
        }
        
        this.backupMovement.Deactivate();
    }

    private IEnumerator HideOnPlayerPassing()
    {
        yield return new WaitWhile(() => this.transform.position.z > this.playerController.transform.position.z + 20);
        this.gameObject.SetActive(false);
    }
}