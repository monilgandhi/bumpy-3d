using System;
using System.Collections.Generic;
using UnityEngine;

public class DonutPowerupFactory : MonoBehaviour 
{
    [Header("Game Components")]
    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private BonusBarBehaviour bonusBarBehaviour;

    [Header("Donut powerup prefabs")]
    [SerializeField]
    private List<DonutPowerupsPrefab> donutPowerupsPrefabs;
    private Dictionary<PowerupType, IDonutPowerup> donutPowerups;
    private Dictionary<PowerupType, IDonutPowerup> utilityBeltPowerups;
    public IReadOnlyDictionary<PowerupType, Sprite> DonutPowerupImageMap { get; private set; }

    private void Awake() 
    {
        Init();    
    }
    private void Init()
    {
        donutPowerups = new Dictionary<PowerupType, IDonutPowerup>();
        utilityBeltPowerups = new Dictionary<PowerupType, IDonutPowerup>();
        Dictionary<PowerupType, Sprite> powerupImageMapLocal = new Dictionary<PowerupType, Sprite>();
        foreach(DonutPowerupsPrefab dpp in donutPowerupsPrefabs)
        {
            GameObject prefab = dpp.Prefab;
            if(prefab == null)
            {
                BumperCopsDebug.LogWarning($"prefab null for {dpp.Type}");
                continue;
            }

            GameObject instantiatedObject = Instantiate(prefab);
            IDonutPowerup powerupObject = (IDonutPowerup)instantiatedObject.GetComponent(typeof(IDonutPowerup));
            if(powerupObject == null)
            {
                BumperCopsDebug.LogWarning($"Unable to find powerup object for {dpp.Type}");
                continue;
            }

            if(dpp.IsUtilityBeltPowerup)
            {
                utilityBeltPowerups.Add(dpp.Type, powerupObject);
            }
            else
            {
                donutPowerups.Add(dpp.Type, powerupObject);
                powerupImageMapLocal.Add(dpp.Type, powerupObject.PowerupSpriteImage);
            }
        }

        DonutPowerupImageMap = powerupImageMapLocal;
    }

    public bool TryGetDonutPowerup(PowerupType type, out IDonutPowerup powerup)
    {
        powerup = default;
        if(donutPowerups == null)
        {
            return false;
        }

        return donutPowerups.TryGetValue(type, out powerup);
    }

    public bool TryGetUtilityPowerup(PowerupType type, out IDonutPowerup powerup)
    {
        powerup = default;
        if(utilityBeltPowerups == null)
        {
            return false;
        }

        return utilityBeltPowerups.TryGetValue(type, out powerup);
    }

    public void ActivatePowerup(IDonutPowerup powerup)
    {
        if(powerup == null)
        {
            BumperCopsDebug.LogWarning("powerup is null");
            return;
        }

        powerup.Activate(enemyManager, player, bonusBarBehaviour);
    }
}

[Serializable]
public class DonutPowerupsPrefab
{
    public GameObject Prefab;
    public PowerupType Type;
    public bool IsUtilityBeltPowerup;
}