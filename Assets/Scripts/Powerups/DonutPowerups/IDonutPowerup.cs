using System;
using UnityEngine;
public interface IDonutPowerup
{
    event Action<DonutPowerupEventArgs> PowerupStarted;
    event Action<DonutPowerupEventArgs> PowerupComplete;

    void Activate(EnemyManager enemyManager, 
                    PlayerController player, 
                    BonusBarBehaviour bonusBarBehaviour);

    PowerupType Type { get; }
    Sprite PowerupSpriteImage { get; }
}

public class DonutPowerupEventArgs
{
    public readonly PowerupType Type;

    public DonutPowerupEventArgs(PowerupType type)
    {
        this.Type = type;
    }
}