using System;
using System.Collections;
using UnityEngine;

public class BigBoostPowerupBehaviour : MonoBehaviour, IDonutPowerup 
{
    [SerializeField]
    private Sprite powerupImage;
    public Sprite PowerupSpriteImage { get { return powerupImage; }}
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;
    private DonutPowerupEventArgs defaultArgs;
    public PowerupType Type { get { return PowerupType.BIG_BOOST; }}
    private PlayerSpeedController playerSpeedController;


    public void Activate(EnemyManager enemyManager, 
                            PlayerController playerController,
                            BonusBarBehaviour bonusBarBehaviour)
    {
        if(playerController == null)
        {
            BumperCopsDebug.LogWarning("player speed controller is null");
            return;
        }

        playerSpeedController = playerController.SpeedController;
        playerController.SpeedController.PlayerBoostUpdate += OnPlayerBoostUpdated;
        
        if(defaultArgs == null)
        {
            defaultArgs = new DonutPowerupEventArgs(Type);
        }

        this.gameObject.SetActive(true);
        PowerupStarted?.Invoke(defaultArgs);
    }

    private void Update() 
    {
        if(!this.playerSpeedController.IsBoostInProgress)
        {
            OnPlayerBoostUpdated(new BoostUpdateEventArgs(false, 0, true));
        }
    }

    private void OnPlayerBoostUpdated(BoostUpdateEventArgs args)
    {
        if(!args.IsTurnedOn && args.IsPowerup)
        {
            playerSpeedController.PlayerBoostUpdate -= OnPlayerBoostUpdated;
            this.gameObject.SetActive(false); 
            BumperCopsDebug.Log($"Powerup of type {Type} ended");
            PowerupComplete?.Invoke(defaultArgs);
        }
    }
}