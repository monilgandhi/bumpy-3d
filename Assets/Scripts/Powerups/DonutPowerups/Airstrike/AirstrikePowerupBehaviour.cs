using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AirstrikePowerupBehaviour : MonoBehaviour, IDonutPowerup
{    
    [SerializeField]
    private Sprite powerupImage;
    public Sprite PowerupSpriteImage { get { return powerupImage; }}
    public event Action<DonutPowerupEventArgs> PowerupStarted;
    public event Action<DonutPowerupEventArgs> PowerupComplete;

    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private float speed;
    private EnemyManager enemyManager;
    private EnemyController enemyController;
    private PlayerController player;
    private List<Rigidbody> bullets;
    private bool isActive;
    private bool isShootInProgress;

    private const int NUMBER_OF_BULLETS = 10;
    private DonutPowerupEventArgs defaultEventArgs;

    private int numberOfbulletsShot;
    private IEnumerator shootingCoroutineRef;
    public PowerupType Type  { get { return PowerupType.AIR_SUPPORT; }}
    private void Awake() 
    {
        defaultEventArgs = new DonutPowerupEventArgs(Type);
        LoadPool();
        this.gameObject.SetActive(false);
    }

    private void LoadPool()
    {
        bullets = new List<Rigidbody>();
        bulletPrefab.SetActive(false);
        for(int i = 0; i < NUMBER_OF_BULLETS; i++)
        {
            GameObject go = Instantiate(bulletPrefab, this.transform);
            go.name = $"bullet_{i}";
            go.transform.localPosition = Vector3.zero;
            bullets.Add(go.GetComponent<Rigidbody>());
        }
    }

    public void Activate(EnemyManager eManager, 
                         PlayerController playerController,
                         BonusBarBehaviour bonusBarBehaviour)
    {
        enemyManager = eManager;
        player = playerController;

        if(enemyManager == null || player == null)
        {
            BumperCopsDebug.LogWarning("enemy or player null");
            return;
        }

        enemyController = enemyManager.currentEnemy;
        if(enemyController == null)
        {
            Deactivate();
            return;
        }

        enemyController.DamageController.EnemyDestructionBegin += OnEnemyDestroyed;
        
        isShootInProgress = false;
        this.gameObject.SetActive(true);
        isActive = true;
        this.transform.position = new Vector3(playerController.transform.position.x,
                                playerController.transform.position.y + 13,
                                playerController.transform.position.z);

        numberOfbulletsShot = 0;
        PowerupStarted?.Invoke(defaultEventArgs);
    }

    private void OnEnemyDestroyed()
    {
        enemyController.DamageController.EnemyDestructionBegin -= OnEnemyDestroyed;
        if(!this.isActive)
        {
            return;
        }
        
        StartCoroutine(StopAllCoroutinesAfterShooting());
    }

    private IEnumerator StopAllCoroutinesAfterShooting()
    {
        yield return new WaitWhile(() => 
            (numberOfbulletsShot < NUMBER_OF_BULLETS / 2));

        Deactivate();
    }

    private void Deactivate()
    {
        this.isActive = false;
        PowerupComplete?.Invoke(defaultEventArgs);
        if(!this.gameObject.activeSelf)
        {
            return;
        }

        if(shootingCoroutineRef != null)
        {
            StopCoroutine(shootingCoroutineRef);
            shootingCoroutineRef = null;
        }

        StartCoroutine(SayByebye());   
    }

    private void FixedUpdate() 
    {
        if(!isActive)
        {
            return;
        }

        if(this.enemyManager.currentEnemy == null)
        {
            // no enemy
            Deactivate();
            return;
        }

        Vector3 targetLocation = new Vector3(this.enemyManager.currentEnemy.transform.position.x,
                this.transform.position.y,
                this.enemyManager.currentEnemy.transform.position.z);

        float distance = Vector3.Distance(targetLocation, transform.position);
        // this need not be speed. Could be any small number
        if(distance > 1.5f)
        {
            //speed = distance/timeRemainingToReach;
            //BumperCopsDebug.Log($"Speed is {speed} & distance is {distance}");
            transform.position = Vector3.MoveTowards(transform.position, 
                                    targetLocation, 
                                    speed);
        }
        else if(!isShootInProgress)
        {
            //BumperCopsDebug.Log("Shootinh now");
            isShootInProgress = true;
            shootingCoroutineRef =Attack();
            StartCoroutine(shootingCoroutineRef);
        }
    }

    private IEnumerator Attack()
    {
        EnemyController enemy = this.enemyManager.currentEnemy;
        foreach(Rigidbody r in bullets)
        {
            //BumperCopsDebug.Log("shooting bullets now");
            r.gameObject.SetActive(true);

            float xPosition = UnityEngine.Random.Range(enemy.transform.position.x - 3, enemy.transform.position.x + 3);
            Vector3 target = new Vector3(xPosition, -1, enemy.transform.position.z);
            Vector3 direction = target - this.transform.position;
            r.AddForce(direction * 10, ForceMode.Impulse);
            ++numberOfbulletsShot;
            yield return new WaitForSeconds(0.1f);
        }

        //BumperCopsDebug.Log("Shootinh complete");
        ResetBullets();

        isShootInProgress = false;
    }

    private void ResetBullets()
    {
        foreach(Rigidbody r in bullets)
        {
            r.transform.localPosition = Vector3.zero;
            r.gameObject.SetActive(false);
        }
    }

    private IEnumerator SayByebye()
    {
        yield return new WaitWhile(() => this.player.transform.position.z < this.transform.position.z + 20);
        this.gameObject.SetActive(false);
    }
}