using UnityEngine;
using System.Collections;
public class AirStrikeBulletBehaviour : MonoBehaviour 
{
    [SerializeField]
    private ParticleSystem explosionOnCollision;

    [SerializeField]
    private ParticleSystem shootingSystem;

    public bool IsActive { get; private set; }

    private void OnCollisionEnter(Collision other) 
    {
        StartCoroutine(PlayExplosion());
    }    

    private IEnumerator PlayExplosion()
    {
        float startTime = Time.time;
        shootingSystem.Stop();
        explosionOnCollision.Play();    
        yield return new WaitWhile(() => (explosionOnCollision.isPlaying && Time.time - startTime < 3.0f));
        explosionOnCollision.Stop();
        IsActive = false;
        this.gameObject.SetActive(false);
    }

    private void OnEnable() 
    {
        IsActive = true;    
    }
}