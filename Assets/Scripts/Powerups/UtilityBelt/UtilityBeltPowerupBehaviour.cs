using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class UtilityBeltPowerupBehaviour : MonoBehaviour 
{
    public event Action<PowerupType> Activated;
    [SerializeField]
    private PowerupType powerupType; 
    public PowerupType PowerupType { get { return powerupType; }}

    [SerializeField]
    private DonutPowerupFactory donutPowerupFactory;

    [SerializeField]
    private TMP_Text coinCostPanel;

    [SerializeField]
    private TMP_Text collectibleCostPanel;
    
    private Button button;

    private IDonutPowerup powerup;

    private void Awake() 
    {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(OnUtilityPowerupActivated);
        donutPowerupFactory.TryGetUtilityPowerup(powerupType, out powerup);
    }

    private void OnUtilityPowerupActivated()
    {
        IDonutPowerup powerup = null;
        if(powerup == null && !donutPowerupFactory.TryGetUtilityPowerup(powerupType, out powerup))
        {
            BumperCopsDebug.LogError("Cannot find powerup");
            return;
        }

        donutPowerupFactory.ActivatePowerup(powerup);
        Activated?.Invoke(powerupType);
    }    

    public bool UpdateCost(int currentCollectibleCount, int coinCost)
    {
        BumperCopsDebug.Log("Updating the cost");
        this.gameObject.SetActive(true);
        if(currentCollectibleCount > 0)
        {
            coinCostPanel.transform.parent.gameObject.SetActive(false);
            collectibleCostPanel.text = $"x {currentCollectibleCount}";
        }
        else
        {
            BumperCopsDebug.Log("Showing the cost panel");
            coinCostPanel.transform.parent.gameObject.SetActive(true);
            coinCostPanel.text = coinCost.ToString();

            collectibleCostPanel.gameObject.SetActive(false);

            if(coinCost > PlayerStats.Instance.CoinCount)
            {
                this.gameObject.SetActive(false);
                return false;
            }
        }

        return true;
    }
}