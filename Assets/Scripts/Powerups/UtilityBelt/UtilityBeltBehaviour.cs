using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using System.Collections;

public class UtilityBeltBehaviour : MonoBehaviour 
{
    public event Action<UtilityBeltEventArgs> PowerupActivated;
    [Header("Gameplay elements")]
    [SerializeField]
    private GameManager gameManager;
    
    [SerializeField]
    private EnemyManager enemyManager;

    [Header("Powerups")]
    [SerializeField]
    private List<UtilityBeltPowerupBehaviour> powerupBehaviours;

    [Header("UI")]
    [SerializeField]
    private Button utiltyBeltButton;
    [SerializeField]
    private GameObject costPanel;

    [SerializeField]
    private TMP_Text costText;

    [SerializeField]
    private TMP_Text existingCountText;

    [SerializeField]
    private GameObject powerupPanel;
    private AnimationCompletedBehaviour animationCompletedBehaviour;

    private bool isOpen = false;
    private EnemyController enemy;
    private int existingCount;
    private bool isAnyPowerupActive;
    private bool canOpen;

    private static Dictionary<PowerupType, CollectibleType> powerupCollectibleMap = new Dictionary<PowerupType, CollectibleType>()
    {
        { PowerupType.AIR_SUPPORT, CollectibleType.AIR_STRIKE }
    };

    private static Dictionary<PowerupType, int> COST_POWERUP = new Dictionary<PowerupType, int>()
    {
        { PowerupType.AIR_SUPPORT, 350 }
    };

    private void Awake() 
    {
        animationCompletedBehaviour = this.GetComponent<AnimationCompletedBehaviour>();
        gameManager.GameStarted += OnGameStarted;
        enemyManager.EnemySpawned += OnEnemySpawned;
        // utiltyBeltButton.onClick.AddListener(delegate { ToggleOpen(); } );
        canOpen = true;

        foreach(UtilityBeltPowerupBehaviour b in powerupBehaviours)
        {
            b.Activated += OnPowerupActivated;
        }
    }

    private void Start() 
    {
        gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        canOpen = false;
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        canOpen = true;
    }

    private void OnPowerupActivated(PowerupType type)
    {
        CollectibleType collectible;
        if(!powerupCollectibleMap.TryGetValue(type, out collectible))
        {
            BumperCopsDebug.LogError($"Power up not found {type}");
            return;
        }

        int coinCost = 0;
        // reduce the collectible
        // check the collectibles
        if(CollectibleInventory.Instance.GetCollectibleAmountInventory(collectible) <= 0)
        {
            if(!COST_POWERUP.TryGetValue(type, out coinCost))
            {
                BumperCopsDebug.LogWarning($"cost not found for {type}");
                return;
            }

            // use coins
            PlayerStats.Instance.ReduceCoins(coinCost);
        }

        CollectibleInventory.Instance.UseCollectible(collectible);
        UpdateExistingCount();
        BumperCopsDebug.Log("Reducing the collectible");

        PowerupActivated?.Invoke(new UtilityBeltEventArgs(type, coinCost));
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        this.enemy = args.enemy;
        this.enemy.DamageController.EnemyDestructionBegin += OnEnemyDestructionBegan;
        StartCoroutine(OpenAndEnter());
    }

    private void OnEnemyDestructionBegan()
    {
        this.enemy.DamageController.EnemyDestructionBegin -= OnEnemyDestructionBegan;
        CloseAndExit();
    }

    private void OnGameStarted(GameEventArgs args)
    {
        this.gameObject.SetActive(true);
        UpdateExistingCount();
    }

    private void UpdateExistingCount()
    {
        isAnyPowerupActive = false;
        foreach(UtilityBeltPowerupBehaviour p in powerupBehaviours)
        {
            int cost = 0;
            if(!COST_POWERUP.TryGetValue(p.PowerupType, out cost))
            {
                BumperCopsDebug.LogWarning($"cost not found for {p.PowerupType}");
                continue;
            }

            CollectibleType collectible;
            if(!powerupCollectibleMap.TryGetValue(p.PowerupType, out collectible))
            {
                BumperCopsDebug.LogWarning($"collectible not found for powerup {p.PowerupType}");
                continue;
            }

            existingCount = CollectibleInventory.Instance.GetCollectibleAmountInventory(collectible);
            isAnyPowerupActive |= p.UpdateCost(existingCount, cost);
        }
    }

    private void CloseAndExit()
    {
        isOpen = false;
        animationCompletedBehaviour.StartAnimationWithBool("exit", true);
        animationCompletedBehaviour.StartAnimationWithBool("open", isOpen);
    }

    private IEnumerator OpenAndEnter()
    {
        if(!canOpen)
        {
            yield break;
        }

        animationCompletedBehaviour.StartAnimationWithBool("exit", false);
        ToggleOpen();
        yield break;
    }

    private void ToggleOpen(bool? forceOpen = null)
    {
        if(!canOpen && !isOpen)
        {
            return;
        }

        if(forceOpen.HasValue)
        {
            isOpen = forceOpen.Value;
        }
        else
        {
            isOpen = !isOpen;
        }
    
        if(isOpen)
        {
            UpdateExistingCount();
            // donot open if there is nothing to select from
            if(existingCount <= 0)
            {
                return;
            }
        }


        BumperCopsDebug.Log($"open value {isOpen}");
        animationCompletedBehaviour.StartAnimationWithBool("open", isOpen);
    }
}

public class UtilityBeltEventArgs
{
    public readonly PowerupType Type;
    public readonly int CoinCost;

    public UtilityBeltEventArgs(PowerupType type, int coinCost = 0)
    {
        this.Type = type;
        this.CoinCost = coinCost;
    }
}