using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CoinStatsBehaviour : MonoBehaviour {

    [SerializeField]
    private TMP_Text amount;

    public void ChangeAmount(string amount)
    {
        if(string.IsNullOrEmpty(amount))
        {
            throw new ArgumentNullException(nameof(amount));
        }

        this.amount.text = amount;
    }
}