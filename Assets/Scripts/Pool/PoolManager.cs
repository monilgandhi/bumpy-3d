using UnityEngine;
using System.Collections.Generic;

public abstract class PoolManager 
{
    protected List<GameObject> poolResources;
    protected int copies;

    protected GameObject parentPrefab;
    private Queue<GameObject> pooledGameObjects;
    private HashSet<GameObject> activeGameObjects;
    public IReadOnlyCollection<GameObject> ActiveGameObjects { get { return activeGameObjects; }}
    private string namePrefix;

    public PoolManager(string namePrefix)
    {
        this.namePrefix = namePrefix;
    }
    
    public virtual void InitPool(bool shuffle = true) 
    {
        pooledGameObjects = new Queue<GameObject>();
        activeGameObjects = new HashSet<GameObject>();
        HydratePool(namePrefix, copies);
    }

    private void HydratePool(string namePrefix, int numberOfCopies)
    {
        BumperCopsDebug.Log($"name prefix is {namePrefix}");
        List<GameObject> newItems = new List<GameObject>();
        if(poolResources == null || poolResources.Count == 0)
        {
            BumperCopsDebug.LogError("pool resources are not available");
            return;
        }

        foreach(GameObject o in poolResources)
        {    
            GameObject mainGo = null;
            for(int i = 0; i < numberOfCopies; i++)
            {
                if(parentPrefab != null)
                {
                    mainGo = GameObject.Instantiate(parentPrefab);

                    if(!IsParentValid(mainGo))
                    {
                        BumperCopsDebug.LogWarning("Invalid parent object");
                        break;
                    }

                    this.InitParentOnCreation(mainGo);

                    GameObject childGo = GameObject.Instantiate(o, mainGo.transform);
                    if(!IsActualPrefabValid(childGo))
                    {
                        BumperCopsDebug.LogWarning("Invalid child object");
                        break;
                    }      

                    this.InitActualPrefabOnCreation(mainGo);              
                }
                else
                {
                    mainGo = GameObject.Instantiate(o);
                    if(!IsActualPrefabValid(mainGo))
                    {
                        BumperCopsDebug.LogWarning("Invalid child object");
                        break;
                    }   

                    this.InitActualPrefabOnCreation(mainGo);
                }

                mainGo.name = $"{namePrefix}-{i}";
                mainGo.SetActive(false);
                newItems.Add(mainGo);
            }   
        }

        newItems.Shuffle();
        foreach(GameObject i in newItems)
        {
            pooledGameObjects.Enqueue(i);
        }
    }

    public bool TryGetFromPool(out GameObject gameObject, bool forceCreate = true)
    {
        gameObject = null;
        if(pooledGameObjects.Count == 0 && poolResources.Count == 0)
        {
            BumperCopsDebug.LogWarning("Pool has not been inited");
            return false;
        }

        if(pooledGameObjects.Count == 0)
        {
            int newCopies = copies / 2;
            copies += newCopies;
            HydratePool(this.namePrefix, newCopies);
        }

        // create new copies
        gameObject = pooledGameObjects.Dequeue();
        activeGameObjects.Add(gameObject);
        return true;
    }

    protected void RecycleObject(GameObject gameObject)
    {
        BumperCopsDebug.Log("Recycling object");
        if(gameObject == null)
        {
            BumperCopsDebug.LogWarning("game object to recycle is empty");
            return;
        }

        if(!activeGameObjects.Contains(gameObject))
        {
            BumperCopsDebug.LogWarning("Game object not found in the active pool");
            return;
        }

        activeGameObjects.Remove(gameObject);
        pooledGameObjects.Enqueue(gameObject);
    }

    protected abstract bool IsParentValid(GameObject objectToVerify);
    protected abstract bool IsActualPrefabValid(GameObject objectToVerify);
    protected abstract void InitParentOnCreation(GameObject objectToVerify);
    protected abstract void InitActualPrefabOnCreation(GameObject objectToVerify);
}