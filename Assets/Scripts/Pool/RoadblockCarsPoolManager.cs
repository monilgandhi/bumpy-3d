using UnityEngine;
using System.Collections.Generic;
public class RoadblockCarsPoolManager : PoolManager 
{
    public RoadblockCarsPoolManager(List<GameObject> prefabs, GameObject parentPrefab) : base("roadblock")
    {
        this.parentPrefab = parentPrefab;
        this.poolResources = prefabs;
        this.copies =15;
    }    

    protected override bool IsParentValid(GameObject objectToVerify)
    {
        return objectToVerify.GetComponent<ObstacleController>() != null;
    }

    protected override bool IsActualPrefabValid(GameObject objectToVerify)
    {
        return objectToVerify.GetComponent<Character>() != null;
    }

    protected override void InitParentOnCreation(GameObject obj)
    {
        ObstacleController controller = obj.GetComponent<ObstacleController>();
        if(controller == null)
        {
            return;
        }
        
        if(controller != null)
        {
            controller.ObstacleDeactivated += OnObstacleDeactivated;
        }
    }

    protected override void InitActualPrefabOnCreation(GameObject obj)
    {
        return;
    }

    private void OnObstacleDeactivated(ObstacleController o)
    {
        if(o == null)
        {
            BumperCopsDebug.LogWarning("controller deactivated is null");
            return;
        }

        base.RecycleObject(o.gameObject);
    }
}