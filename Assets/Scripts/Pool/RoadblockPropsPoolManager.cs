using System;
using UnityEngine;
using System.Collections.Generic;
public class RoadblockPropsPoolManager : PoolManager 
{
    public RoadblockPropsPoolManager(List<GameObject> prefabs) : base("roadblock-props")
    {
        this.poolResources = prefabs;
        this.copies =15;
    }    

    protected override bool IsParentValid(GameObject objectToVerify)
    {
        return true;
    }

    protected override bool IsActualPrefabValid(GameObject objectToVerify)
    {
        return objectToVerify.GetComponent<RoadblockPropsBehaviour>();
    }

    protected override void InitParentOnCreation(GameObject obj)
    {
        return;
    }

    protected override void InitActualPrefabOnCreation(GameObject obj)
    {
        RoadblockPropsBehaviour b = obj.GetComponent<RoadblockPropsBehaviour>();
        b.RoadblockPropDeactivated += OnObjectDeactivated;
    }

    private void OnObjectDeactivated(object o, EventArgs args)
    {
        RoadblockPropsBehaviour go = o as RoadblockPropsBehaviour;
        if(go == null)
        {
            BumperCopsDebug.LogWarning("controller deactivated is null");
            return;
        }

        base.RecycleObject(go.gameObject);
    }
}