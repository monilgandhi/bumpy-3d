using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class LeaderboardBehaviour : MonoBehaviour 
{
    [SerializeField]
    private GameObject scorePrefab;

    [SerializeField]
    private Transform scrollContent;

    [SerializeField]
    private ScoreBehaviour scoreBehaviour;

    [SerializeField]
    private Animator loadingAnimation;

    [SerializeField]
    private ConfirmationDialogueBehaviour confirmationDialogue;
    private List<LeaderboardScoreSectionBehaviour> scoreSection;
    private long playerScore;
    private bool loadingSocialScores;
    private void Awake() 
    {
        scorePrefab.SetActive(false);
        scoreSection = new List<LeaderboardScoreSectionBehaviour>();
        for(int i = 0; i < 10; i++)
        {
            GameObject g = Instantiate(scorePrefab, scrollContent);
            scoreSection.Add(g.GetComponent<LeaderboardScoreSectionBehaviour>());
        }    

        this.scoreBehaviour.ScoreFinalized += (args) =>
        {
            playerScore = args.IsHighScore ? args.NewScore : PlayerStats.Instance.CurrentHighScore;
            PrefetchLeaderboard();
        };

        this.loadingAnimation.gameObject.SetActive(false);
        confirmationDialogue.OkayButtonClicked += StartAuthentication;
        confirmationDialogue.CancelButtonClicked += CancelAuthentication;
    }

    private void StartAuthentication()
    {
        // log to social stats that confirm dialogue has been accepted
        SocialStats.Instance.RecordPermission(true);
        PrefetchLeaderboard();
    }

    private void CancelAuthentication()
    {
        // log to social stats that confirm dialogue has been rejected
        SocialStats.Instance.RecordPermission(false);
        // exit
    }

    private void PrefetchLeaderboard()
    {
        SocialStats.Instance.LeaderboardScoresLoaded += OnSocialScoresLoaded;

        // TODO if score is 0, get the top leaderboard here
        BumperCopsDebug.Log("Prefetching leaderboard score");

        if(SocialStats.Instance.ConfirmationDialogueRequired())
        {
            confirmationDialogue.Show();
        }
    
        loadingSocialScores = true;

        // prefetch anyway to get static scores
        SocialStats.Instance.PreFetchLeaderboard(10, playerScore);
    }

    private void OnSocialScoresLoaded()
    {
        BumperCopsDebug.Log("Scores loaded");
        loadingSocialScores = false;
        SocialStats.Instance.LeaderboardScoresLoaded -= OnSocialScoresLoaded;
    }

    public void OnLeaderboardOpened()
    {
        this.gameObject.SetActive(true);
        BumperCopsDebug.Log("Leaderboard opened");
        if(loadingSocialScores)
        {
            ShowLoadingAnimation();
        }

        StartCoroutine(DisplayScores());
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    private void ShowLoadingAnimation()
    {
        this.loadingAnimation.gameObject.SetActive(true);
    }

    private void OnSignInComplete(SocialPlatformEventArgs e)
    {
        if(e.Authenticated)
        {
            PrefetchLeaderboard();
            ShowLoadingAnimation();
            SocialStats.Instance.AuthenticationComplete -= OnSignInComplete;
        }

        StartCoroutine(DisplayScores());
        //BumperCopsDebug.Log("signin successful");
    }

    private IEnumerator DisplayScores()
    {   
        BumperCopsDebug.Log("Waiting for scores to load");
        yield return new WaitWhile(() => loadingSocialScores);
        this.loadingAnimation.gameObject.SetActive(false);

        BumperCopsDebug.Log($"Loading scores now of length {SocialStats.Instance.UserCentricScores.Count}");
        int idx = 1;
        bool userFound = false;
        Color backgroundColor = Constants.USER_RANK_NOT_ACHIEVED_COLOR;
        foreach(LeaderboardScore s in SocialStats.Instance.UserCentricScores)
        {
            if(s.Score <= 0 && !s.IsUser)
            {
                continue;
            }

            BumperCopsDebug.Log($"Updating score for user {s.UserId}");
            LeaderboardScoreSectionBehaviour ss = scoreSection[idx - 1];
            BumperCopsDebug.Log($"Is user {s.IsUser} with name {s.Name} and id {s.UserId}");
            Sprite rankSprite = null;

            if(userFound)
            {
                backgroundColor = Constants.USER_RANK_ACHIEVED_COLOR;
            }

            if(!s.IsUser)
            {
                rankSprite = SocialStats.GetProfileImage(s.Name);
            }
            else
            {
                backgroundColor = Constants.LEADERBOARD_USER_COLOR;
                userFound = true;
            }

            ss.UpdateScore(idx, s.Name, s.Score, s.IsUser, rankSprite, backgroundColor);       
            ss.gameObject.SetActive(true);
            ++idx;
        }
    }
}