using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardScoreSectionBehaviour : MonoBehaviour 
{    
    [SerializeField]
    private TMP_Text numberTxt;

    [SerializeField]
    private TMP_Text nameTxt;

    [SerializeField]
    private TMP_Text scoreTxt;

    [SerializeField]
    private Image profileImage;

    [SerializeField]
    private Image backgroundColorImage;
    private static Color PlayerColor = new Color(0, 0.8f, 1, 1);

    public void UpdateScore(int number, string name, long score, bool isUser, Sprite profileSprite, Color backgroundColor)
    {
        numberTxt.text = number.ToString();
        nameTxt.text = name;
        scoreTxt.text = score.ToString("n0");
        backgroundColorImage.color = backgroundColor;

        if(profileSprite != null)
        {
            profileImage.enabled = true;
            profileImage.sprite = profileSprite;
        }
        else
        {
            BumperCopsDebug.Log("Profile picture is null");
            profileImage.enabled = false;
        }
        
        if(isUser)
        {
            this.GetComponent<Image>().color = PlayerColor;
        }
    }   
}