using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectibleStats : MonoBehaviour
{
    [SerializeField]
    private Image collectibleImage;

    [SerializeField]
    private TMP_Text collectibleValue;

    [SerializeField]
    private Image mainCollectiblePanel;

    public bool UseCollectible(CollectibleType type, bool displayInventory = false)
    {
        if(!PlayerStats.Instance.CollectibleInventory.UseCollectible(type))
        {
            return false;
        }
        
        if(displayInventory)
        {
            this.gameObject.SetActive(true);
            StartCoroutine(DisplayInventory(type, CollectibleInventory.Instance.GetCollectibleAmountInventory(type)));
        }
        
        return true;
    }

    public void Display(CollectibleType type)
    {
        collectibleImage.sprite = CarUtils.GetCollectibleSprite(type);
        collectibleValue.text = CollectibleInventory.Instance.GetCollectibleAmountInventory(type).ToString();
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        mainCollectiblePanel.gameObject.SetActive(false);
    }

    private IEnumerator DisplayInventory(CollectibleType type, int remainingValue)
    {
        collectibleImage.sprite = CarUtils.GetCollectibleSprite(type);
        collectibleValue.text = $"X{remainingValue.ToString()}";

        mainCollectiblePanel.gameObject.SetActive(true);
        mainCollectiblePanel.color = new Color(mainCollectiblePanel.color.r, mainCollectiblePanel.color.g, mainCollectiblePanel.color.b, 0);
        StartCoroutine(CarUtils.ToggleImage(mainCollectiblePanel, 2));
        yield return new WaitForSeconds(4f);
        StartCoroutine(CarUtils.ToggleImage(mainCollectiblePanel, 2));

        mainCollectiblePanel.color = new Color(mainCollectiblePanel.color.r, mainCollectiblePanel.color.g, mainCollectiblePanel.color.b, 0);
        mainCollectiblePanel.gameObject.SetActive(false);
    }
}