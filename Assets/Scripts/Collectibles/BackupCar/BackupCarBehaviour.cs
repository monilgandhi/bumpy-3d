using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackupCarBehaviour : MonoBehaviour 
{
    public event Action<BackUpCarPowerupEventArgs> BackupSelected;

    [SerializeField]
    private CoinStatsNonGameplayBehaviour coinStats;
    private GameManager gameManager;

    public List<PlayerCharacter> BackupCars { get; private set; }
    public Dictionary<PlayerCharacter, SkillUpgrade[]> BackupCarsStats { get; private set; }
    private PlayerCharacter selectedCharacter;
    private void Awake() 
    {
        this.gameManager = this.GetComponentInParent<GameManager>();
        this.gameManager.GameInitialized += OnGameInitialized;
        this.gameManager.Player.PlayerCharacterChanged += OnNewPlayerCarSelected;
    }

    private void OnNewPlayerCarSelected()
    {
        if(BackupCars == null)
        {
            return;
        }

        SetupBackupCars();
    }

    private void SetupBackupCars()
    {
        BackupCarsStats = new Dictionary<PlayerCharacter, SkillUpgrade[]>();
        BackupCars = new List<PlayerCharacter>();

        if(ShopInventory.Instance.IsLoaded && ShopInventory.Instance.BackupCars.Count > 0)
        {
            BumperCopsDebug.Log("Loading backup cars");
            //BumperCopsDebug.Log($"Backup car count is {ShopInventory.Instance.BackupCars.Count}");
            foreach(string itemId in ShopInventory.Instance.BackupCars)
            {
                //BumperCopsDebug.Log($"BackupCarBehaviour: Loading car with item id {itemId}");
                this.gameManager.PrefabManager.LoadPrefabAsync(itemId, this.GetType(), Constants.GetPlayerPrefabPath(itemId));
            }
        }
    }

    private void OnGameInitialized() 
    {
        //BumperCopsDebug.Log("On Game init");
        this.gameManager.PrefabManager.PrefabLoadRequestComplete += OnCharacterLoadComplete;
        SetupBackupCars();
    }

    private void SetupBackupStats(PlayerCharacter backupCharacter)
    {
        PlayerCharacter playerCharacter = this.gameManager.Player.character;
        SkillUpgrade[] skillUpgrades = new SkillUpgrade[3];
        float attackStatsDiff = backupCharacter.AttackStats - playerCharacter.AttackStats;
        float boostStatsDiff = backupCharacter.BoostStats - playerCharacter.BoostStats;
        float movementStatsDiff = backupCharacter.SpeedStats - playerCharacter.SpeedStats;

        // /BumperCopsDebug.Log($"Car: {args.Character.Id}, attack diff: {args.Character.AttackStats.BasePoints}, boost diff: {args.Character.BoostStats.BasePoints}, movement diff: {args.Character.MovementStats.BasePoints}");
        skillUpgrades[(int)Skill.ATTACK] = new SkillUpgrade(attackStatsDiff);
        skillUpgrades[(int)Skill.BOOST] = new SkillUpgrade(boostStatsDiff);
        skillUpgrades[(int)Skill.MOVEMENT] = new SkillUpgrade(movementStatsDiff);

        if(!BackupCarsStats.ContainsKey(backupCharacter))
        {
            BackupCarsStats.Add(backupCharacter, skillUpgrades);
        }
        else
        {
            BackupCarsStats[backupCharacter] =  skillUpgrades;
        }
    }

    private void OnCharacterLoadComplete(PrefabLoadEventArgs args)
    {
        if(args == null || !args.Success)
        {
            BumperCopsDebug.LogError($"Failed to load character {args.PrefabId}");
            return;
        }

        if(args.CallingClass != this.GetType())
        {
            return;
        }

        PlayerCharacter c = args.GObject.GetComponent<PlayerCharacter>();
        BumperCopsDebug.Log("Adding backup car " + c.Id);
        BackupCars.Add(c);
        SetupBackupStats(c);
    }

    public PlayerCharacter GetSecondaryCharacter()
    {
        return selectedCharacter;
    }

    public void OnSecondaryCharacterSelected(string characterId)
    {
        if(string.IsNullOrEmpty(characterId))
        {
            BumperCopsDebug.LogWarning("Character id is null");
            return;
        }

        foreach(PlayerCharacter c in BackupCarsStats.Keys)
        {
            if(c.Id.Equals(characterId))
            {
                selectedCharacter = c;
                selectedCharacter.ChangeLayerOfMesh(Constants.PLAYER_LAYER);
                break;
            }
        }

        if(BackupSelected != null)
        {
            BackupSelected(new BackUpCarPowerupEventArgs(selectedCharacter));
        }
        /*if(selectedCharacter != null)
        {
            // apply the override stats
            SkillUpgrade[] overrideSkillUpgrades = BackupCarsStats[selectedCharacter];
            for(int i = 0; i < overrideSkillUpgrades.Length; i++)
            {
                BumperCopsDebug.Log($"Applying skill for {(Skill)i} with upgrade value {overrideSkillUpgrades[i].Increment}" );
                selectedCharacter.OverrideSkillStats((Skill)i, overrideSkillUpgrades[i].Increment);
            }

            // deduct money
            if(BackupSelected != null)
            {
                BackupSelected(new BackUpCarPowerupEventArgs(selectedCharacter));
            }
        }*/
    }
}