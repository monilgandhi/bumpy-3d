using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackupSelectionBehaviour : MonoBehaviour 
{   
    [SerializeField]
    private BackupCarBehaviour backupCarBehaviour;

    [SerializeField]
    private List<BackupSectionBehaviour> backupSectionBehaviours;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private CoinStatsNonGameplayBehaviour coinStats;

    [SerializeField]
    private CollectibleStats collectibleStats;


    [SerializeField]
    private InGameShopManager shopManager;

    private HashSet<string> characterIds = new HashSet<string>();
    private BackupSectionBehaviour selectedCarSection;
    private void Awake() 
    {
        foreach(BackupSectionBehaviour b in backupSectionBehaviours)
        {
            b.BuyButton.onClick.AddListener(delegate { OnSecondaryCharacterSelected(b); });
        } 
    }

    public void OnEnable()
    {
        characterIds = new HashSet<string>();
        List<PlayerCharacter> backupCars = backupCarBehaviour.BackupCars;
        if(backupCars == null || backupCars.Count == 0)
        {
            BumperCopsDebug.Log("No backup cars");
            return;
        }

        if(backupCars.Count != backupSectionBehaviours.Count)
        {
            BumperCopsDebug.LogWarning($"Count mismatch between number of cars {backupCars.Count} and slots for backup {backupSectionBehaviours.Count}");
        }

        for(int i = 0; i < backupSectionBehaviours.Count; i++)
        {
            if(backupCars.Count <= i)
            {
                backupSectionBehaviours[i].gameObject.SetActive(false);
                continue;
            }

            PlayerCharacter currentCharacter = backupCars[i];
            BumperCopsDebug.Log($"Current character is {currentCharacter.Id} and player is {this.player.character.Id}");
            int cost = i * 200;
            backupSectionBehaviours[i].Init(currentCharacter, backupCarBehaviour.BackupCarsStats[currentCharacter], i * 200, cost <= PlayerStats.Instance.CoinCount);

            characterIds.Add(currentCharacter.Id);
        }

        DisplayStats();
    }

    private void DisplayStats()
    {
        coinStats.Display();
        //collectibleStats.Display(CollectibleType.BACKUP);
    }

    private void OnSecondaryCharacterSelected(BackupSectionBehaviour backup, bool isDefault = false)
    {   
        //BumperCopsDebug.Log($"Backup selected with id {b.RepresentedItemId}");
        if(backup == null)
        {
            BumperCopsDebug.LogError("Backup section is null");
            return;
        }

        if(backup.Cost > 0 && !shopManager.OnBackupSelected(backup.RepresentedItemId, backup.Cost))
        {
            BumperCopsDebug.LogError("User has less coins than cost of backup.");
        }

        if(selectedCarSection != null)
        {
            selectedCarSection.ToggleSelection(false);
        }

        selectedCarSection = backup;
        backup.OnBackupBought();
        // give the user the backup regardless

        if(!isDefault)
        {
            backupCarBehaviour.OnSecondaryCharacterSelected(selectedCarSection.RepresentedItemId);
        }
    }

    public IEnumerator ClosePanel()
    {
        yield  return new WaitForSeconds(0.5f);
        this.GetComponent<Animator>().SetTrigger("hide");
        yield  return new WaitForSeconds(0.5f);
        this.gameObject.SetActive(false);
    }
}