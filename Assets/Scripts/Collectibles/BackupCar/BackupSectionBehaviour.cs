using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class BackupSectionBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text attackStats;

    [SerializeField]
    private TMP_Text boostStats;

    [SerializeField]
    private TMP_Text level;

    [SerializeField]
    private Image screenShot;

    [SerializeField]
    private TMP_Text price;

    [SerializeField]
    private Image coinIcon;
    
    [SerializeField]
    private Image selectedImage;

    [SerializeField]
    private Button buyButton;
    public Button BuyButton { get { return buyButton; }}

    public string RepresentedItemId { get; private set; }

    private int cost;
    public int Cost 
    {
         get { return cost; }
         set
         {
             cost = value;
            if(value == 0)
            {
                price.text = "Free";
                coinIcon.enabled = false;
            }
            else
            {
                price.text = cost.ToString();
                coinIcon.enabled = true;
            }
         }
    }

    public void OnBackupBought()
    {
        this.Cost = 0;
        ToggleSelection(true);
    }

     public void ToggleSelection(bool selected)
    {
        if(selected)
        {
            this.selectedImage.enabled = true;
            this.buyButton.gameObject.SetActive(false);
        }
        else
        {
            this.buyButton.gameObject.SetActive(true);
            this.selectedImage.enabled = false;
        }
    }

    public void Init(PlayerCharacter backupCarCharacter, SkillUpgrade[] skillUpgrades, int cost, bool isEnabled)
    {
        this.Cost = cost;
        RepresentedItemId = backupCarCharacter.Id;
        // attack stats

        BumperCopsDebug.Log($" section Car: {backupCarCharacter.Id}, attack diff: {skillUpgrades[(int)Skill.ATTACK].Increment}, boost diff: {skillUpgrades[(int)Skill.BOOST].Increment}, movement diff: {skillUpgrades[(int)Skill.MOVEMENT].Increment}");
        SetText(skillUpgrades[(int)Skill.ATTACK].Increment, attackStats);
        SetText(skillUpgrades[(int)Skill.BOOST].Increment, boostStats);

        if(this.Cost == 0)
        {
            price.text = "Free";
        }
        else
        {
            price.text = cost.ToString();
        }

        this.BuyButton.interactable = isEnabled;
        screenShot.sprite = backupCarCharacter.ScreenShot;
        this.gameObject.SetActive(true);
    }    

    private void SetText(float diff, TMP_Text textAsset)
    {
        if(diff > 0)
        {
            textAsset.color = Color.green;
            textAsset.text = $"+{diff}";
        }
        else if(diff < 0)
        {
            textAsset.color = Color.red;
            textAsset.text = $"{diff}";
        } else
        {
            textAsset.color = Color.white;
            textAsset.text = $"0";
        }
    }
}