public enum CollectibleType
{
    NONE,
    COIN,
    BAG_COINS,
    WEAPON_TOKEN,
    COMBO_INCREMENT,
    AIR_STRIKE,
    EXTRA_TIME,
    SCORE_MULTIPLIER
}