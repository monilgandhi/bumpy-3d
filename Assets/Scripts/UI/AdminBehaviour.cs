﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AdminBehaviour : MonoBehaviour
{
    [SerializeField]
    private AdminStatsBehaviour adminStatsBehaviour;

    [SerializeField]
    private bool activeForNonDevelopment = false;

    [SerializeField]
    private InputField assignmentInputText;

    // Use this for initialization
    void Start()
    {
        bool isActive = CarUtils.IsUnityDevelopmentBuild() || activeForNonDevelopment;
        this.gameObject.SetActive(isActive);
        adminStatsBehaviour.gameObject.SetActive(isActive);
        assignmentInputText.onEndEdit.AddListener(UpdateAssignmentName);
    }

    public void GiveCoins()
    {
        PlayerStats.Instance.IncreaseCoins(1000);
    }

    public void SkipTutorial()
    {
        PlayerStats.Instance.MarkTutorialCompleted();
    }

    public void UpdateAssignmentName(string nextAssignment)
    {
        BumperCopsDebug.Log($"next assignment is {nextAssignment}");
        if(string.IsNullOrEmpty(nextAssignment))
        {
            return;
        }

        //BumperCopsDebug.Log($"Next assignment id is {nextAssignment}");
        MissionStats.Instance.UpdateAssignmentIdForCurrentCity(nextAssignment);

        // reset all the missions
        MissionStats.Instance.ResetAllObjectives(nextAssignment);

    }
}
