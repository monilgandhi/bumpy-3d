using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationCompletedBehaviour : MonoBehaviour 
{
    public event Action<AnimationCompleteEventArgs> AnimationOver;
    private Animator animator;
    public Animator Animator { get { return animator; }}
    public bool IsAnimationComplete { get; private set; }

    private bool hideOnCompletion;
    private string Trigger;
    

    private void Awake() 
    {
        hideOnCompletion = false;
        animator = this.GetComponent<Animator>();   
        IsAnimationComplete = true; 
    }

    public void StartAnimationWithTrigger(string triggerName, 
        bool hideOnCompletion, 
        bool restartIfRunning)
    {
        if(string.IsNullOrEmpty(triggerName))
        {
            BumperCopsDebug.LogWarning("Trigger name is empty");
            return;
        }

        this.hideOnCompletion = hideOnCompletion;
        IsAnimationComplete = false;

        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
        
        if(!string.IsNullOrEmpty(Trigger) && 
            Trigger.Equals(triggerName) && 
            restartIfRunning)
        {
            animator.Play(triggerName, -1, 0f);
            return;
        }

        Trigger = triggerName;

        //BumperCopsDebug.Log($"Setting the trigger to {triggerName}");
        this.animator.SetTrigger(triggerName);
    }

    public void StartAnimationWithBool(string boolKey, bool value)
    {
        if(string.IsNullOrEmpty(boolKey))
        {
            BumperCopsDebug.LogWarning("Trigger name is empty");
            return;
        }

        IsAnimationComplete = false;
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }

        Trigger = boolKey;

        animator.SetBool(boolKey, value);
    }

    public void StartAnimationWithInteger(string key, int value)
    {
        if(string.IsNullOrEmpty(key))
        {
            BumperCopsDebug.LogWarning("Trigger name is empty");
            return;
        }

        IsAnimationComplete = false;
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }

        Trigger = key;
        animator.SetInteger(key, value);
    }

    public void AnimationComplete()
    {
        IsAnimationComplete = true;
        AnimationOver?.Invoke(new AnimationCompleteEventArgs(true, Trigger));
        Trigger = null;
        if(this.hideOnCompletion)
        {
            this.gameObject.SetActive(false);
        }
    }
}

public class AnimationCompleteEventArgs
{
    public bool IsAnimationComplete;
    public string Key;

    public AnimationCompleteEventArgs(bool isAnimationComplete, string trigger)
    {
        this.IsAnimationComplete = isAnimationComplete;
        this.Key = trigger;
    }
}
