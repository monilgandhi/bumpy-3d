using System.Collections;
using UnityEngine;
using TMPro;
using BumperCops.Models;
public class CoinStatsNonGameplayBehaviour : MonoBehaviour 
{
    [Header("Required")]
    [SerializeField]
    private GameManager gameManager;

    [Header("Optional")]
    [SerializeField]
    private ScoreAndRewardTransitionBehaviour scoreAndReward;

    [SerializeField]
    private TMP_Text coinStats;

    private long currentAmount;
    private long CurrentAmount
    {
        get { return currentAmount; }
        set
        {
            currentAmount = value;
            coinStats.text = currentAmount.ToString();
        }
    }

    private Animator animator;
    private void Start() 
    {
        animator = this.GetComponent<Animator>();
        gameManager.InGameShopManager.CoinsSpent += OnCoinsSpent;
        gameManager.InGameShopManager.InAppPurchasBehaviour.IAP += OnCoinsPurchased;
        
        if(scoreAndReward != null)
        {
            scoreAndReward.RewardGiven += OnRewardGiven;
        }
    }

    private void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Display()
    {       
        CurrentAmount = PlayerStats.Instance.CoinCount;
        this.gameObject.SetActive(true);
    }

    public void OnRewardGiven(RewardEventArgs args)
    {
        if(args.Rewards.Count == 0)
        {
            // something is wrong
            BumperCopsDebug.LogWarning("no rewards given");
            return;
        }

        uint coinsToAdd = 0;
        foreach(Reward r in args.Rewards)
        {
            if(r.Type == CollectibleType.COIN || r.Type == CollectibleType.BAG_COINS)
            {
                coinsToAdd += r.Amount;
            }
        }

        if(!this.gameObject.activeInHierarchy)
        {
            return;
        }

        StartCoroutine(ChangeCoinAmount(coinsToAdd));
    }

    private void OnEnable() 
    {
        Display();    
    }

    private void OnCoinsPurchased(object o, IAPEventArgs args)
    {
        long amountToAdd = PlayerStats.Instance.CoinCount - this.CurrentAmount;
        if(args == null || args.Product == null || args.Product.Definition == null || args.Product.Definition.payout == null)
        {
            BumperCopsDebug.LogWarning("product metadata is empty. Defaulting to player stats");
        }
        else
        {
            amountToAdd = (long)args.Product.Definition.payout.quantity;
        }

        if(amountToAdd == 0)
        {
            // something is wrong
            BumperCopsDebug.LogWarning("Amount to add is 0");
            CurrentAmount = PlayerStats.Instance.CoinCount;
        }

        if(!this.gameObject.activeInHierarchy)
        {
            return;
        }

        StartCoroutine(ChangeCoinAmount(amountToAdd));
    }

    private void OnCoinsSpent(CoinSpentEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Args null CoinsSpent");
            return;
        }

        if(args.AmountSpent <= 0 || !this.gameObject.activeInHierarchy)
        {
            return;
        }

        StartCoroutine(ChangeCoinAmount(args.AmountSpent * -1));
    }

    private IEnumerator ChangeCoinAmount(long delta)
    {

        long newAmount = this.CurrentAmount + delta;
        BumperCopsDebug.Log($"New amount is {newAmount}");
        
        if(newAmount < 0)
        {
            newAmount = 0;
        }

        long changeToApply = delta / 10;
        //BumperCopsDebug.Log($"Reducing coins new {newAmount} old {CurrentAmount} and delta {delta}");
        animator.SetTrigger("pulsate");
        while((CurrentAmount > newAmount && delta < 0) || (CurrentAmount < newAmount && delta > 0))
        {
            //BumperCopsDebug.Log($"Change in vlaue {changeInValue} with deduction {deduction} and current value {CurrentAmount}");
            CurrentAmount += changeToApply;
            yield return new WaitForEndOfFrame();
        }

        CurrentAmount = newAmount;
        animator.SetTrigger("stop");
    }
}