using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TabBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Button clickButton;

    [SerializeField]
    private RectTransform content;
    public RectTransform Content { get { return content; }}

    public Button SelectButton { get { return clickButton; }}

    private static Color unselectedImagecolor = Color.gray;
    private static Color selectedImagecolor = new Color(0.17f, 0.43f, 0.8f, 1);

    public void Select(int newIndex)
    {
        this.transform.SetSiblingIndex(newIndex);
    }

    public void Deselect(int newIndex)
    {
        this.transform.SetSiblingIndex(newIndex);
    }
}