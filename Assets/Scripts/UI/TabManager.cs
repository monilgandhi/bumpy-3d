using System;
using System.Collections.Generic;
using UnityEngine;

public class TabManager : MonoBehaviour 
{
    public event Action<TabBehaviour> ActivateTab;
    [SerializeField]
    private List<TabBehaviour> tabs;

    private TabBehaviour currentSelectedTab;

    private void Awake() 
    {
        BumperCopsDebug.Log("Awake");
        foreach(TabBehaviour t in tabs)
        {
            t.SelectButton.onClick.AddListener(delegate { SelectTab(t); });
        }
    }
    
    private void SelectTab(TabBehaviour selectedTab)
    {
        BumperCopsDebug.Log($"Tab selected {selectedTab.name}");
        if(currentSelectedTab != selectedTab && currentSelectedTab != null)
        {
            currentSelectedTab.Deselect(0);
        }
        
        currentSelectedTab = selectedTab;
        selectedTab.Select(tabs.Count - 1);
        ActivateTab?.Invoke(selectedTab);
    }

    public void DisplayTabs() 
    {
        if(currentSelectedTab == null)
        {
            SelectTab(tabs[0]);
        }
    }
}