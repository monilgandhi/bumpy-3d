using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ConfirmationDialogueBehaviour : MonoBehaviour
{
    [SerializeField]
    private Button okayButton;

    [SerializeField]
    private Button cancelButton;

    [SerializeField]
    private bool hideParent;

    public event Action OkayButtonClicked;
    public event Action CancelButtonClicked;
    
    private GameObject gameObjectToHide;
    private Animator animator;
    private bool hideOnAnimationComplete;
    private void Awake() 
    {
        okayButton.onClick.AddListener(OnOkay);
        cancelButton.onClick.AddListener(OnCancel);

        gameObjectToHide = this.gameObject;
        if(hideParent)
        {
            gameObjectToHide = this.transform.parent.gameObject;
        }

        gameObjectToHide.SetActive(false);    
        animator = this.GetComponent<Animator>();
        hideOnAnimationComplete = false;
    }
    private void OnOkay()
    {
        OkayButtonClicked?.Invoke();
        animator.SetTrigger("hide");
        hideOnAnimationComplete = true;
    }   

    private void OnCancel()
    {
        CancelButtonClicked?.Invoke();
        animator.SetTrigger("hide");
        hideOnAnimationComplete = true;
    } 

    public void Show()
    {
        gameObjectToHide.SetActive(true);
    }

    public void AnimationComplete()
    {
        if(hideOnAnimationComplete)
        {
            gameObjectToHide.SetActive(false);
            hideOnAnimationComplete = false;
        }
        
        return;
    }
}