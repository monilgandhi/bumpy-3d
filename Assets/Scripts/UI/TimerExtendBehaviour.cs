using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Animator))]
public class TimerExtendBehaviour : MonoBehaviour 
{
    public event Action<TimeExtensionArgs> TimeExtended;
    public event Action<TimeExtensionArgs> TimeExtendedDialogueDisplayed;

    [Header("UI elements")]
    [SerializeField]
    private GameObject buttonPrefab;

    [SerializeField]
    private GameObject adsButtonPrefab;

    [SerializeField]
    [Tooltip("Mid panel where the buttons will be placed")]
    private GameObject midPanel;

    [SerializeField]
    [Tooltip("Description text field")]
    private TMP_Text descriptionTextField;

    [Header("Game elements")]
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private AdsManager adsManager;

    [SerializeField]
    private EnemyManager enemyManager;

    [Header("Admin")]
    [SerializeField]
    private bool showContinueDialogue = true;

    private static List<int> costOptions = new List<int>()
    {
        350
    };

    private static List<int> timeOptions = new List<int>()
    {
        30
    };

    private const int TUTORIAL_TIME_OPTION = 30;
    private int costMultiplier;
    private int dialogueCountThisSession;
    private List<Button> buttonList;
    private Button adsButton;
    private AnimationCompletedBehaviour animationCompletedBehaviour;
    private bool isTutorial;
    private EnemyController enemy;
    private const int MAX_EXTENSION = 3;
    private void Awake() 
    {
        costMultiplier = 1;
        dialogueCountThisSession = 0;
        animationCompletedBehaviour = this.GetComponent<AnimationCompletedBehaviour>();
        this.gameManager.GamePreStart += OnGameStarted;
        if(!CarUtils.IsUnityDevelopmentBuild())
        {
            showContinueDialogue = true;
        }
    }

    private void OnGameStarted()
    {
        if(buttonList != null && buttonList.Count > 0)
        {
            return;
        }

        buttonList = new List<Button>();
        for(int i = 0; i < costOptions.Count; i++)
        {
            GameObject go = Instantiate(buttonPrefab, midPanel.transform);
            go.name = i.ToString();
            Button btn = go.GetComponent<Button>();
            btn.onClick.AddListener(delegate { OnButtonClicked(btn); });
            buttonList.Add(btn);
        }

        if(RemoteConfig.Instance.Config.TimeExtensionRewardedAds && 
            RemoteConfig.Instance.Config.AdsRunning)
        {
            BumperCopsDebug.Log("Creating ads button");
            GameObject go = Instantiate(adsButtonPrefab, midPanel.transform);
            go.name = "ads";
            Button adsBtn = go.GetComponent<Button>();
            adsBtn.onClick.AddListener(delegate { OnButtonClicked(adsBtn); });
            // remove the last button
            buttonList.Add(adsBtn);
        }

        BumperCopsDebug.Log($"button list count {buttonList.Count}");
    }

    private void Start() 
    {
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        this.gameManager.TutorialManager.TutorialStarted += (a) => isTutorial = true;
        this.gameManager.TutorialManager.TutorialEnd += (a) => isTutorial = false;

        Hide(true);    
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        this.enemy = args.enemy;
    }

    private bool ShowForTutorial()
    {
        //++costMultiplier;
        // setup the buttons
        Button btn = buttonList[0];
        btn.interactable = true;
        btn.gameObject.SetActive(true);
        SetBtnPrice(30, btn, 0);

        this.gameObject.SetActive(true);
        TimeExtendedDialogueDisplayed?.Invoke(new TimeExtensionArgs());

        descriptionTextField.text = GetDescription();
        return true;
    }

    public bool ShowIfPossible()
    {
        if(isTutorial)
        {
            BumperCopsDebug.Log("TimerExtendBehaviour: Showing dialog for tutorial");
            return ShowForTutorial();
        }

        if(!showContinueDialogue)
        {
            return false;
        }

        showContinueDialogue = (dialogueCountThisSession++) < MAX_EXTENSION;
        BumperCopsDebug.Log($"Showing continue dialogu {showContinueDialogue}");
        //BumperCopsDebug.LogFormat($"TimerExtendBehaviour: Mutliplier is {costMultiplier}");
        int activeButtons = 0;
        
        // setup the buttons
        for(int i = 0; i < buttonList.Count; i++)
        {
            Button btn = buttonList[i];
            
            AdsButtonBehaviour adsButtonBehaviour = btn.GetComponent<AdsButtonBehaviour>();
            if(adsButtonBehaviour != null && adsManager.CanShowAd(AdsPlacement.TIME_EXTENSION))
            {
                ++activeButtons;
                BumperCopsDebug.Log($"Ads button active with reward {adsManager.TimeExtensionReward}");
                adsButtonBehaviour.Init(adsManager.TimeExtensionReward);
            }
            else if(adsButtonBehaviour == null && i < costOptions.Count)
            {
                int cost = costOptions[i]; //* costMultiplier;
                btn.interactable = PlayerStats.Instance.CoinCount >= cost;
                if(btn.interactable)
                {
                    ++activeButtons;
                }
                
                btn.gameObject.SetActive(true);
                SetBtnPrice(timeOptions[i], btn, cost);
            }
        }

        BumperCopsDebug.Log($"Active Buttons count: {activeButtons}");
        if(activeButtons <= 0)
        {
            this.gameObject.SetActive(false);
            return false;
        }

        this.gameObject.SetActive(true);
        TimeExtendedDialogueDisplayed?.Invoke(new TimeExtensionArgs());

        descriptionTextField.text = GetDescription();
        return true;
    }

    private string GetDescription()
    {
        if(this.enemy == null)
        {
            return "";
        }

        if(isTutorial)
        {
            return $"Do no hit civilian cars to avoid time penalty. Extend time for free this time.";
        }
        else
        {
            return $"Extend time to catch {this.enemy.EnemyCharacteristics.Name}";
        }
    }

    private void Hide(bool instant = false)
    {
        if(instant)
        {
            this.gameObject.SetActive(false);
            return;
        }

        animationCompletedBehaviour.StartAnimationWithTrigger("hide", false, false);
        StartCoroutine(HideAfterAnimationCompletes());
    }

    private IEnumerator HideAfterAnimationCompletes()
    {
        yield return new WaitWhile(() => !animationCompletedBehaviour.IsAnimationComplete);
        this.gameObject.SetActive(false);
    }

    public void OfferRejected()
    {
        Hide();
        TimeExtended?.Invoke(new TimeExtensionArgs());
    }

    private void OnButtonClicked(Button button)
    {
        if(button == null)
        {
            BumperCopsDebug.LogError("Button is null in continue. Exiting the game now");
            // TODO exit the game
        }

        int selectedCost = 0;
        int selectedTime = 0;
        int idx = -1;

        try
        {
            if(button.gameObject.GetComponent<AdsButtonBehaviour>() != null)
            {
                // ads
                adsManager.AdEvent += OnAdEventReceived;
                adsManager.ShowRewardedAd(showContinueDialogue, AdsPlacement.TIME_EXTENSION);
            }
            else
            {
                if(isTutorial)
                {
                    selectedTime = TUTORIAL_TIME_OPTION;
                }
                else 
                {
                    idx = Convert.ToInt32(button.gameObject.name);
                    selectedCost = costOptions[idx]; // * (costMultiplier++);
                    selectedTime = timeOptions[idx];
                }
                
                BumperCopsDebug.Log("Extending time for coins");
                TimeExtended?.Invoke(
                    new TimeExtensionArgs(selectedCost, true, selectedTime, false));
            }
        }
        catch(FormatException)
        {
            BumperCopsDebug.LogError($"Format exception for button with name {button.gameObject.name}");
            // TODO exit the game
        }

        Hide();
    }

    private void OnAdEventReceived(AdEventArgs args)
    {
        // this may be called twice on reward. If the user closes the ad it is counted as rrerward, which may trigger
        // twice
        if(!args.AdComplete)
        {
            return;
        } 

        BumperCopsDebug.Log("Extending time for ads");
        adsManager.AdEvent -= OnAdEventReceived;

        if(args.GiveReward)
        {
            BumperCopsDebug.Log($"reward amount is {args.RewardAmount}");
            TimeExtended?.Invoke(new TimeExtensionArgs(0, true, args.RewardAmount, true));
        }
        else
        {
            BumperCopsDebug.Log("Time cannot be extended for ads since ad closed");
            OfferRejected();
        }
    }

    private void SetBtnPrice(int timeOption, Button btn, int costOption)
    {
        Transform costPanel = btn.transform.Find("costPanel");

        if(costPanel == null)
        {
            BumperCopsDebug.LogError("Cost panel not found");
            return;
        }

        TMP_Text textField = costPanel.GetComponentInChildren<TMP_Text>();

        if(textField == null)
        {
            BumperCopsDebug.LogError("Text field not found in cost button");
            return;
        }

        Image iconImg = costPanel.GetComponentInChildren<Image>();
        if(iconImg != null)
        {
            iconImg.enabled = !isTutorial;
        }

        if(isTutorial)
        {
            textField.text = $"Get {timeOption}s for free";
            return;
        }

        textField.text = $"{timeOption}s for {costOption}";
    }
}