using UnityEngine;
using TMPro;
public class AdminStatsBehaviour : MonoBehaviour {

    [SerializeField]
    private ObstacleManager obstacleManager;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private GameTime gameTimeManager;

    [Header("Panel elements")]
    [SerializeField]
    private TMP_Text playerSpeed;
    [SerializeField]
    private TMP_Text playerBoostDistance;

    [SerializeField]
    private TMP_Text enemySpeed;

    [SerializeField]
    private TMP_Text enemyHealth;

    [SerializeField]
    private TMP_Text gameSpeedText;

    [SerializeField]
    private TMP_Text trafficGeneration;

    [SerializeField]
    private TMP_Text fps;

    [SerializeField]
    private TMP_Text bonusRequirement;

    [SerializeField]
    private TMP_Text gameLevel;

    [SerializeField]
    private TMP_Text gameTime;
    private EnemyController enemy;
    private float deltaTime = 0.0f;

    private float boostStartDistance;
    private void Start() 
    {
        this.gameManager.EnemyManager.EnemySpawned += (eArgs) =>
        {
            this.enemy = eArgs.enemy;
            this.enemyHealth.text = $"Health: {this.enemy.Health.Health.ToString()}";

            this.enemy.DamageController.EnemyDestructionBegin += () =>
            {
                this.enemy = null;
                enemySpeed.text = "";
                enemyHealth.text = "";
            };
        };

        this.gameManager.Player.SpeedController.PlayerBoostUpdate += OnBoostUpdate;
    }

    private void OnBoostUpdate(BoostUpdateEventArgs args)
    {
        if(args.IsTurnedOn)
        {
            boostStartDistance = this.gameManager.Player.transform.position.z;
        }
        else
        {
            this.playerBoostDistance.text = Mathf.RoundToInt(this.gameManager.Player.transform.position.z - boostStartDistance).ToString();
        }
    }

    private void LateUpdate() 
    {
        this.playerSpeed.text = Mathf.RoundToInt(this.gameManager.Player.RigidBody.velocity.magnitude).ToString();
        this.trafficGeneration.text = this.obstacleManager.TrafficGenerationTime.ToString();
        this.gameSpeedText.text = this.gameManager.GameLevelManager.GameSpeed.ToString();
        this.gameLevel.text = this.gameManager.GameLevelManager.CurrentRecommendedLevel.ToString();
        this.gameTime.text = (this.gameTimeManager.PlaySessionTime/60).ToString("n1");

        if(enemy != null)
        {
            this.enemySpeed.text = $"Speed: {Mathf.RoundToInt(enemy.RigidBody.velocity.magnitude).ToString()}";
        }

        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        this.fps.text = Mathf.RoundToInt(1.0f / deltaTime).ToString();
    }
}