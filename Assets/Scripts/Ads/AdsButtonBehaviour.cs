using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class AdsButtonBehaviour : MonoBehaviour 
{
    [SerializeField]
    private bool setUpText = true;
    private int rewardAmount;
    private Button button;
    private TMP_Text buttonText;
    private void Awake() 
    {
        SetupButton();
    }

    public void Init(int reward)
    {
        if(this.button == null)
        {
            SetupButton();
        }
        
        this.rewardAmount = reward;
        this.button.interactable = true;

        if(setUpText)
        {
            SetBtnPrice();
        }
    
        this.gameObject.SetActive(true);
    }    

    private void SetupButton()
    {
        if(this.button != null)
        {
            return;
        }

        BumperCopsDebug.Log("Setting up button");
        this.button = this.GetComponent<Button>() ?? throw new MissingComponentException(nameof(button));
        BumperCopsDebug.Log($"button name is {button.name}");
        this.buttonText = button.GetComponentInChildren<TMP_Text>();
        this.gameObject.SetActive(false);
    }

    private void SetBtnPrice()
    {
        this.buttonText.text = $"Watch to get {this.rewardAmount}s";
    }
}