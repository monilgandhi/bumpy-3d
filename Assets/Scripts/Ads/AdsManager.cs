using System;
using System.Collections;
using UnityEngine;

public class AdsManager : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;

    [Header("Admin")]
    [SerializeField]
    private bool loadVideoAds;

    [SerializeField]
    private bool loadTimeExtensionAds = true;

    [SerializeField]
    private bool loadRewardDialogueAd;

    public event Action<AdEventArgs> AdEvent;
    private IAdNetwork adNetwork;

    private bool timeExtensionCloseReceived;
    private bool giveReward;
    private int timeExtensionReward;
    public int TimeExtensionReward 
    {
        get 
        {
            if(adNetwork == null || !adNetwork.CanShowAd(AdsPlacement.TIME_EXTENSION))
            {
                return timeExtensionReward;
            }

            if(timeExtensionReward <= 0)
            {
                timeExtensionReward = adNetwork.GetReward(AdsPlacement.TIME_EXTENSION);
            }
            
            return timeExtensionReward;
        }
    }

    private bool loadNextAd;
    private AdEventType lastAdEventType;
    private AdsPlacement currentAdsPlacement;
    private void Awake() 
    {
        gameManager.GameStarted += OnGameStarted;
        gameManager.GameOver += OnGameOver;
    }

    private void OnGameStarted(GameEventArgs args)
    {
        #if UNITY_EDITOR
        if(loadTimeExtensionAds)
        {
            LoadRewardedAd(AdsPlacement.TIME_EXTENSION);
            return;
        }
        #endif

        LoadRewardedAd(AdsPlacement.TIME_EXTENSION);
    }

    private void OnGameOver(GameOverReason reason)
    {
        #if UNITY_EDITOR
        if(loadRewardDialogueAd)
        {
            LoadRewardedAd(AdsPlacement.REWARD_DIALOGUE_AD);
            return;
        }
        #endif

        // if rewards are present only then
        LoadRewardedAd(AdsPlacement.REWARD_DIALOGUE_AD);
    }

    private void Start() 
    {
        adNetwork = new AdMobAdNetworkImpl();
        adNetwork.AdEvent += OnAdEventReceived;
        adNetwork.Init();
        currentAdsPlacement = AdsPlacement.NONE;
    }    

    private void Update() 
    {
        if(!timeExtensionCloseReceived)
        {
            return;
        }    

        timeExtensionCloseReceived = false;
        StartCoroutine(ClosedAdDelay());
    }

    private void OnAdEventReceived(AdEventInternalArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogError("Args are null");
            return;
        }

        if(args.Type != AdEventType.LOAD_COMPLETE && args.AdsPlacement != currentAdsPlacement)
        {
            BumperCopsDebug.LogWarning($"Ad placement mismatch current {currentAdsPlacement}, args {args.AdsPlacement}");
            currentAdsPlacement = args.AdsPlacement;
        }

        switch(args.Type)
        {
            case AdEventType.AD_CLOSED:
                BumperCopsDebug.Log("Ad closed");
                lastAdEventType = AdEventType.AD_CLOSED;
                timeExtensionCloseReceived = true;
                break;

            case AdEventType.AD_FAILED:
                BumperCopsDebug.Log("Ad failed");
                lastAdEventType = AdEventType.AD_FAILED;
                timeExtensionCloseReceived = true;
                break;

            case AdEventType.AD_REWARD_EARNED:
                BumperCopsDebug.Log("Reward earned");
                lastAdEventType = AdEventType.AD_REWARD_EARNED;
                giveReward = true;
                break;    
        }
    }

    private IEnumerator ClosedAdDelay()
    {
        BumperCopsDebug.Log("Starting the closed and delay co-routine");
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        FinalizeClose();
    }

    private void FinalizeClose()
    {
        BumperCopsDebug.Log("Invoking ad event");
        AdEvent?.Invoke(
            new AdEventArgs(
                true, giveReward, timeExtensionReward, currentAdsPlacement, lastAdEventType
            ));
        
        giveReward = false;
        timeExtensionCloseReceived = false;
        timeExtensionReward = -1;
        // load the next ad
        if(loadNextAd)
        {
            LoadRewardedAd(currentAdsPlacement);
        }

        currentAdsPlacement = AdsPlacement.NONE;
    }

    public bool CanShowAd(AdsPlacement adPlacement)
    {
        if(adPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invlaid adsPlacement value");
            return false;
        }

        if(!adNetwork.CanShowAd(adPlacement))
        {
            LoadRewardedAd(adPlacement);
        }

        return adNetwork.CanShowAd(adPlacement);
    }

    public void ShowRewardedAd(bool loadNextAd, AdsPlacement adPlacement)
    {
        if(adPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invlaid adsPlacement value");
            return;
        }

        currentAdsPlacement = adPlacement;
        adNetwork.ShowAd(adPlacement);
        this.loadNextAd = loadNextAd;
    }

    private void LoadRewardedAd(AdsPlacement adPlacement)
    {
        BumperCopsDebug.Log("Loading time extension ad");
        adNetwork.LoadAd(adPlacement);
    }
}

public enum AdEventType
{
    LOAD_COMPLETE,
    AD_FAILED,
    AD_OPEN,
    AD_CLOSED,
    AD_REWARD_EARNED
}

class AdEventInternalArgs : EventArgs
{
    public readonly AdEventType Type;
    public readonly AdsPlacement AdsPlacement;

    public AdEventInternalArgs(AdEventType type, AdsPlacement adsPlacement)
    {
        this.Type = type;
        this.AdsPlacement = adsPlacement;
    }
}

public class AdEventArgs : EventArgs
{
    public readonly bool AdComplete;
    public readonly bool GiveReward;
    public readonly int RewardAmount;
    public readonly AdsPlacement AdsPlacement;
    public readonly AdEventType AdEventType;

    public AdEventArgs(bool adComplete, 
        bool giveReward, 
        int rewardAmount, 
        AdsPlacement placement, 
        AdEventType adEventType)
    {
        this.AdComplete = adComplete;
        this.GiveReward = giveReward;
        this.RewardAmount = rewardAmount;
        this.AdsPlacement = placement;
        this.AdEventType = adEventType;
    }
}

public enum AdsPlacement
{
    NONE,
    TIME_EXTENSION,
    TITLE_VIDEO_AD_REWARD,
    REWARD_DIALOGUE_AD
}