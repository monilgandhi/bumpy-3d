using System;
interface IAdNetwork
{
    void Init();
    void LoadAd(AdsPlacement adsplacement, bool personalized = false);
    void ShowAd(AdsPlacement adsPlacement);

    event Action<AdEventInternalArgs> AdEvent;
    int GetReward(AdsPlacement adsPlacement);
    bool CanShowAd(AdsPlacement adsPlacement);
}