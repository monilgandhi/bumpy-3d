using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;

class AdMobAdNetworkImpl : IAdNetwork
{
    public event Action<AdEventInternalArgs> AdEvent;
#if UNITY_ANDROID && UNITY_EDITOR
    private static string TIME_EXTENSION_REWARDED_ADUNIT_ID = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IOS && UNITY_EDITOR
    private static string TIME_EXTENSION_REWARDED_ADUNIT_ID = "ca-app-pub-3940256099942544/5224354917";
#else
    private static string TIME_EXTENSION_REWARDED_ADUNIT_ID;
#endif

    public bool InitComplete { get; private set; }

    private Dictionary<AdsPlacement, RewardedAd> adMap = new Dictionary<AdsPlacement, RewardedAd>();
    private Dictionary<AdsPlacement, string> adUnitMap = new Dictionary<AdsPlacement, string>()
    {
        { AdsPlacement.TIME_EXTENSION, TIME_EXTENSION_REWARDED_ADUNIT_ID },
        { AdsPlacement.TITLE_VIDEO_AD_REWARD, TIME_EXTENSION_REWARDED_ADUNIT_ID },
        { AdsPlacement.REWARD_DIALOGUE_AD, TIME_EXTENSION_REWARDED_ADUNIT_ID }
    };

    private AdRequest npaRequest;
    private AdRequest paRequest;

    private const int MAX_RETRY = 1;

    public void Init()
    {
        MobileAds.Initialize(
            initStatus => 
            {
                InitComplete = true;
            });

        npaRequest = new AdRequest.Builder()
            .AddExtra("npa", "0")
            .Build();

        paRequest = new AdRequest.Builder()
            .Build();

        BumperCopsDebug.Log("Filling the map");
#if UNITY_ANDROID && !UNITY_EDITOR

        adUnitMap[AdsPlacement.TIME_EXTENSION] = 
            RemoteConfig.Instance.Config.TimeExtensionRewardedAdUnitAndroid;

        adUnitMap[AdsPlacement.TITLE_VIDEO_AD_REWARD] = 
            RemoteConfig.Instance.Config.VideoRewardedAdUnitAndroid;

        adUnitMap[AdsPlacement.REWARD_DIALOGUE_AD] = 
            RemoteConfig.Instance.Config.RewardDialogRewardedAdUnitAndroid;
            
#elif UNITY_IOS && !UNITY_EDITOR
        adUnitMap[AdsPlacement.TIME_EXTENSION] = 
            RemoteConfig.Instance.Config.TimeExtensionRewardedAdUnitIOS;

        adUnitMap[AdsPlacement.TITLE_VIDEO_AD_REWARD] = 
            RemoteConfig.Instance.Config.VideoRewardedAdUnitIOS;

        adUnitMap[AdsPlacement.REWARD_DIALOGUE_AD] = 
            RemoteConfig.Instance.Config.RewardDialogRewardedAdUnitIOS;
#endif
    }

    public void LoadAd(AdsPlacement adsPlacement, bool personalized = true)
    {
        if(adsPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid adsplacement value");
            return;
        }

        RewardedAd ad = null;
        if(adMap.TryGetValue(adsPlacement, out ad) && ad != null && ad.IsLoaded())
        {
            return;
        }
        
        string adUnitId = null;
        if(!adUnitMap.TryGetValue(adsPlacement, out adUnitId))
        {
            BumperCopsDebug.LogWarning($"Ad placement not found in adunit map {adsPlacement}");
            return;
        }
        
        BumperCopsDebug.Log($"adunit is {adUnitId}");
        ad = new RewardedAd(adUnitId);
        ad.OnAdLoaded += OnAdLoaded;
        ad.OnAdFailedToLoad += OnAdFailed;
        ad.OnAdFailedToShow += OnAdFailed;
        ad.OnAdOpening += OnAdOpening;
        ad.OnAdClosed += OnAdClosed;
        ad.OnUserEarnedReward += OnAdrewardEarned;

        // remove previous ad
        RewardedAd previousAd; 
        if(adMap.TryGetValue(adsPlacement, out previousAd))
        {
            adMap.Remove(adsPlacement);
        }

        BumperCopsDebug.Log("Ads: Loading ad");
        if(personalized)
        {
            ad.LoadAd(paRequest);
        }
        else
        {
            ad.LoadAd(npaRequest);
        }
        
        adMap.Add(adsPlacement, ad);
        BumperCopsDebug.Log($"ad loaded {ad.IsLoaded()}");
    }

    public bool CanShowAd(AdsPlacement adPlacement)
    {
        if(adPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid adsplacement value");
            return false;
        }

        RewardedAd ad;

        BumperCopsDebug.Log($"Looking for ad with placecment {adPlacement}");

        return adMap.TryGetValue(adPlacement, out ad) && ad.IsLoaded();
    }

    public void ShowAd(AdsPlacement adPlacement)
    {
        if(adPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid adsplacement value");
            return;
        }

        RewardedAd ad;
        if(!adMap.TryGetValue(adPlacement, out ad) || !ad.IsLoaded())
        {
            BumperCopsDebug.Log("Ad is not loaded");
            OnAdFailed(this, null);
            return;
        }

        BumperCopsDebug.Log("Showing ad now");
        ad.Show();
    }

    public int GetReward(AdsPlacement adPlacement)
    {
        if(adPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid adsplacement value");
            return 0;
        }

        RewardedAd ad;
        if(!adMap.TryGetValue(adPlacement, out ad) || !ad.IsLoaded())
        {
            BumperCopsDebug.Log("Ad is not loaded");
            return 0;
        }


        return (int)ad.GetRewardItem().Amount;
    }

    void OnAdLoaded(object sender, EventArgs args)
    {
        RewardedAd ad = sender as RewardedAd;
        SendEvent(ad, AdEventType.LOAD_COMPLETE);
    }

    void OnAdFailed(object sender, AdErrorEventArgs args)
    {
        RewardedAd ad = sender as RewardedAd;
        SendEvent(ad, AdEventType.AD_FAILED);
    }

    void OnAdOpening(object sender, EventArgs args)
    {
        RewardedAd ad = sender as RewardedAd;
        SendEvent(ad, AdEventType.AD_OPEN);
    }

    void OnAdClosed(object sender, EventArgs args)
    {
        RewardedAd ad = sender as RewardedAd;
        SendEvent(ad, AdEventType.AD_CLOSED);
    }

    void OnAdrewardEarned(object sender, Reward args)
    {
        RewardedAd ad = sender as RewardedAd;
        SendEvent(ad, AdEventType.AD_REWARD_EARNED);
    }

    private void SendEvent(RewardedAd ad, AdEventType type)
    {
        if(ad == null)
        {
            BumperCopsDebug.LogWarning("Sender is null onAdLoaded");
            return;
        }

        AdsPlacement adsPlacement = AdsPlacement.NONE;
        foreach(KeyValuePair<AdsPlacement, RewardedAd> adMapKV in adMap)
        {
            if(adMapKV.Value == ad)
            {
                adsPlacement = adMapKV.Key;
                break;
            }
        }

        if(adsPlacement == AdsPlacement.NONE)
        {
            BumperCopsDebug.LogWarning($"Unable to find adsplacement. Cannot send event {type}");
            return;
        }

        AdEvent?.Invoke(new AdEventInternalArgs(type, adsPlacement));
    }
}