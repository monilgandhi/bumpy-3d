﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Newtonsoft.Json;
using System.IO;
using System.Text;
public class EnemyManager : MonoBehaviour
{
    public event Action<EnemyArgs> EnemySpawned;
    public event Action<EnemyArgs> EnemyHealthOver;

    public delegate void AllEnemiesDestroyedEventHandler();
    public event AllEnemiesDestroyedEventHandler AllEnemiesDestroyed;
    public bool IsEnemySpawned { get; private set; }
    public bool IsEnemyActive { get; private set; }

    [Header("Game Elements")]
    [SerializeField]
    private GameObject enemyControllerPrefab;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private RoadManager roadManager;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private PowerupManager powerupManager;

    [Header("UI Elements")]
    [SerializeField]
    private TimerBehaviour timerBehaviour;

    [SerializeField]
    private BonusBarBehaviour multiplierBehaviour;

    [SerializeField]
    private TimerExtendBehaviour timerExtendBehaviour;

    [Header("Admin")]
    [SerializeField]
    private string SpecificEnemyPrefab;

    [SerializeField]
    private bool shouldGenerateEnemy;

    public EnemyController currentEnemy { get; private set; }
    private int timeBetweenEnemySeconds = 5;
    private float lastEnemyDestroyedTime = 0;
    private static readonly string PREFAB_FOLDER = "enemy" + Path.DirectorySeparatorChar + "Cars";
    private const int TUTORIAL_TIME_SECONDS_BETWEEN_ENEMY = 8;
    private bool canCreateEnemies = false;
    private List<GameObject> prefabs = new List<GameObject>();
    private bool isTutorialActive;
    private bool canTutorialCreateEnemies;
    private Queue<EnemySequence> tutorialEnemy = new Queue<EnemySequence>();
    private Queue<GameObject> enemyPool = new Queue<GameObject>();
    private List<GameObject> initedEnemies = new List<GameObject>();
    private GameObject mainControllerObject;
    private EnemyCharacterManager characterManager;
    private EnemySequence currentEnemyCharacteristics;
    
    private void Awake()
    {
        if(!CarUtils.IsUnityDevelopmentBuild())
        {
            shouldGenerateEnemy = true;
        }
        
        LoadTutorialEnemySequence();
        LoadAndInitEnemyPrefabs();
    }

    private void LoadAndInitEnemyPrefabs()
    {
        string prefabLocation = Constants.PREFAB_BASE_LOCATION + Path.DirectorySeparatorChar + PREFAB_FOLDER;
        List<UnityEngine.Object> loadPrefabOjects = Resources.LoadAll(prefabLocation).ToList();

        initedEnemies = new List<GameObject>();
        mainControllerObject = Instantiate(enemyControllerPrefab);
        mainControllerObject.GetComponent<EnemyController>().EnemyReady += OnEnemyReady;
    
        foreach(GameObject g in loadPrefabOjects)
        {
            if(String.IsNullOrEmpty(SpecificEnemyPrefab) || SpecificEnemyPrefab.Equals(g.name))
            {
                GameObject go = Instantiate(g, mainControllerObject.transform);
                Character c = go.GetComponent<Character>();
                if(c == null)
                {
                    BumperCopsDebug.LogWarning("Character not found");
                    continue;
                }

                c.Hide();
                initedEnemies.Add(go);
            }
        }
        
        initedEnemies.Shuffle();
        enemyPool = new Queue<GameObject>(initedEnemies);
    }

    private void OnEnemyReady()
    {
        EnemyArgs args = new EnemyArgs(currentEnemy,
            bounty: currentEnemyCharacteristics.Bounty);

        EnemySpawned(args);
    }

    private void LoadTutorialEnemySequence()
    {
        string filePath = Constants.ENEMY_SEQUENCE_JSON_LOCATION + Path.DirectorySeparatorChar + "tutorial_sequence";
        tutorialEnemy = LoadSequenceFile(filePath);
    }

    private Queue<EnemySequence> LoadSequenceFile(string filePath)
    {
        //BumperCopsDebug.Log("File path is " + filePath);
        TextAsset prefabAttackFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabAttackFile.bytes, 0, prefabAttackFile.bytes.Length);
        List<EnemySequence> enemySequenceList = JsonConvert.DeserializeObject<List<EnemySequence>>(json);
        return new Queue<EnemySequence>(enemySequenceList);
    }
    private void Start() 
    {
        this.gameManager.GameTutorialSessionInitialized += (args) => isTutorialActive = true;
        this.gameManager.GameStarted += (args) =>
        {
            if(!args.NewSessionStarted)
            {
                return;                
            }
            
            lastEnemyDestroyedTime = Time.time;
            canCreateEnemies = !isTutorialActive;
        }; 

        this.player.AttackController.PlayerEnteredAttackZone += OnFoundEnemy;
        this.gameManager.GameOver += (reason) =>
        {
            canCreateEnemies = false;
            if(this.currentEnemy != null)
            {
                IsEnemyActive = false;
                this.currentEnemy.GameOver(reason);
            }
        };

        this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
        this.gameManager.TutorialManager.TutorialEnd += OnTutorialEnd;
        this.gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
        this.gameManager.BonusBehaviour.BonusStarted += (args) => canCreateEnemies = false;
        this.gameManager.BonusBehaviour.BonusEnded += () => 
        {
            canCreateEnemies = isTutorialActive ? canTutorialCreateEnemies : true;
        };

        characterManager = new EnemyCharacterManager(this.player, this.gameManager.GameLevelManager, timerExtendBehaviour);
    }

    private void OnTutorialEnd(TutorialArgs args)
    {
        isTutorialActive = false;
        this.gameManager.TutorialManager.TutorialStepUpdate -= OnTutorialStepUpdate;
        this.gameManager.TutorialManager.TutorialEnd -= OnTutorialEnd;
        this.gameManager.TutorialManager.TutorialStarted -= OnTutorialStarted;
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        isTutorialActive = true;
    }
    
    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        if(args.TutorialStep >= TutorialStep.ENEMY_GENERATION && args.TutorialStep < TutorialStep.HIT ||
            args.TutorialStep >= TutorialStep.DONUT_GENERATION_INIT)
        {
            canCreateEnemies = true;
            canTutorialCreateEnemies = true;
            // remove the listener
            //this.gameManager.TutorialManager.TutorialStepUpdate -= OnTutorialStepUpdate;
            lastEnemyDestroyedTime = Time.time;
            BumperCopsDebug.Log($"Can create enemies. Step is {args.TutorialStep}");
        }
        else
        {
            BumperCopsDebug.Log($"Cannot create enemies. Step is {args.TutorialStep}");
            canCreateEnemies = false;
            canTutorialCreateEnemies = false;
        }
    }

    private void Update()
    {
        if(!shouldGenerateEnemy)
        {
            return;
        }
        
        if(canCreateEnemies && !IsEnemySpawned)
        {
            if(Mathf.RoundToInt(Time.time -  lastEnemyDestroyedTime) >= timeBetweenEnemySeconds)
            {
                SpawnEnemy();
                timeBetweenEnemySeconds = this.isTutorialActive ? TUTORIAL_TIME_SECONDS_BETWEEN_ENEMY : UnityEngine.Random.Range(6, 10);
			    return;
            }
        }
    }

    private void SpawnEnemy()
    {
        SpawnGameEnemy();

        if(currentEnemy == null)
        {
            return;
        }
        
        IsEnemySpawned = true;
        currentEnemy.DamageController.EnemyDestroyed += OnEnemyDestroyed;
        float distanceBetweenEnemy = isTutorialActive ? 
            Constants.SEC_DISTANCE_BETWEEN_ENEMY_PLAYER_SPAWN : 
            this.gameManager.GameLevelManager.EnemyDistanceOnSpawn;
            
        RoadController road = this.roadManager.GetRoadForEnemy(distanceBetweenEnemy);

        BumperCopsDebug.Log($"Road for enemy is {road.MyPathNumber} & player is {this.player.MoveController.Lane.Road.MyPathNumber}");
        //currentEnemy.transform.parent = road.transform;
        Vector3 lanePosition = road.GetLanePosition(UnityEngine.Random.Range(0, road.LaneCount)).Value;
        currentEnemy.transform.position = new Vector3(lanePosition.x, this.player.MoveController.playerYPosition + 0.5f, lanePosition.z);

        //currentEnemy.transform.parent = null;
    }

    private void SpawnTutorialEnemy()
    {
        if(tutorialEnemy.Count == 0)
        {
            throw new KeyNotFoundException(nameof(tutorialEnemy));
        }

        currentEnemyCharacteristics = tutorialEnemy.Dequeue();
    
        SelectAndInitEnemyPrefab();

        if(tutorialEnemy.Count == 0)
        {
            isTutorialActive = false;
        }
    }

    private void SpawnGameEnemy()
    {
        currentEnemyCharacteristics = characterManager.GetNextEnemy();
        //currentEnemyCharacteristics.Crime = (EnemyCrime) crimesList.Pop();

        SelectAndInitEnemyPrefab();
    }

    private void SelectAndInitEnemyPrefab()
    {
        if(enemyPool.Count == 0)
        {
            initedEnemies.Shuffle();
            enemyPool = new Queue<GameObject>(initedEnemies);
        }

        GameObject enemyModel = enemyPool.Dequeue();

        //BumperCopsDebug.Log("Spawning enemy with id " + enemyCharacteristics.Id + " with prefab " + prefabObject.name);
        currentEnemy = mainControllerObject.GetComponent<EnemyController>();

        if(currentEnemy == null)
        {
            throw new MissingComponentException("enemy controller missing in the model");
        }

        currentEnemy.name = currentEnemyCharacteristics.Id;

        //BumperCopsDebug.Log("EnemyManager: Crime is " + enemyCharacteristics.Crime);
        currentEnemy.InitAndStart(enemyModel.GetComponent<Character>(), 
            this.player, 
            this.gameManager, 
            currentEnemyCharacteristics, 
            this.roadManager, 
            this.multiplierBehaviour, 
            powerupManager);
    }

    private void OnEnemyDestroyed()
    {
        currentEnemy.DamageController.EnemyDestroyed -= OnEnemyDestroyed;
        EnemyHealthOver?.Invoke(
            new EnemyArgs(
                this.currentEnemy, 
                currentEnemyCharacteristics.Bounty, 
                this.timerBehaviour.RemainingTime));
                
        this.currentEnemy = null;
        this.lastEnemyDestroyedTime = Time.time;
        IsEnemySpawned = false;
        IsEnemyActive = false;
    }

    private void OnFoundEnemy()
    {
        if(!IsEnemySpawned)
        {
            throw new InvalidOperationException("Enemy not spawned yet");
        }

        this.currentEnemy.Activate();
        IsEnemyActive = true;
    }
}