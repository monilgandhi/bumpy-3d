using System.Collections;
using UnityEngine;

public class EnemyMovementController : NPCMovementController 
{
    private EnemyController enemyController;
    private bool hasChaseBegan;
    private PlayerController player;
    private PowerupManager powerupManager;
    private float movementStats;

    private float requiredTurnTime = 1.0f; 

    private RoadController lastRoadBeforeFork;

    private bool isBackupAheadOfMe;
    private const float ALLOWED_FALLING_TIME_SECS = 3f;

    private void Awake() 
    {
        this.enemyController = this.GetComponentInParent<EnemyController>();
    }

    private void Start()
    {
        this.enemyController.DamageController.EnemyDestructionBegin += OnEnemyDestructionBegin;
    }

    public void InitEnemy(PlayerController player,
                         EnemyController eController, 
                         PowerupManager powerupManager)
    {
        this.enemyController.GameManager.GameLevelManager.GameLevelChanged += OnGameLevelChanged;
        //BumperCopsDebug.Log("EnemyMovementController: enemy controller");
        // initialization
        this.enemyController = eController;
        BoxCollider turnCollider = this.GetComponent<BoxCollider>();
        turnCollider.center = new Vector3(0, turnCollider.center.y, turnCollider.size.z / 2f);

        this.turnWheels = this.enemyController.Character.Wheels;
        this.rigidBody = this.enemyController.RigidBody;
        this.player = player;
        this.powerupManager = powerupManager;

        GameLevelManager gameLevelManager = this.enemyController.GameManager.GameLevelManager;

        // set speed
        this.movementStats = this.enemyController.Character.MovementStats;
        this.Speed = Mathf.RoundToInt(this.movementStats + gameLevelManager.GameSpeed);

        // do not change lanes if attack is ready
        this.player.AttackController.AttackReady += OnAttackReady;
        player.AttackController.PlayerEnteredAttackZone += IFoundYou; 
        this.player.AttackController.PlayerExitAttackZone += OnPlayerExitAttackZone;

        this.player.AttackController.Bumped += OnBumped;
        this.powerupManager.PowerupCollected += OnPowerupCollected;

        // activate
        isActive = true;
        canChangeLanes = false;
    }

    private void OnEnable() 
    {
        BumperCopsDebug.Log("Activating changing lanes");
        StartCoroutine(WaitAndActivateChangeLanes());
    }

    private IEnumerator WaitAndActivateChangeLanes()
    {
        yield return new WaitForSeconds(2f);
        canChangeLanes = true;
    }

    private void OnAttackReady()
    {
        canChangeLanes = false;
    }

    private void OnBumped(BumpEventArgs args)
    {
        canChangeLanes = true;
    }

    private void OnPlayerExitAttackZone()
    {
        hasChaseBegan = false;

        // reset the max speed
        this.Speed = Mathf.RoundToInt(this.movementStats + this.enemyController.GameManager.GameLevelManager.GameSpeed);
    }

    private void OnEnemyDestructionBegin()
    {
        this.isActive = false;
        this.enemyController.RoadManager.RoadTypeChanged -= OnRoadTypeChanged;
        this.player.AttackController.PlayerExitAttackZone -= OnPlayerExitAttackZone;
        this.player.AttackController.PlayerEnteredAttackZone -= IFoundYou; 
        this.player.AttackController.AttackReady -= OnAttackReady;
        this.player.AttackController.Bumped -= OnBumped;
        powerupManager.PowerupCollected -= OnPowerupCollected;   
        this.enemyController.GameManager.GameLevelManager.GameLevelChanged -= OnGameLevelChanged;
    }
    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type == PowerupType.BACKUP)
        {
            this.canChangeLanes = false;
        }
    }

    private void OnGameLevelChanged(GameLevelManager.GameLevelChangedEventArgs args)
    {
        if(args.GameLever == GameLeverType.GAME_SPEED && !isBackupAheadOfMe && isActive)
        {
            this.Speed = Mathf.RoundToInt(this.movementStats + this.enemyController.GameManager.GameLevelManager.GameSpeed);
        }
    }

    private void OnRoadTypeChanged(RoadtypeChangedEventArgs args)
    {
        if(args.To == RoadManager.RoadType.FORK)
        {
            lastRoadBeforeFork = args.LastRoadCurrentType;
        }
    }

    private void FixedUpdate() 
    {
        if(!isActive)
        {
            return;
        }

        if(!changingLanes && this.rigidBody.rotation != Quaternion.identity)
        {
            this.rigidBody.rotation = Quaternion.identity;
        }

        EnemyUpdate();
        
        //BumperCopsDebug.Log("Updating enemy");

        // we use transform direction because enemy changes lanes and hence require all 3 coordinates
        velocityVector = transform.TransformDirection(Vector3.forward);
        base.Move();
    }

    private void EnemyUpdate()
    {
        // check the Yposition. In case the enemy is free falling reset them
        if(enemyController.RigidBody.transform.position.y < this.player.PlayerYPosition - 3)
        {
            float zPosition = this.player.transform.position.z > enemyController.RigidBody.transform.position.z ? this.player.transform.position.z + Constants.DISTANCE_AHEAD_PLAYER_IN_CASE_STUCK : enemyController.RigidBody.transform.position.z;
            enemyController.RigidBody.transform.position = new Vector3(enemyController.RigidBody.transform.position.x, this.player.PlayerYPosition, zPosition);

            return;
        }
        else if (enemyController.RigidBody.transform.position.y > this.player.PlayerYPosition + 3)
        {
            this.rigidBody.AddForce(Vector3.down, ForceMode.Force);
        }

        LaneController myLane = Lane;
        if(myLane == null)
        {
            return;
        }

        // fork approaching
        if(lastRoadBeforeFork != null && myLane.Road.MyPathNumber >= lastRoadBeforeFork.MyPathNumber - 3)
        {
            BumperCopsDebug.Log($"EnemyMovementController: Fork detected moving to middle lane");
            // move to the middle lane

            LaneController selectedLane = CarUtils.GetNextLane(lastRoadBeforeFork.MiddleLane);
            StartCoroutine(base.MakeTurn(selectedLane, 1f));

            lastRoadBeforeFork = null;
        }

        // change the lane when the player is in attack mode
        if(hasChaseBegan && this.enemyController.EnemyCharacteristics.LaneChangeAllowedDuringChase)
        {
            //BumperCopsDebug.Log($"turnTime: {turnTime}, canChangeLanes: {canChangeLanes}, changingLanes: {changingLanes}");
            if(turnTime > requiredTurnTime && canChangeLanes && !changingLanes)
            {
                turnTime = 0f;
                //BumperCopsDebug.Log("turnTime Changing lanes during chase");
                StartCoroutine(base.ReactToTrafficAhead(forceLaneChange: true, turnTime: 0.7f));
            }

            // no turning unless chase has began. We will make sure that no cars are spawn in enemy lanes
            turnTime += Time.deltaTime;
        }
    }

    /// <summary>
    /// Callback function when player finds enemy. 
    /// </summary>
    private void IFoundYou()
    {
        if(!this.isActive)
        {
            return;
        }

        this.hasChaseBegan = true;
        this.Speed = Mathf.RoundToInt(player.SpeedController.MaxSpeed + 2);
    }

    private void OnCollisionEnter(Collision other) 
    {
       //BumperCopsDebug.Log("EnemyMovementController: Collision");
        if(!isActive)
        {
            return;
        }

        if(other.gameObject.layer == Constants.NPC_LAYER && !this.changingLanes && canChangeLanes)
        {
            StartCoroutine(ReactToTrafficAhead(forceLaneChange: true));
            return;
        }
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(!this.isActive && isBackupAheadOfMe)
        {
            return;
        }
        
        if(other.gameObject.layer.Equals(Constants.PLAYER_NPC_LAYER))
        {
            BumperCopsDebug.Log("Reducing the speed since I found player NPC");
            this.Speed = Constants.NPC_SPEED;
            this.reduceSpeed = true;
            this.canChangeLanes = false;
            isBackupAheadOfMe = true;
            return;
        }

        if(other.gameObject.tag.Equals(Constants.CENTER_BARRIER_TAG) || 
            other.gameObject.layer != Constants.NPC_LAYER || 
            changingLanes || 
            !canChangeLanes)
        {
            //BumperCopsDebug.Log("Cannot change enemy's lane");
            return;
        }    

        //BumperCopsDebug.Log("changing enemy's lane");
        // trigger on enemy means we have detected something in the front. Change lanes
        StartCoroutine(ReactToTrafficAhead(other.gameObject.GetComponent<NPCMovementController>(), forceLaneChange: true, 0.7f));
    }
}