using UnityEngine;
using TMPro;
public class WantedPosterBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text enemyBounty;

    [SerializeField]
    private TMP_Text enemyCountText;

    [Header("Game Elements")]
    [SerializeField]
    private EnemyManager enemyManager;

    private Animator wantedPosterAnimator;
    private int enemySpawnedCount;

    private void Awake() 
    {
        enemySpawnedCount = 0;
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        this.enemyManager.EnemyHealthOver += OnEnemyDestroyed;

        this.wantedPosterAnimator = this.GetComponent<Animator>();
        this.gameObject.SetActive(false);
    }    

    private void OnEnemySpawned(EnemyArgs args)
    {
        this.gameObject.SetActive(false);
        ++enemySpawnedCount;
        enemyBounty.text = args.Bounty.ToString("n0");
        enemyCountText.text = $"# {enemySpawnedCount}";
        this.gameObject.SetActive(true);
    }

    private void OnEnemyDestroyed(EnemyArgs args)
    {
        this.wantedPosterAnimator.SetTrigger("captured");
    }
}