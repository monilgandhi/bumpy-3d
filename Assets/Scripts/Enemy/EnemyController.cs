﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(EnemyHealthController))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(GamePlayCanvasManager))]
public class EnemyController : MonoBehaviour
{
    public delegate void EnemyInitCompleteEventHandler();
    public event EnemyInitCompleteEventHandler EnemyReady;

    public delegate void EnemyEscapedEventHandler();
    public event EnemyEscapedEventHandler EnemyEscaped;

    [SerializeField]
    private AudioSource bumpSound;

    public float Speed { get { return this.movementController.Speed; }}
    public Rigidbody RigidBody { get; private set; }
    public Character Character { get; private set; }

    private bool isActive;
    public bool IsActive 
    {
         get { return isActive; }
        private set
        {
            BumperCopsDebug.Log("enemy controller new value for active " + value);
            isActive = value;
        }
    }
    public LaneController Lane 
    {
        get
        {
            return this.movementController.Lane;
        }
    }

    private EnemyHealthController healthController;
    private PowerupManager powerupManager;
    public EnemyHealthController Health { get { return this.healthController; }}
    public EnemyMovementController movementController { get; private set; }
    public EnemyDamageController DamageController { get; private set;}
    public GameManager GameManager { get; private set; }
    public RoadManager RoadManager { get; private set;}
    public PlayerController player { get; private set; }
    private bool InitComplete;
    private float playerAttackStrength = -1;
    public BonusBarBehaviour BonusBarBehaviour { get; private set; }
    public EnemySequence EnemyCharacteristics { get; private set; }

    private GamePlayCanvasManager canvasManager;
    private RigidbodyConstraints initialConstraints;
    private bool enemyDestructionStarted;
    // Use this for initialization
   
    private void Awake() 
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.initialConstraints = this.RigidBody.constraints;
        this.healthController = this.GetComponent<EnemyHealthController>();
        this.movementController = this.GetComponentInChildren<EnemyMovementController>();
        this.canvasManager = this.GetComponent<GamePlayCanvasManager>();
        if(this.movementController == null)
        {
            throw new MissingComponentException(nameof(EnemyMovementController));
        }

        this.DamageController = this.GetComponent<EnemyDamageController>();
        if(this.DamageController == null)
        {
            throw new MissingComponentException(nameof(EnemyDamageController));
        }
        
        this.DamageController.EnemyDestructionBegin += OnEnemyDestructionBegin;
        this.DamageController.EnemyDestroyed += OnEnemyDestroyed;
    }

    private void Start() 
    {
        this.gameObject.SetActive(false);    
    }

    private void LateUpdate() 
    {
        if(!this.IsActive || enemyDestructionStarted)
        {
            return;
        }

        if(IsPlayerAhead())
        {
            // player is ahead. reset the position of the enemy right before the player
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.player.transform.position.z + Constants.DISTANCE_AHEAD_PLAYER_IN_CASE_STUCK);
            return;
        }

        //BumperCopsDebug.LogFormat("EnemyController: Enemy path {0}, Player Path: {1}", this.movementController.PathNumber, this.player.MoveController.PathNumber);
        long enemyPath = this.movementController.PathNumber;
        long playerPath = this.player.MoveController.PathNumber;

        if(enemyPath >= 0 && playerPath >= 0 && 
            enemyPath - playerPath > 3)
        {
            BumperCopsDebug.LogFormat("EnemyController: Enemy path {0}, Player Path: {1}", enemyPath, playerPath);
            EnemyEscaped();
        }
    }
    
    private bool IsPlayerAhead()
    {
        return this.RigidBody.position.z <= this.player.RigidBody.position.z;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!this.InitComplete)
        {
            throw new InvalidProgramException("enemy is not initialized");
        }


        //BumperCopsDebug.Log($" enemy controller collision details name: {this.name}, impulse: {collision.impulse} normal: {collision.impulse.normalized} magnitude: {collision.impulse.magnitude}");
        switch (collision.gameObject.layer)
        {
            case Constants.PLAYER_NPC_LAYER:
                OnBumped(new BumpEventArgs(this.player.character.AttackStats, false, true));
                break;
            
            case Constants.PROJECTILES_LAYER:
                BumperCopsDebug.Log("Enemy hit with projectile");
                OnBumped(new BumpEventArgs((float)this.Health.Health, false));
                break;
        }
    }

    private IEnumerator Rotate(bool turnRight)
    {
        float rotation = turnRight ? UnityEngine.Random.Range(0, 15) : UnityEngine.Random.Range(-15, 0);
        yield return new WaitForFixedUpdate();

        transform.Rotate(new Vector3(0, rotation, 0));

        yield return new WaitForSeconds(0.5f);

        //BumperCopsDebug.Log("Rotating back");
        transform.Rotate(new Vector3(0, rotation * -1, 0));
    }

    public void InitAndStart(Character enemyCharacter,
                    PlayerController playerObj, 
                    GameManager gameManager, 
                    EnemySequence enemyCharacteristics,
                    RoadManager roadManager,
                    BonusBarBehaviour bonusBarBehaviour,
                    PowerupManager powerupManager)
    {
        //BumperCopsDebug.Log("Initing enemy");
        this.EnemyCharacteristics = enemyCharacteristics;
        this.player = playerObj;
        this.GameManager = gameManager;
        this.RoadManager = roadManager;
        this.powerupManager = powerupManager;

        this.player.AttackController.BumpEnded += OnBumped;
        this.BonusBarBehaviour = bonusBarBehaviour;

        this.Character = enemyCharacter;
        if(this.Character == null)
        {
            throw new MissingComponentException(nameof(Character));
        }

        enemyCharacter.InitAndStartEnemy(this.EnemyCharacteristics.Movement);
        OnDataLoadComplete();

        this.gameObject.SetActive(true);
    }

    private void OnEnemyDestructionBegin()
    {
        enemyDestructionStarted = true;
        this.player.AttackController.BumpEnded -= OnBumped;
    }

    private void OnEnemyDestroyed()
    {
        this.Character.Destroyed();
        this.gameObject.SetActive(false);
    }
    
    private void OnBumped(BumpEventArgs args)
    {
        playerAttackStrength = args.bumpStength;
        this.bumpSound.Play();
        this.healthController.OnBumped(playerAttackStrength, this.Character.RearEndPosition.normalized, args.PlayerBumped);
        this.playerAttackStrength = -1;
    }

    private void OnDataLoadComplete()
    {
        //BumperCopsDebug.Log("enemy controller Data load complete");
        this.RigidBody.mass = this.Character.Mass;
        this.RigidBody.maxAngularVelocity = 100;
        this.RigidBody.constraints = this.initialConstraints;
        this.RigidBody.centerOfMass = this.Character.CenterOfMass.transform.localPosition;
        // reset all the forces
        this.RigidBody.velocity = Vector3.zero;
        this.transform.position = Vector3.zero;
        this.transform.rotation = Quaternion.identity;

        this.healthController.Init(this.EnemyCharacteristics.Name, this.EnemyCharacteristics.Health, this.player.character.AttackStats);        
        this.movementController.InitEnemy(player, this, this.powerupManager);
        this.DamageController.Init();
        this.canvasManager.Init(this.GameManager);

        this.InitComplete = true;
        EnemyReady();
    }

    public void Activate()
    {
        IsActive = true;
    }

    public void GameOver(GameOverReason reason)
    {
        IsActive = false;
        if(reason == GameOverReason.HEALTH_OVER)
        {
            this.movementController.Deactivate();    
        }
        
        this.healthController.Deactivate();
    }
}
