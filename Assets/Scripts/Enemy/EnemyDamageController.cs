using System;
using System.Collections;
using UnityEngine;

public class EnemyDamageController : MonoBehaviour 
{
    public delegate void EnemyDestroyedEventHandler();
    public event EnemyDestroyedEventHandler EnemyDestroyed;

    public delegate void EnemyDestructionBeginEventHandler();
    public event EnemyDestructionBeginEventHandler EnemyDestructionBegin;

    private bool enemyDestroyedEventSent;
    private bool enemyDestructionStartedEventSent;

    [SerializeField]
    private AnimationCurve bumpImpulseCurve;
    [SerializeField]
    private AudioSource tireSquealSound;

    [SerializeField]
    private ParticleSystem destroyParticleSystem;

    [SerializeField]
    private ParticleSystem hitParticleSystem;
    private EnemyController enemyController;
    private bool enemyDestructionStarted;

    private void Awake() 
    {
        this.enemyController = this.transform.root.GetComponent<EnemyController>();    
        this.enemyController.Health.EnemyHit += OnPlayerBumped;
    }

    public void Init()
    {
        this.enemyController.BonusBarBehaviour.MegaSirenTurnedOn += OnMegaSireTurnedOn;
        destroyParticleSystem.Stop();
        hitParticleSystem.Stop();
    }

    private void OnMegaSireTurnedOn()
    {
        StartCoroutine(StartParticleEffect());
        StartCoroutine(DestroyEnemy(true));
    }
    
    public void OnPlayerBumped(AttackArgs args) 
    {
        //BumperCopsDebug.Log($"Normal is {args.AttackNormal}");
        bool destroy = args.RemainingHealth <= 0;
        enemyDestructionStarted = destroy;
        enemyDestructionStartedEventSent = destroy;
        StartCoroutine(StartParticleEffect());

        if(destroy)
        {
            StartCoroutine(DestroyEnemy());
        }
        else
        {
            StartCoroutine(Bumped(args.AttackedbyPlayer));
        }
        //BumperCopsDebug.Log("Player bumped");
    }    

    private IEnumerator StartParticleEffect()
    {
        //BumperCopsDebug.Log("EnemyDamageController: Starting particle effect");
        if(!hitParticleSystem.isPlaying)
        {
            hitParticleSystem.Play();
        }

        yield return new WaitWhile(() => hitParticleSystem.isPlaying);
        hitParticleSystem.Stop();
    }

    private IEnumerator ApplyConstantTorque()
    {
        float startTime = 0;
        
        while(startTime < 1.5f)
        {
            float xForce = UnityEngine.Random.Range(-4, 4);
            float yForce = UnityEngine.Random.Range(-10, 10);
            float zForce = UnityEngine.Random.Range(-2, 2);

            Vector3 finalForce = new Vector3(xForce, yForce, zForce);

            this.enemyController.RigidBody.AddTorque(finalForce);
            startTime += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }

    private IEnumerator DestroyEnemy(bool onlyExplode = false)
    {
        EnemyDestructionBegin?.Invoke();

        this.enemyController.BonusBarBehaviour.MegaSirenTurnedOn -= OnMegaSireTurnedOn;
        float random = UnityEngine.Random.value;
        this.enemyController.RigidBody.mass = 1;
        //this.enemyController.RigidBody.inertiaTensorRotation = Quaternion.identity;
        this.enemyController.RigidBody.constraints = RigidbodyConstraints.None;    

        Vector3 smallExplosionPosition = this.enemyController.transform.position;
        if(random < 0.33f)
        {
            smallExplosionPosition = this.enemyController.Character.RearEndPosition;
        } else if (random >= 0.33 && random < 0.66f)
        {
            smallExplosionPosition = this.enemyController.Character.FrontEndPosition;
        }

        smallExplosionPosition = new Vector3(smallExplosionPosition.x + UnityEngine.Random.Range(-1, 1), smallExplosionPosition.y, smallExplosionPosition.z + UnityEngine.Random.Range(-2,2));
        this.enemyController.RigidBody.AddExplosionForce(UnityEngine.Random.Range(1, 5), smallExplosionPosition, 1, 0, ForceMode.Impulse);

        //StartCoroutine(ApplyConstantTorque());
        if(!onlyExplode)
        {
            this.tireSquealSound.Play();

            int count = 0;
            while(count++ <= 5)
            {
                Vector3 forwardDir = Camera.main.transform.forward - this.enemyController.transform.forward;
                this.enemyController.RigidBody.AddForce(Vector3.forward * (this.enemyController.RigidBody.mass * 10), ForceMode.Impulse);

                float xForce = UnityEngine.Random.Range(-4, 4);
                float yForce = UnityEngine.Random.Range(-10, 10);
                float zForce = UnityEngine.Random.Range(-2, 2);

                Vector3 finalForce = new Vector3(xForce, yForce, zForce);

                this.enemyController.RigidBody.AddTorque(finalForce);

                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(1f);
        }

        this.enemyController.RigidBody.AddExplosionForce(UnityEngine.Random.Range(3, 10), this.enemyController.Character.RearEndPosition, 1, -1, ForceMode.Impulse);
        
        float xDirection = 0;
        if(random < 0.33)
        {
            xDirection = -1;
        } else if (random >= 0.33 && random < 0.66)
        {
            xDirection = 1;
        }

        if(!destroyParticleSystem.isPlaying)
        {
            destroyParticleSystem.Play();
        }

        Vector3 forcePosition = this.transform.TransformDirection(new Vector3(xDirection, 0, 1));
        this.enemyController.RigidBody.AddTorque(forcePosition * 30, ForceMode.Impulse);

        float startTime = Time.time;
        yield return new WaitWhile(() => Time.time - startTime < 2.5f && this.enemyController.transform.position.z > this.enemyController.player.transform.position.z - 5) ;

        EnemyDestroyed();
        enemyDestroyedEventSent = true;

        this.enemyController.BonusBarBehaviour.MegaSirenTurnedOn -= OnMegaSireTurnedOn;
    }

    private IEnumerator Bumped(bool bumpedByPlayer)
    {
        RigidbodyConstraints initialConstraints = this.enemyController.RigidBody.constraints;
        this.enemyController.RigidBody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;

        float start = Time.time;
        float maxTime = bumpImpulseCurve.keys[bumpImpulseCurve.length - 1].time;

        this.tireSquealSound.Play();

        bool keepAddingForce = true;
        while (keepAddingForce)
        {
            Vector3 forwardForce = Vector3.forward * (bumpImpulseCurve.Evaluate(Time.time - start) * this.enemyController.RigidBody.mass);
            this.enemyController.RigidBody.AddForce(forwardForce, ForceMode.Impulse);
            Vector3 enemyZ = new Vector3(0, 0, this.enemyController.RigidBody.transform.position.z);
            Vector3 playerZ = new Vector3(0, 0, this.enemyController.player.RigidBody.transform.position.z);
            float distance = Vector3.Distance(enemyZ, playerZ);
            float timeRequiredToReachMyPosition = distance / this.enemyController.player.SpeedController.MaxSpeed;
            keepAddingForce = bumpedByPlayer && 
                (Time.time - start < maxTime || 
                    timeRequiredToReachMyPosition >= Constants.SEC_DISTANCE_BETWEEN_ENEMY_PLAYER_SPAWN);

            yield return new WaitForFixedUpdate();
        }

        // wait until player is atleast SEC_DISTANCE_BETWEEN_ENEMY_PLAYER_SPAWN seconds away from enemy
        while (this.enemyController.RigidBody.velocity.magnitude > this.enemyController.Speed)
        {
            this.enemyController.RigidBody.AddForce(Vector3.forward * -(this.enemyController.RigidBody.mass/2), ForceMode.Impulse);
            yield return new WaitForFixedUpdate();
        }

        // reset the constraints and exit
        this.enemyController.RigidBody.constraints = initialConstraints;
    }

    private IEnumerator MakeWheelsFly()
    {
        // now add a force on the couple of wheels
        int numberOfWheels = UnityEngine.Random.Range(1, this.enemyController.Character.Wheels.Length);
        Rigidbody cameraWheel = null;
        for(int i = 0; i < numberOfWheels; i++)
        {
            WheelCollider w = this.enemyController.Character.Wheels[i];
            if(w == null || w.gameObject == null)
            {
                continue;
            }
            
            Rigidbody wheelRigidbody = w.gameObject.AddComponent<Rigidbody>();
            if(wheelRigidbody == null)
            {
                yield break;
            }

            wheelRigidbody.mass = 30;

            if(cameraWheel == null)
            {
                cameraWheel = wheelRigidbody;
                continue;
            }
        
            wheelRigidbody.AddExplosionForce(200f, transform.position, 10f, 1f, ForceMode.Impulse);
        }

        float startTime = Time.time;
        while(Time.time - startTime < 0.1f)
        {
            Vector3 direction = Camera.main.transform.position - cameraWheel.transform.position;
            // BumperCopsDebug.Log($"Exploding the wheel {direction}");
            cameraWheel.AddForce(direction.normalized * 1000f);
            yield return new WaitForFixedUpdate();
        }
    }
}