﻿using System;
public class EnemyArgs
{
    public readonly EnemyController enemy;
    public readonly int Bounty;
    public readonly int TimeRemaining;

    public readonly string EnemyName;

    public EnemyArgs(EnemyController enemyController, 
                    int bounty)
    {
        this.enemy = enemyController;
        this.Bounty = bounty;
        this.TimeRemaining = -1;
        this.EnemyName = this.enemy.EnemyCharacteristics.Id;
    }

    public EnemyArgs(EnemyController enemyController,
                    int bounty,
                    int timeRemaining)
    {
        this.EnemyName = enemyController.EnemyCharacteristics.Id;
        this.enemy = enemyController;
        this.TimeRemaining = timeRemaining;
        this.Bounty = bounty;
    }
}
