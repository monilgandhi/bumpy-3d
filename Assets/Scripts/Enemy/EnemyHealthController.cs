﻿using System;
using System.Linq;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyHealthController : MonoBehaviour
{
    public event Action<AttackArgs> EnemyHit;
    public int Health { get; private set; }
    private int totalHealth;
    [SerializeField]
    private CanvasGroup enemyHealthStatsCanvas;

    [SerializeField]
    private HorizontalLayoutGroup enemyHealthGroup;

    [SerializeField]
    private TMP_Text enemyName;

    [SerializeField]
    private GameObject enemyHealthBarPrefab;
    private EnemyController enemyController;
    private List<EnemyHealthBarBehaviour> healthBarBehaviours;
    private int requiredHits;
    private int totalHitsTaken = 0;
    public int RemainingHits { get { return requiredHits - totalHitsTaken; }}
    // Start is called before the first frame update
    private void Awake() 
    {
        this.enemyController = this.GetComponent<EnemyController>();
        healthBarBehaviours = new List<EnemyHealthBarBehaviour>();

        for(int i = 0; i < 4; i++)
        {
            GameObject healthBarGO = Instantiate(enemyHealthBarPrefab, this.enemyHealthGroup.transform);
            healthBarBehaviours.Add(healthBarGO.GetComponent<EnemyHealthBarBehaviour>());
        }
    }

    private void Start()
    {
        this.enemyController.DamageController.EnemyDestructionBegin += () => StartCoroutine(OnEnemyDestructionBegin());
    }

    private IEnumerator OnEnemyDestructionBegin()
    {
        foreach(EnemyHealthBarBehaviour b in healthBarBehaviours)
        {
            StartCoroutine(b.GotHit());
            yield return new WaitWhile(() => !b.AnimationComplete);
        }

        StartCoroutine(TurnOffTheCanvasAfterAnimation(healthBarBehaviours[0]));

        this.enemyController.GameManager.GamePaused -= OnGamePaused;
        this.enemyController.GameManager.GameStarted -= OnGameStarted;
        this.enemyController.GameManager.GameOver -= OnGameOver;
    }

    private void OnGamePaused()
    {
        if(enemyHealthStatsCanvas != null)
        {
            // hide the canvas
            enemyHealthStatsCanvas.alpha = 0;
        }
    }

    private void OnGameStarted(GameEventArgs args)
    {
        if(!args.NewSessionStarted && enemyHealthStatsCanvas != null)
        {
            enemyHealthStatsCanvas.alpha = 1;
        }
    }

    public void Init(string name, int health, float playerHitStrength)
    {
        if(playerHitStrength == 0 && health == 0)
        {
            BumperCopsDebug.LogError("Player health and enemy health 0. Cannot init");
            return;
        }
        
        if(playerHitStrength <= 0)
        {
            BumperCopsDebug.LogError("Player strength is less than 0");
            playerHitStrength = health;
        }

        enemyName.text = CarUtils.SanitzeName(name);

        this.totalHealth = this.Health = health;
        requiredHits = health / (int)playerHitStrength;
        totalHitsTaken = 0;
        
        BumperCopsDebug.Log("Setting the total hits taken to 0");
        for(int i = 0; i < healthBarBehaviours.Count; i++)
        {
            if(i < requiredHits)
            {
                healthBarBehaviours[i].Init();
            }
            else
            {
                healthBarBehaviours[i].gameObject.SetActive(false);
            }
        }

        this.enemyController.GameManager.GamePaused += OnGamePaused;
        this.enemyController.GameManager.GameStarted += OnGameStarted;
        this.enemyController.GameManager.GameOver += OnGameOver;
        this.enemyHealthStatsCanvas.alpha = 1;
    }

    private void OnGameOver(GameOverReason r)
    {
        if(this.enemyHealthStatsCanvas != null)
        {
            this.enemyHealthStatsCanvas.alpha = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!this.enemyController.IsActive)
        {
            return;
        }

        if(this.Health.Equals(1) && this.healthBarBehaviours.Count > 1 && !this.enemyController.Character.SmokeParticleEffect.isPlaying)
        {
            BumperCopsDebug.Log("Turning on smoke effect");
            this.enemyController.Character.SmokeParticleEffect.gameObject.SetActive(true);
            this.enemyController.Character.SmokeParticleEffect.Play();
        }

        if(this.enemyController.IsActive && this.enemyHealthStatsCanvas.alpha == 0 && this.Health > 0)
        {
            this.enemyHealthStatsCanvas.alpha = 1;
        }
    }

    public void ReduceHealthOfNPC(float delta)
    {
        this.Health -= Mathf.RoundToInt(delta);
        BumperCopsDebug.Log($"HealthStats: Reducing the health by {delta} from {this.Health}");
        if(this.Health < 0)
        {
            this.Health = 0;
        }
    }

    // TODO reduce health dynamically based on the attack strength
    public void OnBumped(float attackStrength, Vector3 attackNormal, bool attackedByPlayer)
    {
        if(this.totalHitsTaken >= requiredHits)
        {
            return;
        }
        
        //TODO reduce health here
        ReduceHealthOfNPC(attackStrength);
        EnemyHit?.Invoke(new AttackArgs(this.Health, attackStrength, attackNormal, attackedByPlayer));

        // fill it faster if our health is 0
        BumperCopsDebug.Log($"reducing health bar number {requiredHits} and totalhits {totalHitsTaken} {(requiredHits - totalHitsTaken) - 1}");
        EnemyHealthBarBehaviour bar = this.healthBarBehaviours[(requiredHits - totalHitsTaken) - 1];
        ++totalHitsTaken;
        StartCoroutine(bar.GotHit());
        StartCoroutine(TurnOffTheCanvasAfterAnimation(bar));
    }

    private IEnumerator TurnOffTheCanvasAfterAnimation(EnemyHealthBarBehaviour barBehaviour)
    {
        yield return new WaitWhile(() => !barBehaviour.AnimationComplete);

        if(this.Health == 0)
        {
            this.enemyHealthStatsCanvas.alpha = 0;
        }
    }

    public void Deactivate()
    {
        this.enemyHealthStatsCanvas.alpha = 0;
        this.enemyController.GameManager.GamePaused -= OnGamePaused;
        this.enemyController.GameManager.GameStarted -= OnGameStarted;
    }
}
