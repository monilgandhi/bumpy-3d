using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class EnemyHealthBarBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image fillBar;

    public bool AnimationComplete { get ; private set; }

    private void Awake() 
    {
        fillBar.fillAmount = 0;    
    }

    public void Init()
    {
        this.gameObject.SetActive(true);
        BumperCopsDebug.Log("Setting the gameobject to true");
        fillBar.fillAmount = 1; 
    }

    public IEnumerator GotHit()
    {
        if(fillBar.fillAmount < 1)
        {
            yield break;
        }

        AnimationComplete = false;
        StartCoroutine(AnimateScaleAcrossAllAxes(1.2f));
        while(fillBar.fillAmount > 0)
        {
            fillBar.fillAmount -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(AnimateScaleAcrossAllAxes(1.0f));
        AnimationComplete = true;
    }

    private IEnumerator AnimateScaleAcrossAllAxes(float scaleValue)
    {
        RectTransform r = this.gameObject.GetComponent<RectTransform>();

        bool deduct = r.localScale.x > scaleValue;
        while((r.localScale.x > scaleValue && deduct) ||  (r.localScale.x < scaleValue && !deduct))
        {
            float newScale = deduct ? r.localScale.x - Time.deltaTime : r.localScale.y + Time.deltaTime;
            r.localScale = new Vector3(newScale, newScale, 1);
            yield return new WaitForEndOfFrame();
        }
    }
}