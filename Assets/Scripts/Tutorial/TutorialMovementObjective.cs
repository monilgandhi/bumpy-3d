using System;
using System.Collections;
using UnityEngine;

public class TutorialMovementObjective : MonoBehaviour, ITutorialObjectiveStep
{
    public TutorialStep MyStep { get; private set; }
    public event Action ObjectiveComplete;
    private RoadManager roadManager;
    private TutorialGoalsBehaviour goalsBehaviour;
    private CoinBonusBehaviour bonusBehaviour;

    private Texture2D tutorialMap;
    public bool IsObjectiveActive { get; private set; }
    public bool IsObjectiveComplete { get; private set; }
    public bool LockOnCurrentStep { get; private set; }
    private int coinsRequired = 10;
    private TutorialObjectiveFactoryBehaviour factoryBehaviour;

    private void Awake() 
    {
        LockOnCurrentStep = true;
        this.MyStep = TutorialStep.MOVEMENT;
    }

    public void Init(TutorialObjectiveFactoryBehaviour factoryBehaviour)
    {
        this.factoryBehaviour = factoryBehaviour;
        this.roadManager = factoryBehaviour.Road;
        this.goalsBehaviour = factoryBehaviour.GoalsBehaviour;
        tutorialMap = CoinBonusBehaviour.LoadBonusMaps("bonusmap_tutorial")[0];
        this.bonusBehaviour = factoryBehaviour.BonusBehaviour;
        this.bonusBehaviour.BonusEnded += OnBonusEnded;
        factoryBehaviour.PowerupManager.PowerupCollected += OnPowerupCollected;
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.COINS)
        {
            return;
        }

        if(coinsRequired >= 0)
        {
            --coinsRequired;
            StartCoroutine(this.goalsBehaviour.CompleteNextGoal(coinsRequired));
        }
        
        if(coinsRequired == 0)
        {
            StartCoroutine(WaitForAnimation());
        }
    }
    
    public void Activate()
    {
        PlaceCoins();
        IsObjectiveActive = true;
        goalsBehaviour.SetUpCheckMarks(coinsRequired, this, "Collect", "coins");
    }

    private void PlaceCoins()
    {
        RoadController road = this.roadManager.GetRoadForBonuses();
        Color[] tutorialMapBitsMap = tutorialMap.GetPixels();
        bonusBehaviour.PlaceBonus(road, tutorialMapBitsMap);
    }

    private IEnumerator WaitForAnimation()
    {
        yield return new WaitUntil(() => factoryBehaviour.GoalsBehaviour.IsAnimationComplete);
        OnAllGoalsComplete();
    }
    
    private void OnAllGoalsComplete()
    {
        factoryBehaviour.PowerupManager.PowerupCollected -= OnPowerupCollected;
        IsObjectiveActive = false;
        ObjectiveComplete?.Invoke();

        this.bonusBehaviour.BonusEnded -= OnBonusEnded;
    }

    private void OnBonusEnded()
    {
        if(coinsRequired > 0)
        {
            PlaceCoins();
            return;
        }
    }
}