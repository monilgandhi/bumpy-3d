using System;
public interface ITutorialObjectiveStep 
{
    TutorialStep MyStep { get; }
    bool IsObjectiveComplete { get; }
    bool IsObjectiveActive { get; }
    bool LockOnCurrentStep { get; }
    event Action ObjectiveComplete;
    void Activate();
    void Init(TutorialObjectiveFactoryBehaviour factoryBehaviour);
}