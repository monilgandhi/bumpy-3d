using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour 
{
    [SerializeField]
    private TouchManager touchManager;

    [SerializeField]
    private CinematicBars cinematicBars;

    [SerializeField]
    private ObstacleManager obstacleManager;

    public delegate void TutorialEventHandler(TutorialArgs args);
    public event TutorialEventHandler TutorialStarted;
    public event TutorialEventHandler TutorialEnd;
    public event TutorialEventHandler TutorialStepUpdate;

    private GameManager gameManager;
    private bool isStepActive;
    private TutorialStep currentStep;
    private bool isNewSession;
    private bool isTutorialActive;
    private ObstacleController firstObstacle;

    // sometimes the event fires a bit late and we may not get in time.
    private bool firstObstacleReceived;
    private ITutorialObjectiveStep objectiveStep;
    private TutorialObjectiveFactoryBehaviour tutorialObjectiveStepFactory;
    private void Start() 
    {
        this.touchManager.Tapped += OnTapped;
        this.touchManager.SwipeUp += OnTapped;
        this.gameManager = this.transform.root.GetComponent<GameManager>();
        this.gameManager.GameTutorialSessionInitialized += OnGameTutorialInitialized;
        this.tutorialObjectiveStepFactory = this.GetComponent<TutorialObjectiveFactoryBehaviour>();
    }

    private void OnFirstObstacleGenerated(ObstacleEventArgs args)
    {
        BumperCopsDebug.Log("TutorialManager: First obstacle is " + args.Obstacle.name);
        this.firstObstacle = args.Obstacle;
        firstObstacleReceived = true;
        this.obstacleManager.FirstObstacleGenerated -= OnFirstObstacleGenerated;
    }

    private void OnGameStarted(GameEventArgs args)
    {
        BumperCopsDebug.Log("starting a new session");
        isNewSession = args.NewSessionStarted;
        OnCinematicBarsHidden();
        this.gameManager.GameStarted -= OnGameStarted;
    }
    
    private void OnGameTutorialInitialized(GameEventArgs args)
    {
        BumperCopsDebug.Log("TutorialManager: Starting tutorial");
        isTutorialActive = true;
        this.gameManager.GameStarted += OnGameStarted;
        // this.cinematicBars.CinematicBarsHidden += OnCinematicBarsHidden;
        this.obstacleManager.FirstObstacleGenerated += OnFirstObstacleGenerated;

        TutorialStarted?.Invoke(new TutorialArgs(isTutorialActive, currentStep));

        this.gameManager.EnemyManager.EnemySpawned += OnEnemySpawned;
    }

    private void OnCinematicBarsHidden()
    {
        if(isTutorialActive && isNewSession)
        {
            this.StartTutorial();
        }

        this.cinematicBars.CinematicBarsHidden -= OnCinematicBarsHidden;
    }

    private void LateUpdate() 
    {
        // handle the obstacle event
        if(currentStep < TutorialStep.OBSTACLE_GENERATION)
        {
            return;
        }    

        if(currentStep == TutorialStep.OBSTACLES_INSTRUCTION && !isStepActive)
        {
            if(firstObstacle != null)
            {
                Vector3 obstacleZ = new Vector3(0, 0, this.firstObstacle.transform.position.z);
                Vector3 playerZ = new Vector3(0, 0, this.gameManager.Player.transform.position.z);

                //BumperCopsDebug.Log("TutorialManager: Distance is " + Vector3.Distance(obstacleZ, playerZ));
                if(Vector3.Distance(obstacleZ, playerZ) <= 150)
                {
                    ActivateAndSendEvent();
                    isStepActive = true;
                }
            }
            else if(firstObstacleReceived && !isStepActive)
            {
                ActivateAndSendEvent();
                isStepActive = true;
            }
        }
    }

    private void OnTapped(TapGestureArgs args)
    {
        if(!isStepActive)
        {
            return;
        }

        if(objectiveStep == null || !objectiveStep.IsObjectiveActive)
        {
            MoveToNextStep();
        }
    }

    private void MoveToNextStep()
    {
        isStepActive = false;

        // mark the current step complete

        bool isTutorialActive = currentStep != TutorialStep.OVER;
        TutorialStepUpdate?.Invoke(new TutorialArgs(isTutorialActive, true, currentStep));

        ++currentStep;
        BumperCopsDebug.Log("TutorialManager: Current INACTIVE step is " + currentStep);
        ActivateNextStep();
    }

    private void ActivateNextStep()
    {
        if(this.isStepActive)
        {
            return;
        }

        // special handling for certain events
        switch(this.currentStep)
        {
            case TutorialStep.DRIVE:
            case TutorialStep.NEAR_MISS_INSTRUCTION:
            case TutorialStep.BOOST_SPAWNED:
                // fire off immediately
                ActivateAndSendEvent();
                break;

            case TutorialStep.OBSTACLE_GENERATION:
            case TutorialStep.NEAR_MISS_OBJECTIVE:
            case TutorialStep.ATTACK_ZONE_OBJECTIVE:
            case TutorialStep.MOVEMENT:
            case TutorialStep.HIT:
            case TutorialStep.ENEMY_GENERATION:
            //case TutorialStep.POWERUP_USE_OBJECTIVE:
                // for all objectives we do not pause the game
                ActivateAndSendEvent(false);
                break;

            case TutorialStep.BUMP_INITIATION:
                this.gameManager.EnemyManager.EnemySpawned -= OnEnemySpawned;

                this.gameManager.Player.AttackController.PlayerEnteredAttackZone += OnPlayerEnteredAttackZone;
                this.gameManager.Player.AttackController.AttackReady += OnAttackMeterFilled;
                this.gameManager.EnemyManager.EnemyHealthOver += OnEnemyHealthOver;
              
                break;
            // this is activated when the next enemy is spawned
            case TutorialStep.ENEMY_DESTROYED:
                this.gameManager.Player.AttackController.AttackReady -= OnAttackMeterFilled;
                break;

            // this event is activated with enemy spawning
            case TutorialStep.DONUT_GENERATION_INIT:
                BumperCopsDebug.Log("Donut generation init");
                ActivateAndSendEvent(false);
                this.gameManager.PowerupManager.PowerupSpawned += OnPowerupSpawned;
                this.gameManager.PowerupManager.TimedPowerupExpired += OnPowerupExpired;
                break;

            case TutorialStep.OVER:
                this.gameManager.EnemyManager.EnemyHealthOver -= OnEnemyHealthOver;
                ActivateAndSendEvent();
                TutorialEnd?.Invoke(new TutorialArgs(false, TutorialStep.OVER));
            
                break;
        }
    }

    private void OnPowerupSpawned(PowerupSpawnedEventArgs args)
    {
        if(args == null || args.Reward == null)
        {
            return;
        }

        if(args.Reward.Type != PowerupType.DONUT)
        {
            return;
        }

        //case TutorialStep.DONUT_INSTRUCTION:
        ActivateAndSendEvent();        
        this.gameManager.PowerupManager.PowerupSpawned -= OnPowerupSpawned;
    }
    private void OnEnemyHealthOver(EnemyArgs args)
    {
        BumperCopsDebug.Log("Enemy health over");
        ActivateAndSendEvent(false);
    }

    private void OnPowerupExpired(CollectiblePickedEventArgs args)
    {
        if (args.Type == PowerupType.BIG_BOOST)
        {
            this.gameManager.PowerupManager.TimedPowerupExpired -= OnPowerupExpired;
            ActivateAndSendEvent(false);
        }
    }

    private void OnPowerupReady(CollectiblePickedEventArgs args)
    {
        if (args.Type == PowerupType.BIG_BOOST)
        {
            // this should move to POWERUP_SELECTION_INIT from POWERUP_SELECTION_COMPLETE
            BumperCopsDebug.Log("Air support collected");
            //StartCoroutine(WaitAndActivate());
            ActivateAndSendEvent(false);
            this.gameManager.PowerupManager.PowerupReady -= OnPowerupReady;
        }
    }

    private void OnAttackMeterFilled()
    {
        this.gameManager.Player.AttackController.AttackReady -= OnAttackMeterFilled;
        ActivateAndSendEvent();
    }

    private void OnPlayerEnteredAttackZone()
    {
        this.gameManager.Player.AttackController.PlayerEnteredAttackZone -= OnPlayerEnteredAttackZone;
        // move to next event
        ActivateAndSendEvent();
    }

    private void OnBonusEnded()
    {
        BumperCopsDebug.Log("Bonus ended");
        this.gameManager.BonusBehaviour.BonusEnded -= OnBonusEnded;
        // move to next event
        ActivateAndSendEvent(false);
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        bool pause = currentStep == TutorialStep.ENEMY_SPAWNED;
        if(!pause)
        {
            BumperCopsDebug.Log($"Activating step {currentStep}");
            ActivateAndSendEvent(false);
            return;
        }

        // move to next event
        StartCoroutine(WaitAndActivate());
    }

    private IEnumerator WaitAndActivate(int seconds = 2)
    {
        yield return new WaitForSeconds(seconds);
        ActivateAndSendEvent();
    }

    private void ActivateAndSendEvent(bool pauseGame = true)
    {
        if(isStepActive)
        {
            return;
        }

        isStepActive = true;

        if(currentStep != TutorialStep.OVER)
        {
            BumperCopsDebug.Log("TutorialManager: Sending step ACTIVE event for " + currentStep);
            GetAndActivateNextObjectiveStep();
            TutorialStepUpdate(new TutorialArgs(true, currentStep, pauseGame));
        }
        else
        {
            BumperCopsDebug.Log("TutorialManager: Sending completed event for step " + currentStep);
            // mark completed
            TutorialStepUpdate?.Invoke(new TutorialArgs(false, currentStep));
        }

        if(!pauseGame && objectiveStep == null)
        {
            BumperCopsDebug.Log($"TutorialManager: Moving from {currentStep} to next Step {currentStep + 1}");
            MoveToNextStep();
        }
    }

    private void GetAndActivateNextObjectiveStep()
    {
        // if 1 objective is active we cannot assign another one
        if(objectiveStep != null && objectiveStep.IsObjectiveActive)
        {
            BumperCopsDebug.Log($"Objective is not complete {objectiveStep.MyStep}");
            return;
        }

        if(objectiveStep != null)
        {
            objectiveStep.ObjectiveComplete -= OnObjectiveComplete;
        }
        
        BumperCopsDebug.Log($"Looking for objective of step {currentStep}");
        objectiveStep = this.tutorialObjectiveStepFactory.GetBehaviour(currentStep);

        if(objectiveStep != null)
        {
            BumperCopsDebug.Log("Objective found, activating");
            objectiveStep.Activate();
            objectiveStep.ObjectiveComplete += OnObjectiveComplete;

            // if the objective is not active then we move to next step. 
            // This is the case when the objective spans over multiple steps. For instance arresting the robber
            // This objective starts at ATTACK_ZONE_OBJECTIVE and ends when robber is caught
            if(!objectiveStep.LockOnCurrentStep)
            {
                MoveToNextStep();
            }
        }
    }

    private void OnObjectiveComplete()
    {
        //BumperCopsDebug.Log($"Objective complete {o.MyStep}");
        MoveToNextStep();
    }
    
    private void StartTutorial()
    {
        ActivateAndSendEvent(false);
    }
}