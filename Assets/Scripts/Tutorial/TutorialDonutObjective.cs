using System;
using System.Collections;
using UnityEngine;

public class TutorialDonutObjective : MonoBehaviour, ITutorialObjectiveStep
{
    public TutorialStep MyStep { get; private set; }
    public event Action ObjectiveComplete;
    private PowerupManager powerupManager;
    private TutorialGoalsBehaviour goalsBehaviour;

    public bool IsObjectiveActive { get; private set; }
    public bool IsObjectiveComplete { get; private set; }
    public bool LockOnCurrentStep { get; private set; }
    private TutorialObjectiveFactoryBehaviour factoryBehaviour;

    private void Awake() 
    {
        LockOnCurrentStep = true;
        //this.MyStep = TutorialStep.POWERUP_USE_OBJECTIVE;
    }

    public void Init(TutorialObjectiveFactoryBehaviour factoryBehaviour)
    {
        this.factoryBehaviour = factoryBehaviour;
        this.goalsBehaviour = factoryBehaviour.GoalsBehaviour;
        factoryBehaviour.PowerupManager.TimedPowerupExpired += OnPowerupCompleted;
    }

    private void OnPowerupCompleted(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.BIG_BOOST)
        {
            return;
        }

        StartCoroutine(ShowPowerupCompleted());
    }

    private IEnumerator ShowPowerupCompleted()
    {
        StartCoroutine(factoryBehaviour.GoalsBehaviour.CompleteNextGoal(0));
        yield return new WaitUntil(() => factoryBehaviour.GoalsBehaviour.IsAnimationComplete);
        OnAllGoalsComplete();
    }
    
    public void Activate()
    {
        IsObjectiveActive = true;
        goalsBehaviour.SetUpCheckMarks(1, this, "Use", "Powerup (<color=#FFC200><b>Tap</color></b> to use)");
    }
    
    private void OnAllGoalsComplete()
    {
        factoryBehaviour.PowerupManager.TimedPowerupExpired -= OnPowerupCompleted;
        IsObjectiveActive = false;
        ObjectiveComplete?.Invoke();
    }
}