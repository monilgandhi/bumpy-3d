using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialBehaviour : MonoBehaviour 
{
    [Header("UI elements")]
    [SerializeField]
    private Animator handAnimator;

    [SerializeField]
    private TutorialManager tutorialManager;

    [SerializeField]
    private CanvasGroup info;

    [SerializeField]
    private TMP_Text infoText;

    [SerializeField]
    private TMP_Text tapText;

    private Image lightBox;
    private const string DRIVE_TEXT = "Slide your <color=#0084FF><b>fingers</b></color> to move the <color=#0084FF><b>car</b></color>";
    private const string NEAR_MISS_TEXT = "Drive close to <color=#0084FF><b>civilian cars</b></color> to <color=#0084FF><b>Over take</b></color> and get coins.";
    private const string OBSTACLES_TEXT = "<color=red><b>Avoid </b></color> hitting <color=#0084FF><b>civilian cars</b></color>.";
    private const string DONUT_TEXT = "Collect donuts for <color=#0084FF><b>powerups</b></color>";
    //private const string BONUS_TEXT = "With near miss you can fill the <color=#0084FF><b>Bonus Bar</b></color> and increase the <color=#0084FF><b>Multiplier</b></color>.";
    //private const string BONUS_RESET_TEXT = "Hitting any vehicle will <color=#0084FF><b>reset</b></color> <color=#0084FF><b>Bonus Bar</b></color>.";
    private const string ENEMY_TEXT = "We have reports of a <color=#0084FF><b>criminal</b></color> on the run. Let's <color=#0084FF><b>catch</b></color> the criminal";
    private const string BOOST_TEXT = "Collect <color=#0084FF><b>arrows</b></color> to get <color=#0084FF><b>Speed burst.</b>";
    //private const string CLOCK_TEXT = "We need to catch the robber before the <color=#0084FF><b>Time</b></color> runs out.";
    private const string BUMP_FILL_TEXT = "Awesome!! Stay in the <color=#0084FF><b>circle</b></color> to attack.";
    private const string POWERUP_USE = "Tap to use <color=#0084FF><b>powerup</b></color>";
    //private const string ENEMY_DESTROYED_TEXT = "Great job on catching the robber. Looks like this robber has a <color=#0084FF><b>Bounty</b></color> on them.";
    //private const string UPGRADE_TEXT = "Use <color=#FF7800><b>Coins</b></color> to upgrade your car. You can <color=#0084FF><b>Upgrade or Buy your</b></color> car by visiting the garage.";
    private const string TUTORIAL_OVER_TEXT = "You are ready officer. Be the <color=#0084FF><b>hero</b></color> the city needs.";
      
    private bool shouldRunTutorial = false;
    private void Start() 
    {
        this.tutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;  
        this.lightBox = this.GetComponent<Image>();
        this.handAnimator.gameObject.SetActive(false);
        ToggleInfo(false); 
    }     

    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        BumperCopsDebug.Log($"TutorialBehaviour: tutorial step update {args.TutorialStep}. is active {args.IsTutorialStepActive}");
        switch(args.TutorialStep)
        {                
            case TutorialStep.DRIVE:
                ToggleAnimation(args.TutorialStep, args.IsTutorialStepActive);
                this.infoText.text = DRIVE_TEXT;
                ToggleInfo(args.PauseGame); 
                break;

            case TutorialStep.OBSTACLES_INSTRUCTION:
                this.infoText.text = OBSTACLES_TEXT;
                ToggleInfo(args.PauseGame); 
                break;
                
            case TutorialStep.NEAR_MISS_INSTRUCTION:
                this.infoText.text = NEAR_MISS_TEXT;
                ToggleInfo(args.PauseGame); 
                break;

            case TutorialStep.ENEMY_SPAWNED:
                this.infoText.text = ENEMY_TEXT;
                ToggleInfo(args.PauseGame); 
                break;

            case TutorialStep.BOOST_SPAWNED:
                this.infoText.text = BOOST_TEXT;
                ToggleInfo(args.PauseGame); 
                break;

            case TutorialStep.BUMP_INITIATION:
                //ToggleAnimation(args.TutorialStep, args.IsTutorialStepActive);
                this.infoText.text = BUMP_FILL_TEXT;
                ToggleInfo(args.PauseGame); 
                break;

            /*case TutorialStep.POWERUP_USE_INSTRUCTION:
                ToggleAnimation(args.TutorialStep, args.IsTutorialStepActive);
                this.infoText.text = POWERUP_USE;
                ToggleInfo(args.PauseGame); 
                break;*/
                
            case TutorialStep.DONUT_INSTRUCTION:
                this.infoText.text = DONUT_TEXT;
                ToggleInfo(args.PauseGame); 
                break;
      
            case TutorialStep.OVER:
                this.infoText.text = TUTORIAL_OVER_TEXT;
                ToggleInfo(args.PauseGame); 
                break;
        }

    }

    private void ToggleInfo(bool enable)
    {
        this.info.alpha = Convert.ToInt16(enable);
        this.info.gameObject.SetActive(enable);
        this.tapText.gameObject.SetActive(enable);       
        this.lightBox.enabled = enable;
    }

    public void ToggleSwipeAnimation(bool enable)
    {
        if(!enable)
        {
            this.handAnimator.gameObject.SetActive(false);
            return;
        }

        this.handAnimator.gameObject.SetActive(true);
        this.handAnimator.SetTrigger("swipeUp");
    }

    private void ToggleAnimation(TutorialStep currentStep, bool isStepActive)
    {
        if(!isStepActive)
        {
            this.handAnimator.gameObject.SetActive(false);
            return;
        }

        string state = null;
        switch(currentStep)
        {
            case TutorialStep.DRIVE:
                state = "slide";
                break;
            /*case TutorialStep.POWERUP_USE_INSTRUCTION:
                state = "singletap";
                break;
            //case TutorialStep.BUMP_FILL:
             //   state = "swipeUp";
             //   break;
            case TutorialStep.POWERUP_USE_OBJECTIVE:
                state = "swipeUp";
                break;*/
        }

        if(string.IsNullOrEmpty(state))
        {
            return;
        }

        this.handAnimator.gameObject.SetActive(true);
        this.handAnimator.SetTrigger(state);
    }
}