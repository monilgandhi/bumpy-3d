using System.Collections.Generic;
using UnityEngine;

public class TutorialObjectiveFactoryBehaviour : MonoBehaviour 
{
    [SerializeField]
    private RoadManager roadManager;
    public RoadManager Road { get { return roadManager; }}

    [SerializeField]
    private TutorialGoalsBehaviour tutorialGoalsBehaviour;
    public TutorialGoalsBehaviour GoalsBehaviour { get { return tutorialGoalsBehaviour; }}

    [SerializeField]
    private GameManager gameManager;
    public CoinBonusBehaviour BonusBehaviour { get { return gameManager.BonusBehaviour; }}
    public PowerupManager PowerupManager { get { return gameManager.PowerupManager; }}
    public PlayerController Player { get { return gameManager.Player; }}
    public EnemyManager EnemyManager { get { return gameManager.EnemyManager; }}
    public TutorialManager TutorialManager { get { return gameManager.TutorialManager; }}
    private Dictionary<TutorialStep, ITutorialObjectiveStep> tutorialObjectiveDictionary = new Dictionary<TutorialStep, ITutorialObjectiveStep>();
    private void Start() 
    {
        ITutorialObjectiveStep driveStep = this.GetComponent<TutorialMovementObjective>();
        tutorialObjectiveDictionary.Add(driveStep.MyStep, driveStep);

        //ITutorialObjectiveStep nearMiss = this.GetComponent<TutorialNearMissObjective>();
        //tutorialObjectiveDictionary.Add(nearMiss.MyStep, nearMiss);

        ITutorialObjectiveStep attackReady = this.GetComponent<TutorialCatchRobberObjective>();
        tutorialObjectiveDictionary.Add(attackReady.MyStep, attackReady);

        //ITutorialObjectiveStep powerupUse = this.GetComponent<TutorialDonutObjective>();
        //tutorialObjectiveDictionary.Add(powerupUse.MyStep, powerupUse);
    }    

    public ITutorialObjectiveStep GetBehaviour(TutorialStep step)
    {
        ITutorialObjectiveStep tutorialObjectiveBehaviour;
        if(tutorialObjectiveDictionary.TryGetValue(step, out tutorialObjectiveBehaviour))
        {
            BumperCopsDebug.Log($"found for Step is {step}");
            tutorialObjectiveBehaviour.Init(this);
        }
        
        return tutorialObjectiveBehaviour;
    }
}