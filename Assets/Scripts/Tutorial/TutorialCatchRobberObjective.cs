using System;
using System.Collections;
using UnityEngine;

public class TutorialCatchRobberObjective : MonoBehaviour, ITutorialObjectiveStep
{
    public TutorialStep MyStep { get; private set; }
    public bool IsObjectiveComplete { get; private set; }
    public bool IsObjectiveActive { get; private set; }
    public bool LockOnCurrentStep { get; private set; }
    public event Action ObjectiveComplete;
    public event Action ObjectiveNearCompletion;

    private TutorialObjectiveFactoryBehaviour factoryBehaviour;

    private void Awake() 
    {
        LockOnCurrentStep = false;
        MyStep = TutorialStep.ATTACK_ZONE_OBJECTIVE;    
    }

    public void Activate()
    {
        factoryBehaviour.GoalsBehaviour.SetUpCheckMarks(this, "Arrest the robber");
    }

    public void Init(TutorialObjectiveFactoryBehaviour factoryBehaviour)
    {
        this.factoryBehaviour = factoryBehaviour;
        this.factoryBehaviour.EnemyManager.EnemyHealthOver += OnEnemyHealthOver;
        factoryBehaviour.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
    }

    private void OnTutorialStepUpdate(TutorialArgs a)
    {
        if(a.TutorialStep == TutorialStep.HIT)
        {
            LockOnCurrentStep = true;
        }
    }

    private void OnAllGoalsComplete()
    {
        ObjectiveComplete?.Invoke();

        // deregister the event handler
        this.factoryBehaviour.EnemyManager.EnemyHealthOver -= OnEnemyHealthOver;
    }

    private void OnEnemyHealthOver(EnemyArgs e)
    {
        StartCoroutine(EnemyHealthOver());
    } 

    private IEnumerator EnemyHealthOver()
    {
        StartCoroutine(factoryBehaviour.GoalsBehaviour.CompleteNextGoal(0));
        yield return new WaitUntil(() => factoryBehaviour.GoalsBehaviour.IsAnimationComplete);
        OnAllGoalsComplete();
    }
}