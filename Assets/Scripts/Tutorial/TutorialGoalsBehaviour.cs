using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class TutorialGoalsBehaviour : MonoBehaviour 
{
    //public delegate void TutorialGoalsEventHandler();
    //public event TutorialGoalsEventHandler AllGoalsComplete;
    [SerializeField]
    private Image checkMarkImage;

    [SerializeField]
    private TMP_Text goal;
    private ITutorialObjectiveStep currentObjectiveStep;
    private string currentGoalPrefixText;
    private string currentGoalSuffixText;
    private CanvasGroup canvasGroup;
    public bool IsAnimationComplete { get; private set;}
    private Animator animator;
    private void Awake() 
    {
        this.canvasGroup = this.GetComponent<CanvasGroup>();
        this.canvasGroup.alpha = 0;
        animator = this.GetComponent<Animator>();
    }   

    // prefixText is part of text before goalCount
    // suffixText is part of text after goalCount
    public void SetUpCheckMarks(int goalCount, ITutorialObjectiveStep objectiveStep, string prefixText, string suffixText)
    {
        if(goalCount <= 0 || string.IsNullOrEmpty(prefixText))
        {
            BumperCopsDebug.LogWarning("ArgumentException: goalCount or prefixText");
            return;
        }

        IsAnimationComplete = false;
        //BumperCopsDebug.Log($"Tutorial: Setting up check marks for {objectiveStep.GetType()}, goal {goalCount}");
        currentGoalPrefixText = prefixText;
        currentGoalSuffixText = suffixText;

        if(objectiveStep != null)
        {
            currentObjectiveStep = objectiveStep;
        }

        this.canvasGroup.alpha = 1;
        goal.text = String.Format("{0} <color=#FFC200>{1}</color> {2}", currentGoalPrefixText, goalCount, currentGoalSuffixText);

        /*if(objectiveStep.MyStep == TutorialStep.POWERUP_USE_OBJECTIVE)
        {
            animator.SetTrigger("powerup");
        }*/
    }

    public void SetUpCheckMarks(ITutorialObjectiveStep objectiveStep, string plainText)
    {
        IsAnimationComplete = false;
        //BumperCopsDebug.Log("Tutorial: Setting up check marks");
        currentGoalPrefixText = plainText;

        if(objectiveStep != null)
        {
            currentObjectiveStep = objectiveStep;
        }

        this.canvasGroup.alpha = 1;
        goal.text = plainText;
    }

    public IEnumerator CompleteNextGoal(int newGoalCount)
    {
        //BumperCopsDebug.Log($"Current count before is {newGoalCount} for objective {currentObjectiveStep.GetType()}");
        if(newGoalCount > 0)
        {
            goal.text = String.Format("{0} <color=#FFC200>{1}</color> {2}", currentGoalPrefixText, newGoalCount, currentGoalSuffixText);
            yield break;
        }
        else
        {
            animator.SetTrigger("complete");
            yield return new WaitForSeconds(1.5f);

            //BumperCopsDebug.Log("Completing the goal");
            this.canvasGroup.alpha = 0;
            IsAnimationComplete = true;
        }
    }
}