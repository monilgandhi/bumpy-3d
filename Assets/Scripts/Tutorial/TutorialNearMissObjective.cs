using System;
using UnityEngine;

public class TutorialNearMissObjective : MonoBehaviour, ITutorialObjectiveStep
{
    public TutorialStep MyStep { get; private set; }
    public bool IsObjectiveComplete { get; private set; }
    public bool IsObjectiveActive { get; private set; }
    public bool LockOnCurrentStep { get; private set; }
    public event Action ObjectiveComplete;
    public event Action ObjectiveNearCompletion;

    private TutorialObjectiveFactoryBehaviour factoryBehaviour;
    private int requiredNearMissVehicles = 3;
    private void Awake() 
    {
        MyStep = TutorialStep.NEAR_MISS_OBJECTIVE;    
    }

    public void Activate()
    {
        LockOnCurrentStep = true;
        IsObjectiveActive = true;
        factoryBehaviour.GoalsBehaviour.SetUpCheckMarks(requiredNearMissVehicles, this, "Drive close to", "civilians");
    }

    public void Init(TutorialObjectiveFactoryBehaviour factoryBehaviour)
    {
        this.factoryBehaviour = factoryBehaviour;
        foreach(NearMissBehaviour n in this.factoryBehaviour.Player.NearMiss)
        {
            n.NearMiss += OnNearMiss;
        }
    }

    private void OnAllGoalsComplete()
    {
        IsObjectiveActive = false;
        ObjectiveComplete?.Invoke();

        // deregister the event handler
        foreach(NearMissBehaviour n in this.factoryBehaviour.Player.NearMiss)
        {
            n.NearMiss -= OnNearMiss;
        }
    }

    private void OnNearMiss(NearMissEventArgs args)
    {
        --requiredNearMissVehicles;
        StartCoroutine(factoryBehaviour.GoalsBehaviour.CompleteNextGoal(requiredNearMissVehicles));

        if(requiredNearMissVehicles == 0)
        {
            OnAllGoalsComplete();
        }
    }
}