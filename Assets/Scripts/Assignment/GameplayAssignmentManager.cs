using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

[RequireComponent(typeof(GameManager))]
public class GameplayAssignmentManager : MonoBehaviour 
{
    public event Action LoadComplete;
    public event Action<ObjectiveCompleteEventArgs> ObjectiveComplete;
    public event Action<ObjectiveCompleteEventArgs> ObjectiveSkipped;

    [Header("Start game UI")]
    [SerializeField]
    protected CanvasGroup objectiveCanvasGroup;

    [SerializeField]
    private MissionAndScorePanelBehaviour missionAndScorePanel;

    [SerializeField]
    private InGameShopManager inGameShopManager;
    public Assignment CurrentAssignment { get; protected set; }
    private List<AbstractAssignmentObjective> currentObjectives = new List<AbstractAssignmentObjective>();
    private const string ASSIGNMENT_FOLDER_NAME = "assignments";
    private GameManager gameManager;
    
    private void Start() 
    {
        this.gameManager = this.GetComponent<GameManager>();

        if(objectiveCanvasGroup != null)
        {
            objectiveCanvasGroup.alpha = 0;        
        }
    
        string currentAssignmentId = MissionStats.Instance.CurrentAssignmentId;
        BumperCopsDebug.Log($"GameplayAssignmentManager: Current assignment is {currentAssignmentId}");

        if(MissionStats.Instance.IsLastAssignment(currentAssignmentId))
        {
            LoadComplete?.Invoke();
            return;
        }

        CurrentAssignment = LoadAssignment(currentAssignmentId);
        missionAndScorePanel.MissionSkipped += OnMissionSkipped;
        RegisterEventListenersForObjectives();
    }    
    
    private void OnMissionSkipped(MissionSkippedEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.AssignmentId) || string.IsNullOrEmpty(args.ObjectiveId))
        {
            BumperCopsDebug.LogError("Mission skipped args are invalid");
            return;
        }

        if(!args.AssignmentId.Equals(CurrentAssignment.Id))
        {
            BumperCopsDebug.LogWarning("Assignment Ids do not match");
            return;
        }

        foreach(AbstractAssignmentObjective ob in currentObjectives)
        {
            if(!ob.AssignmentObjective.Id.Equals(args.ObjectiveId))
            {
                continue;
            }

            // we could hook up ingame manger to objective complete event. However, it did not 
            // seem right to make dpeendency of assignment manager in ingamshopmanager
            inGameShopManager.ObjectiveSkipped(ob.AssignmentObjective.SkipCost, ob.AssignmentObjective.Id);
            ob.ForceCompleteForCoins();
            OnObjectiveCompleted(ob);
        }
    }

    private void RegisterEventListenersForObjectives()
    {
        CurrentAssignment.Objectives.ForEach(f => 
        {
            AbstractAssignmentObjective objective = (AssignmentObjectiveFactory.GetClass(f, this.gameManager.Player, 
                this.gameManager.EnemyManager, this.gameManager.ScoreBehaviour, this.gameManager.CoinCollectibleManager, this.gameManager));

            objective.ObjectiveComplete += (args) => OnObjectiveCompleted(args);
            objective.ObjectiveUpdated += OnObjectiveUpdated;

            currentObjectives.Add(objective);
        });
    }

    private void OnObjectiveUpdated(AbstractAssignmentObjective obj)
    {
        BumperCopsDebug.Log($"GameplayAssignmentManager: obj updated for id {obj.AssignmentObjective.Id}");
        foreach(AssignmentObjectives actualObj in CurrentAssignment.Objectives)
        {
            if(actualObj.Id.Equals(obj.AssignmentObjective.Id))
            {
                actualObj.PlayerCount = obj.CompletedValue;
            }
        }
    }

    private void OnObjectiveCompleted(AbstractAssignmentObjective obj)
    {   
        foreach(AssignmentObjectives actualObj in CurrentAssignment.Objectives)
        {
            if(actualObj.Id.Equals(obj.AssignmentObjective.Id))
            {
                actualObj.CompletedInThisSession = true;
                actualObj.PlayerCount = actualObj.Count;
            }
        }

        ObjectiveComplete?.Invoke(new ObjectiveCompleteEventArgs(
            obj.AssignmentObjective, 
            CurrentAssignment.Id,
            obj.IsObjectiveSkippedForCoins));
        StartCoroutine(DisplayCompletedObjective(obj.AssignmentObjective.Description));
        obj.ObjectiveComplete -= OnObjectiveCompleted;
    }

    public void MarkAssignmentComplete()
    {
        if(CurrentAssignment == null)
        {
            return;
        }

        if(!CheckIfAssignmentComplete())
        {
            BumperCopsDebug.LogWarning("Cannot mark assignment complete");
            return;
        }

        CurrentAssignment.IsAssignmentComplete = true;
        MissionStats.Instance.UpdateAssignmentIdForCurrentCity(CurrentAssignment.NextAssignmentId);

        if(!MissionStats.Instance.IsLastAssignment(CurrentAssignment.NextAssignmentId))
        {
            CurrentAssignment = LoadAssignment(CurrentAssignment.NextAssignmentId);
            RegisterEventListenersForObjectives();
        }
        else
        {
            CurrentAssignment = null;
        }
    }

    protected bool CheckIfAssignmentComplete()
    {
        int completedObjectives = 0;
        foreach(AssignmentObjectives actualObj in CurrentAssignment.Objectives)
        {
            if(actualObj.IsComplete)
            {
                ++completedObjectives;
            }
        }

        BumperCopsDebug.Log($"Completed count is {completedObjectives} and objective count is {CurrentAssignment.Objectives.Count}");
        return (completedObjectives == CurrentAssignment.Objectives.Count);
    }

    private IEnumerator DisplayCompletedObjective(string completedObject)
    {
        if(!objectiveCanvasGroup.gameObject.activeInHierarchy)
        {
            yield break;
        }
        
        objectiveCanvasGroup.GetComponentInChildren<TMP_Text>().text = $"<s>{completedObject}</s>";
        objectiveCanvasGroup.alpha = 1;

        yield return new WaitForSecondsRealtime(1f);

        while(objectiveCanvasGroup.alpha > 0)
        {
            objectiveCanvasGroup.alpha -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }
    
    public static Assignment LoadAssignment(string id)
    {
        string filePath = ASSIGNMENT_FOLDER_NAME + 
            Path.DirectorySeparatorChar + 
            PlayerStats.Instance.CurrentCity;

        //BumperCopsDebug.Log($"Json for assignment is {json}");
        Assignment requestedAssignment;
        if(!ConfigCache.TryGetConfig<Assignment>(id, filePath, out requestedAssignment))
        {
            return requestedAssignment;
        }

        requestedAssignment.Objectives.ForEach(o => 
        {
            // load other metadata
            o.PlayerCount = MissionStats.Instance.GetObjectiveScore(o.Id);
            o.CompletedInThisSession = false;
        });

        return requestedAssignment;
    }

    public static string GetAssignmentName(int assignmentNumber)
    {
        if(assignmentNumber <= 0)
        {
            return null;
        }

        return $"Mission {assignmentNumber}";
    }
}

public class AssignmentCompleteEventArgs
{
    public readonly Assignment CompletedAssignment;
    public readonly string NextAssignmentId;

    public AssignmentCompleteEventArgs(Assignment completedAssignment,
                                        string nextAssingmentId)
    {
        this.CompletedAssignment = completedAssignment;
        this.NextAssignmentId = nextAssingmentId;
    }
}

public class ObjectiveCompleteEventArgs
{
    public readonly AssignmentObjectives CompletedObjective;
    public bool ObjectiveSkipped;
    public readonly string AssignmentId;

    public ObjectiveCompleteEventArgs(
            AssignmentObjectives completedObjective, 
            string assignmentId, 
            bool objectiveSkipped)
    {
        this.CompletedObjective = completedObjective;
        this.AssignmentId = assignmentId;
        this.ObjectiveSkipped = objectiveSkipped;
    }
}