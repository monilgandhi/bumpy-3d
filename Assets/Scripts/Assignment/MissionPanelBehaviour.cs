using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// single mission panel behaviour (prefab)
public class MissionPanelBehaviour : MonoBehaviour 
{
    public Action<string> ObjectiveSkipped;

    [SerializeField]
    private TMP_Text objectiveTextField;

    [SerializeField]
    private Image completedFillImage;

    [SerializeField]
    private TMP_Text completionText;

    [SerializeField]
    private TMP_Text skipButtonText;

    [SerializeField]
    private Button skipButton;
    private int requiredCount;
    public string ObjectiveId { get; private set; }
    private void Awake() 
    {
        if(skipButton != null)
        {
            skipButton.onClick.AddListener(OnSkipMissionClicked);
        }
    }

    public void Load(string objectiveText, 
        int playerCount, 
        string objectiveId,
        int requiredCount, 
        int costToDismiss)
    {
        if(requiredCount <= 0)
        {
            BumperCopsDebug.LogWarning("required count cannot be less than 0");
            return;
        }

        this.requiredCount = requiredCount;
        this.ObjectiveId = objectiveId;
        completedFillImage.fillAmount = (float)playerCount / (float) requiredCount;
        completionText.text = $"{playerCount} / {requiredCount}";
        objectiveTextField.text = objectiveText;

        if(skipButton == null || skipButtonText == null)
        {
            return;
        }
        
        skipButtonText.text = $"Skip for {costToDismiss}";
        skipButton.gameObject.SetActive(playerCount < this.requiredCount && costToDismiss > 0);
        skipButton.interactable = PlayerStats.Instance.CoinCount >= costToDismiss;
    }

    public void LoadAndReplace(string objectiveText, 
        int playerCount, 
        string objectiveId, 
        int requiredCount)
    {
        this.GetComponent<Animator>().SetBool("replace", true);
        Load(objectiveText, playerCount, objectiveId, requiredCount, 0);
    }

    private void OnSkipMissionClicked()
    {
        ObjectiveSkipped?.Invoke(this.ObjectiveId);
        //this.completedFillImage.fillAmount = 1.0f;
        //completionText.text = $"{requiredCount} / {requiredCount}";
        //skipButton.gameObject.SetActive(false);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }
}