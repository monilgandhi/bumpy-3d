using System;
using UnityEngine;
public class MissionStats
{
    private string currentAssignmentId;

    public string CurrentAssignmentId 
    {
         get { return currentAssignmentId; }
         private set
         {
            currentAssignmentId = value;
         }
    }

    public bool IsLastAssignment(string assignmentId)
    {
        if(string.IsNullOrEmpty(assignmentId))
        {
            throw new ArgumentNullException(nameof(assignmentId));
        }

        return assignmentId.Equals("city1_8");
    }

    private static MissionStats instance;
    public static MissionStats Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new MissionStats();
            }

            return instance;
        }
    }

    private MissionStats()
    {
        if(string.IsNullOrEmpty(PlayerStats.Instance.CurrentCity))
        {
            BumperCopsDebug.LogWarning("Player city is not loaded");
            return;
        }

        LoadCity();
        CurrentAssignmentId = PersistenceManager.CurrentAssignmentId;
    }

    public void LoadCity()
    {
        PersistenceManager.currentCity = PlayerStats.Instance.CurrentCity;
    }

    public void UpdateObjectiveCompletionCount(int count, string objectiveId)
    {
        if(string.IsNullOrEmpty(objectiveId))
        {
            throw new ArgumentNullException(nameof(objectiveId));
        }

        PersistenceManager.SetObjectiveScore(objectiveId, count);
    }

    public int GetObjectiveScore(string objectiveId)
    {
        if(string.IsNullOrEmpty(objectiveId))
        {
            throw new ArgumentNullException(nameof(objectiveId));
        }

        return PersistenceManager.GetObjectiveScore(objectiveId);
    }

    public void UpdateAssignmentIdForCurrentCity(string assignmentId)
    {
        if(string.IsNullOrEmpty(assignmentId))
        {
            throw new ArgumentNullException(nameof(assignmentId));
        }

        LoadCity();
        PersistenceManager.CurrentAssignmentId = assignmentId;
        CurrentAssignmentId = assignmentId;
    }

    public void ResetAllObjectives(string assignmentId)
    {
        if(!CarUtils.IsUnityDevelopmentBuild())
        {
            BumperCopsDebug.LogWarning("this functionality is only available in development");
            return;
        }

        Assignment assignment = GameplayAssignmentManager.LoadAssignment(assignmentId);
        if(assignment == null)
        {
            BumperCopsDebug.LogWarning("Cannot find assignment");
            return;
        }

        foreach(AssignmentObjectives o in assignment.Objectives)
        {
            PersistenceManager.SetObjectiveScore(o.Id, 0);
        }
    }

    private static class PersistenceManager
    {
        
        public static string currentCity = "city1";
        private const string ASSIGNMENT_ID_PREFIX = "mission_{0}_{1}";
        private const string OBJECTIVE_ID_PREFIX = "objective_{0}_{1}";
        private const string DEFAULT_ASSIGNMENT_ID = "{0}_1";
        private const string VERSION = "v2";

        public static string CurrentAssignmentId
        {
            get
            {
                string key = String.Format(ASSIGNMENT_ID_PREFIX, currentCity, VERSION);
                return PlayerPrefs.GetString(key, String.Format(DEFAULT_ASSIGNMENT_ID, currentCity));
            }

            set
            {
                string key = String.Format(ASSIGNMENT_ID_PREFIX, currentCity, VERSION);
                //BumperCopsDebug.Log($"Storing new assignment for {key} with id {assignmentId}");
                PlayerPrefs.SetString(key, value);
                PlayerPrefs.Save();
            }
        }

        public static int GetObjectiveScore(string objectiveId)
        {
            string key = String.Format(OBJECTIVE_ID_PREFIX, objectiveId, VERSION);
            return PlayerPrefs.GetInt(key, 0);
        }

        public static void SetObjectiveScore(string objectiveId, int value)
        {
            string key = String.Format(OBJECTIVE_ID_PREFIX, objectiveId, VERSION);
            PlayerPrefs.SetInt(key, value);
            PlayerPrefs.Save();
        }
    }
}