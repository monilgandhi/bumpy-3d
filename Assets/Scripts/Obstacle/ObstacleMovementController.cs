using System;
using UnityEngine;
using System.Collections;

public class ObstacleMovementController : NPCMovementController 
{
    private ObstacleController controller;
    private bool isBeingDestroyed;
    private bool inited;
    public bool TurnInPlayersWay { get; private set; }

    private LaneDirection movementDirection;
    private int laneBeforeMegaSiren;
    public void Init(LaneDirection direction, bool canChangeLanes)
    {
        //BumperCopsDebug.Log($"Can change lanes {canChangeLanes}");
        vectorDirection = direction == LaneDirection.SAME ? Vector3.forward : Vector3.back;
        movementDirection = direction;
        
        laneBeforeMegaSiren = -1;
        if(!inited)
        {
            this.controller = this.transform.root.GetComponent<ObstacleController>() ?? throw new MissingComponentException(nameof(ObstacleController));
            this.Character = this.controller.Character;

            BoxCollider turnCollider = this.GetComponent<BoxCollider>();
            turnCollider.center = new Vector3(0, turnCollider.center.y, turnCollider.size.z / 2f);

            this.controller.ObstacleHitController.Destroyed += (args) => isBeingDestroyed = false;
            this.rigidBody = this.controller.RigidBody;

            if(this.rigidBody == null)
            {
                throw new MissingComponentException(nameof(Rigidbody));
            }
    
            this.turnWheels = this.controller.Character.Wheels;
            inited = true;
        }
        else
        {
            ResetValues();
        }

        switch(this.Character.ObstacleType)
        {
            case ObstacleType.ROAD_BLOCK:
                this.Speed = 0;
                break;

            default:
                if(this.Character.HeadLights != null)
                {
                    if(direction == LaneDirection.OPPOSITE)
                    {
                        // flicker the headlights
                        this.Character.HeadLights.StartWarningLights();
                    }
                    else
                    {
                        this.Character.HeadLights.StopLights();
                    }
                }

                this.Speed = Constants.NPC_SPEED; //Mathf.RoundToInt(UnityEngine.Random.Range(minSpeed, maxSpeed));
                if(this.turnWheels.Length == 0)
                {
                    throw new ArgumentNullException(nameof(turnWheels));
                }
                break;
        }
    
        isActive = true;
        this.canChangeLanes = canChangeLanes;
    }
        
    public void MoveToSide()
    {
        if(!this.isActive)
        {
            return;
        }

        LaneController lane = this.Lane;
        if(lane == null)
        {
            //BumperCopsDebug.LogWarning($"Lane not found {this.transform.root.name}, deactivating");
            //this.controller.Deactivate();
            return;
        }

        LaneController nextLane = lane.Road.GetEdgeLaneClosestToMe(lane);
        if(nextLane.Id.Equals(lane.Id))
        {
            return;
        }

        StartCoroutine(MakeTurn(nextLane, 0.5f, true));
        canChangeLanes = false;
    }

    protected override void ResetValues()
    {
        //BumperCopsDebug.Log($"ObstacleMovementController: Reset values for {this.transform.root.name}");
        isBeingDestroyed = false;
        TurnInPlayersWay = false;
        base.ResetValues();
    }

    private void FixedUpdate() 
    {
        if(isBeingDestroyed || !isActive)
        {
            return;
        }
        
        LaneController lane = this.Lane;
        if(lane == null)
        {
            ApplyDownwardForce();
            return;
        }

        if(!this.movementDirection.Equals(lane.LaneDirection))
        {
            // deactivate
            this.controller.Deactivate();
            return;
        }

        // we use forward vector because we are not changing lanes of the obstacles yet.
        // when hit, if the y is anything but 0, the car will start flying
        // additionally, making them invactive would stop them immediately which looks rather odd
        //velocityVector = transform.TransformDirection(Vector3.forward);

        velocityVector = !changingLanes ? vectorDirection : transform.TransformDirection(Vector3.forward);
        base.Move();
    }

    public void MarkTurnChance(LaneController lane, bool canturn)
    {
        if(TurnInPlayersWay)
        {
            return;
        }

        //BumperCopsDebug.Log($"ObstacleMovementController: maketurnchance {TurnInPlayersWay} {this.transform.root.name}");
        //BumperCopsDebug.Log("ObstacleMovementController: Turning now");
        TurnInPlayersWay = true;
        if(canturn && lane != null && !changingLanes)
        {
            StartCoroutine(MakeTurn(lane, 5f));
        }
    }

    void ApplyDownwardForce()
    {
        // apply downward force
        this.rigidBody.AddForce(Vector3.down * this.rigidBody.mass, ForceMode.Force);
        return;
    }

    private void OnTriggerEnter(Collider other) 
    {
        NPCMovementController otherNpc = other.GetComponent<NPCMovementController>();
        if(this.gameObject.layer.Equals(other.gameObject.layer) || otherNpc == null)
        {
            return;
        }

        //BumperCopsDebug.Log("ObstacleMovementController: Reacting to traffic");
        StartCoroutine(ReactToTrafficAhead(otherNpc));
    }
}