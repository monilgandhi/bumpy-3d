﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public event Action<ObstacleEventArgs> FirstObstacleGenerated;
    private bool firstObstacleEventFired;
    public event EventHandler<NearMissEventArgs> ObstacleRemovedDuringBonus;

    public float TrafficGenerationTime { get { return gameLevelManager.TrafficDensity; }}
    [Header("Game elements")]
    [SerializeField]
    private RoadManager roadManager;

    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private CoinBonusBehaviour coinBonusBehaviour;
    

    [Header("Obstacle related")]
    [SerializeField]
    private GameObject movingObstaclePrefab;

    [Header("Admin")]
    [SerializeField]
    private string SpecificObstacle;

    [SerializeField]
    private bool shouldGenerateObstacles;

    private float distanceBetweenObstaclesPerLane = 18f;
    private int obstacleNumber;

    private float obstacleTimer = 0;
    private bool canCreateObstacles = false;
    private static string CAR_OBSTACLE_PATH = $"Prefabs{Path.DirectorySeparatorChar}obstacles{Path.DirectorySeparatorChar}Car";
    private static string TRUCK_OBSTACLE_PATH = $"Prefabs{Path.DirectorySeparatorChar}obstacles{Path.DirectorySeparatorChar}Truck";

    private List<ObstacleController> carObstaclePool = new List<ObstacleController>();
    private List<ObstacleController> truckObstaclePool = new List<ObstacleController>();
    private List<ObstacleController> activeObstacles = new List<ObstacleController>();
    private Queue<PowerupReward> collectibleQueue = new Queue<PowerupReward>();
    private float[] farthestObstaclePositionPerLane = new float[Constants.TOTAL_LANES];
    private ObstacleController[] farthestObstaclePerLane = new ObstacleController[Constants.TOTAL_LANES];
    private bool isTutorialActive;
    private float obstacleTurningChance = DEFUALT_OBSTACLE_TURNING_CHANCE;
    private const float DEFUALT_OBSTACLE_TURNING_CHANCE = 0.15f;
    private const float TRUCK_SPAWNING_PROBABILITY = 0.2f;
    private const float MAX_OBSTACLES_TUTORIAL = 5;
    private float lastGenerationTime;
    private bool isBonusActive;
    private RoadblockObstacleManager roadblockObstacleManager;
    private RoadblockEventArgs roadblockInformation;
    private GameLevelManager gameLevelManager;
    
    // Use this for initialization

    private void Awake() 
    {
        if(!CarUtils.IsUnityDevelopmentBuild())
        {
            shouldGenerateObstacles = true;
        }

        LoadAllObstacles();
        this.roadblockObstacleManager = this.GetComponentInChildren<RoadblockObstacleManager>();
    }

    void Start()
    {
        gameLevelManager = this.gameManager.GameLevelManager;

        this.gameManager.GameTutorialSessionInitialized += (args) => 
        {
            BumperCopsDebug.Log("tutorial is Active");
            isTutorialActive = true;
            this.obstacleTurningChance = 0;
            this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
            this.gameManager.TutorialManager.TutorialEnd += OnTutorialEnded;
        };

        this.gameManager.GamePreStart += () => 
        {
            BumperCopsDebug.Log($"is tutorial active {isTutorialActive}");
            // if tutorial is active we do not generate obstacles here.
            canCreateObstacles = !isTutorialActive;
        };
        
        obstacleTimer = TrafficGenerationTime;
        gameManager.GameOver += (reason) =>
        {
            ObstacleController[] controllers = this.GetComponentsInChildren<ObstacleController>();
            // make all NPCs stop
            foreach(ObstacleController m in controllers)
            {
                if(m != null)
                {
                    m.Deactivate();
                }
            }
        };

        this.gameManager.TimerExtendBehaviour.TimeExtendedDialogueDisplayed += OnTimeExtendedDialogueDisplayed;
        this.player.MoveController.PlayerInObstacleVicinity += OnPlayerInObstacleVicinity;

        if(gameManager.GameMode == GameMode.HARD)
        {
            this.gameManager.PowerupManager.PowerupSpawned += OnCollectibleSpawned;
        }

        this.coinBonusBehaviour.BonusStarted += OnBonusStarted;
        this.roadblockObstacleManager.RoadblockEstablished += OnRoadblockEstablished;
    }

    private void OnBonusStarted(CoinBonusEventArgs args)
    {
        StartCoroutine(RemoveObstaclesForBonus());
    }


    private void OnRoadblockEstablished(object o, RoadblockEventArgs args)
    {
        this.roadblockInformation = args;

        foreach(ObstacleController c in this.activeObstacles)
        {
            c.MovementController.Deactivate();
        }
    }

    private void OnCollectibleSpawned(PowerupSpawnedEventArgs args)
    {
        //BumperCopsDebug.Log($"ObstacleManager: enqueuing reward of type {args.Reward.Type}");
        collectibleQueue.Enqueue(args.Reward);
    }

    private void OnPlayerInObstacleVicinity(PlayerObstacleEventArgs args)
    {
        if(args.Obstacle == null)
        {
            return;
        }

        ObstacleController o = args.Obstacle.transform.root.GetComponent<ObstacleController>();

        if(o == null || o.MovementController == null)
        {
            return;
        }
        
        //BumperCopsDebug.Log("ObstacleManager: Obstacle in vicinity of player. Making it turn");
        if(!o.MovementController.isActive)
        {
            //BumperCopsDebug.LogWarning($"Player movement collider hit with non active obstacle with id {args.Obstacle.name}");
            return;
        }    

        LaneController playersLane = player.MoveController.Lane;
        if(o.MovementController.TurnInPlayersWay)
        {
            return;
        }

        // turn into players lane
        LaneController laneToTurn = null;
        LaneController obstacleLane = o.MovementController.Lane;
        LaneController obstacleCurrentLane = obstacleLane;

        if(obstacleCurrentLane == null || obstacleCurrentLane.LaneDirection == LaneDirection.OPPOSITE)
        {
            //do not turn
            return;
        }

        int obstacleCurrentLaneId = obstacleLane.Id;
        int playerCurrentLaneId = playersLane.Id;
        // select the lane, next or previous in players direction
        if(obstacleLane != null && 
            playerCurrentLaneId < obstacleCurrentLaneId && 
            obstacleLane.PreviousLane != null)
        {
            laneToTurn = obstacleLane.PreviousLane.GetComponent<LaneController>();
        }
        else if (obstacleLane != null && obstacleLane.NextLane != null)
        {
            laneToTurn = obstacleLane.NextLane.GetComponent<LaneController>();
        }

        // do not turn in opposite direction
        bool canTurn = UnityEngine.Random.value < obstacleTurningChance && laneToTurn != null;
        o.MovementController.MarkTurnChance(laneToTurn, canTurn);
    }

    private void OnTimeExtendedDialogueDisplayed(TimeExtensionArgs args)
    {
        List<int> removedIndex = new List<int>(); 
        // clear all the obstacles until player + 1 road

        List<ObstacleController> obstaclesToDeactivate = new List<ObstacleController>();
        activeObstacles.ForEach(o => {
            if(o.MovementController.Lane != null && o.MovementController.PathNumber <= this.player.MoveController.PathNumber + 1)
            {
                obstaclesToDeactivate.Add(o);
            }; 
        });

        // we create a separate array else we may get an error of modifying the array "activeObstacle" while it is being iterated
        // since deactivating it removes it from the array 
        obstaclesToDeactivate.ForEach(o => o.Deactivate());
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        this.isTutorialActive = false;
        this.gameManager.TutorialManager.TutorialStepUpdate -= OnTutorialStepUpdate;
        this.gameManager.TutorialManager.TutorialEnd -= OnTutorialEnded;
    }
    
    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        canCreateObstacles = false;

        BumperCopsDebug.Log($"ObstacleManager: Current step is {args.TutorialStep} and is tutorial active {this.isTutorialActive}");
        if(args.TutorialStep >= TutorialStep.OBSTACLE_GENERATION)
        {
            canCreateObstacles = true;
        }
    }

    private void FixedUpdate() 
    {
        for(int i = 0; i < farthestObstaclePerLane.Length; i++)
        {
            if(farthestObstaclePerLane[i] != null && farthestObstaclePerLane[i].gameObject.activeSelf)
            {
                farthestObstaclePositionPerLane[i] = farthestObstaclePerLane[i].transform.position.z;
            }
        }    
    }


    // Update is called once per frame
    void LateUpdate()
    {
        //BumperCopsDebug.Log("ObstacleManager: Update called");
        obstacleTimer += Time.deltaTime;

        if (CanSpawnObstacle())
        {
            ObstacleSpawnInformation obstacleSpawnInfo = null;

            // unable to get lane id and hence we skip this instantiation
            if(!TryGetNextObstacleLaneIdPosition(out obstacleSpawnInfo))
            {
                //BumperCopsDebug.LogWarning("ObstacleManager: Could not get position for the obstacle");
                return;
            }

            //BumperCopsDebug.Log("ObstacleManager: Spawning in lane id with " + obstaclePositionAndLaneId.Item1 + " and position " + obstaclePositionAndLaneId.Item2);

            if(SpawnObstacle(obstacleSpawnInfo))
            {
                // reset the timer
                obstacleTimer = 0;
                lastGenerationTime = Time.time;
            }
        }
    }

    private bool TryGetNextObstacleLaneIdPosition(out ObstacleSpawnInformation obstacleInformation)
    { 
        obstacleInformation = null;
        int minDistance = obstacleNumber < 6 ? 1 : Constants.ROAD_DISTANCE_BETWEEN_NPC_PLAYER_SPAWN - 1;
        int maxDistance = obstacleNumber < 6 ? 2 : Constants.ROAD_DISTANCE_BETWEEN_NPC_PLAYER_SPAWN;

        // check if collectible is present
        PowerupReward reward = this.gameManager.PowerupManager.DefaultReward;
        bool isFromQueue = false;
        if(collectibleQueue.Any())
        {
            reward = collectibleQueue.Peek();
            isFromQueue = true;
        }

        int startRoad = Constants.ROAD_DISTANCE_BETWEEN_NPC_PLAYER_SPAWN - 1;

        if(roadblockInformation != null)
        {
            LaneController playerLane = this.player.MoveController.Lane;
            if(playerLane == null)
            {
                BumperCopsDebug.LogWarning("Player lane is null");
                return false;
            }

            if(playerLane.Road.MyPathNumber + startRoad >= roadblockInformation.RoadblockEndRoadNumber)
            {
                //BumperCopsDebug.Log($"Resetting roadblock informaiton player road {playerLane.Road.MyPathNumber + startRoad} anmd end road is {roadblockInformation.RoadblockEndRoadNumber}");
                roadblockInformation = null;
            }
            else
            {
                //BumperCopsDebug.Log("player is still far away. Not generating");
                return false;
            }
        }

        RoadController road = roadManager.GetRoadForObstacle(startRoad, startRoad + 1);

        //BumperCopsDebug.Log($"Generating at road number {road.MyPathNumber}");
        if(road == null)
        {
            return false;
        }
        
        // this is used for DOUBLE sided roads.
        // since we have 3 & 2 lanes on each side it is possible that one side will be selected
        // only 2/5 times vs 3/5. Hence to randomize a bit we sometimes make iot 2.5/5 vs 2.5/5
        // giving more weightage to the opposite lane
        // for forks we automatically select the side of the player. This is done in GetLaneWrtPlayer
        bool allLaneSelectionEqual = UnityEngine.Random.value <= 0.5f;
        
        int laneId = -1;

        if(reward.IsDefaultReward)
        {
            laneId = CarUtils.GetLaneWrtPlayer(road, player.MoveController.Lane, allLaneSelectionEqual);
        }
        else
        {
            LaneController enemyLane = this.enemyManager.currentEnemy != null ? this.enemyManager.currentEnemy.Lane : null;
            HashSet<int> laneIds;
            if(PowerupManager.TryGetGenerationLanesIndex(road, player.MoveController.Lane, -1, 1, out laneIds))
            {
                laneId = laneIds.First();
            }
        }
        
        if(laneId < 0 || laneId >= road.LaneCount)
        {
            // enqueue the reward
            BumperCopsDebug.LogError($"ObstacleManager: Lane id is invalid (negative or greater than count) {laneId}");
            return false;
        }

        Vector3 selectedLaneWorldPosition = road.GetLanePosition(laneId).Value;
        float zDistance;
        if(farthestObstaclePerLane[laneId] == null)
        {
            zDistance = selectedLaneWorldPosition.z;
        }
        else
        {
            zDistance = selectedLaneWorldPosition.z > farthestObstaclePositionPerLane[laneId] + distanceBetweenObstaclesPerLane ? 
                selectedLaneWorldPosition.z : 
                farthestObstaclePositionPerLane[laneId] + distanceBetweenObstaclesPerLane;
        }

        if(road.WorldEnd.z < zDistance)
        {
            //BumperCopsDebug.LogWarning("ObstacleManager: Road not available");
            return false;
        }
        
        if(isFromQueue)
        {
            collectibleQueue.Dequeue();
        }

        // first we assign the localposition wrt to road to the generated object since we will parent the controller to the road. 
        // This way since the road is also moving, the obstacle will actually spawn on the road.
        // Immediately after we activate the object we will deparent it and convert the localpositions to world positions
        // This ensures that obstacles are generated in increasing order rather than random order
        Vector3 localPosition = road.transform.InverseTransformPoint(new Vector3(selectedLaneWorldPosition.x, player.PlayerYPosition, zDistance));
        obstacleInformation = new ObstacleSpawnInformation (laneId, road.GetLaneDirection(laneId), localPosition, reward, road);
        return true;
    }

    private IEnumerator RemoveObstaclesForBonus()
    {
        LaneController playerLane = this.player.MoveController.Lane;
        while(playerLane == null)
        {
            yield return new WaitForSeconds(0.5f);
            playerLane = this.player.MoveController.Lane;
        }

        long road = playerLane.Road.MyPathNumber;

        foreach(ObstacleController o in this.activeObstacles)
        {
            if(!o.MovementController.isActive)
            {
                continue;
            }

            LaneController obstacleLane = o.MovementController.Lane;
            if(obstacleLane == null)
            {
                continue;
            }        

            if(obstacleLane.Road.MyPathNumber <= road + 2)
            {
                StartCoroutine(o.ObstacleHitController.ExplodeDeath());
                ObstacleRemovedDuringBonus?.Invoke(this, new NearMissEventArgs(o.Character.ObstacleType, 0, o.name, o.NearMissReward, o.AssignedLaneDirection));
                o.ClaimReward();
            }
        }
    }

    private bool CanSpawnObstacle()
    {
        if(!shouldGenerateObstacles)
        {
            return false;
        }

        //canCreateObstacles = false;
        if(!canCreateObstacles)
        {
            return false;
        }

        if(isTutorialActive && activeObstacles.Count >= MAX_OBSTACLES_TUTORIAL)
        {
            return false;
        }

        LaneController playerLane = this.player.MoveController.Lane;
        if(playerLane == null)
        {
            return false;
        }

        // the generation time that is specified in sequence.json is for 5 lanes. 
        // However there are roads with less than 5 lanes. Hence we increase the time as (time per lane in sequence * 5 / acutal lane count)

        float actualTime = TrafficGenerationTime;
        switch(playerLane.Road.RoadType)
        {
            case RoadManager.RoadType.FORK:

                if(playerLane.LaneDirection == LaneDirection.OPPOSITE)
                {
                    actualTime = actualTime * Constants.TOTAL_LANES / playerLane.Road.LaneCountOppositeDirection;
                }
                else
                {
                    actualTime = actualTime * Constants.TOTAL_LANES / playerLane.Road.LaneCountSameDirection;
                }

                break;
            
            default:
                break;
        }

        //BumperCopsDebug.Log($"ObstacleManager: Actual generation time is {actualTime}");
        return obstacleTimer >= actualTime;
    }

    private bool CanSpawnAtPosition(ObstacleController controller, Vector3 spawnPosition)
    {
        BoxCollider boxCollider = controller.gameObject.GetComponentInChildren<BoxCollider>();
        RaycastHit hitInfo;

        if((Physics.BoxCast(spawnPosition, boxCollider.size, new Vector3(0, 0, -1) * distanceBetweenObstaclesPerLane, out hitInfo) || 
            Physics.BoxCast(spawnPosition, boxCollider.size, transform.forward * distanceBetweenObstaclesPerLane, out hitInfo)) && 
            hitInfo.collider.gameObject.layer == Constants.NPC_LAYER)
        {
            BumperCopsDebug.LogWarning($"ObstacleManager: hit info {hitInfo.collider.transform.root.name}");
            return false;
        }

        return true;  
    }

    private ObstacleController GetVehicleFromPool()
    {
        float truckProbability = UnityEngine.Random.value;
        bool selectTruck = (UnityEngine.Random.value < TRUCK_SPAWNING_PROBABILITY || carObstaclePool.Count <= 0) && truckObstaclePool.Count > 0;

        int selectedObstacleIdx = -1;
        ObstacleController controller = null;
        if(selectTruck)
        {
            selectedObstacleIdx = UnityEngine.Random.Range(0, truckObstaclePool.Count);
            controller = truckObstaclePool[selectedObstacleIdx];
            truckObstaclePool.RemoveAt(selectedObstacleIdx);
        }
        else if (carObstaclePool.Count > 0)
        {
            selectedObstacleIdx = UnityEngine.Random.Range(0, carObstaclePool.Count);
            //BumperCopsDebug.Log($"ObstacleManager: index is {selectedObstacleIdx} and size is {carObstaclePool.Count}");
            controller = carObstaclePool[selectedObstacleIdx];
            carObstaclePool.RemoveAt(selectedObstacleIdx);
        }
        
        return controller;  
    }

    private bool SpawnObstacle(ObstacleSpawnInformation obstacleSpawnInfo)
    {
        ObstacleController controller = GetVehicleFromPool();

        if(controller == null)
        {
            BumperCopsDebug.LogWarning("No vehicle in pool available");
            return false;
        }

        controller.name = "obstacle_" + obstacleNumber++;
        controller.InitMovingObstacle(this.player, obstacleSpawnInfo.LaneDirection, obstacleSpawnInfo.Reward, isTutorialActive, gameManager);
        controller.transform.parent = obstacleSpawnInfo.Road.transform;

        // first we assign the localposition since we will parent the controller to the road. 
        // This way since the road is also moving, the obstacle will actually spawn on the road.
        // Immediately after we activate the object we will deparent it and convert the localpositions to world positions
        // This ensures that obstacles are generated in increasing order rather than random order
        controller.transform.position = obstacleSpawnInfo.LocalSpawnPositionWrtRoad;
        
        Vector3 worldPosition = obstacleSpawnInfo.Road.transform.TransformPoint(obstacleSpawnInfo.LocalSpawnPositionWrtRoad);
        controller.gameObject.SetActive(true);
        controller.transform.parent = null;
        controller.gameObject.transform.position = worldPosition;
        
        // BumperCopsDebug.Log($"Vehicle {controller.name} placed on road {obstacleSpawnInfo.Road.MyPathNumber}");
        //BumperCopsDebug.Log($"Game object placed on position {obstacleSpawnInfo.SpawnPosition.z} and player on position {this.player.transform.position.z} name {controller.name}");
        //BumperCopsDebug.Log("Spawning in lane id with " + laneId + " position " + selectedLanePosition);

        //BumperCopsDebug.Log($"ObstacleManager: PositionY: {obstacleSpawnInfo.SpawnPosition.y}, name: {controller.name}");

        if(isTutorialActive && !firstObstacleEventFired)
        {
            firstObstacleEventFired = true;
            BumperCopsDebug.Log($"ObstacleManager: First obstacle event with name {controller.name}");
            FirstObstacleGenerated?.Invoke(new ObstacleEventArgs(controller));
        }

        activeObstacles.Add(controller);
        farthestObstaclePerLane[obstacleSpawnInfo.LaneId] = controller;
        //controller.transform.parent = null;
        return true;
    }

    private void LoadAllObstacles()
    {
        GameObject[] carPrefabs = Resources.LoadAll<GameObject>(CAR_OBSTACLE_PATH);
        GameObject[] truckPrefabs = Resources.LoadAll<GameObject>(TRUCK_OBSTACLE_PATH);

        Dictionary<ObstacleType, GameObject[]> prefabDictionary = new Dictionary<ObstacleType, GameObject[]>()
        {
            { ObstacleType.CAR, carPrefabs},
            { ObstacleType.TRUCK, truckPrefabs},
        };

        LoadPool(prefabDictionary);
    }

    private void OnObstacleDestroyed(ObstacleEventArgs args)
    {
        // obstacle is destroyed. Check if the obstacle had any rewards besides coins. if they did recycle
        if(args.Obstacle == null)
        {
            BumperCopsDebug.LogWarning("Destroyed obstacle is null");
            return;
        }

        if(args.Obstacle.NearMissReward != null && !args.Obstacle.NearMissRewardClaimed && !args.Obstacle.NearMissReward.IsDefaultReward)
        {
            // add the reward back to the queue
            collectibleQueue.Enqueue(args.Obstacle.NearMissReward);
        }
    }

    private void OnObstacleDeactivated(ObstacleController obj)
    {
        if(obj == null || obj.gameObject == null)
        {
            BumperCopsDebug.LogWarning("Obstacle has been destroyed");
            return;
        }

        if(CarUtils.IsTruck(obj.Character.ObstacleType))
        {
            truckObstaclePool.Add(obj);
        }
        else
        {
            carObstaclePool.Add(obj);
        }

        activeObstacles.Remove(obj);
    }
    
    private void LoadPool(Dictionary<ObstacleType, GameObject[]> obstaclePrefabsList)
    {
        movingObstaclePrefab.SetActive(false);
        foreach(ObstacleType type in obstaclePrefabsList.Keys)
        {
            GameObject[] prefabs = obstaclePrefabsList[type];
            bool isTruck = CarUtils.IsTruck(type);

            foreach(GameObject obstaclePrefab in obstaclePrefabsList[type])
            {
                if(!string.IsNullOrEmpty(SpecificObstacle) && !obstaclePrefab.name.Equals(SpecificObstacle))
                {
                    continue;
                }

                GameObject prefab = obstaclePrefab as GameObject;
                if(prefab == null)
                {
                    BumperCopsDebug.LogError("Found an invalid obstacle prefab");
                    continue;
                }

                int totalCopies = isTruck ? 3 : 5;
                for(int copies = 0; copies < totalCopies; copies++)
                {
                    GameObject obstacle = Instantiate(movingObstaclePrefab);
                    ObstacleController controller = obstacle.GetComponent<ObstacleController>();
                    if(controller == null)
                    {
                        throw new MissingComponentException(nameof(ObstacleController));
                    }

                    Instantiate(prefab, obstacle.transform);
                    obstacle.SetActive(true);
                    controller.ObstacleDeactivated += OnObstacleDeactivated;
                    controller.ObstacleHitController.Destroyed += OnObstacleDestroyed;
                    obstacle.SetActive(false);

                    if(isTruck)
                    {
                        truckObstaclePool.Add(controller);
                    }
                    else
                    {
                        carObstaclePool.Add(controller);
                    }
                }
            }
        }

        truckObstaclePool.Shuffle();
        carObstaclePool.Shuffle();
    }

    private class ObstacleSpawnInformation
    {
        public readonly int LaneId;
        public readonly Vector3 LocalSpawnPositionWrtRoad;
        public readonly LaneDirection LaneDirection;
        public readonly RoadController Road;
        public readonly PowerupReward Reward;

        public ObstacleSpawnInformation(int laneId, LaneDirection laneDirection, Vector3 localSpawnPositionWrtRoad, PowerupReward reward, RoadController road)
        {
            this.LaneId = laneId;
            this.LaneDirection = laneDirection;
            this.LocalSpawnPositionWrtRoad = localSpawnPositionWrtRoad;
            this.Reward = reward;
            this.Road = road;
        }
    }
}
