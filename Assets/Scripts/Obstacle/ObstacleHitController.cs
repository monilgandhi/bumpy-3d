using System.Collections;
using UnityEngine;

public class ObstacleHitController : MonoBehaviour 
{
    [SerializeField]
    private GameObject smokePrefab;

    [SerializeField]
    private GameObject destroyParticleSystemPrefab;

    [SerializeField]
    private AnimationCurve bumpImpulseCurve;
    public delegate void DestroyedEventHandler(ObstacleEventArgs controller);
    public event DestroyedEventHandler Destroyed;
    private ObstacleController controller;
    private bool IsBeingDestroyed;
    public int PlayerHitCount { get; private set; }
    private GameObject smoke;
    private float hitTimer;
    private ParticleSystem explosionParticleSystem;
    private AudioSource explosionAs;

    private bool firstTimeInit = false;

    private void Awake() 
    {
        GameObject go = Instantiate(destroyParticleSystemPrefab, this.transform);
        explosionParticleSystem = go.GetComponent<ParticleSystem>();

        this.controller = this.GetComponent<ObstacleController>() ?? throw new MissingComponentException(nameof(ObstacleController));
        smoke = Instantiate(smokePrefab, this.controller.transform);
    }

    public void Init(BoxCollider characterCollider)
    {
        if(!firstTimeInit)
        {
            if(CarUtils.IsTruck(this.controller.Character.ObstacleType))
            {
                smoke.transform.localScale = new Vector3(3f, 3f, 3f);    
            }
            else
            {
                smoke.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            }
            
            smoke.transform.localPosition = new Vector3(characterCollider.center.x, this.controller.transform.position.y, characterCollider.center.z + (characterCollider.size.z / 2));
            firstTimeInit = true;
        }

        //BumperCopsDebug.Log($"ObstacleHitController_{this.controller.name}: smokke position is {smoke.transform.localPosition} with bounds as {r.size} and center as {r.center}");
        smoke.SetActive(false);
        explosionParticleSystem.Stop();
        PlayerHitCount = 0;
        IsBeingDestroyed = false;
    }

    private void OnCollisionStay(Collision other) 
    {
        if((other.gameObject.layer == Constants.NPC_LAYER && IsBeingDestroyed) || 
            other.gameObject.layer == Constants.ROAD_LAYER || 
            other.gameObject.layer == Constants.ENVIRONMENT_LAYER)
        {
            //BumperCopsDebug.Log($"ObstacleMovementController: Already being destroyed {this.transform.root.name}");
            return;
        }

        if ((other.gameObject.layer == Constants.ENEMY_LAYER || other.gameObject.layer == Constants.PLAYER_LAYER) && IsBeingDestroyed && !this.controller.IsVanishing)
        {
            hitTimer += Time.deltaTime;
            // this means that the enemy is stuck. So let's vanish if the enemy is stuck for more than 1 seconds
            if(hitTimer > 0.5f)
            {
                StartCoroutine(this.controller.Vanish());
            }
        }
    }

    void OnCollisionEnter(Collision other) 
    {
        if((other.gameObject.layer == Constants.NPC_LAYER && IsBeingDestroyed) || 
            other.gameObject.layer == Constants.ROAD_LAYER || 
            other.gameObject.layer == Constants.ENVIRONMENT_LAYER)
        {
            //BumperCopsDebug.Log($"ObstacleMovementController: Already being destroyed {this.transform.root.name}");
            return;
        }
        
        //BumperCopsDebug.Log($"ObstacleMovementController: Collision recorded {this.transform.root.name} with {other.transform.root.name}");
        if((other.gameObject.layer == Constants.PLAYER_LAYER || 
            other.gameObject.layer == Constants.PLAYER_NPC_LAYER ||
            other.gameObject.layer == Constants.PROJECTILES_LAYER) && !IsBeingDestroyed)
        {
            IsBeingDestroyed = true;
            smoke.SetActive(true);
            this.controller.DisableNearMiss();
            StartCoroutine(GotHit(other.impulse, true));
        }    
        else if (other.gameObject.layer == Constants.ENEMY_LAYER  && !IsBeingDestroyed)
        {
            //BumperCopsDebug.Log("ObstacleHitController: hit with enemy");
            IsBeingDestroyed = true;
            smoke.SetActive(true);
            StartCoroutine(GotHit(other.impulse, false));
        }
        else if (other.gameObject.tag.Equals(Constants.CENTER_BARRIER_TAG))
        {
            BumperCopsDebug.LogWarning("Obstacle hit center barrier");
            this.controller.MovementController.Deactivate();
        }
    }

    public IEnumerator GotHit(Vector3 force, bool isHitByPlayer)
    {
        hitTimer += Time.deltaTime;
        if(Destroyed != null)
        {
            Destroyed(new ObstacleEventArgs(this.controller));
        }
        
        LaneController nextLaneTarget = CarUtils.GetNextLane(this.controller.MovementController.Lane);
        this.controller.RigidBody.constraints = RigidbodyConstraints.None;
        this.controller.MovementController.MakeInactive();
        // /BumperCopsDebug.Log("ObstacleHitController: Force is " + force);
        // do an explosion

        // get the direction of player

        //yield return new WaitForSeconds(f);
        float explosionMagnitude = force.magnitude < (this.controller.RigidBody.mass / 2) ? force.magnitude : this.controller.RigidBody.mass / 2;
        this.controller.RigidBody.mass = 1;

        if(nextLaneTarget == null)
        {
            this.controller.RigidBody.AddExplosionForce(600f, this.controller.RigidBody.transform.position, 30);
        }
        else
        {
            Vector3 target = new Vector3(nextLaneTarget.transform.position.x, nextLaneTarget.transform.position.y, nextLaneTarget.transform.position.z + 100);
            Vector3 direction = target - this.controller.RigidBody.position;
            for(int i = 0; i < 3; i++)
            {
                this.controller.RigidBody.AddForce(force, ForceMode.Force);
                //this.controller.RigidBody.AddForce(force, ForceMode.Force);
                yield return new WaitForEndOfFrame();
            }
        }
        //

        if(isHitByPlayer)
        {
            // vanish immediately
            StartCoroutine(this.controller.Vanish());
        }
    }

    public void RecordPlayerHit()
    {
        ++PlayerHitCount;
    }

    public IEnumerator ExplodeDeath()
    {
        RecordPlayerHit();
        IsBeingDestroyed = true;
        this.controller.DisableNearMiss();
        this.controller.MovementController.MakeInactive();

        // remove all constraints
        this.controller.RigidBody.constraints = RigidbodyConstraints.None;
        this.controller.RigidBody.mass = 1;

        float random = UnityEngine.Random.value;
        float xDirection = 0;
        if(random < 0.33)
        {
            xDirection = -1;
        } else if (random >= 0.33 && random < 0.66)
        {
            xDirection = 1;
        }

        if(!explosionParticleSystem.isPlaying)
        {
            explosionParticleSystem.Play();
            //explosionAs.Play();
        }

        Vector3 forcePosition = this.transform.TransformDirection(new Vector3(xDirection, 1, 0));
        this.controller.RigidBody.AddTorque(forcePosition * 50, ForceMode.Impulse);
        this.controller.RigidBody.AddExplosionForce(300f, this.controller.RigidBody.transform.position, 1000);
        //yield return new WaitUntil(() => explosionParticleSystem.isPlaying);

        yield return new WaitForSeconds(3.5f);
        this.controller.Deactivate();
    }
}