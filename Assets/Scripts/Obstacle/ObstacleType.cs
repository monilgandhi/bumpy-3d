﻿using System;
public enum ObstacleType
{
    NONE,
    CAR,
    TRUCK,
    CONSTRUCTION,
    GARBAGE,
    TAXI,
    ENEMY,
    FIRE_TRUCK,
    ROAD_BLOCK
}
