using System;
using UnityEngine;

public class RoadblockPropsBehaviour : MonoBehaviour 
{
    public event EventHandler RoadblockPropDeactivated;
    private float hitStarttime = -1;

    private Rigidbody rb;

    private void Awake() 
    {
        rb = this.GetComponent<Rigidbody>();
    }
    
    private void LateUpdate() 
    {
        if(this.transform.position.z + 10 < Camera.main.transform.position.z || (hitStarttime > 0 && Time.time - hitStarttime > 4))
        {
            Deactivate();
        }
    }   

    private void Deactivate()
    {
        if(RoadblockPropDeactivated != null)
        {
            RoadblockPropDeactivated(this, null);
        }

        rb.velocity = rb.angularVelocity = Vector3.zero;
    
        this.gameObject.SetActive(false);
        hitStarttime = -1;
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(hitStarttime <= 0 && other.collider.gameObject.layer == Constants.PLAYER_LAYER || other.collider.gameObject.layer == Constants.ENEMY_LAYER)
        {
            // BumperCopsDebug.Log("On collision enter");
            rb.AddTorque(transform.right * 100, ForceMode.Impulse);
            rb.AddExplosionForce(400, this.transform.position, 400, 2);
            hitStarttime = Time.time;
        }    
    }
}