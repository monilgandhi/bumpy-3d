using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(GamePlayCanvasManager))]
[RequireComponent(typeof(ObstacleHitController))]
public class ObstacleController : MonoBehaviour 
{
    public delegate void ObstacleEventHandler(ObstacleController o);
    public event ObstacleEventHandler ObstacleDeactivated;
    [SerializeField]
    private GameObject nearMissPrefab;

    [Header("UI Elements")]
    [SerializeField]
    private List<Image> turnImages = new List<Image>();


    [SerializeField]
    private GameObject defaultRewardGameObject;
    private TMP_Text defaultRewardText;

    public Character Character{ get; private set; }
    public Rigidbody RigidBody { get; private set; }

    public ObstacleMovementController MovementController { get; private set; }
    public ObstacleHitController ObstacleHitController { get; private set; }
    private GameObject nearMissGameObject;

    private bool inited = false;
    public PowerupReward NearMissReward { get; private set; }
    public bool NearMissRewardClaimed { get; private set; }
    public bool IsVanishing { get; private set; }
    public LaneDirection AssignedLaneDirection;
    private RigidbodyConstraints constraints;
    private GamePlayCanvasManager canvasManager;
    private long roadblockRoadNumber;

    public float ActiveStartTime { get; private set; }
    private void Awake() 
    {
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.MovementController = this.GetComponentInChildren<ObstacleMovementController>() ?? throw new MissingComponentException(nameof(ObstacleMovementController));
        this.ObstacleHitController = this.GetComponent<ObstacleHitController>();
        this.Character = this.GetComponentInChildren<Character>();
        this.canvasManager = this.GetComponent<GamePlayCanvasManager>();
        SetupNearMiss();
        //nearMissPrefab.SetActive(false);
        turnImages.ForEach(t => t.enabled = false);

        this.defaultRewardText = defaultRewardGameObject.GetComponentInChildren<TMP_Text>();
    }    

    private void SetupNearMiss()
    {
        nearMissGameObject = Instantiate(nearMissPrefab, this.Character.transform);
        nearMissGameObject.SetActive(true);
    }

    public void InitMovingObstacle(PlayerController player, 
                                    LaneDirection direction, 
                                    PowerupReward nearMissReward, 
                                    bool isTutorialActive,
                                    GameManager gameManager)
    {
        Init(player, direction, nearMissReward, isTutorialActive, gameManager);
        this.Character.TurnLights.Init(turnImages);
    }

    public void InitNonMovingObstacle(PlayerController player, 
                                      PowerupReward nearMissReward,
                                      GameManager gameManager)
    {
        Init(player, LaneDirection.NONE, nearMissReward, false, gameManager);
        this.MovementController.enabled = false;

        if(this.Character.ObstacleType == ObstacleType.ROAD_BLOCK && this.Character.PoliceLight != null )
        {
            BumperCopsDebug.Log("Turning on the attack lights");
            this.Character.PoliceLight.EnableAttackLights();
        }

        this.ObstacleHitController.enabled = false;
    }

    private void Init(PlayerController player, 
                      LaneDirection direction, 
                      PowerupReward nearMissReward, 
                      bool isTutorialActive,
                      GameManager gameManager)
    {
        this.gameObject.SetActive(true);
        BoxCollider characterCollider = this.Character.GetComponent<BoxCollider>();

        if(!inited)
        {
            this.RigidBody.centerOfMass = this.Character.CenterOfMass.transform.position;
            inited = true;
            // adjust the canvas elements
            // get the top of the collider

            // BumperCopsDebug.Log($"box extents {characterCollider.bounds.extents}");
            // move the nearmiss reward on top of the car
            // take the max of box collider and deduct minimum of image

            this.defaultRewardGameObject.transform.position = 
                new Vector3(this.defaultRewardGameObject.transform.position.x, 
                    characterCollider.bounds.max.y + 1, 
                    this.defaultRewardGameObject.transform.position.z);

            this.constraints = this.RigidBody.constraints;

            this.canvasManager.Init(gameManager);

            player.SirenPowerupEffect.SirePowerupPlayed += OnSirenPlayed;
        }

        foreach(NearMissBehaviour n in this.GetComponentsInChildren<NearMissBehaviour>())
        {
            n.InitObstacleNearMiss(isTutorialActive, player, characterCollider);        
        }

        this.ObstacleHitController.Init(characterCollider);
        this.MovementController.Init(direction, !isTutorialActive);
        this.RigidBody.constraints = this.constraints;
        AssignedLaneDirection = direction;
        IsVanishing = false;
        
        // reset the rotation
        this.transform.rotation = Quaternion.identity;
        if(direction == LaneDirection.OPPOSITE)
        {
            this.transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        this.RigidBody.mass = this.Character.Mass;
    
        this.NearMissReward = nearMissReward;
        this.NearMissRewardClaimed = false;
        
        //this.UpdateUI();

        // activate
        nearMissGameObject.SetActive(true);
        ActiveStartTime = Time.time;
    }

    private void OnSirenPlayed()
    {
        this.MovementController.MoveToSide();
    }

    private void UpdateUI()
    {
        if(this.NearMissRewardClaimed)
        {
            //this.rewardImage.gameObject.SetActive(false);
            this.defaultRewardGameObject.SetActive(false);
            return;
        }

        this.defaultRewardGameObject.SetActive(this.NearMissReward.IsDefaultReward);

        if(this.NearMissReward.IsDefaultReward)
        {
            this.defaultRewardText.text = this.NearMissReward.Amount.ToString();
            this.defaultRewardGameObject.GetComponent<RectTransform>().localEulerAngles = this.AssignedLaneDirection == LaneDirection.OPPOSITE ? new Vector3(0, 180, 0) : Vector3.zero;
            // /BumperCopsDebug.Log($"ObstacleController: angle is {this.defaultRewardGameObject.transform.eulerAngles}");
        }
    }

    public void DisableNearMiss()
    {
        nearMissGameObject.SetActive(false);
        
        // claim hide the reward
        ClaimReward();
    }

    private void LateUpdate() 
    {
        DestroyIfPossible();

        if(this.Character.ObstacleType == ObstacleType.ROAD_BLOCK || roadblockRoadNumber < 0 || !this.MovementController.isActive)
        {
            return;
        }

        LaneController lane = this.MovementController.Lane;

        if(lane == null)
        {
            return;
        }

        if(lane.Road.MyPathNumber >= roadblockRoadNumber - 2 && lane.Road.MyPathNumber <= roadblockRoadNumber)
        {
            BumperCopsDebug.Log($"Stopping sing my road is {lane.Road.MyPathNumber} and obstacle is at {roadblockRoadNumber} name {this.name}");
            // stop
            this.MovementController.Deactivate();
        }
    }

    public IEnumerator Vanish(bool instant = false)
    {
        IsVanishing = true;
        //yield return new WaitForSeconds(1.0f);

        int blinkCount = 0;
        while(blinkCount < 5 && !instant)
        {
            ++blinkCount;
            this.Character.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            this.Character.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.2f);
        }

        this.Deactivate();
        IsVanishing = false;
    }

    public void Deactivate()
    {
        Vector3 position = transform.TransformPoint(this.gameObject.transform.position);
        //BumperCopsDebug.Log($"ObstacleController: {this.name} Disabling the controller now position {position.z} & camera position {Camera.main.transform.position.z}");
        // BumperCopsDebug.Log($"ObstacleController: {this.name} Activetime: {Time.time - ActiveStartTime}");
        //this.MovementController.Deactivate();
        ObstacleDeactivated?.Invoke(this);

        this.gameObject.SetActive(false);
    }

    protected void DestroyIfPossible()
    {
        if(this.transform.position.z + 10 < Camera.main.transform.position.z || this.RigidBody.position.y < -10)
        {
            Deactivate();
        }
    }

    public PowerupReward ClaimReward()
    {
        if(this.NearMissRewardClaimed)
        {
            return null;
        }

        NearMissRewardClaimed = true;
        // UpdateUI();
        return NearMissReward;
    }

    public void UpdateDefaultRewardAmount(int newAmount)
    {
        if(!this.NearMissReward.IsDefaultReward)
        {
            return;
        }

        this.NearMissReward.UpdateAmount(newAmount);
        this.defaultRewardText.text = this.NearMissReward.Amount.ToString();
        //StartCoroutine(UpdateRewardValue());
    }

    private IEnumerator UpdateRewardValue()
    {
        if(!this.defaultRewardText.gameObject.activeInHierarchy)
        {
            yield break;
        }

        Image glowImage = this.defaultRewardText.GetComponentInChildren<Image>();
        if(glowImage == null)
        {
            yield break;
        }

        while(glowImage.color.a < 1)
        {
            glowImage.color = new Color(glowImage.color.r, glowImage.color.g, glowImage.color.b, glowImage.color.a + (Time.deltaTime * 1));
            yield return new WaitForEndOfFrame();
        }

        // change the number
        this.defaultRewardText.text = this.NearMissReward.Amount.ToString();

        while(glowImage.color.a > 0)
        {
            glowImage.color = new Color(glowImage.color.r, glowImage.color.g, glowImage.color.b, glowImage.color.a - (Time.deltaTime * 1));
            yield return new WaitForEndOfFrame();
        }

        glowImage.color = new Color(glowImage.color.r, glowImage.color.g, glowImage.color.b, 0);
    }
}