using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class RoadblockObstacleManager : MonoBehaviour 
{
    public event EventHandler<RoadblockEventArgs> RoadblockEstablished;
    [SerializeField]
    private RoadManager roadManager;

    [SerializeField]
    private List<GameObject> roadBlocksObstaclePrefabs;

    [SerializeField]
    private List<GameObject> roadBlockPropsPrefabs;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private GameObject controllerPrefab;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private BonusBarBehaviour multiplierBehaviour;

    [Header("Admin")]
    [SerializeField]
    private bool shouldGenerateRoadBlock;
    private bool canGenerateRoadBlock;
    private float roadBlockGenerationTimer = 0;
    private int roadblockRoadCount;
    private RoadblockCarsPoolManager roadblockObstaclesPoolManager;
    private RoadblockPropsPoolManager roadblockPropsPoolManager;
    private const float OBSTACLE_TIMER = 20;

    private void Awake() 
    {
        roadblockObstaclesPoolManager = new RoadblockCarsPoolManager(roadBlocksObstaclePrefabs, controllerPrefab);
        roadblockObstaclesPoolManager.InitPool();

        roadblockPropsPoolManager = new RoadblockPropsPoolManager(roadBlockPropsPrefabs);
        roadblockPropsPoolManager.InitPool();
    }

    private void Start() 
    {
        this.multiplierBehaviour.MegaSirenTurnedOn += () => 
        {
            StartCoroutine(RemoveObstaclesForBonus());
        };    
    }

    private void Update() 
    {        
        if(Input.GetKeyDown(KeyCode.B))
        {
            StartCoroutine(RemoveObstaclesForBonus());
        }

        bool forceGenerate = Input.GetKeyDown(KeyCode.R);
        if(!CanSpawnRoadBlock(forceGenerate))
        {
            return;
        }

        if(StartRoadBlockSpawn())
        {
            roadBlockGenerationTimer = 0f;
        }
    }

    private void SpawnRoadblockProps(RoadController road)
    {
        float zPositionOffset = (road.WorldEnd.z - road.LeftBottomWorldStart.z) / 2;

        int currentLaneId = 0;
        for(; currentLaneId < road.LaneCount; currentLaneId++)
        {
            Vector3? lanePosition = road.GetLanePosition(currentLaneId);

            if(!lanePosition.HasValue)
            {
                BumperCopsDebug.LogWarning("Lane position missing");
                continue;
            }

            GameObject go = null;

            if(!roadblockPropsPoolManager.TryGetFromPool(out go))
            {
                BumperCopsDebug.LogWarning("Unable to get object from pool");
                continue;
            }

            go.transform.rotation = Quaternion.identity;
            go.transform.position = Vector3.zero;
            go.transform.parent = road.transform;
            go.transform.localPosition = new Vector3(lanePosition.Value.x, 1, zPositionOffset);
            go.SetActive(true);
        }
    }

    private IEnumerator RemoveObstaclesForBonus()
    {
        LaneController playerLane = this.player.MoveController.Lane;
        while(playerLane == null)
        {
            yield return new WaitForSeconds(0.5f);
            playerLane = this.player.MoveController.Lane;
        }

        long road = playerLane.Road.MyPathNumber;

        foreach(GameObject go in this.roadblockObstaclesPoolManager.ActiveGameObjects)
        {
            if(go == null)
            {
                BumperCopsDebug.LogWarning("Null game object in pool");
                continue;
            }

            ObstacleController o = go.GetComponent<ObstacleController>();

            if(o == null || o.ObstacleHitController == null)
            {
                continue;
            }

            StartCoroutine(o.ObstacleHitController.ExplodeDeath());
        }
    }
    private void SpawnRoadBlockCars(RoadController road, int numberOfRoadBlocks)
    {
        float zPositionOffset = road.LeftBottomLocalStart.z;
        float roadLength = road.WorldEnd.z - road.LeftBottomWorldStart.z;
        float distanceBetweenRoadblocks = roadLength / numberOfRoadBlocks;

        for(int k = 0; k < numberOfRoadBlocks; k++)
        {
            bool[] blockedLanes  = DetermineBlockedLanes(road, road.LaneCount - 2);

            // now generate the road blocks
            for(int j = 0; j < blockedLanes.Length - 1; j++)
            {
                if(!blockedLanes[j])
                {
                    continue;
                }

                // find if this lane and the next lane are both blocked
                GameObject go = null;

                if(!roadblockObstaclesPoolManager.TryGetFromPool(out go))
                {
                    BumperCopsDebug.LogWarning("Unable to get object from pool");
                    continue;
                }
                
                ObstacleController controller = go.GetComponent<ObstacleController>();
                controller.InitNonMovingObstacle(this.player, this.gameManager.PowerupManager.DefaultReward, gameManager);
                Vector3? lanePosition = road.GetLanePosition(j);

                go.transform.parent = road.transform;
                if(blockedLanes[j] && blockedLanes[j+1])
                {
                    // rotate and set it horizontally
                    go.transform.localPosition = new Vector3(lanePosition.Value.x, 1, zPositionOffset);
                    go.transform.rotation = Quaternion.Euler(0, UnityEngine.Random.Range(30, 150), 0);
                    ++j;
                }
                else
                {
                    // set it vertically
                    go.transform.localPosition = new Vector3(lanePosition.Value.x, 1, zPositionOffset);
                    go.transform.rotation = Quaternion.Euler(0, 180, 0);
                }

                go.SetActive(true);
            }

            zPositionOffset += distanceBetweenRoadblocks;
        }
    }

    private void SpawnRoadBlocks(RoadController startRoad, int roadblockRoadCount)
    {
        BumperCopsDebug.Log($"Placing on path number {startRoad.MyPathNumber}");
        long startRoadPathNumber = startRoad.MyPathNumber;
        
        int numOfRoadblocks = 1; 
        for(int i = 0; i < roadblockRoadCount; i++)
        {
            startRoad = roadManager.GetRoadByNumber(startRoadPathNumber + i);
            if(startRoad == null)
            {
                BumperCopsDebug.LogWarning("Road null for road block");
                continue;
            }

            BumperCopsDebug.Log($"Current road number for road block is {i} and total roads {startRoadPathNumber + roadblockRoadCount}");
            if(i == 0 || i == (roadblockRoadCount - 1))
            {
                BumperCopsDebug.Log($"Generating props on road number {i}");
                SpawnRoadblockProps(startRoad);
            }
            else
            {
                SpawnRoadBlockCars(startRoad, numOfRoadblocks);
            }

            if(startRoad == null)
            {
                BumperCopsDebug.LogWarning("Road null for road block");
                continue;
            }
        }
    }

    private bool StartRoadBlockSpawn()
    {
        roadblockRoadCount = UnityEngine.Random.Range(4,5);
        RoadController startRoad = roadManager.GetRoadForObstacle(Constants.ROAD_AHEAD_PLAYER_OR_NPC - roadblockRoadCount, Constants.ROAD_AHEAD_PLAYER_OR_NPC - 1);
        long initalPathNumber = startRoad.MyPathNumber;
        if(startRoad == null || startRoad.RoadType == RoadManager.RoadType.FORK)
        {
            return false;
        }

        SpawnRoadBlocks(startRoad, roadblockRoadCount);
        //BumperCopsDebug.Log($"Placing car on {road.MyPathNumber} and end road is {road.MyPathNumber + roadblockRoadCount}");
        if(RoadblockEstablished != null)
        {
            RoadblockEstablished(this, new RoadblockEventArgs(startRoad, roadblockRoadCount));
        }

        return true;    
    }

    private bool[] DetermineBlockedLanes(RoadController road, int numToGeneratePerRoad)
    {
        int previousSelectedLane = -1;

        bool[] occupiedLane = new bool[road.LaneCount];

        while(numToGeneratePerRoad > 0)
        {
            int selectedLane = UnityEngine.Random.Range(0, road.LaneCount);
            while(occupiedLane[selectedLane])
            {
                selectedLane = UnityEngine.Random.Range(0, road.LaneCount);
            }

            BumperCopsDebug.Log($"selected lane is {selectedLane}");
            occupiedLane[selectedLane] = true;
            previousSelectedLane = selectedLane;
            --numToGeneratePerRoad;
        }

        return occupiedLane;
    }

    private bool CanSpawnRoadBlock(bool forceGenerate = false)
    {
        if(!shouldGenerateRoadBlock && !forceGenerate)
        {
            return false;
        }

        roadBlockGenerationTimer += Time.deltaTime;

        // only generate if the obstacled timer is up and enemy is present and enemy greater than equal to player level
        return (roadBlockGenerationTimer > OBSTACLE_TIMER && 
                this.gameManager.EnemyManager.currentEnemy != null) ||
             forceGenerate;
    }
}


public class RoadblockEventArgs : EventArgs
{
    public readonly long RoadblockEndRoadNumber;
    public readonly long RoadblockStartRoadNumber;
    public RoadblockEventArgs(RoadController roadblockStartRoad, int roadblockRoadCount)
    {
        this.RoadblockStartRoadNumber = roadblockStartRoad.MyPathNumber;
        this.RoadblockEndRoadNumber = RoadblockStartRoadNumber + roadblockRoadCount;
    }

}