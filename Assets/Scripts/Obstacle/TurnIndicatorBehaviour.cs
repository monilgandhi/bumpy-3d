using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnIndicatorBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Light left;

    [SerializeField]
    private Light right;

    private LightBehaviour currentTurnSignal;
    private LightBehaviour leftLightBehaviour;
    private LightBehaviour rightLightBehaviour;

    private bool isCurrentTurnSignalOn;

    private const float TURN_LIGHT_FREQUENCY = 3;
    private void Awake() 
    {
        leftLightBehaviour = left.GetComponent<LightBehaviour>();
        rightLightBehaviour = right.GetComponent<LightBehaviour>();
    }

    public void MakeTurn(bool isLeft)
    {
        BumperCopsDebug.Log($"TurnIndicatorBehaviour: Making turn {isLeft} for {this.gameObject.transform.root.name}");
        currentTurnSignal = isLeft ? leftLightBehaviour : rightLightBehaviour;
        currentTurnSignal.StartTurnLights();
    }

    public void CompleteTurn()
    {
        currentTurnSignal.StopLights();
    }

    public void Init(List<Image> turnImages)
    {
        if(turnImages == null || turnImages.Count < 2)
        {
            BumperCopsDebug.LogWarning("turn images null or less than 2");
            return;
        }
        
        this.leftLightBehaviour.InitTurnLight(turnImages[0], TURN_LIGHT_FREQUENCY);
        this.rightLightBehaviour.InitTurnLight(turnImages[1], TURN_LIGHT_FREQUENCY);
    }
}