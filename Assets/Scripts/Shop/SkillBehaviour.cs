using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SkillBehaviour : MonoBehaviour 
{
    [SerializeField]
    private bool isLocked;

    [SerializeField]
    private Skill type;

    [SerializeField]
    private Image fillImage;

    [SerializeField]
    private TMP_Text upgradeCost;

    [SerializeField]
    private Button upgradeButton;

    [SerializeField]
    private ShopCarModel carModel;

    [SerializeField]
    private TMP_Text maxUnlocked;

    [SerializeField]
    private TMP_Text skillPoints;

    [SerializeField]
    private InGameShopManager shopManager;

    private SkillStats stats;
    private bool skillPointsRemainingToAward;
    private string currentItemId;
    private void Awake() 
    {
        carModel.CarModelLoaded += OnCarModelLoaded;  
        BumperCopsDebug.Log("SkillBehaviour: Awake");
        if(upgradeButton != null && !isLocked)
        {
            BumperCopsDebug.Log("Setting up event listener");
            //upgradeButton.onClick.AddListener( delegate { this.shopManager.OnSkillUpgrade(this.type); });
            //shopManager.SkillUpgradePurchased += UpgradeSkill;
        }
    }

    private void UpdateStats()
    {
        BumperCopsDebug.Log("type: " + this.type + " final points: " + this.stats.FinalSkillPoints + " all upgrades: " + this.stats.PointsAfterAllUpgrades);

        if(ShopInventory.Instance.IsItemUnlocked(currentItemId) && !this.isLocked)
        {
            this.fillImage.fillAmount = this.stats.FinalSkillPoints / (float)this.stats.PointsAfterAllUpgrades;
        }

        this.skillPoints.text = CarUtils.DisplaySkillPointsValue(this.stats.Type, this.stats.FinalSkillPoints).ToString();
    }

    public void Display()
    {
        UpdateCost();
    }

    private void UpdateCost()
    {   
        if(!ShopInventory.Instance.IsItemUnlocked(currentItemId) || this.isLocked)
        {
            return;
        }
        
        bool noUpgradeLeft = false;

        if(noUpgradeLeft)
        {
            this.upgradeButton.interactable = false;
            this.upgradeButton.gameObject.SetActive(false);
            this.maxUnlocked.gameObject.SetActive(true);
        }
        else 
        {
            this.upgradeButton.interactable = true;
            this.upgradeButton.gameObject.SetActive(true);
            this.maxUnlocked.gameObject.SetActive(false);
            this.upgradeCost.text = this.stats.NextUpgradeCost.ToString();
        }

        /*if(!noUpgradeLeft && this.stats.NextUpgradeCost > PlayerStats.Instance.CoinCount && !isLocked && !this.shopManager.InAppPurchasBehaviour.AreProductsAvailableForPurchase)
        {
            this.upgradeButton.interactable = false;
        }*/
    }

    private void UpgradeSkill(SkillUpgradeEventArgs args)
    {
        BumperCopsDebug.Log("SkillBehaviour: Upgrading skill");
        UpdateCost();
        if(args.skill != this.type || !ShopInventory.Instance.IsItemUnlocked(args.ItemId))
        {
            return;
        }

        StartCoroutine(Upgrade());
    }

    private IEnumerator Upgrade()
    {
        float requiredFill = this.stats.FinalSkillPoints / (float)this.stats.PointsAfterAllUpgrades;
        BumperCopsDebug.Log("SkillBehaviour: Upgrading now type: " + this.type + " final points: " + this.stats.FinalSkillPoints + " all upgrades: " + this.stats.PointsAfterAllUpgrades);

        while(this.fillImage.fillAmount < requiredFill)
        {
            this.fillImage.fillAmount += Time.unscaledDeltaTime;
            BumperCopsDebug.Log($"fill amount is {this.fillImage.fillAmount}");
            yield return new WaitForSecondsRealtime(0.1f);
        }

        this.skillPoints.text = CarUtils.DisplaySkillPointsValue(this.stats.Type, this.stats.FinalSkillPoints).ToString();
        yield return null;
    }

    private void OnCarModelLoaded(ModelLoadEventArgs args)
    {
        // it is possible that this card is turned off but the eventhandler will still be kicked off
        /*BumperCopsDebug.Log("SkillBehaviour: Loading model " + args.ItemId);
        currentItemId = args.ItemId;
        switch(type)
        {   
            case Skill.ATTACK:
                this.stats = carModel.AttackStats;
                break;
            case Skill.BOOST:
                this.stats = carModel.BoostStats;
                break;
            case Skill.MOVEMENT:
                this.stats = carModel.MovementStats;
                break;
        }

        //BumperCopsDebug.Log("PointsAfterAllUpgrades" + this.stats.PointsAfterAllUpgrades);
        UpdateStats();
        UpdateCost();*/
    }
}