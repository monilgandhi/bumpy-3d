using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;

public class Shop2dModel : MonoBehaviour 
{
    public event Action<ModelLoadEventArgs> PowerupModelLoaded;    
    [SerializeField]
    private ShopPanelBehaviour shopPanel;

    [SerializeField]
    private Image itemImage;

    private const string IMAGE_FOLDER = "screenshots";
    private string itemId;
    private void Awake() 
    {
        this.shopPanel.LoadNewItem += LoadModel;    
    }    

    private void LoadModel(LoadNewItemArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("args to load item are null");
            return;
        }

        if(args.ItemType != ShopItemType.POWERUP)
        {
            Hide();
            return;            
        }

        itemId = args.ItemId;
        this.gameObject.SetActive(true);
        StartCoroutine(Load());
    }

    private IEnumerator Load()
    {
        string basePath = null;

        basePath = CarUtils.GetConfigLocationForShopItem(this.itemId, ShopItemType.POWERUP) + Path.DirectorySeparatorChar + 
                    IMAGE_FOLDER + Path.DirectorySeparatorChar;

        BumperCopsDebug.Log($"Loading image {basePath}");
        ResourceRequest lockedCarRequest = Resources.LoadAsync<Sprite>(basePath + itemId);

        yield return new WaitUntil(() => lockedCarRequest.isDone);

        itemImage.sprite = lockedCarRequest.asset as Sprite;
        PowerupModelLoaded?.Invoke(new ModelLoadEventArgs(itemId));
    }

    private void Hide()
    {
        itemImage.gameObject.SetActive(false);
    }
}