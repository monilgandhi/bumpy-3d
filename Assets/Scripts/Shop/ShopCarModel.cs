using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
public class ShopCarModel : MonoBehaviour
{
    public event Action<ModelLoadEventArgs> CarModelLoaded;    
    public event Action<ModelLoadEventArgs> WeaponLoaded;

    [Header("Game Elements")]    
    [SerializeField]
    private InGameShopManager shopManager;

    [SerializeField]
    private ShopNavigation shopNavigation;

    [SerializeField]
    private ShopPanelBehaviour shopPanel;
    
    [Header("Shop")]
    [SerializeField]
    private Light carmodelLight;

    [SerializeField]
    private ParticleSystem glow;

    [SerializeField]
    private PaintCustomizationBehaviour paintCustomizationBehaviour;

    public PlayerCharacter Character { get; private set; }
    private string currentItemId;
    private bool isRotating;
    private void Awake() 
    {
        this.shopPanel.LoadNewItem += LoadCarModel;
        this.shopNavigation.GoBackClicked += () => HideCharacter();
        this.shopNavigation.ExitClicked += () => Hide();

        this.shopManager.CarPurchased += OnCurrentCarUnlocked;
        this.paintCustomizationBehaviour.LoadNewPaint += OnLoadNewPaint;
    }
    
    private void Start() 
    {
        BumperCopsDebug.Log("REgistering to prefab manager");
        this.shopManager.PrefabManager.PrefabLoadRequestComplete += OnCharacterLoaded;    
        this.gameObject.SetActive(false);
    }

    private void OnLoadNewPaint(PaintCustomizationEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.SkinName))
        {
            BumperCopsDebug.LogWarning("Paint event args is null");
            return;
        }

        BumperCopsDebug.Log($"Loading new skin with name {args.SkinName}");
        if(!Character.LoadNewMaterial(args.SkinName))
        {
            BumperCopsDebug.LogWarning($"Unable to find material with name {args.SkinName} for {this.currentItemId}");
            return;
        }
    }

    private void HideCharacter()
    {
        if(Character != null)
        {
            Character.gameObject.SetActive(false);
            if(Character.Weapon != null)
            {
                Character.Weapon.Deactivate();
            }
        }
        
        Hide();
        isRotating = false;
        this.gameObject.SetActive(false);
    }

    public void Hide()
    {
        BumperCopsDebug.Log("Hide called");
        StopAllCoroutines();        
        isRotating = false;
        this.gameObject.SetActive(false);
    }

    private void OnCharacterLoaded(PrefabLoadEventArgs args)
    {
        if(args == null || args.GObject == null)
        {
            BumperCopsDebug.LogWarning("OnCharacterLoaded args null");
            return;
        }

        if(args.CallingClass != this.GetType())
        {
            return;
        }
        
        BumperCopsDebug.Log($"Prefab load complete {args.PrefabId}");

        Character = args.GObject.GetComponent<PlayerCharacter>();
        if(Character == null)
        {
            throw new MissingComponentException(nameof(Character));
        }

        this.Character.LoadConfig();
        this.Character.ToggleWheels(false);

        currentItemId = Character.Id;
        OnCharacterDataLoaded(null);
    }

    private void OnCharacterDataLoaded(CharacterDataLoadEventArgs args)
    {
        BumperCopsDebug.Log($"ShopCarModel: Car model loaded for {currentItemId}");
        CarModelLoaded?.Invoke(new ModelLoadEventArgs(currentItemId));
        DisplayModel();
    }

    private void DisplayModel()
    {
        //StartCoroutine(Rotate());

        Character.transform.parent = this.transform;
        Character.gameObject.transform.localPosition = Vector3.zero;
        Character.gameObject.transform.localRotation = Quaternion.identity;

        Character.ChangeLayerOfMesh(Constants.SHOP_LAYER);

        // BumperCopsDebug.Log("Displaying model");

        // turn the spotlights off if not unlocked
        bool isUnlocked = ShopInventory.Instance.IsItemUnlocked(currentItemId);
        if(isUnlocked)
        {
            carmodelLight.enabled = true;
            glow.Play();    
        }
        else
        {
            carmodelLight.enabled = false;
            glow.Stop();
        }

        Character.LoadNewMaterial(PlayerStats.Instance.GetSelectedSkin(Character.Id));
        Character.gameObject.SetActive(true);
        // StartCoroutine(Rotate());
    }

    private void LoadCarModel(LoadNewItemArgs args)
    {
        if(string.IsNullOrEmpty(args.ItemId))
        {
            throw new ArgumentNullException(nameof(args.ItemId));
        }

        if(args.ItemType != ShopItemType.CAR && args.ItemType != ShopItemType.WEAPON)
        {
            HideCharacter();
            return;
        }

        BumperCopsDebug.Log($"Loading new car models with {args.ItemId} and type {args.ItemType}");
        LoadNewCharacter(args.CarId);
        if(args.ItemType == ShopItemType.WEAPON)
        {
            LoadNewWeapon(args.ItemId);
        }
    }


    private void LoadNewWeapon(string itemId)
    {
        if(this.Character == null)
        {
            BumperCopsDebug.Log("Character is null. Cannot load weapon");
            return;
        }

        if(Character.Weapon != null)
        {
            BumperCopsDebug.Log("Turning off current weapon");
            Character.Weapon.Deactivate();
        }

        this.Character.WeaponLoaded += OnWeaponLoaded;
        this.Character.LoadNewWeapon(itemId, this.shopManager.PrefabManager);
    }

    private void OnWeaponLoaded(bool success)
    {
        this.Character.WeaponLoaded -= OnWeaponLoaded;
        if(!success)
        {
            BumperCopsDebug.LogError("Failed to load the weapon");
            return;
        }

        BumperCopsDebug.Log("Weapon loaded");
        Character.ChangeLayerOfMesh(Constants.SHOP_LAYER);
        Character.Weapon.Activate(false);
        StartCoroutine(Character.Weapon.Animate(0));
        WeaponLoaded?.Invoke(new ModelLoadEventArgs(Character.Weapon.Id));
    }

    private void LoadNewCharacter(string itemId)
    {
        this.gameObject.SetActive(true);

        if(Character != null)
        {
            if(Character.Id.Equals(itemId))
            {
                OnCharacterDataLoaded(null);
                return;
            }
            // turn this character off
            Character.gameObject.SetActive(false);
        }
        
        BumperCopsDebug.Log($"Loading the prefab with id {itemId}");
        this.shopManager.PrefabManager.LoadPrefabAsync(itemId, this.GetType(), Constants.GetPlayerPrefabPath(itemId));
    }

    private void OnNotEnoughCoins(NotEnoughCoinsEventArgs args)
    {
        Character.gameObject.SetActive(false);
    }

    private void OnDialogueStatusChanged(InAppPurchaseDialogueEventArgs args)
    {
        if(!args.IsShown)
        {
            Character.gameObject.SetActive(true);
        }
    }
    
    private IEnumerator Rotate()
    {
        if(isRotating)
        {
            yield break;
        }

        isRotating = true;
        string characterId = this.Character.Id;
        while(this.Character != null)
        {
            Character.gameObject.transform.Rotate (0, 50 * Time.unscaledDeltaTime, 0);
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }

    private void OnCurrentCarUnlocked(ShopItemEventArgs args)
    {
        carmodelLight.enabled = true;
        this.currentItemId = args.CarId;
        CarModelLoaded?.Invoke(new ModelLoadEventArgs(currentItemId));
    }
}

public class ModelLoadEventArgs
{
    public readonly string ItemId;

    public ModelLoadEventArgs(string itemId)
    {
        this.ItemId = itemId;
    }
}