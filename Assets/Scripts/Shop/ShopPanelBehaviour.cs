using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopPanelBehaviour : MonoBehaviour 
{
    public event Action<LoadNewItemArgs> LoadNewItem;

    [Header("Game elements")]
    [SerializeField]
    private InGameShopManager shopManager;

    [Header("UI Elements")] 

    [SerializeField]
    private GameObject carSelectionBehaviour;

    [SerializeField]
    private Transform content;

    private Dictionary<string, ItemCellBehaviour> carCells;
    private Dictionary<string, ItemCellBehaviour> weaponCells;
    private Dictionary<string, ItemCellBehaviour> powerupCell;
    private Dictionary<string, ItemCellBehaviour> activeCells;

    private RectTransform rectTransform;
    private string selectedItemId;
    private ShopItemType activeType;
    private void Awake() 
    {   
        this.rectTransform = this.GetComponent<RectTransform>();
        carCells = new Dictionary<string, ItemCellBehaviour>();
        weaponCells = new Dictionary<string, ItemCellBehaviour>();
        powerupCell = new Dictionary<string, ItemCellBehaviour>();

        //this.shopManager.NotEnoughCoins += OnContinueClicked;
        this.shopManager.CarPurchased += OnItemPurchased;
        this.shopManager.WeaponPurchased += OnItemPurchased;
    }    

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    
    public void Show(ShopItemType shopItemType)
    {
        if(shopItemType == ShopItemType.NONE)
        {   
            BumperCopsDebug.LogWarning("Invalid shop item type");
            return;
        }

        if(!this.gameObject.activeSelf)
        {
            this.DisplayCard();
        }

        LoadTab(shopItemType);
    }

    private void OnNewTabActivated(TabBehaviour selectedTab)
    {
        if(selectedTab == null)
        {
            BumperCopsDebug.LogWarning("Activated tab is null");
            return;
        }

        content = selectedTab.Content.transform;

        ShopTabBehaviour sTab = selectedTab as ShopTabBehaviour;
        if(sTab == null)
        {
            BumperCopsDebug.LogWarning("Invalid tab for shop");
            return;
        }

        BumperCopsDebug.Log($"Loading tab {sTab.TabItemType}");

        selectedItemId = sTab.TabItemType == ShopItemType.WEAPON ? 
            PlayerStats.Instance.WeaponId: 
            PlayerStats.Instance.CarId;

        LoadTab(sTab.TabItemType);
    }

    private void OnItemPurchased(ShopItemEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.SelectedItem))
        {
            BumperCopsDebug.LogWarning("Invalid ShopItemEventArgs onCarPurchased");
            return;
        }

        ItemCellBehaviour ccb;
        if(activeCells == null || activeCells.Count == 0 || !activeCells.TryGetValue(args.SelectedItem, out ccb))
        {
            return;
        }

        ccb.Init(args.SelectedItem, false, true, args.ItemType);
    }
    
    public void LoadTab(ShopItemType itemType)
    {
        activeType = itemType;
        IReadOnlyList<ShopItem> items = null;
        if(activeCells != null)
        {
            foreach(ItemCellBehaviour icb in activeCells.Values)
            {
                icb.Hide();
            }
        }

        switch(itemType)
        {
            case ShopItemType.CAR:
                items = ShopInventory.Instance.AllCars;
                selectedItemId = PlayerStats.Instance.CarId;
                activeCells = carCells;
                break;

            case ShopItemType.WEAPON:
                items = ShopInventory.Instance.AllWeapons;
                selectedItemId = PlayerStats.Instance.WeaponId;
                activeCells = weaponCells;
                break;

            case ShopItemType.POWERUP:
                items = ShopInventory.Instance.AllPowerups;
                activeCells = powerupCell;
                selectedItemId = items[0].Id;;
                break;
        }

        if(string.IsNullOrEmpty(selectedItemId))
        {
            selectedItemId = items[0].Id;
        }

        BumperCopsDebug.Log($"Loading car cells with selected itemid {selectedItemId}");
        foreach(ShopItem i in items)
        {
            ItemCellBehaviour icb;
            bool needsInit = false;
            if(!activeCells.TryGetValue(i.Id, out icb))
            {
                GameObject go = Instantiate(carSelectionBehaviour, content);
                go.name = i.Id;
                icb = go.GetComponent<ItemCellBehaviour>();
                needsInit = true;
                activeCells.Add(i.Id, icb);
            }
            
            bool isSelected = i.Id.Equals(selectedItemId);
            icb.Show();
            if(needsInit)
            {
                icb.SelectorButton.onClick.AddListener(delegate { OnNewItemClicked(i.Id); });
                icb.Init(i.Id, !i.IsUnlocked, isSelected, itemType);
            }

            if(isSelected)
            {
                BumperCopsDebug.Log($"new item clicked with id {i.Id}");
                OnNewItemClicked(i.Id);
            }
        }        
    }

    public void OnNewItemClicked(string itemId)
    {
        BumperCopsDebug.Log($"behaviour clicked with name {itemId}");

        if(selectedItemId != itemId)
        {
            ItemCellBehaviour selectedItem = null;
            if(!string.IsNullOrEmpty(selectedItemId) && activeCells.TryGetValue(selectedItemId, out selectedItem))
            {
                selectedItem.ToggleItemSelection(false);
            }

            if(!activeCells.TryGetValue(itemId, out selectedItem))
            {
                BumperCopsDebug.LogError($"Could not find item with id {itemId}");
                return;
            }

            selectedItem.ToggleItemSelection(true);
        }

        selectedItemId = itemId;
        string carId = PlayerStats.Instance.CarId;
        if(activeType == ShopItemType.CAR)
        {
            carId = selectedItemId;
        }

        LoadNewItem?.Invoke(new LoadNewItemArgs(itemId, activeType, carId));
    }

    private void DisplayCard()
    {
        BumperCopsDebug.Log("Displaying card");
        this.gameObject.SetActive(true);
    }
}