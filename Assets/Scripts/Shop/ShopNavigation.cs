using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopNavigation : MonoBehaviour 
{
    public event Action GoBackClicked;
    public event Action ExitClicked;
    [SerializeField]
    private InGameShopManager shopManager;

    [SerializeField]
    private ShopPanelBehaviour shopPanel;

    [SerializeField]
    private IAPurchaseBehaviour iapPanel;

    [SerializeField]
    private AnimationCompletedBehaviour animationCompletedBehaviour;

    [SerializeField]
    private Button ContinueButton;

    [SerializeField]
    private Button exitButton;

    [SerializeField]
    private ShopCarModel shopCarModel;

    private TMP_Text continueText;
    private ShopItemType activeType;
    private void Awake() 
    {   
        activeType = ShopItemType.NONE;
        exitButton.onClick.AddListener(Close);
        ContinueButton.onClick.AddListener(ContinueButtonClicked);
        //animationCompletedBehaviour.gameObject.SetActive(false);
        continueText = this.ContinueButton.GetComponentInChildren<TMP_Text>();
        this.shopManager.NotEnoughCoins += OnNotEnoughCoins;
        this.iapPanel.DialogueStatusChanged += OnDialogStatusChanged;
        animationCompletedBehaviour.StartAnimationWithTrigger("hide", true, false);
    }

    public void Show()
    {
        activeType = ShopItemType.NONE;
        shopPanel.gameObject.SetActive(false);
        iapPanel.gameObject.SetActive(false);
        this.gameObject.SetActive(true);
        continueText.text = "Tap to continue";

        if(!this.gameObject.activeInHierarchy)
        {
            animationCompletedBehaviour.gameObject.SetActive(true);
            return;
        }

        animationCompletedBehaviour.StartAnimationWithTrigger("flip", false, false);
    }

    private void OnDialogStatusChanged(InAppPurchaseDialogueEventArgs args)
    {
        if(!args.IsShown)
        {
            Show();
        }
    }

    private void OnNotEnoughCoins(NotEnoughCoinsEventArgs args)
    {
        OnCoinPurchaseClicked();
    }

    public void OnCarClicked()
    {
        activeType = ShopItemType.CAR;
        ShowShopPanel();
    }   

    public void OnWeaponsClicked()
    {
        activeType = ShopItemType.WEAPON;
        ShowShopPanel();
    }   

    public void OnPowerupClicked()
    {
        activeType = ShopItemType.POWERUP;
        ShowShopPanel();
    }

    private void ShowShopPanel()
    {
        if(activeType == ShopItemType.COINS)
        {
            iapPanel.Show();
            shopPanel.Hide();
            shopCarModel.Hide();
        }
        else
        {
            shopPanel.Show(activeType);    
            iapPanel.Hide();
        }

        animationCompletedBehaviour.StartAnimationWithTrigger("flip", false, false);
        continueText.text = "Go back to shop";
        this.gameObject.SetActive(false);
    }

    public void OnCoinPurchaseClicked()
    {
        activeType = ShopItemType.COINS;
        ShowShopPanel();
    }

    private void Close()
    {
        animationCompletedBehaviour.StartAnimationWithTrigger("hide", true, false);
        ExitClicked?.Invoke();
    }

    private void ContinueButtonClicked()
    {
        if(activeType == ShopItemType.NONE)
        {
            Close();
        }
        else
        {
            animationCompletedBehaviour.StartAnimationWithTrigger("flip", false, false);
            Show();
            GoBackClicked?.Invoke();
        }
    }
}