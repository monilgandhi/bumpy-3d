using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class StatsPanelBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image fillImage;

    [SerializeField]
    private ShopCarModel carModel;

    [SerializeField]
    private StatsType type;
    
    // city1_sedan3
    private const int MAX_BOOST = 100;
    private const int MAX_HANDLING = 1;
    private void Awake() 
    {
        carModel.CarModelLoaded += OnCarModelLoaded;  
        BumperCopsDebug.Log("SkillBehaviour: Awake");
    }

    private void UpdateStats()
    {
        if(type == StatsType.BOOST)
        {
            fillImage.fillAmount = carModel.Character.BoostStats / MAX_BOOST;
        }
        else if(type == StatsType.HANDLING)
        {
            fillImage.fillAmount = carModel.Character.HandlingStats / MAX_HANDLING;
        }
        
    }

    private void OnCarModelLoaded(ModelLoadEventArgs args)
    {
        UpdateStats();
    }
}

public enum StatsType
{
    NONE,
    BOOST,
    HANDLING
}