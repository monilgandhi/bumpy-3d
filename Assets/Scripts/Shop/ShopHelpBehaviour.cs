using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopHelpBehaviour : MonoBehaviour 
{
    [Header("Shop elements")]
    [SerializeField]
    private ShopPanelBehaviour shopPanelBehaviour;

    [Header("Where to find - collectible help")]
    [SerializeField]    
    private TMP_Text whereToFindText;
    [SerializeField]
    private TMP_Text userHas;

    [SerializeField]
    private Image collectionUserHas;

    [Header("Title")]
    [SerializeField]
    private TMP_Text titleText;

    [Header("description")]
    [SerializeField]
    private TMP_Text descriptionText;

    [Header("Stats Panel")]
    [SerializeField]
    private GameObject statsPanel;

    private void Awake() 
    {
        shopPanelBehaviour.LoadNewItem += OnLoadNewItem;
    }

    private void OnLoadNewItem(LoadNewItemArgs args)
    {
        BumperCopsDebug.Log("CarCardManager: load item " + args.ItemId);
        titleText.text = ShopInventory.Instance.ItemName(args.ItemId);
        string description = ShopInventory.Instance.ItemDescription(args.ItemId);
        descriptionText.text = description;
        descriptionText.gameObject.SetActive(!string.IsNullOrEmpty(description));
        statsPanel.gameObject.SetActive(args.ItemType == ShopItemType.CAR);
        //BumperCopsDebug.Log("Cost of item is " + ShopInventory.Instance.CurrentItemCost + " and player coin count is " + PlayerStats.Instance.CoinCount);
    }

    public void ShowCollectibleCount(CollectibleType collectible, long userCount)
    {
        this.gameObject.SetActive(true);
        if(collectible == CollectibleType.NONE && userCount == 0)
        {
            Hide();
            return;
        }

        if(userCount == 0)
        {
            userHas.gameObject.SetActive(false);
        }
        else
        {
            userHas.gameObject.SetActive(true);
            userHas.text = $"You have {userCount}";
            
            if(collectible == CollectibleType.NONE)
            {
                collectionUserHas.enabled = false;
                return;
            }

            collectionUserHas.enabled = true;
            BumperCopsDebug.Log($"Getting collectible sprite for {collectible}");
            collectionUserHas.sprite = CarUtils.GetCollectibleSprite(collectible);
        }
    }

    public void SetTitle(string title)
    {
        if(string.IsNullOrEmpty(title))
        {
            titleText.enabled = false;
            return;
        }

        titleText.enabled = true;
        titleText.text = title;
    }

    public void ToggleFindInTreasureChest(bool show)
    {
        whereToFindText.gameObject.SetActive(show);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}