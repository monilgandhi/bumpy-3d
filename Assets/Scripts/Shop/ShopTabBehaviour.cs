using UnityEngine;

public class ShopTabBehaviour : TabBehaviour 
{
    [SerializeField]
    private ShopItemType itemType;
    public ShopItemType TabItemType { get { return itemType; }}
}