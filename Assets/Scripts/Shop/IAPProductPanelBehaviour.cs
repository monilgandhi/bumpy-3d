using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class IAPProductPanelBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Button buyButton;

    [SerializeField]
    private TMP_Text cost;

    [SerializeField]
    private TMP_Text quantity;

    public Button BuyButton { get { return buyButton; }}

    public void Setup(IAPProduct product)
    {
        BumperCopsDebug.Log($"Metadata: localizedprice: {product.Metadata.localizedPriceString}, title: {product.Metadata.localizedTitle} description: {product.Metadata.localizedDescription}");
        this.cost.text = product.Metadata.localizedPriceString;
        this.quantity.text = product.Metadata.localizedDescription;
    }   
}