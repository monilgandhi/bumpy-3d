using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InGameShopManager : MonoBehaviour 
{
    public event Action<ShopItemEventArgs> CarPurchased;
    public event Action<ShopItemEventArgs> WeaponPurchased;
    public event Action<ShopItemEventArgs> PowerupPurchased;
    public event Action<ShopItemEventArgs> SkinPurchased;
    public event Action<CoinSpentEventArgs> CoinsSpent;
    public event Action<NotEnoughCoinsEventArgs> NotEnoughCoins;
    public event Action ShopClosed;

    [SerializeField]
    private GameManager gameManager;
    public PrefabManager PrefabManager { get { return this.gameManager.PrefabManager; }}

    [SerializeField]
    private IAPurchaseBehaviour purchaseManager;
    public IAPurchaseBehaviour InAppPurchasBehaviour { get { return purchaseManager; } }

    [SerializeField]
    private Camera ShopCamera;

    [SerializeField]
    private ShopPanelBehaviour shopPanel;

    [SerializeField]
    private ShopNavigation shopNavigation;

    [SerializeField]
    private CarInteractionPanelBehaviour carInteractionPanel;

    [SerializeField]
    private ParticleSystem unlockingParticleSystem;

    public ShopPanelBehaviour ShopPanel { get { return shopPanel; }}
    private ShopCarModel carModel;
    private Assignment currentAssignment;
    public int CompletedAssignment 
    {
         get 
         {
            if(currentAssignment == null)
            {
                return 0;
            }

            return currentAssignment.AssignmentNumber - 1; 
         }}

    public IAPurchaseManager IAPurchaseManager { get; private set; }
    private void Awake() 
    {
        ShopCamera.gameObject.SetActive(false);
        //this.shopCanvas.gameObject.SetActive(false);
        this.carModel = this.GetComponentInChildren<ShopCarModel>();
        IAPurchaseManager = this.GetComponent<IAPurchaseManager>();
        this.InAppPurchasBehaviour.DialogueStatusChanged += OnDialogueStatusChanged;
        this.shopPanel.LoadNewItem += OnNewItemLoaded;
        this.shopNavigation.ExitClicked += OnExitClicked;
        this.carInteractionPanel.NewItemPurchaseClicked += OnNewItemPurchaseClicked;
    }

    private void OnDialogueStatusChanged(InAppPurchaseDialogueEventArgs args)
    {
        if(args == null)
        {
            throw new ArgumentNullException(nameof(args));
        }

        if(!args.IsShown)
        {
            DisplayShop();
        }
    }
    
    private void OnExitClicked()
    {
        ShopCamera.gameObject.SetActive(false);
        ShopClosed?.Invoke();
    }

    public void DisplayShop()
    {
        BumperCopsDebug.Log("Shop is now active");
        ShopCamera.gameObject.SetActive(true);
        this.shopNavigation.Show();
    }

    private void OnNewItemLoaded(LoadNewItemArgs args)
    {
        ShopInventory.Instance.SelectShopCar(args.ItemId);;
    }

    private bool ValidatePurchaseCost(string purchaseItemId, 
        int purchaseCost, 
        string carId, 
        ShopItemType itemType)
    {
        if(itemType == ShopItemType.CAR || itemType == ShopItemType.WEAPON || itemType == ShopItemType.POWERUP)
        {
            return purchaseCost == ShopInventory.Instance.ItemCost(purchaseItemId);

        } else if (itemType == ShopItemType.SKIN)
        {
            IReadOnlyList<ShopItem> skins = ShopInventory.Instance.GetItemSkins(carId);
            foreach(ShopItem s in skins)
            {
                if(s.Id.Equals(purchaseItemId))
                {
                    return s.Cost == purchaseCost;
                }
            }
        }

        return false;
    }

    public void ObjectiveSkipped(int cost, string objectiveId)
    {
        if(cost <= 0 || string.IsNullOrEmpty(objectiveId))
        {
            BumperCopsDebug.LogWarning("Invalid arguments");
            return;
        }

        if(PlayerStats.Instance.CoinCount < cost)
        {
            BumperCopsDebug.LogWarning($"Player has less coins {PlayerStats.Instance.CoinCount} than required {cost} to unlock objective");
            PlayerStats.Instance.ReduceCoins((int)PlayerStats.Instance.CoinCount);
        }
        else
        {
            PlayerStats.Instance.ReduceCoins(cost);
        }

        OnCoinsSpent(cost, "Mission skipped", objectiveId);
    }

    private void OnNewItemPurchaseClicked(ShopItemEventArgs args)
    {
        if(args == null || 
            string.IsNullOrEmpty(args.SelectedItem) || 
            args.ItemCost <= 0 ||
            args.ItemType == ShopItemType.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid args on new item purchase");
            return;
        }

        if(!ValidatePurchaseCost(args.SelectedItem, args.ItemCost, args.CarId, args.ItemType))
        {
            // todo show the corrrect dialogue
            BumperCopsDebug.LogWarning($"Item costs do not match. Purchaseitem: {args.SelectedItem}, car: {args.CarId}, cost: {args.ItemCost}");
            return;
        }

        CollectibleType currency = ShopInventory.Instance.GetItemCurrency(args.SelectedItem, args.CarId, args.ItemType);
        BumperCopsDebug.Log($"Current is {currency}");
        long currencyWithPlayer = currency == CollectibleType.COIN ? 
            PlayerStats.Instance.CoinCount :
            PlayerStats.Instance.CollectibleInventory.GetCollectibleAmountInventory(currency);

        // check if the player can actuall purchase
        if(currencyWithPlayer < args.ItemCost)
        {
            BumperCopsDebug.Log($"Not enough coins player {currencyWithPlayer} required {args.ItemCost}");
            NotEnoughCoins(new NotEnoughCoinsEventArgs(args.ItemCost - (int)PlayerStats.Instance.CoinCount));
            // TODO show IAP here. but also log
            return;
        }
        
        switch(args.ItemType)
        {
            case ShopItemType.POWERUP:
                if(args.CollectibleType == CollectibleType.NONE)
                {
                    BumperCopsDebug.LogError("invalid collectible type");
                    return;
                }

                CollectibleInventory.Instance.AddCollectible(args.CollectibleType);
                break;

            case ShopItemType.WEAPON:
            case ShopItemType.CAR:
                ShopInventory.Instance.UnlockItem(args.SelectedItem);
                break;

            case ShopItemType.SKIN:
                ShopInventory.Instance.UnlockSkin(args.SelectedItem, args.CarId);
                break;
        }

        // reduce the coin count
        if(currency == CollectibleType.COIN)
        {
            PlayerStats.Instance.ReduceCoins(args.ItemCost);
        }
        else
        {
            BumperCopsDebug.Log($"Reducing collectible {currency} by {args.ItemCost}");
            PlayerStats.Instance.CollectibleInventory.UseCollectible(currency, args.ItemCost);
        }
        
        // add the new model to unlocked
        
        //BumperCopsDebug.Log("Pruchasing car " + currentItemId);

        switch(args.ItemType)
        {
             case ShopItemType.POWERUP:
                PowerupPurchased?.Invoke(args);
                break;

            case ShopItemType.WEAPON:
                WeaponPurchased?.Invoke(args);
                break;

            case ShopItemType.CAR:
                CarPurchased?.Invoke(args);
                break;

            case ShopItemType.SKIN:
                BumperCopsDebug.Log("Invoking skin purchase");
                SkinPurchased?.Invoke(args);
                break;    
        }

        unlockingParticleSystem.Play();
        OnCoinsSpent(args.ItemCost, Constants.CAR_COIN_SPENT_REASON, args.SelectedItem);
    }

    public bool OnBackupSelected(string backupItemId, int cost)
    {
        if(cost > PlayerStats.Instance.CoinCount)
        {
            return false;
        }

        // we can upgrade.
        // reduce the coins and play the animation
        PlayerStats.Instance.ReduceCoins(cost);
        OnCoinsSpent(cost, Constants.BACKUP_COIN_SPENT_REASON, backupItemId);

        return true;
    }

    private void OnCoinsSpent(long coinsSpent, string reason, string itemId)
    {
        CoinsSpent?.Invoke(new CoinSpentEventArgs(coinsSpent, reason, itemId));
    }
}

public class ShopItemEventArgs
{
    public readonly string SelectedItem;
    public readonly string CarId;
    public readonly int ItemCost;
    public readonly ShopItemType ItemType;
    public readonly CollectibleType CollectibleType;
    public ShopItemEventArgs(string purchaseItem, string carId, int cost, ShopItemType itemType)
    {
        this.ItemCost = cost;
        this.SelectedItem = purchaseItem;
        this.CarId = carId;
        this.ItemType = itemType;
    }

    public ShopItemEventArgs(string purchaseItem, int cost, CollectibleType collectibleType)
    {
        this.ItemCost = cost;
        this.SelectedItem = purchaseItem;
        this.ItemType = ShopItemType.POWERUP;
        this.CollectibleType = collectibleType;
    }

    public ShopItemEventArgs(string purchaseItem, int cost, ShopItemType itemType)
    {
        this.ItemCost = cost;
        this.SelectedItem = purchaseItem;
        this.ItemType = itemType;
    }
}

public enum ShopItemType
{
    NONE,
    CAR,
    SKIN,
    WEAPON,
    COINS,
    POWERUP
}