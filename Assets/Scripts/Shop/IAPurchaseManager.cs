using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPurchaseManager : MonoBehaviour, IStoreListener
{
    public event EventHandler<ProductEventArgs> ProductsLoaded;
    public event EventHandler<ProductPurchaseEventArgs> ProductPurchased;
    private static IStoreController m_StoreController; // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    public static string kProductIDConsumable = "com.tinybits.bumpercops.2500coins_test"; 

    public static IReadOnlyCollection<IAPProduct> ProductMetadata { get; private set; }

    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing() 
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        ProductCatalog catalog = ProductCatalog.LoadDefaultCatalog();

        if(catalog == null)
        {
            BumperCopsDebug.Log("catalog null");
        }
        else
        {
            BumperCopsDebug.Log($"product count: {catalog.allProducts.Count}, valid products: {catalog.allValidProducts.Count}");
            BumperCopsDebug.Log($"valid products: {catalog.allValidProducts.Count}");
        }
        // Create a builder, first passing in a suite of Unity provided stores.
        ConfigurationBuilder configurationBuilder = null;

#if UNITY_ANDROID && !UNITY_EDITOR
        if(StandardPurchasingModule.Instance().appStore == AppStore.GooglePlay)
        {
            //configurationBuilderAnd finish adding the subscription product. Notice this uses store-specific IDs, illustrating
            // if the Product ID was configured differently between Apple and Google stores. Also note that
            // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
            // must only be referenced here. 
            configurationBuilder =
                ConfigurationBuilder.Instance(Google.Play.Billing.GooglePlayStoreModule.Instance());
        }
#endif

        if(configurationBuilder == null)
        {
            configurationBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        }
        
        IAPConfigurationHelper.PopulateConfigurationBuilder(ref configurationBuilder, catalog);
        BumperCopsDebug.Log($"products in configuration builder: {configurationBuilder.products.Count}");
        foreach(ProductDefinition p in configurationBuilder.products)
        {
            BumperCopsDebug.Log($"Item id is {p.id}");
        }
        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        // configurationBuilder.AddProduct(kProductIDConsumable, ProductType.Consumable);
        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, configurationBuilder);
    }


    public static bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyCoins(string productId)
    {
        BumperCopsDebug.Log($"Bought product with id {productId}");
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(productId);
    }

    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (!IsInitialized())
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            BumperCopsDebug.LogError("BuyProductID FAIL. Not initialized.");
            return;
        }

        // ... look up the Product reference with the general product identifier and the Purchasing 
        // system's products collection.
        Product product = m_StoreController.products.WithID(productId);

        // If the look up found a product for this device's store and that product is ready to be sold ... 
        if (product == null || !product.availableToPurchase)
        {
            // ... report the product look-up failure situation  
            BumperCopsDebug.LogError("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            return;
        }

        BumperCopsDebug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
        // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
        // asynchronously.
        m_StoreController.InitiatePurchase(product);
    }

    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            BumperCopsDebug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            // ... begin restoring purchases
            BumperCopsDebug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                BumperCopsDebug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            BumperCopsDebug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        BumperCopsDebug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
        SetupProducts();
    }

    private void SetupProducts()
    {
        List<IAPProduct> metadatas = new List<IAPProduct>();

        foreach(Product p in m_StoreController.products.all)
        {
            if(!p.availableToPurchase)
            {
                continue;
            }

            IAPProduct product = new IAPProduct(p.definition.id, p.metadata, p.definition);
            metadatas.Add(product);
        }
        

        ProductMetadata = metadatas;
        ProductsLoaded?.Invoke(this, new ProductEventArgs(ProductMetadata));  
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        BumperCopsDebug.LogError($"Store initialization failed: {error}");
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
    {
        if(args == null || args.purchasedProduct == null || args.purchasedProduct.definition == null || args.purchasedProduct.definition.payout == null)
        {
            BumperCopsDebug.LogError("Purchase product information is null");
            return PurchaseProcessingResult.Pending;
        }

        if(!args.purchasedProduct.availableToPurchase)
        {
            BumperCopsDebug.LogError("Product not available for purchase");
        }


    bool validPurchase = true; // Presume valid for platforms with no R.V.
    IPurchaseReceipt receipt = null;

    // Unity IAP's validation logic is only included on these platforms.
    #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR

        validPurchase = false;
        // Prepare the validator with the secrets we prepared in the Editor
        // obfuscation window.
        var validator = new CrossPlatformValidator(UnityEngine.Purchasing.Security.GooglePlayTangle.Data(),
            UnityEngine.Purchasing.Security.AppleTangle.Data(), Application.identifier);

        try {
            // On Google Play, result has a single product ID.
            // On Apple stores, receipts contain multiple products.
            BumperCopsDebug.Log($"Reciept is {args.purchasedProduct.receipt}");
            IPurchaseReceipt[] receipts = validator.Validate(args.purchasedProduct.receipt);

            validPurchase = receipts != null && receipts.Length > 0;
            receipt = receipts[0];
        } catch (IAPSecurityException e) {
            BumperCopsDebug.LogError($"Invalid receipt, not unlocking content, {e.Message}");
        }
    #endif

        if(!validPurchase)
        {
            BumperCopsDebug.LogError("Invalid purchase not giving coins");
            return PurchaseProcessingResult.Pending;
        }

        BumperCopsDebug.Log($"Giving out purchased product quantity {args.purchasedProduct.definition.payout.quantity}");
        PlayerStats.Instance.IncreaseCoins((long)args.purchasedProduct.definition.payout.quantity);

        IAPProduct product = new IAPProduct(args.purchasedProduct.definition.id, args.purchasedProduct.metadata, args.purchasedProduct.definition);
        ProductPurchased(this, new ProductPurchaseEventArgs(product, args.purchasedProduct.receipt, receipt));

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        BumperCopsDebug.LogError(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}

public class ProductEventArgs : EventArgs
{
    public readonly IReadOnlyCollection<IAPProduct> ProductsMetadata;

    public ProductEventArgs(IReadOnlyCollection<IAPProduct> metadata)
    {
        this.ProductsMetadata = metadata;
    }
}

public class ProductPurchaseEventArgs : EventArgs
{
    public readonly IAPProduct Product;
    public readonly UnityEngine.Purchasing.Security.IPurchaseReceipt PurchaseReceipt;
    public readonly string Receipt;

    public ProductPurchaseEventArgs(IAPProduct product, string receipt, UnityEngine.Purchasing.Security.IPurchaseReceipt purchaseReceipt)
    {
        this.Product = product;
        this.Receipt = receipt;
        this.PurchaseReceipt = purchaseReceipt;
    }
}
