using UnityEngine;
using UnityEngine.UI;
public class PaintButtonBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Button paintButton;  
    public Button PaintButton { get { return paintButton; }}
}