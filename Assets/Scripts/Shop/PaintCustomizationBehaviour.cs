using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintCustomizationBehaviour : MonoBehaviour 
{
    public event Action<PaintCustomizationEventArgs> LoadNewPaint;

    [Header("Game elements")]
    [SerializeField]
    private ShopCarModel carModel;

    [SerializeField]
    private InGameShopManager inGameShopManager;

    [Header("UI elements")]
    [SerializeField]
    private GameObject paintButtonPrefab;

    [SerializeField]
    private HorizontalLayoutGroup contentPanel;

    [SerializeField]
    private Sprite lockedIcon;

    [SerializeField]
    private Sprite unlockedIcon;
    private List<PaintButtonBehaviour> paintButtons;
    private IReadOnlyList<ShopItem> skins;

    private const string PAINT_BUTTON_PREFIX = "paint_";

    private void Awake() 
    {
        InitButtons();
        carModel.CarModelLoaded += OnCarModelLoaded;
        inGameShopManager.SkinPurchased += OnSkinsPurchased;
        //tabManager.ActivateTab += OnNewTabActivated;
    }

    private void OnNewTabActivated(TabBehaviour tab)
    {
        if(tab == null || tab as ShopTabBehaviour == null)
        {
            BumperCopsDebug.LogWarning("Activated Tab is null");
            return;
        }

        ShopTabBehaviour shopTab = tab as ShopTabBehaviour;
        this.gameObject.SetActive(shopTab.TabItemType == ShopItemType.CAR); 
    }

    private void OnSkinsPurchased(ShopItemEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.SelectedItem))
        {
            BumperCopsDebug.LogWarning(("Empty args on skins purchased;"));
            return;
        }

        int selectedPaintIdx = -1;
        for(int i = 0; i < skins.Count; i++)
        {
            if(skins[i].Id.Equals(args.SelectedItem))
            {
                selectedPaintIdx = i;
                break;
            }
        }

        if(selectedPaintIdx < 0 || selectedPaintIdx > paintButtons.Count - 1)
        {
            BumperCopsDebug.LogWarning($"Could not find skin {args.SelectedItem} for car {args.CarId}");
            return;
        }
    }

    private void InitButtons()
    {
        if(paintButtons != null && paintButtons.Count > 0)
        {
            return;
        }

        paintButtons = new List<PaintButtonBehaviour>();
        for(int i = 0; i < 3; i++)
        {
            GameObject go = Instantiate(paintButtonPrefab, contentPanel.transform);
            go.name = $"{PAINT_BUTTON_PREFIX}{i}";
            PaintButtonBehaviour btn = go.GetComponent<PaintButtonBehaviour>();
            btn.PaintButton.onClick.AddListener(delegate{ OnButtonClicked(btn.PaintButton); });
            go.SetActive(false);
            paintButtons.Add(btn);
        }
    }

    private void OnCarModelLoaded(ModelLoadEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Empty argument for loading new item");
            return;
        }

        InitButtons();

        // turn off  the buttons
        for(int i = 0; i < paintButtons.Count; i++)
        {
            paintButtons[i].gameObject.SetActive(false);
        }
        
        string selectedSkinName = PlayerStats.Instance.GetSelectedSkin(args.ItemId);
        BumperCopsDebug.Log($"selected skin name is {selectedSkinName}");
        bool isItemUnlocked = PlayerStats.Instance.IsItemUnlocked(args.ItemId);

        skins = ShopInventory.Instance.GetItemSkins(args.ItemId);

        if(skins == null)
        {
            BumperCopsDebug.LogWarning($"No skins rrerturned for item with id {args.ItemId}");
            return;
        }

        // load the name of materials from shop.
        for(int i = 0; i < skins.Count; i++)
        {
            PaintButtonBehaviour currentPaintButton = paintButtons[i];
            currentPaintButton.gameObject.SetActive(true);
            currentPaintButton.PaintButton.interactable = isItemUnlocked;
            string skinId = skins[i].Id;
            if(selectedSkinName.Equals(skinId))
            {
                currentPaintButton.PaintButton.Select();
            }
        }
    }

    public void OnButtonClicked(Button button)
    {
        BumperCopsDebug.Log($"Button selected with name {button.gameObject.name}");

        // remove the prefix
        string[] idxSplit = button.name.Split('_');
        if(idxSplit.Length < 2)
        {
            BumperCopsDebug.LogWarning($"Invalid paint bucket name {button.name}");
            return;
        }

        int idx = -1;
        if(!Int32.TryParse(idxSplit[1], out idx) || idx > skins.Count - 1)
        {
            BumperCopsDebug.LogWarning($"Unable to parse index for paint bucket {button.name}");
            return;
        }

        BumperCopsDebug.Log($"material name is {skins[idx]}");
        LoadNewPaint?.Invoke(new PaintCustomizationEventArgs(skins[idx].Id));
    }
}

public class PaintCustomizationEventArgs
{
    public string SkinName;

    public PaintCustomizationEventArgs(string skinName)
    {
        this.SkinName = skinName;
    }
}