using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CarInteractionPanelBehaviour : MonoBehaviour 
{
    public event Action<ShopItemEventArgs> NewItemPurchaseClicked;

    [Header("Game elements")]

    [SerializeField]
    private PaintCustomizationBehaviour paintCustomization;

    [SerializeField]
    private InGameShopManager inGameShopManager;

    [SerializeField]
    private ShopCarModel carModel;

    [SerializeField]
    private Shop2dModel shop2DModel;

    [Header("UI elements")]
    [SerializeField]
    private TMP_Text itemCost;

    [SerializeField]
    private Image costIcon;

    [SerializeField]
    private Button selectButton;

    [SerializeField]
    private Button purchaseButton;

    [SerializeField]
    private ShopHelpBehaviour collectibleHelpBehaviour;

    private string currentCarId;
    private bool isCurrentItemUnlocked;
    private string selectedItemId;
    private int itemCostForPurchase;
    private ShopItemType selectedItemType;
    private string playerSelectedCar;
    private string playerSelectedSkin;
    private string playerSelectedWeapon;

    private void Awake() 
    {
        BumperCopsDebug.Log($"Name is {this.gameObject.name}");
        purchaseButton.onClick.AddListener(OnItemPurchaseClicked);
        selectButton.onClick.AddListener(OnNewItemSelected);
        paintCustomization.LoadNewPaint += OnLoadNewPaint;
        inGameShopManager.CarPurchased += OnNewItemPurchased;  
        inGameShopManager.SkinPurchased += OnNewItemPurchased;
        inGameShopManager.WeaponPurchased += OnNewItemPurchased;
        inGameShopManager.PowerupPurchased += OnNewItemPurchased;
        
        carModel.CarModelLoaded += OnCarModelLoaded;
        carModel.WeaponLoaded += OnWeaponModelLoaded;
        shop2DModel.PowerupModelLoaded += OnPowerupLoaded;

        playerSelectedCar = PlayerStats.Instance.CarId;
        playerSelectedSkin = PlayerStats.Instance.GetSelectedSkin(playerSelectedCar);
    }

    private void OnItemPurchaseClicked()
    {
        BumperCopsDebug.Log($"Purchasing item {selectedItemId}, for cost {itemCostForPurchase}");

        ShopItemEventArgs args = null;
        switch(selectedItemType)
        {
            case ShopItemType.POWERUP:
                CollectibleType type = ShopInventory.Instance.PowerupCollectibleType(selectedItemId);
                args = new ShopItemEventArgs(selectedItemId, itemCostForPurchase, type);
                break;

            default:
                args = new ShopItemEventArgs(selectedItemId, currentCarId, itemCostForPurchase, selectedItemType);
                break;
        }

        NewItemPurchaseClicked?.Invoke(args);
    }

    private void OnPowerupLoaded(ModelLoadEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.ItemId))
        {
            BumperCopsDebug.LogWarning("args are null on weapon model loaded");
            return;
        }

        selectedItemId = args.ItemId;
        isCurrentItemUnlocked = true;
        selectedItemType = ShopItemType.POWERUP;

        SetupForItem();
    }

    private void OnWeaponModelLoaded(ModelLoadEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.ItemId))
        {
            BumperCopsDebug.LogWarning("args are null on weapon model loaded");
            return;
        }

        selectedItemId = args.ItemId;
        selectedItemType = ShopItemType.WEAPON;
        isCurrentItemUnlocked = ShopInventory.Instance.IsItemUnlocked(selectedItemId);

        SetupForItem();
    }

    private void OnCarModelLoaded(ModelLoadEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.ItemId))
        {
            BumperCopsDebug.LogWarning("args are null on car model loaded");
            return;
        }

        selectedItemId = args.ItemId;
        currentCarId = args.ItemId;
        selectedItemType = ShopItemType.CAR;

        //BumperCopsDebug.Log($"player item is {ShopInventory.Instance.PlayersSelectedItem.Id} and arg item is {currentItemId}");
        isCurrentItemUnlocked = ShopInventory.Instance.IsItemUnlocked(selectedItemId);
        SetupForItem();
    }

    private void SetupForItem()
    {
        int currentItemCost = ShopInventory.Instance.ItemCost(selectedItemId);
        CollectibleType currency = ShopInventory.Instance.GetItemCurrency(selectedItemId, currentCarId, selectedItemType);
        long currentCurrencyRemaining = currency == CollectibleType.COIN ? 
                                        PlayerStats.Instance.CoinCount : 
                                        CollectibleInventory.Instance.GetCollectibleAmountInventory(currency);


        switch(selectedItemType)
        {
            case ShopItemType.CAR:
                BumperCopsDebug.Log($"car selected with unlock status {isCurrentItemUnlocked}");
                if(isCurrentItemUnlocked)
                {
                    bool isSelected = playerSelectedCar.Equals(currentCarId);
                    this.SetSelectButton(isSelected, currentCarId, ShopItemType.CAR);
                }
                else
                {
                    this.SetPurchaseButton(currentItemCost, currentCarId, ShopItemType.CAR);
                }

                int missionRequirement = ShopInventory.Instance.ItemMissionRequirement(currentCarId);
                if(inGameShopManager.CompletedAssignment < missionRequirement)
                {
                    // show mission locked button
                    this.MissionLock(missionRequirement);
                }
                else if(ShopInventory.Instance.IsItemContentLocked(currentCarId))
                {
                    this.ContentLocked();
                }

                paintCustomization.gameObject.SetActive(true);
                collectibleHelpBehaviour.Hide();
                break;

            case ShopItemType.POWERUP:
                CollectibleType type = ShopInventory.Instance.PowerupCollectibleType(selectedItemId);
                int currentAmount = CollectibleInventory.Instance.GetCollectibleAmountInventory(type);
                SetPurchaseButton(currentItemCost, selectedItemId, ShopItemType.POWERUP);
                this.collectibleHelpBehaviour.ShowCollectibleCount(CollectibleType.NONE, currentAmount);
                this.collectibleHelpBehaviour.ToggleFindInTreasureChest(false);

                break;

            case ShopItemType.WEAPON:
                BumperCopsDebug.Log($"currency remaining for weapons {currentCurrencyRemaining}");
                if(!isCurrentItemUnlocked)
                {
                    SetPurchaseButton(currentItemCost, selectedItemId, ShopItemType.WEAPON, currency);
                }
                else
                {
                    SetSelectButton(PlayerStats.Instance.WeaponId.Equals(selectedItemId), selectedItemId, ShopItemType.WEAPON);
                }
                
                paintCustomization.gameObject.SetActive(false);

                this.collectibleHelpBehaviour.ShowCollectibleCount(currency, currentCurrencyRemaining);
                this.collectibleHelpBehaviour.ToggleFindInTreasureChest(true);
                break;
        }
    }

    private void OnNewItemSelected()
    {
        isCurrentItemUnlocked = true;
        this.SetSelectButton(true, selectedItemId, selectedItemType);
        SelectItemIfPossibleForPlay(selectedItemId, selectedItemType, currentCarId);
        SetupForItem();
    }

    private void OnNewItemPurchased(ShopItemEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogError("args arrer null on car purchased");
            return;
        }

        BumperCopsDebug.Log($"New item purhcased with car id {args.CarId} and product id {args.SelectedItem}");

        if(args.ItemType == ShopItemType.POWERUP)
        {

        }
        else
        {
            currentCarId = args.CarId;
        }

        itemCostForPurchase = 0;
        isCurrentItemUnlocked = true;
        selectedItemType = args.ItemType;
        OnNewItemSelected();
    }

    private void SelectItemIfPossibleForPlay(string itemId, ShopItemType itemType, string carId)
    {
        switch(itemType)
        {
            case ShopItemType.WEAPON:
                if(!ShopInventory.Instance.SelectNewItemForPlay(carId, carId))
                {
                    BumperCopsDebug.LogWarning("Unable to select the car for playing");
                    return;
                }

                playerSelectedWeapon = carId;
                break;

            case ShopItemType.CAR:
                if(!ShopInventory.Instance.SelectNewItemForPlay(carId, carId))
                {
                    BumperCopsDebug.LogWarning("Unable to select the car for playing");
                    return;
                }

                playerSelectedCar = carId;
                break;

            case ShopItemType.SKIN:
                if(!ShopInventory.Instance.SelectSkinForPlay(itemId, carId))
                {
                    BumperCopsDebug.LogWarning("Unable to select the car for playing");
                    return;
                } 

                playerSelectedSkin = itemId;  
                break;
        }   
    }

    private void OnLoadNewPaint(PaintCustomizationEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.SkinName))
        {
            BumperCopsDebug.LogWarning("empty args in NewItemSelected");
            return;
        }

        BumperCopsDebug.Log($"Loading new paing {args.SkinName}");
        foreach(ShopItem c in ShopInventory.Instance.GetItemSkins(currentCarId))
        {
            if(!c.Id.Equals(args.SkinName))
            {
                continue;
            }

            BumperCopsDebug.Log($"Is skin {c.Id} unlocked {c.IsUnlocked}");

            if(c.IsUnlocked)
            {
                this.SetSelectButton(c.IsSelectedItem, c.Id, ShopItemType.SKIN);
            }
            else
            {
                int cost = c.IsUnlocked ? 0 : c.Cost;
                this.SetPurchaseButton(cost, c.Id, ShopItemType.SKIN);
            }
        }
    }

    public void MissionLock(int requiredMission)
    {
        // show mission locked button
        itemCost.text = $"Complete mission {requiredMission}";
        purchaseButton.interactable = false;
    }

    private void SetSelectButton(bool isSelected, 
        string itemId, 
        ShopItemType itemType)
    {
        selectedItemId = itemId;
        selectedItemType = itemType;
        purchaseButton.gameObject.SetActive(false);
        selectButton.gameObject.SetActive(!isSelected);
    }

    private void SetPurchaseButton(int requiredCost, 
        string itemId, 
        ShopItemType itemType,
        CollectibleType currency = CollectibleType.COIN)
    {
        selectedItemId = itemId;
        itemCostForPurchase = requiredCost;
        this.selectedItemType = itemType;

        selectButton.gameObject.SetActive(false);

        // set purchase button
        purchaseButton.gameObject.SetActive(true);
        itemCost.text = requiredCost.ToString();
        purchaseButton.interactable = true;
        long playerCollectibleCount = currency == CollectibleType.COIN ?
            PlayerStats.Instance.CoinCount :
            PlayerStats.Instance.CollectibleInventory.GetCollectibleAmountInventory(currency);
        
        BumperCopsDebug.Log($"player collectible  count is {playerCollectibleCount} for {currency}");

        if(currency == CollectibleType.COIN)
        {
            purchaseButton.interactable = requiredCost <= playerCollectibleCount ||
                this.inGameShopManager.InAppPurchasBehaviour.AreProductsAvailableForPurchase;
        }
        else
        {
            purchaseButton.interactable = requiredCost <= playerCollectibleCount;
        }

        costIcon.sprite = CarUtils.GetCollectibleSprite(currency);
    }

    public void ContentLocked()
    {
        itemCost.text = "Coming Soon";
        purchaseButton.interactable = false;
    }
}