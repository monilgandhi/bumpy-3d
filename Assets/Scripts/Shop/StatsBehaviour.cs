using UnityEngine;
using TMPro;
public class StatsBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text value;

    [SerializeField]
    private ShopCarModel carModel;

    [SerializeField]
    private bool isBounty;

    [SerializeField]
    private bool isAttack;

    private void Awake() 
    {
        carModel.CarModelLoaded += OnCarModelLoaded;
    }

    public void OnCarModelLoaded(ModelLoadEventArgs args)
    {
        BumperCopsDebug.Log("Car model loaded, loading stats now");
        if(isAttack)
        {
            value.text = $"{carModel.Character.AttackStats}";
        }
        else
        {
            value.text = $"{carModel.Character.BoostStats}";
        }
    }
}