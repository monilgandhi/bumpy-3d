using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ItemCellBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image screenShotImage;

    [SerializeField]
    private Image lockImage;

    [SerializeField]
    private Button selectorbutton;
    public Button SelectorButton { get { return selectorbutton; }}
    private Sprite lockedCarImage;
    private string itemId;
    private bool isLocked;
    
    private bool selectItem;
    public bool IsSelected 
    {
        get { return selectItem; }
        private set
        {
            selectItem = value;
            ToggleSelection();
        }
    }
    private const string IMAGE_FOLDER = "screenshots";
    public void Init(string itemId, bool isLocked, bool isSelected, ShopItemType itemType)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }

        if(itemType != ShopItemType.CAR && itemType != ShopItemType.WEAPON && itemType != ShopItemType.POWERUP)
        {
            BumperCopsDebug.LogWarning($"Unsupported itemType {itemType}");
            return;
        }

        this.itemId = itemId;
        this.isLocked = isLocked;
        StartCoroutine(Load(itemType));
        IsSelected = isSelected;
    }

    private IEnumerator Load(ShopItemType type)
    {
        string basePath = null;

        basePath = CarUtils.GetConfigLocationForShopItem(this.itemId, type) + Path.DirectorySeparatorChar + 
                    IMAGE_FOLDER + Path.DirectorySeparatorChar;

        BumperCopsDebug.Log($"Loading image {basePath + itemId}");
        ResourceRequest lockedCarRequest = Resources.LoadAsync<Sprite>(basePath + itemId);

        yield return new WaitUntil(() => lockedCarRequest.isDone);

        lockedCarImage = lockedCarRequest.asset as Sprite;

        if(lockedCarImage == null)
        {
            BumperCopsDebug.LogWarning($"Could not load the screenshot for {this.itemId}");
            this.gameObject.SetActive(false);
            yield break;
        }

        screenShotImage.sprite = lockedCarImage;
        lockImage.enabled = isLocked;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void ToggleItemSelection(bool isSelected)
    {
        IsSelected = isSelected;
    }

    private void ToggleSelection()
    {
        RectTransform r = this.GetComponent<RectTransform>();
        float newScale = IsSelected ? 1.4f : 1.0f;
        r.localScale = new Vector3(newScale, newScale, newScale);
    }
}