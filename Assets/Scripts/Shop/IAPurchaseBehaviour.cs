using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class IAPurchaseBehaviour : MonoBehaviour 
{
    public event Action<InAppPurchaseDialogueEventArgs> DialogueStatusChanged;
    public event EventHandler<IAPEventArgs> IAP;

    [SerializeField]
    private InGameShopManager shopManager;

    [SerializeField]
    private GameObject panelPrefab;

    [SerializeField]
    private GridLayoutGroup gridLayoutContainer;
    public delegate void InAppPurchaseEventHandler(InAppPurchaseType type);

    public bool AreProductsAvailableForPurchase { private set; get; }
    private void Start() 
    {
        AreProductsAvailableForPurchase = false;
        this.shopManager.NotEnoughCoins += OnNotEnoughCoins;

        if(IAPurchaseManager.IsInitialized())
        {
            OnProductsLoaded(this, new ProductEventArgs(IAPurchaseManager.ProductMetadata));
        }

        this.shopManager.IAPurchaseManager.ProductsLoaded += OnProductsLoaded;
        this.shopManager.IAPurchaseManager.ProductPurchased += OnInAppPurchase;
    }

    private void OnProductsLoaded(object o, ProductEventArgs args)
    {
        if(args == null || args.ProductsMetadata == null || args.ProductsMetadata.Count == 0)
        {
            BumperCopsDebug.LogWarning("No products available");
            return;
        }

        foreach(IAPProduct p in args.ProductsMetadata)
        {
            GameObject panelObject = Instantiate(panelPrefab, gridLayoutContainer.transform);
            IAPProductPanelBehaviour panelBehaviour = panelObject.GetComponent<IAPProductPanelBehaviour>();
            panelBehaviour.BuyButton.onClick.AddListener(delegate { OnProductPurchaseClicked(p.ProductId); });
            panelBehaviour.Setup(p);
        }

        AreProductsAvailableForPurchase = true;
    }

    private void OnProductPurchaseClicked(string productId)
    {
        this.shopManager.IAPurchaseManager.BuyCoins(productId);
    }

    private void OnNotEnoughCoins(NotEnoughCoinsEventArgs args)
    {
        this.Show();
    }

    private  void OnInAppPurchase(object o, ProductPurchaseEventArgs args)
    {
        IAPEventArgs eventArgs = null;
        if(args != null)
        {
            eventArgs = new IAPEventArgs(args.Product);
        }

        IAP(this, eventArgs);
        DialogueStatusChanged?.Invoke(new InAppPurchaseDialogueEventArgs(false));
    }   

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}


public class IAPEventArgs : EventArgs
{
    public readonly IAPProduct Product;

    public IAPEventArgs(IAPProduct product)
    {
        this.Product = product;
    }
}