using System.Collections;
using UnityEngine;

public class AttackEffectBehaviour : MonoBehaviour 
{
    [SerializeField]
    private bool playBodySlam;

    [SerializeField]
    private ParticleSystem bodySlam;
    
    private void Awake() 
    {
        this.bodySlam.Stop();
    }    

    public void Attack(PlayerController player)
    {
        if(playBodySlam)
        {
            this.bodySlam.Play();
            //BumperCopsDebug.Log("Shaking camera");
            player.ShakeCamera(5, 0.5f);
        }
    }
}