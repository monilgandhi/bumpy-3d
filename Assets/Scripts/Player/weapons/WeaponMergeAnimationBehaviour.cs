using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMergeAnimationBehaviour : MonoBehaviour 
{
    public event EventHandler<WeaponMergeAnimationEventArgs> WeaponPartAnimationComplete;

    [SerializeField]
    private int totalParts;

    [SerializeField]
    [Tooltip("Exclude the main part that flies on camera")]
    private List<GameObject> parts;

    [SerializeField]
    private GameObject mainPart;

    private Animator animator;
    private void Awake() 
    {
        this.animator = this.GetComponent<Animator>();
    }

    public void PartAssembled(int partNumber)
    {
        if(WeaponPartAnimationComplete != null)
        {
            WeaponPartAnimationComplete(this, new WeaponMergeAnimationEventArgs(partNumber, totalParts == partNumber));
        }
    }

    public void StartMerge()
    {
        this.animator.SetTrigger("merge");
    }

    private IEnumerator Disintegrate()
    {
        // now add a force on the couple of wheels
        for(int i = 0; i < parts.Count; i++)
        {
            GameObject p = this.parts[i];
            if(p == null || p.gameObject == null)
            {
                continue;
            }
            
            Rigidbody partRigidBody = p.gameObject.GetComponent<Rigidbody>();
            if(partRigidBody == null)
            {
                yield break;
            }

            partRigidBody.mass = 30;
            partRigidBody.AddExplosionForce(200f, transform.position, 10f, 1f, ForceMode.Impulse);
        }

        float startTime = Time.time;
        while(Time.time - startTime < 0.1f)
        {
            Vector3 direction = Camera.main.transform.position - mainPart.transform.position;
            Rigidbody mainPartRigidbody = mainPart.GetComponent<Rigidbody>();
            mainPartRigidbody.AddForce(direction.normalized * 1000f, ForceMode.Impulse);
            yield return new WaitForFixedUpdate();
        }
    }
}

public class WeaponMergeAnimationEventArgs : EventArgs
{
    public readonly int PartNumber;
    public readonly bool LastPart;

    public WeaponMergeAnimationEventArgs(int partNumber, bool finalPart)
    {
        this.PartNumber = partNumber;
        this.LastPart = finalPart;
    }
}