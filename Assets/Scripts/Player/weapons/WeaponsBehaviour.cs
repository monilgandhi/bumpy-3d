using System.Collections;
using UnityEngine;
public class WeaponsBehaviour : MonoBehaviour
{
    [SerializeField]
    private string id;

    [SerializeField]
    private WeaponMergeAnimationBehaviour mergeWeapon;
    public WeaponMergeAnimationBehaviour MergeWeaponAnimation { get { return mergeWeapon; }}

    [SerializeField]
    private GameObject actualWeapon;

    [SerializeField]
    private float distanceFromEnemy;
    public float WeaponReach { get { return distanceFromEnemy; }}
    private AnimationCompletedBehaviour actualWeaponAnimator;
    public bool FireAnimationComplete { get; private set; }
    public bool IsActive { get; private set; }
    public string Id { get { return id; }}

    private bool isAnimationOver;
    private void Awake() 
    {
        actualWeaponAnimator = actualWeapon.GetComponent<AnimationCompletedBehaviour>();
        actualWeaponAnimator.AnimationOver += OnAnimationOver;
    }

    private void OnAnimationOver(AnimationCompleteEventArgs args)
    {
        BumperCopsDebug.Log($"Animation over with key {args.Key}");
        isAnimationOver = args != null && args.Key.Equals("Fire");
    }

    private void Start() 
    {
        if(!this.IsActive)
        {
            this.gameObject.SetActive(false);
        }
    }

    public IEnumerator Animate(int count = 1, float wait = 0.0f)
    {
        BumperCopsDebug.Log("Firing animation starting");
        FireAnimationComplete = false;
        if(this.mergeWeapon != null && this.mergeWeapon.gameObject.activeSelf)
        {
            this.mergeWeapon.gameObject.SetActive(false);
        }

        this.actualWeapon.SetActive(true);

        if(wait > 0)
        {
            yield return new WaitForSecondsRealtime(wait);
        }
        
        if(count == 0)
        {
            while(IsActive)
            {
                isAnimationOver = false;
                actualWeaponAnimator.StartAnimationWithTrigger("Fire", false, false);
                BumperCopsDebug.Log("StartedAnimation");
                yield return new WaitWhile(() => !isAnimationOver);
            }
        }
        else
        {
            for(int i = 0; i < count; i++)
            {
                isAnimationOver = false;
                actualWeaponAnimator.StartAnimationWithTrigger("Fire", false, false);
                yield return new WaitWhile(() => !isAnimationOver);
            }
        }

        BumperCopsDebug.Log("Animation complete");
        FireAnimationComplete = true;
    }

    public void Activate(bool mergeAnimation)
    {
        this.gameObject.SetActive(true);
        BumperCopsDebug.Log($"Turning on {this.gameObject.name}");
        IsActive = true;
        if(mergeAnimation && mergeWeapon != null)
        {
            mergeWeapon.gameObject.SetActive(true);
            this.actualWeapon.SetActive(false);
            mergeWeapon.StartMerge();
        } 
        else
        {
            if(mergeWeapon != null)
            {
                mergeWeapon.gameObject.SetActive(false);
            }

            this.actualWeapon.SetActive(true);
        }
    }    

    public void Deactivate(bool animate = false)
    {
        if(!IsActive)
        {
            return;
        }
        IsActive = false;
        BumperCopsDebug.Log($"Turning off {this.gameObject.name}");

        if(animate)
        {
            StartCoroutine(Disappear());
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }

    private IEnumerator Disappear()
    {
        int flickerCount = 0;
        float timeToWait = 0.3f;
        while(flickerCount < 3)
        {
            ++flickerCount;
            actualWeapon.gameObject.SetActive(false);
            yield return new WaitForSeconds(timeToWait);
            actualWeapon.gameObject.SetActive(true);
            yield return new WaitForSeconds(timeToWait);
        }

        this.gameObject.SetActive(false);
    }
}