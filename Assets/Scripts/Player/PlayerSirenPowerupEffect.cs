using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlayerSirenPowerupEffect : MonoBehaviour 
{
    public event Action SirePowerupPlayed;
    [SerializeField]
    private GameObject playerSirenEffectPrefab;

    [Header("UI elemnts")]
    [SerializeField]
    private List<BonusLightsBehaviour> bonusLightsBehaviour;

    private ParticleSystem playerSirenEffect;
    private bool isSirenEffectOn;

    private void Awake() 
    {
        GameObject go = Instantiate(playerSirenEffectPrefab, this.transform);
        playerSirenEffect = go.GetComponent<ParticleSystem>();    
    }

    public void Init(PowerupManager powerupManager)
    {
        powerupManager.PowerupCollected += OnPowerupCollected;
        powerupManager.TimedPowerupExpired += OnTimedPowerupExpired;
        isSirenEffectOn = false;
    }

    private void OnTimedPowerupExpired(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.SIREN)
        {
            return;
        }

        isSirenEffectOn = false;
        bonusLightsBehaviour.ForEach(t => t.ToggleScreenPanels(false));
        StopCoroutine(StartPlayingEffect());
        BumperCopsDebug.Log("Siren power expired");
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.SIREN)
        {
            return;
        }

        isSirenEffectOn = true;
        bonusLightsBehaviour.ForEach(t => t.ToggleScreenPanels(true));
        StartCoroutine(StartPlayingEffect());
    }

    private IEnumerator StartPlayingEffect()
    {
        while(isSirenEffectOn)
        {
            SirePowerupPlayed?.Invoke();
            playerSirenEffect.Play();
            yield return new WaitForSeconds(1f);
            playerSirenEffect.Stop();
            yield return new WaitForSeconds(1f);
        }
    }
}