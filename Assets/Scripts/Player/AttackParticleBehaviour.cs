﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackParticleBehaviour : MonoBehaviour
{
    public bool IsInAttackCircle { get; private set; }
    public event Action AttackMeterFilled;
    [SerializeField]
    private ParticleSystem followAttack;

    [SerializeField]
    private ParticleSystem flashEffectOnReady;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private EnemyManager enemyManager;
    private float distanceBehindEnemy;
    private EnemyController enemy;
    private bool canBump;
    private SphereCollider sphereCollider;
    private float colliderOriginalRadius;
    private bool playerEnteredTheAttackZone = false;
    private bool canActivateAttackZone = false;
    private float attackMeterFill;
    /// <summary>
    /// this has been measured during gameplay. Hence the magic number;
    /// </summary>
    private const float COLLIDER_MIN_SIZE = 1.8f;
    // Use this for initialization

    public bool IsPerfectAttack 
    {
        get
        {
            return this.canBump && this.flashEffectOnReady.isPlaying;
        }
    }

    private void Awake() 
    {
        this.sphereCollider = this.GetComponent<SphereCollider>();
        colliderOriginalRadius = this.sphereCollider.radius;
    }

    void Start()
    {
        //enemy events
        this.enemyManager.EnemySpawned += OnEnemySpawned;

        this.player.AttackController.Bumped += OnAttacked;
        this.player.healthController.HealthOver += ResetParticle;
        this.player.AttackController.PlayerEnteredAttackZone += () => 
        {
            BumperCopsDebug.Log("Entering the attack zone");
            playerEnteredTheAttackZone = true;
        };

        this.player.AttackController.WeaponReady += () =>
        {
            distanceBehindEnemy = this.player.AttackController.WeaponReach;
        };

        canActivateAttackZone = true;
        ResetParticle();
    }

    /// <summary>
    /// During follow and attack we keep reducing the zone so that we can force the player to attack straight
    /// Once the attack is done we want to increase the zone back to normal
    /// </summary>
    /// <returns></returns>
    private IEnumerator ResizeTheZone()
    {
        while(this.sphereCollider.radius < colliderOriginalRadius)
        {
            this.sphereCollider.radius += Time.deltaTime;
            this.followAttack.transform.localScale = new Vector3(this.sphereCollider.radius, this.sphereCollider.radius, this.sphereCollider.radius);
            yield return new WaitForEndOfFrame();
        }
    }

    /// <summary>
    /// Reset the particle. Called in the beginning and once the enemy is destroyed
    /// </summary>
    private void ResetParticle()
    {
        //BumperCopsDebug.Log("AttackParticleBehaviour: Resetting the particle");
        this.enemy = null;
        playerEnteredTheAttackZone = false;
        followAttack.gameObject.SetActive(false);
        flashEffectOnReady.gameObject.SetActive(false);
    }

    /// <summary>
    /// Event call back when the player bumps the enemy
    /// </summary>
    private void OnAttacked(BumpEventArgs e)
    {
        ChangeColorOfAttackParticle(Color.red);
        attackMeterFill = 0f;
        this.canBump = false;
        playerEnteredTheAttackZone = false;
        flashEffectOnReady.Stop();
        flashEffectOnReady.gameObject.SetActive(false);
        canActivateAttackZone = false;
        StartCoroutine(ResizeTheZone());
        StartCoroutine(WaitAndActivateAttackZone());
    }

    // it is possible that the enemy is hit, but they stay within then attack zone for few moments
    // before running away. This will cause the particle to stay behind and cause a wierd bumping effect.
    private IEnumerator WaitAndActivateAttackZone()
    {
        yield return new WaitForSeconds(2f);
        canActivateAttackZone = true;
    }

    private void OnAttackMeterReady()
    {
        this.canBump = true;
        PlayAttackBarCompleteEffect();
    }

    private void PlayAttackBarCompleteEffect()
    {
        followAttack.Stop();
        ChangeColorOfAttackParticle(Color.green);
        followAttack.Play();

        ParticleSystem.MainModule main = flashEffectOnReady.main;
        main.useUnscaledTime = true;

        flashEffectOnReady.gameObject.SetActive(true);
        if(!flashEffectOnReady.isPlaying)
        {
            flashEffectOnReady.Play();
        }

        //BumperCopsDebug.Log("Attack meter filled");
        AttackMeterFilled?.Invoke();
    }

    private void ChangeColorOfAttackParticle(Color newColor)
    {
        ParticleSystem[] ps = followAttack.GetComponentsInChildren<ParticleSystem>();
        foreach(ParticleSystem p in ps)
        {
            ParticleSystem.MainModule main = p.main;
            main.startColor = new Color(newColor.r, newColor.g, newColor.b, main.startColor.color.a);
        }
    }
    private void LateUpdate()
    {
        if(enemy == null || !this.gameObject.activeSelf)
        {
            return;
        }

        SetPosition();
    }

    private void SetPosition()
    {
        if(!playerEnteredTheAttackZone)
        {
            this.transform.position = new Vector3(this.enemy.transform.position.x,
                this.transform.position.y,
                this.enemy.transform.position.z - distanceBehindEnemy);
        }
        else
        {
            this.transform.position = new Vector3(this.enemy.transform.position.x,
                this.transform.position.y,
                this.player.transform.position.z);
        }
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        attackMeterFill = 0;
        distanceBehindEnemy = this.player.AttackController.WeaponReach;
        this.enemy = args.enemy;
        this.enemy.DamageController.EnemyDestructionBegin += OnEnemyDestroyed;
        followAttack.gameObject.SetActive(true);
        followAttack.Play();
    }
    
    private void OnEnemyDestroyed()
    {
        //BumperCopsDebug.Log("AttackParticleBehaviour: Enemy destroyed");
        ResetParticle();
    }

    private void OnTriggerExit(Collider other) 
    {
        if(other.gameObject.layer == Constants.PLAYER_LAYER)
        {
            // BumperCopsDebug.Log("Not in attack circle");
            this.IsInAttackCircle = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.layer != Constants.PLAYER_LAYER ||
            this.enemy == null)
        {
            return;
        }

        this.IsInAttackCircle = true;
        if (!this.canBump && 
            this.enemy != null &&
            playerEnteredTheAttackZone)
        {
            //BumperCopsDebug.Log($"Other gameobject name is {other.gameObject.layer}");
            IncreaseAttackMeter();
        }
    }

    private void IncreaseAttackMeter()
    {
        attackMeterFill += Time.deltaTime * 0.65f;

        if (attackMeterFill >= 1.0f)
        {
            OnAttackMeterReady();
        }
    }
}
