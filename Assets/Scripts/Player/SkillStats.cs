using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class SkillStats : MonoBehaviour 
{
    public event SkillDataLoadedEventHandler SkillDataLoaded;
    public delegate void SkillDataLoadedEventHandler(SkillStats o);
    [SerializeField]
    private Skill type;
    public Skill Type { get { return type; }}

    [SerializeField]
    private float basePoints;
    public float BasePoints { get { return basePoints; }}

    public bool HasUpgraded
    {
        get { return this.currentUpgrade == null; }
    }
    private float incrementPoints 
    {
        get 
        {
            // we use override upgrade before current upgrade.
            SkillUpgrade upgrade = this.overrideUpgrade == null ? currentUpgrade : overrideUpgrade;
            if(upgrade == null)
            {
                return 0;
            }

            return upgrade.Increment; 
        }
    }
    public float FinalSkillPoints
    {
        get
        {
            return basePoints + incrementPoints;
        }
    }

    private Character character;
    private List<SkillUpgrade> allUpgrades = new List<SkillUpgrade>();
    private const string STATS_RESOURCE_FOLDER = "player";
    private const string META_FILE_NAME = "metadata";

    private SkillUpgrade currentUpgrade;

    private SkillUpgrade nextUpgrade;
    private SkillUpgrade overrideUpgrade;
    private SkillMetaData skillMetaData;
    private bool dataLoaded;
    public float PointsAfterAllUpgrades { get; private set; }
    public int NextUpgradeCost 
    {
        get 
        {
            if(this.nextUpgrade == null)
            {
                return -1;
            }
            
            return this.nextUpgrade.Cost; 
        }
    }

    public bool HasNextUpgrade
    {
        get { return this.nextUpgrade != null; }
    }
    
    public void LoadSkillData(Character c)
    {
        this.character = c;
        if(!c.IsPlayer || dataLoaded)
        {
            // data load complete for enemy
            SkillDataLoaded(this);
            return;
        }

        LoadUpgrades();
        LoadUpgradeMetadata();

        if(this.type == Skill.ATTACK)
        {
            PointsAfterAllUpgrades = skillMetaData.AttackMaxPoints;
            RetrieveCurrentUpgrade(PlayerStats.Instance.GetSkillUpgradeId(Skill.ATTACK, this.character.Id));
        } else if(this.type == Skill.MOVEMENT)
        {
            PointsAfterAllUpgrades = skillMetaData.MovementMaxPoints;
            //BumperCopsDebug.Log("Upgrade id at load is " + PlayerStats.Instance.CurrentMovementUpgradeId);
            RetrieveCurrentUpgrade(PlayerStats.Instance.GetSkillUpgradeId(Skill.MOVEMENT, this.character.Id));
        } else if(this.type == Skill.BOOST)
        {
            PointsAfterAllUpgrades = skillMetaData.BoostMaxPoints;
            //BumperCopsDebug.Log("Upgrade id at load is " + PlayerStats.Instance.CurrentBoostUpgradeId);
            RetrieveCurrentUpgrade(PlayerStats.Instance.GetSkillUpgradeId(Skill.BOOST, this.character.Id));
        }

        dataLoaded = true;
        if(SkillDataLoaded != null)
        {
            SkillDataLoaded(this);
        }
    }

    private void Start() 
    {
        this.LoadSkillData(this.GetComponent<Character>());
        //BumperCopsDebug.Log("Data loaded for skill " + this.type);
    }

    private void RetrieveCurrentUpgrade(int upgradeId)
    {
        if(this.allUpgrades.Count < upgradeId)
        {
            throw new KeyNotFoundException(upgradeId.ToString());
        }

        this.currentUpgrade = this.allUpgrades[upgradeId];
        this.nextUpgrade = null;
        if(this.currentUpgrade.NextUpgradeId < 0)
        {
            return;
        }

        nextUpgrade = this.allUpgrades[this.currentUpgrade.NextUpgradeId];
    }

    public void OnUpgrade()
    {
        if(this.nextUpgrade == null)
        {
            BumperCopsDebug.LogError("Max upgrade reached");
            return;
        }

        // now give the upgrade
        RetrieveCurrentUpgrade(this.nextUpgrade.Id);
        //PlayerStats.Instance.UpgradeSkill(this.character.Id, this.currentUpgrade.Id, this.type);
    }

    /// <summary>
    /// Call this function to inititalize the prefab attack stats.
    /// </summary>
    private void LoadUpgrades()
    {
        string filePath = STATS_RESOURCE_FOLDER + Path.DirectorySeparatorChar + PlayerStats.Instance.CurrentCity + Path.DirectorySeparatorChar +
            this.character.Id + Path.DirectorySeparatorChar + Enum.GetName(typeof(Skill), type).ToLower();

        BumperCopsDebug.Log("File path is " + filePath);
        TextAsset prefabAttackFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabAttackFile.bytes, 0, prefabAttackFile.bytes.Length);
        allUpgrades = JsonConvert.DeserializeObject<List<SkillUpgrade>>(json);
    }

    private void LoadUpgradeMetadata()
    {
        string filePath = STATS_RESOURCE_FOLDER + Path.DirectorySeparatorChar + PlayerStats.Instance.CurrentCity + Path.DirectorySeparatorChar +
            this.character.Id + Path.DirectorySeparatorChar + META_FILE_NAME;

        BumperCopsDebug.Log("File path is " + filePath);
        TextAsset prefabAttackFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabAttackFile.bytes, 0, prefabAttackFile.bytes.Length);
        skillMetaData = JsonConvert.DeserializeObject<SkillMetaData>(json);
    }

    public void InitEnemyStats(float statValue)
    {
        this.basePoints = statValue;
    }

    public void ApplyOverrideSkillUpgrade(SkillUpgrade overrideUpgrade)
    {
        if(overrideUpgrade == null)
        {
            BumperCopsDebug.LogWarning("Override upgrade is null");
            return;
        }

        this.overrideUpgrade = overrideUpgrade;
    }

    public void ResetOverrideSkillUpgrade()
    {
        this.overrideUpgrade = null;
    }
}