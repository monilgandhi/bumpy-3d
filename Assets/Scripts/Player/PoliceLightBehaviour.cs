using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PoliceLightBehaviour : MonoBehaviour 
{
    [SerializeField]
    private AudioSource policeSiren;

    public void EnableWarningLights(bool turnSoundOn = false)
    {
        LightBehaviour[] lList = this.GetComponentsInChildren<LightBehaviour>();
        foreach(LightBehaviour l in lList)
        {
            l.StartWarningLights();
        }

        if(turnSoundOn)
        {
            this.policeSiren.Play();
        }
    }

    public void EnableAttackLights(bool turnSoundOn = false)
    {
        LightBehaviour[] lList = this.GetComponentsInChildren<LightBehaviour>();
        foreach(LightBehaviour l in lList)
        {
            l.StartAttackLights();
        }

        if(!this.policeSiren.isPlaying && turnSoundOn)
        {
            this.policeSiren.Play();
        }
    }

    public void Disable()
    {
        this.GetComponentsInChildren<LightBehaviour>().ToList().ForEach(l => l.StopLights());  
        this.policeSiren.Stop();
    }
}