using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerSpeedController : MonoBehaviour 
{
    public event Action<BoostUpdateEventArgs> PlayerBoostUpdate;
    
    private float speed = Constants.PATROL_SPEED; 
    public float Speed { get { return speed; }}
    public float MaxSpeed 
    { 
        get 
        {
            if(this.player == null || this.player.MovementStats == 0)
            {
                return 0;
            }

            return this.player.MovementStats + this.player.GameManager.GameLevelManager.GameSpeed; 
        }
    }

    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private AudioSource speedIncreaseSound;

    [SerializeField]
    private AudioSource boostSound;

    [SerializeField]
    private GameObject speedCloudPrefab;

    [SerializeField]
    private GameObject boostWindShieldGameObject;

    private float boostStartSpeed;
    private EnemyController enemy;
    private bool hasEnemySpawned;
    private bool hasEnteredAttackZone;
    private PlayerController player;
    private float boostStartTimer = -1;
    public bool IsBoostInProgress { get; private set; }
    private AnimationCurve smallBoostCurve;
    public AnimationCurve boostPowerupCurve;
    private AnimationCurve currentActiveBoostCurve;

    List<ParticleSystem> dustCloud = new List<ParticleSystem>();
    private ParticleSystem boostWindShield;

    public bool CanIncreaseSpeed()
    {
        return this.MaxSpeed < 100;
    }
    
    private void Awake() 
    {
        this.player = this.transform.root.GetComponent<PlayerController>();
        this.player.PlayerCharacterChanged += OnPlayerCharacterChanged;
        GameObject go = Instantiate(boostWindShieldGameObject, this.transform);
        boostWindShield = go.GetComponent<ParticleSystem>();
    }

    private void OnPlayerCharacterChanged()
    {
        this.smallBoostCurve = this.player.character.BoostCurve;
        boostWindShield.transform.parent = this.player.character.transform;
        boostWindShield.transform.position = this.player.character.AttackLocation;
        SetupBoostPowerupCurve();
    }

    private void Start() 
    {
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        // the user cannot leave the attack zone until bumped once
        this.player.AttackController.PlayerExitAttackZone += OnExitAttackZone;
        this.player.AttackController.PlayerEnteredAttackZone += OnEnteredAttackZone;

        //BumperCopsDebug.Log("Player max speed is " + this.playerMovementStats.FinalSkillPoints);
        this.player.PowerupManager.PowerupCollected += OnPowerupCollected;

        if(player.character != null)
        {
            OnPlayerCharacterChanged();
        }
    }

    private void OnEnteredAttackZone()
    {
        //BumperCopsDebug.Log("Entered attack zone");
        this.hasEnteredAttackZone = true;

        // remove the boost time;
        ToggleBoost(false, currentActiveBoostCurve == boostPowerupCurve);
    }

    private void OnExitAttackZone()
    {
        //BumperCopsDebug.Log("Player exit attack zone");
        this.hasEnteredAttackZone = false;
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type == PowerupType.BOOST || args.Type == PowerupType.BIG_BOOST)
        {
            if(IsBoostInProgress && currentActiveBoostCurve == boostPowerupCurve)
            {
                return;
            }
            
            ToggleBoost(true, args.Type == PowerupType.BIG_BOOST);
        }
    }

    private void SetupBoostPowerupCurve()
    {
        Keyframe[] newFrames = new Keyframe[smallBoostCurve.keys.Length];
        for(int i = 0; i < newFrames.Length; i++)
        {
            Keyframe currentFrame = smallBoostCurve[i];
            newFrames[i] = new Keyframe(currentFrame.time, currentFrame.value, currentFrame.inTangent, currentFrame.outTangent, currentFrame.inWeight, currentFrame.outWeight);
        }

        float pecentDifference = newFrames[newFrames.Length - 2].time / newFrames[newFrames.Length - 1].time;

        // TODO get the time from powerup
        newFrames[newFrames.Length - 1].time = 3.0f;
        newFrames[newFrames.Length - 2].time = newFrames[newFrames.Length - 1].time * pecentDifference;
        
        //BumperCopsDebug.Log($"Second to last is at {newFrames[newFrames.Length - 2].time} and last is at {newFrames[newFrames.Length - 1].time}");

        boostPowerupCurve = new AnimationCurve(newFrames);
        boostPowerupCurve.SmoothTangents(newFrames.Length - 1, smallBoostCurve[smallBoostCurve.length - 1].inTangent);
        boostPowerupCurve.SmoothTangents(newFrames.Length - 2, smallBoostCurve[smallBoostCurve.length - 1].outTangent);
    }

    private void OnEnemySpawned(EnemyArgs e)
    {
        //BumperCopsDebug.Log("New enemy spawned");
        this.enemy = e.enemy;

        // we stop everything as soon as enemies destruction has started
        // else we will get null reference when enemy becomes null
        this.enemy.DamageController.EnemyDestructionBegin += () => 
        {
            //BumperCopsDebug.Log("Destruction begin");
            this.hasEnteredAttackZone = false;
            this.hasEnemySpawned = false;
            boostStartTimer = -1;
            
            // reset the speed to patrol speed
            this.speed = Constants.PATROL_SPEED;
        };

        this.hasEnemySpawned = true;
        StartCoroutine(PlaySpeedupEffects());
    }

    private IEnumerator PlaySpeedupEffects()
    {
        this.speedIncreaseSound.Play();

        if(dustCloud != null && dustCloud.Count > 0)
        {
            dustCloud.ForEach(c => c.Play());
            yield return new WaitWhile(() => dustCloud.Last().isPlaying);
        }

        this.player.character.WheelTrails.ForEach(wt => wt.Play());
        yield return new WaitForSeconds(this.speedIncreaseSound.clip.length - this.speedIncreaseSound.time);
        this.player.character.WheelTrails.ForEach(wt => wt.Stop());
    }

    private float GetCurrentBoostLength()
    {
        return this.currentActiveBoostCurve.keys[this.currentActiveBoostCurve.length - 1].time;
    }

    private void FixedUpdate() 
    {
        this.speed = MaxSpeed;
        if(this.IsBoostInProgress)
        {
            if(Time.time - boostStartTimer > GetCurrentBoostLength())
            {
                //BumperCopsDebug.Log("Toggling boost off");
                ToggleBoost(false, currentActiveBoostCurve == boostPowerupCurve);
                return;
            }

            this.speed = this.boostStartSpeed + 
                (this.boostStartSpeed * this.currentActiveBoostCurve.Evaluate(Time.time - boostStartTimer));

            //BumperCopsDebug.Log("speed is : " + this.speed * this.currentActiveBoostCurve.Evaluate(Time.time - boostStartTimer));
            return;
        }

        this.boostStartSpeed = this.Speed;
    }

    private void ToggleBoost(bool turnOn, bool isPowerup)
    {
        if(this.hasEnteredAttackZone && turnOn)
        {
            return;
        }

        currentActiveBoostCurve = isPowerup ? boostPowerupCurve : smallBoostCurve;

        this.IsBoostInProgress = turnOn;
        this.boostStartTimer = turnOn ? Time.time : -1;

        PlayerBoostUpdate?.Invoke(new BoostUpdateEventArgs(turnOn, GetCurrentBoostLength(), isPowerup));

        if(turnOn)
        {
            if(isPowerup)
            {
                boostWindShield.Play();
            }
            
            boostSound.Play();
            BumperCopsDebug.Log($"Boost On: Distance {this.player.transform.position.z}");
        }
        else
        {
            boostSound.Stop();
            boostWindShield.Stop();
        }
    }
}