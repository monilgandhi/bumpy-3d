using System.Collections.Generic;
using UnityEngine;

public class PlayerBoostBehaviour : MonoBehaviour 
{
    [SerializeField]
    private List<ParticleSystem> boostParticles;

    [SerializeField]
    private List<ParticleSystem> smokeTrail;
    private float particleStartTime = 10.0f;
    private PlayerController player;
    private void Awake() 
    {
        StopBoost();
    }   

    public void Init(PlayerController player)
    {
        this.player = player;
        this.player.SpeedController.PlayerBoostUpdate += OnBoostUpdate;
    }

    private void OnBoostUpdate(BoostUpdateEventArgs args)
    {
        if(args.IsTurnedOn)
        {
            BumperCopsDebug.Log("Starting boost");
            StartBoost();
        }
        else
        {
            StopBoost();
        }
    }

    private void StartBoost()
    {
        if(boostParticles != null)
        {
            boostParticles.ForEach(b => b.Play());
        }

        if(smokeTrail != null)
        {
            smokeTrail.ForEach(b => b.Play());
        }
    } 

    private void StopBoost()
    {
        if(boostParticles != null)
        {
            boostParticles.ForEach(b => b.Stop());
        }    

        if(smokeTrail != null)
        {
            smokeTrail.ForEach(b => b.Stop());
        }    
    }

}