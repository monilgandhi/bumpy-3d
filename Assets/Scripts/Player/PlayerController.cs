﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class PlayerController : MonoBehaviour
{
    public event Action PlayerCharacterChanged;
    [SerializeField]
    private RoadManager roadManager;

    [SerializeField]
    private CinemachineBrain cinemachineBrain;

    [SerializeField]
    private GameManager gameManager;
    public GameManager GameManager { get { return gameManager; }}

    public EnemyManager EnemyManager { get {return this.gameManager.EnemyManager;}}
    public SlowMotionBehaviour slowMotion { get { return this.gameManager.SlowMotion; }}

    [SerializeField]
    private TouchManager touch;
    public TouchManager TouchManager { get { return touch; } }

    [SerializeField]
    private TimerBehaviour timerBehaviour;

    [SerializeField]
    private Canvas playerCanvas;

    [SerializeField]
    private ParticleSystem bonusBodySlam;

    [SerializeField]
    private PowerupManager powerupManager;
    public PowerupManager PowerupManager { get { return powerupManager; }}

    [SerializeField]
    private PrefabManager prefabManager;
    private PlayerSirenPowerupEffect sirenPowerupEffect;
    public PlayerSirenPowerupEffect SirenPowerupEffect { get { return sirenPowerupEffect; }}
    public CameraController CameraController { get; private set; }

    public bool IsBackupCharacter { get { return this.PlayerCarBehaviour.IsBackupCharacter; }}
    public ParticleSystem BonusBodySlam { get { return bonusBodySlam; }}
    //public PlayerTextBehaviour PlayerText { get { return playerText; }}
    public TimerBehaviour Timer { get { return this.timerBehaviour; }}
    public PlayerHealthController healthController {get {return this.GetComponent<PlayerHealthController>();}}
    public bool HasWeaponUnlocked { get { return this.AttackController.HasWeaponUnlocked; }}

    public PlayerCharacter character 
    {
        get 
        {
            return this.PlayerCarBehaviour != null ? this.PlayerCarBehaviour.Character : null; 
        }    
    }
    
    public PlayerCarBehaviour PlayerCarBehaviour { get; private set; }
    public MovePlayerController MoveController { get; private set;}
    public PlayerSpeedController SpeedController { get; private set;}
    public PlayerAttackController AttackController {get; private set; }
    public PlayerShieldBehaviour ShieldBehaviour { get; private set; }
    public float MovementStats 
    {
        get 
        {
            if(this.character == null)
            {
                return 0;
            }

            return this.character.SpeedStats; 
        }
    }

    public Rigidbody RigidBody { get; private set; }
    public ICinemachineCamera ActiveCamera { get { return cinemachineBrain.ActiveVirtualCamera; }}
    public IList<NearMissBehaviour> NearMiss 
    {
         get 
         {
            if(this.character == null)
            {
                return null;
            }
            
            return this.character.NearMissBehaviours; 
         }
    }

    public float PlayerYPosition { get { return this.MoveController.playerYPosition; }}
    public RoadManager RoadManager { get { return roadManager; }}
    private bool gameOver;

    private void Awake() 
    {
        //BumperCopsDebug.Log("Playercontroller awake again");
        // instantiate the model
        this.MoveController = this.GetComponentInChildren<MovePlayerController>();
        this.AttackController = this.GetComponentInChildren<PlayerAttackController>();
        this.SpeedController = this.GetComponentInChildren<PlayerSpeedController>();
        this.RigidBody = this.GetComponent<Rigidbody>();
        this.PlayerCarBehaviour = this.GetComponent<PlayerCarBehaviour>();
        this.CameraController = this.cinemachineBrain.GetComponent<CameraController>();
        this.sirenPowerupEffect = this.GetComponentInChildren<PlayerSirenPowerupEffect>();
        this.ShieldBehaviour = this.GetComponent<PlayerShieldBehaviour>();

        prefabManager.PrefabLoadRequestComplete += OnCharacterLoadComplete;
        InitSelectedModel();
    }

    private void InitSelectedModel()
    {
        if(this.character != null && this.character.Id.Equals(PlayerStats.Instance.CarId))
        {
            // this is done because the prefabs layer is changed by shop. 
            InitCharacterAfterLoad(this.character);
            return;
        }

        prefabManager.LoadPrefabAsync(PlayerStats.Instance.CarId, 
            this.GetType(), 
            Constants.GetPlayerPrefabPath(PlayerStats.Instance.CarId));
    }
    
    private void BruteForceLoad()
    {
        //BumperCopsDebug.Log($"Loading model {PlayerStats.Instance.CarId}");
        string prefabPath = Constants.GetPlayerPrefabPath(PlayerStats.Instance.CarId);
        GameObject playerPrefab = Resources.Load<GameObject>(prefabPath);
        playerPrefab.SetActive(false);

        GameObject o = Instantiate(playerPrefab);
        InitCharacterAfterLoad(o.GetComponent<PlayerCharacter>());
    }

    private void InitCharacterAfterLoad(PlayerCharacter character)
    {
        this.PlayerCarBehaviour.InitPrimaryCharacter(character);
        this.character.Init(this, PlayerStats.Instance.GetSelectedSkin(this.character.Id));
        // start the police light. For some reason there is drop in FPS when the lights are started.
        // many solutions were tried (reducing the quality of flares, bking the lights) but there is 
        // still minimal drop is fps due to rendering. We will turn on the warning lights in the title screen
        // with the hopes that when the game runs, the rendering effect is mitigated
        TurnOnWarningPoliceLight(false);
        this.PlayerCharacterChanged?.Invoke();
    }

    private void OnCharacterLoadComplete(PrefabLoadEventArgs args)
    {
        if(args == null || args.GObject == null)
        {
            BumperCopsDebug.LogWarning("gameobject are null");
            BruteForceLoad();
            // brute force;
            return;
        }

        if(!args.CallingClass.Equals(this.GetType()) || args.PrefabId != PlayerStats.Instance.CarId)
        {
            return;
        }

        BumperCopsDebug.Log("Player load complete");

        PlayerCharacter c = args.GObject.GetComponent<PlayerCharacter>();

        if(c == null)
        {
            throw new MissingComponentException($"missing player compoenent in prefab with id {args.PrefabId}");
        }

        InitCharacterAfterLoad(c);
    }

    void Start()
    {
        //SwitchCharacter(this.primaryCharacter);
        this.healthController.HealthOver += () => this.character.PoliceLight.Disable();
        this.AttackController.PlayerEnteredAttackZone += () =>
        {
            //BumperCopsDebug.Log("PlayerController: Turning on police light");
            this.gameManager.EnemyManager.currentEnemy.DamageController.EnemyDestructionBegin += TurnOffPoliceLight;
            this.character.PoliceLight.EnableAttackLights();  
        };

        this.gameManager.EnemyManager.EnemySpawned += (args) => TurnOnWarningPoliceLight(true);

        this.sirenPowerupEffect.Init(this.powerupManager);
        this.gameManager.GameOver += (r) => 
        {
            gameOver = true;
            playerCanvas.gameObject.SetActive(false);
        };

        // turning it off since the game has started
        this.gameManager.ShopClosed += OnShopClosed;
        this.gameManager.GameSessionInitialized += (args) => TurnOffPoliceLight();
        this.gameManager.GameTutorialSessionInitialized += (args) => TurnOffPoliceLight();
        this.gameManager.GamePaused += () => this.playerCanvas.gameObject.SetActive(false);
        this.gameManager.GameStarted += OnGameStarted;
    }

    private void OnShopClosed()
    {
        InitSelectedModel();
    }

    private void OnGameStarted(GameEventArgs args)
    {
        this.character.ToggleCarWheelsMovement(false);
        this.playerCanvas.gameObject.SetActive(true);
    } 
       
    private void TurnOffPoliceLight()
    {
        //turn off the lights
        this.character.PoliceLight.Disable();
    }

    private void TurnOnWarningPoliceLight(bool turnSoundOn)
    {
        //turn off the lights
        this.character.PoliceLight.EnableWarningLights(turnSoundOn);
    }

    private IEnumerator OnObstacleHit()
    {
        for(int i = 0; i < 4; i++)
        {
            this.character.gameObject.GetComponent<Renderer>().enabled = false;
            yield return new WaitForSeconds(0.1f);
            this.character.gameObject.GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ShakeCamera(float amplitude, float seconds)
    {
        CinemachineVirtualCamera virtualCamera = (CinemachineVirtualCamera)this.ActiveCamera;
        if(virtualCamera == null || virtualCamera.tag != "player_main_camera")
        {
            BumperCopsDebug.LogWarning("Cannot find camera for shake");
            return;
        }

        //BumperCopsDebug.Log("Shaking now");
        //BumperCopsDebug.Log("PlayerController: Shaking camera)");
        StartCoroutine(virtualCamera.GetComponent<CameraShake>().Shake(amplitude, seconds));
    }
}
