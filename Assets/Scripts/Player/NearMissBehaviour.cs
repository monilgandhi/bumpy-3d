using UnityEngine;

public class NearMissBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Side side;

    [SerializeField]
    private GameObject particleSystemPrefab;

    [SerializeField]
    private GameObject obstacleNearMissPrefab;

    public delegate void NearMissEventHandler(NearMissEventArgs args);
    public event NearMissEventHandler NearMiss;
    private PlayerController player;
    public ObstacleController ObstacleController { get; private set; }

    private AudioSource nearMissAudio;
    
    private bool isPlayer = false;
    private GameObject obstacleNearMissHelperSystem;
    private ParticleSystem nearMissParticleSystem;
    private bool triggerAnimation;
    private BoxCollider obstacleNearMissCollider;
    private bool initComplete;
    private void Awake() 
    {
        isPlayer = this.GetComponentInParent<PlayerCharacter>() != null;
        if(isPlayer)
        {
            PlayerAwake();
            return;
        }

        ObstacleController = this.transform.root.GetComponent<ObstacleController>();
        // reset the position to zero
        this.transform.localPosition = Vector3.zero;
        this.transform.localScale = this.ObstacleController.transform.localScale;

        obstacleNearMissCollider = this.gameObject.AddComponent<BoxCollider>();
    }

    private void PlayerAwake()
    {
        player = this.transform.root.GetComponent<PlayerController>();
        this.nearMissAudio = this.GetComponentInParent<AudioSource>();

        if(this.nearMissAudio == null)
        {
            BumperCopsDebug.LogWarning("No audio source found for near miss");
        }

        if(particleSystemPrefab == null)
        {
            return;
        }
        
        GameObject playerNearMiss = Instantiate(particleSystemPrefab, this.transform);
        playerNearMiss.transform.localScale = new Vector3(2,2,2);
        playerNearMiss.transform.localPosition = new Vector3(side == Side.LEFT ? -0.5f : 0.5f, 0, 0);
        nearMissParticleSystem = playerNearMiss.GetComponent<ParticleSystem>();
        initComplete = true;
        return;
    }
    
    public void InitObstacleNearMiss(bool triggerAnimation, PlayerController player, BoxCollider characterBoxCollider)
    {
        this.player = player;
        this.triggerAnimation = triggerAnimation;
        if(!initComplete)
        {
            // we set this here for two reasons
            // 1. Optimizing initial load of the obstacles. Hence we init/Awake all the nearmiss objects in the beginning. However Remoteconfig is not available and can break the code
            // 2. This enables us to change the size of the nearmiss collider at run time;
            float xSize = RemoteConfig.Instance.Config.NearMissBounds;
            obstacleNearMissCollider.size = new Vector3(xSize, characterBoxCollider.size.y, characterBoxCollider.bounds.extents.z);
            obstacleNearMissCollider.center = Vector3.zero;

            //BumperCopsDebug.Log($"NearMissBehaviour: Details for {this.transform.root.name} is size: {characterBoxCollider.size}, center:{characterBoxCollider.center}, extents: {characterBoxCollider.bounds.extents}, min: {characterBoxCollider.bounds.min}");

            // 0.4 is the adjustment factor. Not sure why it is 0.4f
            float adjustmentDelta = (xSize/2) - 0.4f;
            //BumperCopsDebug.Log($"Adjustment delta: {adjustmentDelta}");

            if(this.side == Side.LEFT)
            {
                this.transform.localPosition = new Vector3(characterBoxCollider.bounds.min.x - adjustmentDelta, characterBoxCollider.center.y, 0);
            } 
            else
            {
                this.transform.localPosition = new Vector3(characterBoxCollider.bounds.max.x + adjustmentDelta, characterBoxCollider.center.y, 0);
                // we need to apply this offset because the pivot of particle system is not centered
                //nearMissparticleSystem.transform.localPosition = new Vector3(0.6f, yPosParticle, 0);
            }

            obstacleNearMissCollider.isTrigger = true;
            obstacleNearMissCollider.enabled = true;
    
            if(triggerAnimation && obstacleNearMissHelperSystem == null)
            {
                obstacleNearMissHelperSystem = Instantiate(obstacleNearMissPrefab, this.transform);
            }

            initComplete = true;
        }

        if(obstacleNearMissHelperSystem != null)
        {
            obstacleNearMissHelperSystem.SetActive(false);
        }
    }

    private void Update() 
    {
        if(!triggerAnimation)
        {
            return;
        }

        if(ObstacleController.transform.position.z < player.transform.position.z || this.ObstacleController.IsVanishing)
        {
            return;
        }

        if(ObstacleController.transform.position.z - player.transform.position.z <= 50)
        {
            // start the animation
            if(this.side == Side.LEFT && this.player.transform.position.x < ObstacleController.transform.position.x)
            {
                obstacleNearMissHelperSystem.SetActive(true);
            }
            else if(this.side == Side.RIGHT && this.player.transform.position.x > ObstacleController.transform.position.x)
            {
                obstacleNearMissHelperSystem.SetActive(true);
            }
        }
    }

    private void OnTriggerExit(Collider other) 
    {  
        if(!other.tag.Equals(this.tag))
        {
            return;
        }

        if(this.gameObject.layer == Constants.NPC_LAYER && other.gameObject.layer == Constants.PLAYER_LAYER)
        {
            // disable the collider
            // BumperCopsDebug.Log($"NearMissBehaviour: Disabling nearmisses for {this.transform.root.name}");
            this.transform.parent.gameObject.SetActive(false);
        }
        else if(this.gameObject.layer == Constants.PLAYER_LAYER && other.gameObject.layer == Constants.NPC_LAYER)
        {
            NearMissBehaviour obstacleNearmiss = other.gameObject.GetComponent<NearMissBehaviour>();

            if(obstacleNearmiss == null)
            {
                BumperCopsDebug.LogWarning("Obstacle near miss is absent");
                return;
            }

            if(obstacleNearmiss.ObstacleController == null || obstacleNearmiss.ObstacleController.MovementController == null)
            {
                BumperCopsDebug.LogWarning($"NearMissBehaviour: Unable to find controller for {other.name}");
                return;
            }

            //BumperCopsDebug.Log($"NearMissBehaviour: Adding the npc for near miss with name {controller.name}");
            if(NearMiss != null)
            {
                PowerupReward reward = obstacleNearmiss.ObstacleController.ClaimReward();
                if(reward != null)
                {
                    //BumperCopsDebug.Log("Sending event Near miss for player");
                    NearMiss(new NearMissEventArgs(obstacleNearmiss.ObstacleController.Character.ObstacleType, 
                        Time.time, 
                        obstacleNearmiss.ObstacleController.name, 
                        reward, 
                        obstacleNearmiss.ObstacleController.AssignedLaneDirection));
                }
            }

            if(this.nearMissAudio != null)
            {
                this.nearMissAudio.Play();
            }            

            if(this.nearMissParticleSystem != null)
            {
                this.nearMissParticleSystem.Play();
            }            
        }  
    }

    public enum Side
    {
        LEFT,
        RIGHT
    }
}