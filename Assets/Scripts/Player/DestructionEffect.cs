using System.Collections;
using UnityEngine;

public class DestructionEffect : MonoBehaviour 
{
    public delegate void DestructionEffectCompleteHandler();
    public event DestructionEffectCompleteHandler DeathEffectComplete;
    [SerializeField]
    private ParticleSystem hit;
    [SerializeField]
    private ParticleSystem smoke;
    [SerializeField]
    private ParticleSystem fire;
    [SerializeField]
    private ParticleSystem explosion;
    [SerializeField]
    private ParticleSystem deathSmoke;
    private void Awake() 
    {
        this.hit.Stop();
        this.smoke.Stop();
        this.fire.Stop();
        this.explosion.Stop();
        this.deathSmoke.Stop();
    }
    public IEnumerator Hit(Vector3 contactPosition, PlayerController player)
    {
        player.ShakeCamera(2, 0.5f);
        this.hit.transform.position = contactPosition;
        this.hit.Play();
        yield return new WaitWhile(() => this.hit.isPlaying);
    }

    public void TurnOffFirstHit()
    {
        this.smoke.Stop();
    }

    public void TurnOffSecondHit()
    {
        this.fire.Stop();
    }

    public IEnumerator StartFirstHit()
    {
        this.smoke.Play();
        yield return null;
    }

    public IEnumerator StartSecondHit()
    {
        if(this.hit.isPlaying)
        {
            yield return new WaitWhile(() => this.hit.isPlaying);
        }

        this.fire.Play();
        yield return null;
    }

    public IEnumerator Death(PlayerController player)
    {
        // stop other effects
        this.smoke.Stop();
        this.fire.Stop();

        if(this.explosion.isPlaying)
        {
            this.explosion.Stop();
        }

        this.explosion.Play();

        //play the camera shake
        player.ShakeCamera(4, 1f);
        // need to add some forces
        // remove all constraints
        player.RigidBody.constraints = RigidbodyConstraints.None;

        //rigidbody.AddTorque(transform.up * 5000f, ForceMode.Impulse);
        player.RigidBody.AddExplosionForce(800f, player.character.AttackLocation, 5f, 1f, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);

        this.deathSmoke.Play();
        yield return new WaitForSeconds(5f);
        DeathEffectComplete();
    }
}