using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : MonoBehaviour 
{
    public event Action<PlayerObstacleCollisionEventArgs> PlayerObstacleCollided;
    public event HealthOverEventHandler Death;
    public delegate void HealthOverEventHandler();
    public event HealthOverEventHandler HealthOver;
    private PlayerController player;
    private float destructionStartTime = -1;
    private int playerHit = 0;
    private bool healthImproves = false;

    private const float FIRST_HIT_TIMER = 8.0f;
    private const float SECOND_HIT_TIMER = 5.0f;

    private bool shieldEffectTurnedOn;
    private bool playerInAttackZone;
    private bool playerPowerupBoostTurnedOn;
    private bool weaponEffectTurnedOn;
    private void Awake() 
    {
        this.player = this.GetComponent<PlayerController>();   
        playerInAttackZone = playerPowerupBoostTurnedOn = shieldEffectTurnedOn = false;
    }

    private void Start() 
    {    
        this.player.AttackController.PlayerEnteredAttackZone += () => playerInAttackZone = true;
        this.player.AttackController.PlayerExitAttackZone += () => 
        {
            StartCoroutine(RestartObstacleHitEffect());
        };
        
        this.player.SpeedController.PlayerBoostUpdate += OnPlayerBoostUpdate;
        this.player.ShieldBehaviour.ShieldEvent += OnPlayerShieldEvent;
        //this.player.AttackController.WeaponReady += OnWeaponReady;
        //this.player.AttackController.WeaponEnded += OnWeaponEnded;
    }    

    private void OnWeaponReady()
    {
        this.weaponEffectTurnedOn = true;
    }

   private void OnWeaponEnded()
    {
        this.weaponEffectTurnedOn = false;
    }

    private void OnPlayerShieldEvent(ShieldEventArgs args)
    {
        BumperCopsDebug.Log($"Shield effect is {args.Activated}. Forgive");
        shieldEffectTurnedOn = args.Activated;
    }

    private void OnPlayerBoostUpdate(BoostUpdateEventArgs args)
    {
        playerPowerupBoostTurnedOn = args.IsPowerup && args.IsTurnedOn;
    }

    private IEnumerator RestartObstacleHitEffect()
    {
        yield return new WaitForSeconds(1.5f);
        playerInAttackZone = false;
    }

    private bool CanForgiveObstacleHit()
    {
        BumperCopsDebug.Log($"Can forgive obstacle: attack zone {playerInAttackZone}, boost turnedon {playerPowerupBoostTurnedOn}, shield {shieldEffectTurnedOn}");
        return playerInAttackZone || playerPowerupBoostTurnedOn || shieldEffectTurnedOn || weaponEffectTurnedOn;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //BumperCopsDebug.Log("PlayerHealthController: Can obstacles effect " + canObstacleHitEffect);
        if (collision.gameObject.layer != Constants.NPC_LAYER)
        {
            return;
        }

        ObstacleController obstacleController = collision.gameObject.GetComponent<ObstacleController>();
        if(obstacleController == null || obstacleController.MovementController == null)
        {
            BumperCopsDebug.LogWarning("PlayerHealthController: Cannot find movement controller of NPC");
            return;
        }

        if(obstacleController.ObstacleHitController.PlayerHitCount > 0 || obstacleController.IsVanishing)
        {
            return;
        }

        BumperCopsDebug.Log($"Is hit forgiven {CanForgiveObstacleHit()}");
        obstacleController.ObstacleHitController.RecordPlayerHit();
        //++playerHit;
        //BumperCopsDebug.Log("PLayer hit is " + playerHit);

        PlayerObstacleCollided?.Invoke(new PlayerObstacleCollisionEventArgs(
                obstacleController.name, 
                !this.player.GameManager.IsTutorial && !CanForgiveObstacleHit()));

        destructionStartTime = Time.time;

        if(playerHit == 1)
        {
            //BumperCopsDebug.Log("starting hit number 1");
        } else if (playerHit == 2)
        {
            //BumperCopsDebug.Log("starting hit number 2");
        } else if (playerHit == 3)
        {
            //BumperCopsDebug.Log("death");
            // DEATH
            HealthOver();
        }
    }
}