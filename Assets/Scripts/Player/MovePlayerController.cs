﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MovePlayerController : MonoBehaviour
{
    public event Action<PlayerObstacleEventArgs> PlayerInObstacleVicinity;

    public delegate void PlayerApproachingForkRoadEventHandler();
    public event PlayerApproachingForkRoadEventHandler ApproachingForkRoad;
    public event PlayerApproachingForkRoadEventHandler EnteredForkRoad;
    private bool forkRoadApproachEventSent;
    [SerializeField]
    private float touchSensitivity = 10f;

    [SerializeField]
    private float mouseSensitivity = 10f;
    private bool playerDead;
    private Vector3 velocityVector;

    private PlayerController playerController;
    private PlayerSpeedController speedController;
    private bool isAcceptingInput;
    public float playerYPosition { get; private set; }
    private bool startCarMovement;
    private RoadController lastRoadBeforeFork;
    private float laneTimer;
    private LaneController lane;
    public LaneController Lane
    {
        get
        {
            if(playerController.character == null || playerDead)
            {
                return null;
            }

            laneTimer += Time.deltaTime;

            if(laneTimer < 2f && lane != null)
            {
                return lane;
            }   

            laneTimer = 0f;
            WheelHit laneHit;

            lane = null;
            foreach(WheelCollider w in this.playerController.character.Wheels)
            {
                if(w == null || !w.GetGroundHit(out laneHit))
                {
                    continue;
                }

                lane = laneHit.collider.GetComponent<LaneController>();
                if(lane == null)
                {
                    continue;
                    //throw new InvalidProgramException("Player car touching other than road");
                }

                return lane;
            }

            // shoot at ray down
            Ray downRay = new Ray(this.transform.position, Vector3.down);
            Debug.DrawRay(downRay.origin, downRay.direction, Color.red);
            RaycastHit[] hits;
            if(Physics.Raycast(downRay))
            {
                hits = Physics.RaycastAll(downRay);
                if(hits[0].collider.GetComponent<LaneController>() != null)
                {
                    lane =  hits[0].collider.GetComponent<LaneController>();
                }
                else
                {
                    BumperCopsDebug.LogWarning("hit found but no lane for name " + hits[0].collider.name);
                }
            }

            return lane;
        }
    }

    public long PathNumber
    {
        get
        {
            LaneController lane = Lane;
            if(lane == null)
            {
                return -1;
            }

            if(lane.Road == null)
            {
                BumperCopsDebug.LogWarning("Road is null for a lane");
                return -1;
            }

            return lane.Road.MyPathNumber;
        }
    }
    
    private bool hasEnteredAttackZone;
    // Use this for initialization
    void Start()
    {
        playerYPosition = this.transform.root.position.y;
        playerController = this.transform.root.GetComponent<PlayerController>();
        playerController.healthController.HealthOver += OnGameOver;
        this.playerController.Timer.TimeUp += OnGameOver;

        this.speedController = this.GetComponentInChildren<PlayerSpeedController>();
        this.playerController.slowMotion.SlowMotionStarted += () => this.isAcceptingInput = false;
        this.playerController.slowMotion.SlowMotionEnded += () => this.isAcceptingInput = true;
        this.playerController.GameManager.GameStarted += (args) => this.isAcceptingInput = true;
        this.playerController.GameManager.StartPlaying += OnNewSession;
        this.playerController.GameManager.GameTutorialSessionInitialized += (args) => OnNewSession();

        this.playerController.RoadManager.RoadTypeChanged += OnRoadTypeChanged;
        //BumperCopsDebug.Log("Max speed is " + this.speedController.Speed);
    }

    private void OnRoadTypeChanged(RoadtypeChangedEventArgs args)
    {
        if(args.To == RoadManager.RoadType.FORK)
        {
            lastRoadBeforeFork = args.LastRoadCurrentType;
        }
    }

    void OnGameOver()
    {
        BumperCopsDebug.Log("Game Over");
        this.playerDead = true;
        this.isAcceptingInput = false;
        this.startCarMovement = false;
        //StartCoroutine(Stop());
    }

    void OnNewSession()
    {
        //BumperCopsDebug.Log("MovePlayerController: Starting new sessions");
        this.startCarMovement = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(this.playerDead || this.playerController.character == null)
        {
            playerController.RigidBody.velocity = Vector3.zero;
            return;
        }

        if(Lane == null || this.playerController.transform.position.y >= playerYPosition + 1.0f)
        {
            BumperCopsDebug.Log("MovePlayerController: Adjusting position");
            this.playerController.transform.position = new Vector3(this.playerController.transform.position.x, 
                playerYPosition, 
                this.playerController.transform.position.z);
        }

        if(this.isAcceptingInput)
        {
            Rotate();
            Move();
            CheckForkRoadEvents();
        }

        else if (startCarMovement)
        {
            Move();
        }
    }

    private void CheckForkRoadEvents()
    {
        LaneController playerLane = Lane;
        if(lastRoadBeforeFork == null || playerLane == null || playerLane.Road == null)
        {
            return;
        }

        if(playerLane.Road.MyPathNumber >= lastRoadBeforeFork.MyPathNumber - 4 && !forkRoadApproachEventSent)
        {
            if(ApproachingForkRoad != null)
            {
                ApproachingForkRoad();
            }
            
            forkRoadApproachEventSent = true;
        }

        // player has entered the fork road. reset
        if(playerLane != null && playerLane.Road.MyPathNumber >= lastRoadBeforeFork.MyPathNumber + 1)
        {
            if(EnteredForkRoad != null)
            {
                EnteredForkRoad();
            }

            lastRoadBeforeFork = null;
            forkRoadApproachEventSent = false;
        }
    }

    private void Move()
    {
        velocityVector = transform.TransformDirection(Vector3.forward);

        // we select between enemy speed (if enemy is present) and max user speed whichever is smaller 
        //AdjustSpeed();
        if(this.playerController.AttackController.BumpInProgress)
        {
            return;
        }

        playerController.RigidBody.velocity = velocityVector.normalized * this.speedController.Speed;
    }

    void AdjustSpeed()
    {
        // when the speed of the player is lower than the required speed. Keep increasing the speed
        if (playerController.RigidBody.velocity.magnitude < this.speedController.Speed)
        {
            playerController.RigidBody.AddForce(velocityVector * this.speedController.Speed, ForceMode.Acceleration);
        } 
        // if the speed is more than 10% the required speed, reduce the speed
        // however the speed the speed needs to be adjusted even when bumping i.e. do not reduce the speed when bumping.
        else if((!playerController.AttackController.BumpInProgress && playerController.RigidBody.velocity.magnitude > this.speedController.Speed + (this.speedController.Speed * 0.02f)))
        {
            float difference = playerController.RigidBody.velocity.magnitude - this.speedController.Speed;
            // we take the difference between current speed and required
            playerController.RigidBody.AddForce(velocityVector * (-difference), ForceMode.Acceleration);
            //playerController.RigidBody.velocity -= velocityVector * (playerController.RigidBody.velocity.magnitude - requiredSpeed);
        }
        else
        {
            // keep the speed steady
            playerController.RigidBody.velocity = velocityVector.normalized * playerController.RigidBody.velocity.magnitude;
        } 
    }

    void Rotate()
    {
        Quaternion rotationMin = Quaternion.Euler(new Vector3 (0f, -15f, 0f));
        Quaternion rotationMax = Quaternion.Euler(new Vector3 (0f, 15f, 0f));
        
        float moveHorizontal = 0.0f;
#if UNITY_EDITOR
        moveHorizontal = Input.GetAxis("Mouse X") * mouseSensitivity;
#elif UNITY_IOS || UNITY_ANDROID
        if(Input.touchCount <= 0 || Input.GetTouch(0).phase != TouchPhase.Moved)
        {
            return ;
        }

        moveHorizontal = Input.GetTouch(0).deltaPosition.x * touchSensitivity;
        
#endif
        
        /*if(Mathf.Abs(moveHorizontal) > 4.5)
        {
            StartCoroutine(PlayWheelTrails(Mathf.Abs(moveHorizontal) * 0.1f));
        }*/

        Quaternion rotation = this.playerController.transform.rotation;
        
        if ((moveHorizontal < 0 && rotation.y > rotationMin.y) ||  
             (moveHorizontal > 0 && rotation.y < rotationMax.y))
        {
            rotation.y += Quaternion.Euler(new Vector3 (0f, moveHorizontal, 0f)).y;
            this.playerController.transform.rotation = rotation;
        }
    }

    public IEnumerator PlayWheelTrails(float seconds)
    {
        float maxSeconds = 0.5f;
        this.playerController.character.WheelTrails.ForEach(wt => wt.Play());
        yield return new WaitForSeconds(Math.Min(maxSeconds, seconds));
        this.playerController.character.WheelTrails.ForEach(wt => wt.Stop());
    }

    private IEnumerator Stop()
    {
        while(playerController.RigidBody.velocity.magnitude > 0)
        {
            playerController.RigidBody.AddForce(Vector3.back * playerController.RigidBody.velocity.magnitude, ForceMode.Acceleration);
            yield return new WaitForFixedUpdate();
        }
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.layer != Constants.NPC_LAYER)
        {
            return;
        }

        if(PlayerInObstacleVicinity != null)
        {
            //BumperCopsDebug.Log($"MovePlayerController: Found object with name {other.transform.root.name}");
            PlayerInObstacleVicinity(new PlayerObstacleEventArgs(other.gameObject));
        }
    }
}