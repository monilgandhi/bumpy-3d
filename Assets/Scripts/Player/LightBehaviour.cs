﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LightBehaviour : MonoBehaviour
{
    private Color originalColor;
    private Light[] lightObjects;
    private LensFlare lensFlare;

    private enum WaveForm { tri, sqr, saw, inv, noise };
    [SerializeField]
    private WaveForm waveform = WaveForm.sqr;
    private float amplitude = 2.0f; // amplitude of the wave
   
    [SerializeField]
    private float warningFrequency;
   
   [SerializeField]
    private float attackFrequency = 0.5f; // cycle frequency per second

    [SerializeField]
    private Image turnImage;

    [SerializeField]
    private bool multipleChildLights;
    private float baseStart = 0.0f; // start 
    private float phase = 0.0f; // start point inside on wave cycle
    private float frequency; // cycle frequency per second
    private bool isTurnLight;

    // Use this for initialization
    void Awake()
    {
        if(multipleChildLights)
        {
            lightObjects = GetComponentsInChildren<Light>();
        }
        else
        {
            lightObjects = GetComponents<Light>();
        }
        
        ToggleLights(false);
        originalColor = lightObjects[0].color;
    }

    private void ToggleLights(bool enabled)
    {
        foreach(Light l in lightObjects)
        {
            l.enabled = enabled;
        }
    }
    
    private void ToggleTurnImage(bool isEnabled = false)
    {
        if(turnImage == null)
        {
            return;
        }

        turnImage.enabled = isEnabled;
    }

    private void ChangeColor(Color newColor)
    {
        foreach(Light l in lightObjects)
        {
            l.color = newColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!lightObjects[0].enabled)
        {
            return;
        }

        ChangeColor(originalColor * EvalWave());

        if(isTurnLight)
        {
            turnImage.color = Color.white * EvalWave();
        }
    }

    public void StartAttackLights()
    {   
        if(lightObjects == null || lightObjects.Length == 0)
        {
            BumperCopsDebug.LogWarning("null light objects");
            return;
        }

        ToggleLights(true);
        frequency = attackFrequency;
    }

    public void StartWarningLights()
    {
        if(lightObjects == null || lightObjects.Length == 0)
        {
            BumperCopsDebug.LogWarning("null light objects");
            return;
        }
            
        ToggleLights(true);
        frequency = warningFrequency;
    }

    public void StartTurnLights()
    {      
        if(lightObjects == null || lightObjects.Length == 0)
        {
            BumperCopsDebug.LogWarning("null light objects");
            return;
        }
            
        ToggleLights(true);

        isTurnLight = true;
        ToggleTurnImage(true);
    }

    public void StopLights()
    {
        //BumperCopsDebug.Log("LightBehaviour: Stopping lights");
        ToggleLights(false);
        ToggleTurnImage(false);
    }

    public void InitTurnLight(Image turnImage, float turnLightFrequency)
    {
        this.turnImage = turnImage;
        this.frequency = turnLightFrequency;
    }

    private float EvalWave()
    {
        float x = (Time.time + phase) * frequency;
        float y;
        x = x - Mathf.Floor(x); // normalized value (0..1)

        switch (waveform)
        {
            case WaveForm.tri:
                if (x < 0.5f)
                    y = 4.0f * x - 1.0f;
                else
                    y = -4.0f * x + 3.0f;
                break;
            case WaveForm.sqr:
                if (x < 0.5f)
                    y = 1.0f;
                else
                    y = -1.0f;
                break;
            case WaveForm.saw:
                y = x;
                break;
            case WaveForm.inv:
                y = 1.0f - x;
                break;
            case WaveForm.noise:
                y = 1f - (Random.value * 2);
                break;
            default:
                y = 1.0f;
                break;
        }

        return (y * amplitude) + baseStart;
    }

    // it is possible that the parent gameobject will be disabled before turn is complete which will keep the lights blinking
    private void OnDisable() 
    {
        if(lightObjects == null)
        {
            return;
        }

        StopLights();
    }
}
