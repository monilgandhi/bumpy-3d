using System;
using System.Collections;
using UnityEngine;

public class PlayerShieldBehaviour : MonoBehaviour
{
    public event Action<ShieldEventArgs> ShieldEvent;

    [SerializeField]
    private GameObject shieldPrefab;

    private ParticleSystem shieldParticleSystem;
    private PlayerController player;
    private PowerupType powerupThatTurnedOnShield;

    private bool isPowerupInProgress;
    private void Awake()
    {
        GameObject go = Instantiate(shieldPrefab, this.transform);
        shieldParticleSystem = go.GetComponent<ParticleSystem>();
        shieldParticleSystem.Stop();
        player = this.GetComponentInParent<PlayerController>() ?? throw new MissingReferenceException(nameof(PlayerController));
        powerupThatTurnedOnShield = PowerupType.NONE;
    }    

    private void Start()
    {
        this.player.PowerupManager.PowerupCollected += OnPowerupCollected;
        this.player.PowerupManager.TimedPowerupExpired += OnPowerupEnded;
        this.player.healthController.PlayerObstacleCollided += OnPlayerCollidedWithObstacle;
    }
    
    private void OnPlayerCollidedWithObstacle(PlayerObstacleCollisionEventArgs args)
    {
        if(!args.IsHitCounted && !isPowerupInProgress && !shieldParticleSystem.isPlaying)
        {
            StartCoroutine(FlickerShield());
        }
    }

    private void OnPlayerExitAttackZone()
    {
        BumperCopsDebug.Log("Shield can be affected");
        ExpireShield();
    }

    private void TurnOnShieldEffect(bool playParticleSystem, bool doesHitAffectShield)
    {
        ShieldEvent?.Invoke(new ShieldEventArgs(true));
        if(playParticleSystem)
        {
            shieldParticleSystem.Play();
        }
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type == PowerupType.SHIELD || 
            args.Type == PowerupType.BIG_BOOST || 
            args.Type == PowerupType.SIREN)
        {
            isPowerupInProgress = args.Type == PowerupType.SHIELD;
            powerupThatTurnedOnShield = args.Type;
            TurnOnShieldEffect(args.Type == PowerupType.SHIELD, true);
        }
    }

    private void OnPowerupEnded(CollectiblePickedEventArgs args)
    {
        if(powerupThatTurnedOnShield == args.Type && args.Type != PowerupType.SHIELD)
        {
            ExpireShield();
            StartCoroutine(FlickerShield());
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        // shield powerup case
        if(isPowerupInProgress && 
            !other.gameObject.layer.Equals(Constants.ENEMY_LAYER))
        {
            ExpireShield();
            StartCoroutine(FlickerShield());
            return;
        }
    }

    private IEnumerator FlickerShield()
    {
        int flickerCount = 0;
        while(flickerCount < 5)
        {
            shieldParticleSystem.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            shieldParticleSystem.gameObject.SetActive(true);
            shieldParticleSystem.Play();    
            yield return new WaitForSeconds(0.1f);
            flickerCount++;
        }

        shieldParticleSystem.Stop();
    }

    private void ExpireShield()
    {
        BumperCopsDebug.Log("Expiring Shield");
        ShieldEvent?.Invoke(new ShieldEventArgs(false));
        powerupThatTurnedOnShield = PowerupType.NONE;
    }
}

public class ShieldEventArgs
{
    public readonly bool Activated;
    public ShieldEventArgs(bool activated)
    {   
        this.Activated = activated;
    }
}