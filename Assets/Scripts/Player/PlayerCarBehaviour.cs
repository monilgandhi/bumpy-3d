using System;
using UnityEngine;
public class PlayerCarBehaviour : MonoBehaviour 
{    
    public event Action PlayerCharacterChanged;
    public delegate void BackupCarEventHandler();
    public event BackupCarEventHandler BackupCalled;
    [SerializeField]
    private TouchManager touchManager;

    [SerializeField]
    private Vector3 modelPosition;
    private PlayerController controller;
    private PlayerCharacter primaryCharacter;
    public PlayerCharacter Character {get; private set;}
    public bool IsBackupCharacter { get; private set; }

    public void InitPrimaryCharacter(PlayerCharacter character)
    {
        if(this.controller == null)
        {
            this.controller = this.GetComponent<PlayerController>();
        }

        this.primaryCharacter = character;
        SwitchCharacter(this.primaryCharacter);
    }

    private void SwitchCharacter(PlayerCharacter charToUse)
    {
        if(this.Character != null)
        {
            this.Character.gameObject.SetActive(false);
        }
        
        this.Character = charToUse;
        this.Character.transform.parent = this.transform;
        this.Character.transform.localPosition = modelPosition;
        this.controller.RigidBody.centerOfMass = this.Character.CenterOfMass.transform.position;
        this.controller.RigidBody.mass = this.Character.Mass;
        this.controller.RigidBody.maxAngularVelocity = 25;
        this.Character.gameObject.SetActive(true);
        this.Character.transform.localRotation = Quaternion.identity;
        this.Character.ChangeLayerOfMesh(Constants.PLAYER_LAYER);
        this.Character.ToggleWheels(true);
        this.Character.ToggleCarWheelsMovement(move: true);
    }
}