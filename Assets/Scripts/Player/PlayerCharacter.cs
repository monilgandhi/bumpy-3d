using System;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
public class PlayerCharacter : MonoBehaviour 
{
    public event Action<bool> WeaponLoaded;
    [SerializeField]
    private string carId;
    public string Id { get { return carId; }}

    [SerializeField]
    private float mass;
    public float Mass { get { return mass; }}

    [SerializeField]
    private GameObject centerOfMass;
    public GameObject CenterOfMass { get { return centerOfMass; }}

    [SerializeField]
    private GameObject rearEndPosition;
    public Vector3 RearEndPosition { get { return this.rearEndPosition.transform.position; }}

    [SerializeField]
    private WheelCollider[] wheels;
    public WheelCollider[] Wheels { get { return this.wheels; }}

    [SerializeField]
    private PoliceLightBehaviour policeLight;
    public PoliceLightBehaviour PoliceLight { get { return policeLight; }}

    [SerializeField]
    private PlayerBoostBehaviour speedBoost;
    public PlayerBoostBehaviour SpeedBoost { get { return this.speedBoost; }}

    [SerializeField]
    private GameObject attackLocation;
    public Vector3 AttackLocation { get { return this.attackLocation.transform.position; } }

    [SerializeField]
    private GameObject explosionForceLocations;
    [SerializeField]
    private List<NearMissBehaviour> nearMissBehaviours;
    public IList<NearMissBehaviour> NearMissBehaviours { get { return nearMissBehaviours; }}

    [SerializeField]
    private List<ParticleSystem> wheelTrails;
    public List<ParticleSystem> WheelTrails { get { return wheelTrails; }}

    [SerializeField]
    private Sprite screenShot;
    public Sprite ScreenShot { get { return screenShot; }}

    [SerializeField]
    private AnimationCurve boostCurve;
    public AnimationCurve BoostCurve { get { return boostCurve; }}
    public float BoostLength { get; private set; }
    public float AttackStats { get { return carConfig.Attack; }}
    public float SpeedStats { get { return START_DEFAULT_SPEED; }}
    public float BoostStats { get { return carConfig.Boost; }}
    public float HandlingStats { get { return carConfig.Handling; }}
    public WeaponsBehaviour Weapon { get; private set; }
    private CarConfig carConfig;
    private MeshRenderer meshRenderer;
    private Dictionary<string, Material> carMaterials;
    private bool isConfigLoadComplete;
    private PrefabManager prefabManager;
    private const int START_DEFAULT_SPEED = 60;
    private void Awake() 
    {
        BoostLength = BoostCurve.keys.Last().time;
        meshRenderer = this.GetComponent<MeshRenderer>();
        LoadConfig();
        LoadAllSkins();
    }

    public void LoadNewWeapon(string weaponId, PrefabManager prefabManager)
    {
        BumperCopsDebug.Log($"Loading new weapon {weaponId}");
        if(string.IsNullOrEmpty(weaponId))
        {
            BumperCopsDebug.LogError("weaponId is null");
            return;
        }

        if(Weapon != null && Weapon.Id.Equals(weaponId))
        {
            WeaponLoaded?.Invoke(true);
            return;
        } else if (Weapon != null && !Weapon.Id.Equals(weaponId))
        {
            Weapon.Deactivate();
        }

        this.prefabManager = prefabManager;
        this.prefabManager.PrefabLoadRequestComplete += OnPrefabLoadComplete;
        this.prefabManager.LoadPrefabAsync(weaponId, typeof(PlayerCharacter), Constants.GetWeaponPrefabPath(weaponId));
    }

    private void OnPrefabLoadComplete(PrefabLoadEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogError("args are null for prefab load compelete event");
            return;
        }

        if(args.CallingClass != typeof(PlayerCharacter))
        {
            return;
        }

        this.prefabManager.PrefabLoadRequestComplete -= OnPrefabLoadComplete;
        if(args.Success && args.GObject == null)
        {
            WeaponLoaded?.Invoke(false);
            BumperCopsDebug.LogError("null game object for loaded prefab");
            return;
        }

        Weapon = args.GObject.GetComponent<WeaponsBehaviour>();
        if(Weapon == null)
        {
            WeaponLoaded?.Invoke(false);
            BumperCopsDebug.LogError($"Weapon is null with id {args.PrefabId}");
            return;
        }

        BumperCopsDebug.Log($"Weapon with id {args.PrefabId} loaded");
        Weapon.transform.parent = this.transform;
        Weapon.transform.localPosition = attackLocation.transform.localPosition;
        Weapon.transform.localRotation = Quaternion.identity;

        WeaponLoaded?.Invoke(true);
    }

    public bool LoadNewMaterial(string materialName)
    {
        if(carMaterials == null)
        {
            LoadAllSkins();
        }
        
        if(string.IsNullOrEmpty(materialName))
        {
            BumperCopsDebug.LogError("Material name is empty");
            return false;
        }

        if(this.meshRenderer == null)
        {
            meshRenderer = this.GetComponent<MeshRenderer>();
        }

        Material m = null;
        if(!carMaterials.TryGetValue(materialName, out m))
        {
            BumperCopsDebug.LogWarning($"Unable to find material with name {materialName}");
            return false;
        }

        BumperCopsDebug.Log($"Replacing new material with name {materialName}");

        meshRenderer.material = m;
        return true;
    }

    private void LoadAllSkins()
    {
        string folderPath = CarUtils.GetConfigLocationForShopItem(this.Id, ShopItemType.CAR);
        carMaterials = new Dictionary<string, Material>();

        string filePath = folderPath + Path.DirectorySeparatorChar + "materials";
        BumperCopsDebug.Log($"Loading skins at path {filePath}");
        Material[] mList = Resources.LoadAll<Material>(filePath);

        if(mList == null || mList.Length == 0)
        {
            BumperCopsDebug.Log($"Unable to load materials for car {this.Id}");
            return;
        }

        foreach(Material m in mList)
        {
            carMaterials.Add(m.name, m);
        }
    }

    public void LoadConfig()
    {
        if(isConfigLoadComplete)
        {
            return;
        }

        string filePath = CarUtils.GetConfigLocationForShopItem(this.carId, ShopItemType.CAR) + Path.DirectorySeparatorChar + this.carId;

        BumperCopsDebug.Log("File path is " + filePath);
        TextAsset prefabAttackFile = Resources.Load<TextAsset>(filePath);
        string json = Encoding.UTF8.GetString(prefabAttackFile.bytes, 0, prefabAttackFile.bytes.Length);
        carConfig = JsonConvert.DeserializeObject<CarConfig>(json);
        isConfigLoadComplete = true;
        this.StopWheelTrails();
    }

    public void Init(PlayerController player, string materialName)
    {
        if(!LoadNewMaterial(materialName))
        {
            BumperCopsDebug.LogWarning($"Unable to load correct material with name {materialName}");
        }
        
        SpeedBoost.Init(player);
    }
    
    public void ToggleWheels(bool enable)
    {
        // turn off each of the wheels
        for(int i = 0; i < this.Wheels.Length; i++)
        {
            this.Wheels[i].enabled = enable;
        }
    }

    public void PlayWheelTrails()
    {
        BumperCopsDebug.Log("Character: Starting wheel trails");   
        if(this.wheelTrails == null ||  this.wheelTrails.Count == 0)
        {
            BumperCopsDebug.LogWarning("Wheel trails are absent");
            return;
        }

        foreach(ParticleSystem p in this.wheelTrails)
        {
            p.Play();
        }
    }

    public void StopWheelTrails()
    {
        BumperCopsDebug.Log("Character: Stopping wheel trails");
        if(this.wheelTrails == null ||  this.wheelTrails.Count == 0)
        {
            BumperCopsDebug.LogWarning("Wheel trails are absent");
            return;
        }

        foreach(ParticleSystem p in this.wheelTrails)
        {
            p.Stop();
        }
    }

    public void ChangeLayerOfMesh(int newLayer)
    {

        foreach(Transform t in this.GetComponentsInChildren<Transform>(true))
        {
            //BumperCopsDebug.Log($"Changing layer of {t.gameObject.name}");
            t.gameObject.layer = newLayer;
        }
    }

    public void ToggleCarWheelsMovement(bool move)
    {
        foreach(WheelCollider w in this.wheels)
        {
            RotationBehaviour r = w.GetComponent<RotationBehaviour>();
            if(r == null)
            {
                BumperCopsDebug.LogWarning("Rotation behaviour missing");
                return;
            }

            r.StartRotationImmediately = move;
        }
    }
}