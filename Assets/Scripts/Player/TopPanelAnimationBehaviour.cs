using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TopPanelAnimationBehaviour : MonoBehaviour 
{
    [SerializeField]
    private GameObject coinAnimationPanel;

    [SerializeField]
    private GameObject clockAnimationPanel;
    private TMP_Text coinTextField;
    private AnimationCompletedBehaviour coinAnimator;
    
    private TMP_Text clockTextField;
    private AnimationCompletedBehaviour clockAnimator;
    private const float ANIMATION_TIME = 0.6f;

    private int coinsToAddText;
    public bool IsClockAnimationComplete { get { return this.clockAnimator.IsAnimationComplete; }}
    public bool IsCoinAnimationComplete {  get { return this.coinAnimator.IsAnimationComplete; }}
    private void Start() 
    {
        this.coinTextField = this.coinAnimationPanel.GetComponentInChildren<TMP_Text>() ?? throw new MissingReferenceException(nameof(TMP_Text));
        this.coinAnimator = this.coinAnimationPanel.GetComponent<AnimationCompletedBehaviour>() ?? throw new MissingComponentException(nameof(AnimationCompletedBehaviour));
        
        this.clockTextField = this.clockAnimationPanel.GetComponentInChildren<TMP_Text>() ?? throw new MissingReferenceException(nameof(TMP_Text));
        this.clockAnimator = this.clockAnimationPanel.GetComponent<AnimationCompletedBehaviour>() ?? throw new MissingComponentException(nameof(AnimationCompletedBehaviour));

        ResetBothGameObjects();
    }    

    private void ResetBothGameObjects()
    {
        this.coinAnimationPanel.SetActive(false);
        this.clockAnimationPanel.SetActive(false);
    }

    public void PlayCoinAnimation(int coinstoAdd)
    {
        if(IsCoinAnimationComplete)
        {
            coinsToAddText = 0;
        }

        coinsToAddText += coinstoAdd;

        this.coinTextField.text = $"+ {coinsToAddText}";
        this.coinTextField.color = Color.white;
        coinAnimator.StartAnimationWithTrigger("coin", true, true);
    }

    public void PlayClockAnimation(string str, Color color)
    {
        this.clockTextField.text = str;
        this.clockTextField.color = color;
        clockAnimator.StartAnimationWithTrigger("clock", true, true);
    }
}