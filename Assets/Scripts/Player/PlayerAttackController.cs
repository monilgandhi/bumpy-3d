﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackController : MonoBehaviour
{
    public event Action PlayerEnteredAttackZone;
    public event Action PlayerExitAttackZone;
    public event Action WeaponReady;
    public event Action WeaponEnded;
    public event Action<BumpEventArgs> Bumped;
    public event Action<BumpEventArgs> BumpEnded;

    public delegate void PlayerReadyToAttackEventHandler();
    public event PlayerReadyToAttackEventHandler AttackReady;

    public float WeaponReach 
    {
        get 
        {
            if(weapon != null && weapon.gameObject.activeSelf)
            {
                return weapon.WeaponReach;
            }

            return Constants.ATTACK_ZONE_REACH;
        }
    }

    [SerializeField]
    private AttackEffectBehaviour bumpEffect;

    [SerializeField]
    private AttackParticleBehaviour attackParticle;

    [SerializeField]
    private AnimationCurve bumpCurve;

    [SerializeField]
    private PrefabManager prefabManager;
    private EnemyController enemyController;
    private PlayerController playerController;
    private PlayerSpeedController speedController;
    private bool enteredAttackZone;
    public bool BumpInProgress { get; private set;}
    private bool canAttack = false;
    private WeaponsBehaviour weapon; 
    private bool weaponInUse;

    public bool HasWeaponUnlocked { get; private set; }
    // Start is called before the first frame update
    
    private void Awake() 
    {
        this.playerController = this.transform.root.GetComponent<PlayerController>();
        this.playerController.PlayerCharacterChanged += OnPlayerCharacterChanged;
        HasWeaponUnlocked = !string.IsNullOrEmpty(PlayerStats.Instance.WeaponId);
    }

    private void OnPlayerObstacleCollided(PlayerObstacleCollisionEventArgs args)
    {
        if(!weaponInUse || enteredAttackZone || BumpInProgress)
        {
            return;
        }

        DeactivateWeapon();
    }

    private void DeactivateWeapon()
    {
        weaponInUse = false;
        WeaponEnded?.Invoke();
        this.weapon.Deactivate(true);
    }

    private void OnPlayerCharacterChanged()
    {
        this.transform.position = this.playerController.character.AttackLocation;
        this.playerController.character.WeaponLoaded += OnWeaponLoaded;

        HasWeaponUnlocked = !string.IsNullOrEmpty(PlayerStats.Instance.WeaponId);

        if(this.playerController.character.Weapon != null)
        {
            this.playerController.character.Weapon.Deactivate();
        }
        
        if(HasWeaponUnlocked)
        {
            this.playerController.PowerupManager.PowerupCollected += OnPowerupCollected;
            BumperCopsDebug.Log($"Loading new weapong {PlayerStats.Instance.WeaponId}");
            this.playerController.character.LoadNewWeapon(PlayerStats.Instance.WeaponId, this.prefabManager);
        }
    }

    private void OnWeaponLoaded(bool success)
    {
        if(!success)
        {
            BumperCopsDebug.LogWarning("Weapon load failed");
            return;
        }

        BumperCopsDebug.Log("Weapon loaded");
        this.weapon = this.playerController.character.Weapon;
        this.weapon.Deactivate();
    }

    void Start()
    {
        // this.playerController.TouchManager.SwipeUp += BumpIfPossible;
        this.attackParticle.AttackMeterFilled += OnAttackMeterFilled;
        this.playerController.EnemyManager.EnemySpawned += OnEnemySpawned;
        this.speedController = this.playerController.SpeedController;
    }

    private void AdjustAttackCollider()
    {
        BoxCollider collider = this.GetComponent<BoxCollider>();
        Vector3 currentAttackZoneSize = this.GetComponent<BoxCollider>().size;
        Vector3 currentAttackZoneCenter = this.GetComponent<BoxCollider>().center;
        this.GetComponent<BoxCollider>().size = new Vector3(currentAttackZoneSize.x, currentAttackZoneSize.y, WeaponReach);
        this.GetComponent<BoxCollider>().center = Vector3.zero;
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args == null )
        {
            BumperCopsDebug.LogWarning("Args for powerup collected are null");
            return;
        }

        if(args.Type != PowerupType.WEAPON)
        {
            return;
        }
        
        BumperCopsDebug.Log("Weapon Collected: Starting animation");
        weaponInUse = true;
        this.weapon.Activate(false);
        WeaponReady?.Invoke();
        //StartCoroutine(StartWeaponCollectedEffect());
    }

    private void PartAnimationComplete(object o, WeaponMergeAnimationEventArgs args)
    {
        this.playerController.ShakeCamera(4f, 0.05f);
        if(args.LastPart)
        {
            this.weapon.MergeWeaponAnimation.WeaponPartAnimationComplete -= PartAnimationComplete;
            StartCoroutine(this.weapon.Animate(2, 0.5f));
            StartCoroutine(CompleteWeaponCollectedEffect());
        }
    }

    private IEnumerator CompleteWeaponCollectedEffect()
    {
        yield return new WaitUntil(() => weapon.FireAnimationComplete);
        // move the camera back
        this.playerController.CameraController.ToggleWeaponCamera(false);
        yield return new WaitForSecondsRealtime(1.0f);
        WeaponReady.Invoke();
    }

    private IEnumerator StartWeaponCollectedEffect()
    {
        this.weapon.MergeWeaponAnimation.WeaponPartAnimationComplete += PartAnimationComplete;
        this.playerController.CameraController.ToggleWeaponCamera(true);
        yield return new WaitForSecondsRealtime(0.5f);

        weapon.Activate(true);
        yield break;
    }

    // Update is called once per frame
    void OnAttackMeterFilled()
    {
        if(AttackReady != null && !canAttack)
        {
            canAttack = true;
            AttackReady?.Invoke();
            BumpIfPossible(null);
        }
    }

    private void BumpIfPossible(TapGestureArgs args)
    {
        if(!canAttack || !this.attackParticle.IsInAttackCircle)
        {
            return;
        }

        BumperCopsDebug.Log($"Checking if can bump {canAttack} & {this.attackParticle.IsInAttackCircle}");
        StartCoroutine(Bump());
    }

    private void OnEnemySpawned(EnemyArgs e) 
    {
        this.enemyController = e.enemy;
        this.enemyController.DamageController.EnemyDestructionBegin += () => 
        {
            this.enteredAttackZone = false;
            this.enemyController = null;
        };
    }

    public IEnumerator Bump()
    {
        if(this.enemyController == null)
        {
            BumperCopsDebug.Log("Enemy controller is null. returnring");
            yield break;
        }

        BumpInProgress = true;

        // reset the position of the player

        //this.playerController.transform.rotation = Quaternion.identity;
        // add position constraint so the car does not move up and down
        this.playerController.RigidBody.constraints |= RigidbodyConstraints.FreezePositionY;

        float start = Time.time;
        // position right behind the enemy

        yield return new WaitForFixedUpdate();

        float strength = weaponInUse ? 
            this.playerController.character.AttackStats * 2 : 
            this.playerController.character.AttackStats;

        BumperCopsDebug.Log($"PlayerAttackController: Strength after is {strength}");
        // make the events
        BumpEventArgs args = new BumpEventArgs(strength, weaponInUse);
        Bumped?.Invoke(args);

        canAttack = false;
        float bumpForce;
        this.bumpEffect.Attack(this.playerController);
        
        if(this.weapon != null && this.weaponInUse)
        {
            BumperCopsDebug.Log($"PlayerAttackController: Playing weapon animation");
            StartCoroutine(weapon.Animate());
            yield return new WaitWhile(() => !weapon.FireAnimationComplete);
            DeactivateWeapon();
        }
        else
        {
            while (Time.time - start < 0.25)
            {
                bumpForce = this.playerController.RigidBody.mass * 10;
                playerController.RigidBody.AddForce(Vector3.forward * bumpForce, ForceMode.Impulse);
                yield return new WaitForFixedUpdate();
            }
        }

        BumpInProgress = false;
        // remove the rigidbody constraint
        this.playerController.RigidBody.constraints &= ~RigidbodyConstraints.FreezePositionY;
        StartCoroutine(OnPlayerBumpedEnemy());
        BumpEnded?.Invoke(args);
    }

    private void Update() 
    {
        if(this.enteredAttackZone || this.enemyController == null || this.BumpInProgress)
        {
            return;
        }    

        if(!enteredAttackZone && this.enemyController.transform.position.z - this.transform.position.z <= WeaponReach)
        {
            BumperCopsDebug.Log("PlayerAttackController: Player entered attack zone");
            IndicateEnteredAttackZone();
        }
    }

    private void IndicateEnteredAttackZone()
    {
        enteredAttackZone = true;
        PlayerEnteredAttackZone();
    }

    private IEnumerator OnPlayerBumpedEnemy()
    {
        BumperCopsDebug.Log("Player bumped");
        // we wait here for two seconds because it may happen that the enemy's collider
        // may trigger again, indicating we are in attack zone. 
        // this causes bug where the circle is behind the enemy ( with player) and distance is huge.
        yield return new WaitForSeconds(2f);
        enteredAttackZone = false;
        PlayerExitAttackZone?.Invoke();
    }

    // we have to do this because some times OnTriggerEnter gets executed after bumping (in case enemy takes a little while to leave the zone and then reenters)
    // thus we have enemy that is really far away for bumping
    private IEnumerator TurnColliderOffOnBump()
    {
        Collider collider = this.GetComponent<BoxCollider>();
        // coroutine is already running
        if(!collider.enabled)
        {
            yield break;
        }

        collider.enabled = false;
        yield return new WaitForSeconds(1f);
        collider.enabled = true;

    }
}

public class BumpEventArgs
{
    public readonly float bumpStength;
    public readonly bool PlayerBumped;
    public readonly bool WeaponUsed;
    public BumpEventArgs(float bs, bool weaponUsed, bool playerBumped = true)
    {
        this.bumpStength = bs;
        this.PlayerBumped = playerBumped;
        this.WeaponUsed = weaponUsed;
    }
}
