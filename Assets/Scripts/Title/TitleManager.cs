using System;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour 
{
    public event Action<GameEventArgs> ExitTitlePage;

    [Header("Game Components")]
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private ScoreAndRewardTransitionBehaviour scoreAndRewardTransition;

    [SerializeField]
    private ChallengeManager challengeManager;

    [SerializeField]
    private AdsManager adsManager;

    [SerializeField]
    private RatingDialog ratingManager;

    [Header("UI Elements")]
    [SerializeField]
    private Canvas titleCanvas;

    [SerializeField]
    private ConfirmationDialogueBehaviour tutorialReplayBehaviour;

    [SerializeField]
    private ConfirmationDialogueBehaviour ratingDialogue;

    [SerializeField]
    private TMP_Text coinsValue;

    [SerializeField]
    private TMP_Text highScoreValue;

    [SerializeField]
    private GameObject upgradeNotificationPanel;

    [SerializeField]
    private Animator shopAnimation;

    [SerializeField]
    private GameObject adminPanel;

    [Header("Buttons")]
    [SerializeField]
    private Button shopButton;

    [SerializeField]
    private Button challengeButton;

    [SerializeField]
    private Button rewardedAdsbutton;

    [SerializeField]
    private Button ratingButton;
    [SerializeField]
    private TipBehaviour tips;

    private TMP_Text upgradeNotificationCount;

    private bool isTutorialReplayDialogueActive;

    private void Awake() 
    {
        adminPanel.SetActive(CarUtils.IsUnityDevelopmentBuild());

        this.gameManager.GameInitialized += OnGameInitialized;
        this.upgradeNotificationCount = this.upgradeNotificationPanel.GetComponentInChildren<TMP_Text>();
        this.shopButton.onClick.AddListener(OnShopButtonClicked);
        this.challengeButton.onClick.AddListener(OnChallengeButtonClicked);
        this.rewardedAdsbutton.onClick.AddListener(OnRewardAdClicked);

        // rating dialogue
        this.ratingButton.onClick.AddListener(OnRatingButtonClicked);

        ratingDialogue.OkayButtonClicked += OnRatingOk;
        ratingDialogue.CancelButtonClicked += OnRatingCanceled;

        this.tutorialReplayBehaviour.CancelButtonClicked += () => 
        {
            // mark the current version completed
            PlayerStats.Instance.MarkTutorialCompleted();
            ExitAndStart(false);
        };
        
        this.tutorialReplayBehaviour.OkayButtonClicked += () => ExitAndStart(true);
        this.challengeManager.PlayNowClicked += OnNewSession;
        this.titleCanvas.gameObject.SetActive(false);
    }

    private void OnRatingButtonClicked()
    {
        ratingDialogue.Show();
    }
    
    private void OnRatingCanceled()
    {
        this.gameManager.Analytics.RecordRatingbuttonClosed();
        DisplayTitlePage();
    }

    private void OnRatingOk()
    {
        this.gameManager.Analytics.RecordRatingbuttonOk();
        StartCoroutine(WaitForReviewToBeComplete());
    }

    private IEnumerator WaitForReviewToBeComplete()
    {
        BumperCopsDebug.Log("Checking for the review dialogue now");
        int ratingDialogueStatus = RemoteConfig.Instance.Config.RatingDialogue;
        BumperCopsDebug.Log($"Rating dialogu statis {ratingDialogueStatus}");
        BumperCopsDebug.Log($"is inbuilt dialogue available {this.ratingManager.IsInbuiltDialogueAvailable()}");
        if(this.ratingManager.IsInbuiltDialogueAvailable() && ratingDialogueStatus >= 0)
        {
            this.ratingManager.ShowInbuiltRatingDialogue();
            yield return new WaitForSecondsRealtime(2f);
            BumperCopsDebug.Log("Showing the review dialogue now");
            yield return new WaitUntil(() => this.ratingManager.HasFlowFinished);
        }
        else if(ratingDialogueStatus <= 0)
        {
            this.ratingManager.TakeUserToRateApp();
        }
    }

    private void SetupButtons()
    {
        bool isTutorialComplete = PlayerStats.Instance.IsTutorialComplete();
        this.challengeButton.gameObject.SetActive(isTutorialComplete);
        this.shopButton.gameObject.SetActive(isTutorialComplete);

        BumperCopsDebug.Log($"utc time is {(DateTime.UtcNow - NotificationStats.Instance.InstallTimeUTC).TotalDays}");
        //Do not show after 3 days
        bool showRatingButton = isTutorialComplete && 
            (DateTime.UtcNow - NotificationStats.Instance.InstallTimeUTC).TotalDays < 3;

        this.ratingButton.gameObject.SetActive(showRatingButton);
    }

    private void OnScoreRewardCanvasClosed()
    {
        scoreAndRewardTransition.ScoreRewardCanvasClosed -= OnScoreRewardCanvasClosed;
        DisplayTitlePage();
    }

    private void OnRewardAdClicked()
    {
        this.adsManager.AdEvent += OnAdEvent;
        this.adsManager.ShowRewardedAd(true, AdsPlacement.TITLE_VIDEO_AD_REWARD);
        this.titleCanvas.gameObject.SetActive(false);
    }

    private void OnAdEvent(AdEventArgs args)
    {
        if(!args.AdComplete || args.AdsPlacement != AdsPlacement.TITLE_VIDEO_AD_REWARD)
        {
            BumperCopsDebug.Log($"Ad not complete or ad placement incorrect {args.AdComplete}, {args.AdsPlacement}");
            return;
        }

        this.adsManager.AdEvent -= OnAdEvent;

        if(args.GiveReward)
        {
            BumperCopsDebug.Log("Giving reward");
            scoreAndRewardTransition.ScoreRewardCanvasClosed += OnScoreRewardCanvasClosed;
            scoreAndRewardTransition.GiveVideoReward();
        }
        else
        {
            this.titleCanvas.gameObject.SetActive(true);
        }
    }

    private void OnChallengeButtonClicked()
    {
        this.challengeManager.Show(true);
        OpenChallengeDialog();
    }

    private void OpenChallengeDialog()
    {
        this.titleCanvas.gameObject.SetActive(false);
        this.gameManager.Analytics.RecordChallengeButtonClick();
        this.challengeManager.ChallengeManagerClosed += OnChallengeManagerClosed;
    }

    private void OnChallengeManagerClosed()
    {
        BumperCopsDebug.Log("closing challenge manager");
        this.challengeManager.ChallengeManagerClosed -= OnChallengeManagerClosed;
        DisplayTitlePage();
    }

    private void ShopClosed()
    {
        this.gameManager.ShopClosed -= ShopClosed;
        DisplayTitlePage();
    }

    private void OnShopButtonClicked()
    {
        this.gameManager.ShopClosed += ShopClosed;
        this.titleCanvas.gameObject.SetActive(false);
        this.gameManager.Analytics.RecordShopButtonClick();
    }

    private void OnGameInitialized()
    {
        BumperCopsDebug.Log($"Is tutorial complete {PlayerStats.Instance.IsTutorialComplete()} is current version complete {PlayerStats.Instance.IsCurrentTutorialVersionComplete()}");
        this.isTutorialReplayDialogueActive = PlayerStats.Instance.IsTutorialComplete() && 
            !PlayerStats.Instance.IsCurrentTutorialVersionComplete();
        
        BumperCopsDebug.Log($"IS replay dialogue active {this.isTutorialReplayDialogueActive}");
        DisplayTitlePage();
        BumperCopsDebug.Log("Calling social stats to load");
        SocialStats.Load();
    }

    private void DisplayTitlePage()
    {
        this.ratingManager.PrepRatingDialog();
        this.coinsValue.text = PlayerStats.Instance.CoinCount.ToString();

        if(PlayerStats.Instance.CurrentHighScore == 0)
        {
            this.highScoreValue.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            this.highScoreValue.transform.parent.gameObject.SetActive(true);
            this.highScoreValue.text = PlayerStats.Instance.CurrentHighScore.ToString("n0");
        }

        this.titleCanvas.gameObject.SetActive(true);
        SetPossibleUpgrades();

        BumperCopsDebug.Log($"Ads: Checking if video ad is ready {adsManager.CanShowAd(AdsPlacement.TITLE_VIDEO_AD_REWARD)}");
        rewardedAdsbutton.gameObject.SetActive(adsManager.CanShowAd(AdsPlacement.TITLE_VIDEO_AD_REWARD));

        SetupButtons();
        tips.Show();

        // first show rating dialog
        if(this.ratingManager.ShouldShowRating)
        {
            PlayerStats.Instance.UpdateLastReviewTime(Convert.ToInt64((DateTime.UtcNow - Constants.EPOCH).TotalSeconds));
            ratingDialogue.Show();
            return;
        }

        // then show challenge manager
        if(challengeManager.Show(false))
        {
            BumperCopsDebug.Log("Showing challenge manager");
            OpenChallengeDialog();
            return;
        }
    }

    private void SetPossibleUpgrades()
    {
        int possibleUpgrades = GetPossibleUpgradesCount();
        if(possibleUpgrades == 0)
        {
            this.upgradeNotificationPanel.SetActive(false);
            this.shopAnimation.SetBool("upgradePossible", false);
            return;
        }

        this.upgradeNotificationPanel.SetActive(true);
        this.upgradeNotificationCount.text = possibleUpgrades.ToString();

        this.shopAnimation.SetBool("upgradePossible", true);
    }

    private int GetPossibleUpgradesCount()
    {
        /*int possibleUpgrades = 0;
        int requiredCost = this.gameManager.Player.AttackStats.NextUpgradeCost;
        if(PlayerStats.Instance.CoinCount < requiredCost)
        {
            // no upgrades possible. Hide the notification
            return possibleUpgrades;
        }

        ++possibleUpgrades;
        requiredCost += this.gameManager.Player.MovementStats.NextUpgradeCost;
        if(PlayerStats.Instance.CoinCount < requiredCost)
        {
            return possibleUpgrades;
        }

        return ++possibleUpgrades;*/
        return 0;
    }

    public void OnNewSession()
    {
        if(!this.isTutorialReplayDialogueActive)
        {
            ExitAndStart(!PlayerStats.Instance.IsTutorialComplete());
        }
        else
        {
            this.tutorialReplayBehaviour.Show();
        }
    }

    private void ExitAndStart(bool startTutorial)
    {
        this.titleCanvas.gameObject.SetActive(false);
        ExitTitlePage?.Invoke(new GameEventArgs(startTutorial));
    }
}