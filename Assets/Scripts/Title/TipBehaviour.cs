using UnityEngine;
using TMPro;
using System.Collections;
using System.Collections.Generic;

public class TipBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text tipText;
    private float previousTipStartTime;
    private int currentTipIndex;
    private static List<string> tips = new List<string>()
    {
        "Use utility belt in tight situations",
        "Find weapon tokens in treasure chest",
        "Treasure chest can be easily obtained by finishing missions or playing more",
        "Every car has different boost. Try all of them",
        "Weapons arrest criminal in one hit",
        "Weapon is lost if you hit other cars",
        "Near miss gives you coins and boosts your score",
        "Arresting an enemy without hitting civilians gives you a 2x multiplier"
    };

    private void Awake() 
    {
        tipText.text = $"Tip: {tips[currentTipIndex]}";
    }

    private void Update() 
    {
        if(Time.time - previousTipStartTime <= 5)
        {
            return;
        }    

        previousTipStartTime = Time.time;
        ++currentTipIndex;
        if(currentTipIndex >= tips.Count)
        {
            currentTipIndex = 0;
        }
        
        StartCoroutine(SwitchTip());
    }

    public void Show()
    {
        tips.Shuffle();
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public IEnumerator SwitchTip()
    {
        float alpha = 1;
        while(alpha > 0)
        {
            alpha -= Time.unscaledDeltaTime * 3;
            tipText.color = new Color(1, 1, 1, alpha);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(0.2f);
        tipText.text = $"Tip: {tips[currentTipIndex]}";
        while(alpha < 1)
        {
            alpha += Time.unscaledDeltaTime * 3;
            tipText.color = new Color(1, 1, 1, alpha);
            yield return new WaitForEndOfFrame();
        }
    }
}