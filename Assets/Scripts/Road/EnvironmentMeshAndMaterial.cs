using System;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentMeshAndMaterial : MonoBehaviour
{

    [SerializeField]
    private List<Material> materials;
    private MeshRenderer meshRenderer;
    
    public void Init()
    {
        if(meshRenderer == null)
        {
            this.meshRenderer = this.GetComponentInChildren<MeshRenderer>();
        }

        int random = UnityEngine.Random.Range(0, materials.Count - 1);
        meshRenderer.material = materials[random];
    }
}