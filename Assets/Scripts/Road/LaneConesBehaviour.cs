using System.Collections.Generic;
using UnityEngine;

public class LaneConesBehaviour : MonoBehaviour 
{
    private List<Vector3> positions = new List<Vector3>();
    private MeshRenderer[] cones;
    private void Awake() 
    {
        cones = this.GetComponentsInChildren<MeshRenderer>();
        if(cones == null)
        {
            return;
        }

        for(int i = 0; i < cones.Length; i++)
        {
            positions.Add(cones[i].transform.position);
        }
    }

    private void OnDisable()
    {
        if(cones.Length != positions.Count)
        {
            BumperCopsDebug.LogWarning("Cones and positions do not match");
            return;
        }

        for(int i = 0; i < cones.Length; i++)
        {
            cones[i].transform.position = positions[i];
            cones[i].transform.rotation = Quaternion.identity;
        }
    }
}