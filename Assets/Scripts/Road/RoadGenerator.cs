using System;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour 
{
    [SerializeField]
    private List<GameObject> singleRoadPrefabs;

    [SerializeField]
    private List<GameObject> mergeRoadPrefabs;

    [SerializeField]
    private List<GameObject> doubleLaneRightPrefab;

    [SerializeField]
    private GameObject forkStartPrefab;

    [SerializeField]
    private List<GameObject> forkGenericPrefab;

    [SerializeField]
    private List<GameObject> envPrefabs;

    [SerializeField]
    private List<GameObject> specialEnvPrefabs;

    private Dictionary<RoadManager.RoadType, Queue<GameObject>>[] roadPoolsByLanesCount;
    private Dictionary<RoadManager.RoadType, GameObject>[] roadPrefabsByLaneCount;
    private Queue<GameObject> envPool;
    private Queue<GameObject> specialEnvPool;
    private Queue<GameObject> forkStartPool;
    private RoadManager roadManager;
    private int initialPathNumber = 9;
    private float specialEnvTimer = -1;
    private bool nextRoadStartFork;
    private const float SPECIAL_ENV_TIMER_EXPIRY = 20;
    private void Awake() 
    {
        HydratePools();
        roadManager = this.GetComponent<RoadManager>();
        roadManager.RoadTypeChanged += OnRoadtypeChanged;
    }

    private void OnRoadtypeChanged(RoadtypeChangedEventArgs args)
    {
        if(args.To == RoadManager.RoadType.FORK)
        {
            nextRoadStartFork = true;
        }   
    }

    private void InitRoadTypes(List<GameObject> roadTypePrefab)
    {
        foreach(GameObject roadPrefab in roadTypePrefab)
        {
            roadPrefab.SetActive(false);
            Queue<GameObject> singleRoadLaneQueue = InstantiatePrefab(roadPrefab, initialPathNumber);

            GameObject initedObj = singleRoadLaneQueue.Peek();
            if(initedObj == null || initedObj.GetComponent<RoadController>() == null)
            {
                BumperCopsDebug.LogError("invalid road object inited");
                singleRoadLaneQueue.Dequeue();
                continue;
            }

            RoadController rc = initedObj.GetComponent<RoadController>() ;
            if(roadPoolsByLanesCount[rc.LaneCount] == null)
            {
                roadPoolsByLanesCount[rc.LaneCount] = 
                    new Dictionary<RoadManager.RoadType, Queue<GameObject>>();

                roadPrefabsByLaneCount[rc.LaneCount] = new Dictionary<RoadManager.RoadType, GameObject>();
            }

            BumperCopsDebug.Log($"Inserting lane count {rc.LaneCount} and type {rc.RoadType}");
            roadPoolsByLanesCount[rc.LaneCount].Add(rc.RoadType, singleRoadLaneQueue);
            roadPrefabsByLaneCount[rc.LaneCount].Add(rc.RoadType, roadPrefab);
        }
    }

    private void HydratePools()
    {
        forkStartPrefab.SetActive(false);
     
        // init
        // peek the first item. get road controller. get the number of rows
        // put in the list
        roadPoolsByLanesCount = new Dictionary<RoadManager.RoadType, Queue<GameObject>>[5];
        roadPrefabsByLaneCount = new Dictionary<RoadManager.RoadType, GameObject>[5];
        
        InitRoadTypes(singleRoadPrefabs);
        InitRoadTypes(forkGenericPrefab);
        InitRoadTypes(doubleLaneRightPrefab);
        InitRoadTypes(mergeRoadPrefabs);

        forkStartPool = InstantiatePrefab(forkStartPrefab);

        envPool = new Queue<GameObject>();
        for(int j = 0; j < initialPathNumber; j++)
        {
            envPrefabs.Shuffle();
            for(int i = 0; i < envPrefabs.Count; i++)
            {
                GameObject prefab = envPrefabs[i];
                prefab.SetActive(false);

                envPool.Enqueue(Instantiate(prefab, this.transform));
            }
        }

        /*specialEnvPool = new Queue<GameObject>();
        for(int i = 0; i < specialEnvPrefabs.Count; i++)
        {
            GameObject prefab = specialEnvPrefabs[i];
            prefab.SetActive(false);

            Queue<GameObject> gameObjectQueue = InstantiatePrefab(prefab);
            while(gameObjectQueue.Count > 0)
            {
                specialEnvPool.Enqueue(gameObjectQueue.Dequeue());
            }
        }*/
    }

    private Queue<GameObject> InstantiatePrefab(GameObject prefab, int copies = 1)
    {
        Queue<GameObject> prefabCopies = new Queue<GameObject>();
        for(int i = 0; i < copies; i++)
        {
            prefabCopies.Enqueue(Instantiate(prefab, this.transform));            
        }

        return prefabCopies;
    }

    public void RecycleRoad(RoadController rc)
    {
        rc.gameObject.SetActive(false);
        rc.LeftEnvironment.SetActive(false);
        rc.LeftEnvironment.transform.parent = null;

        rc.RightEnvironment.SetActive(false);
        rc.RightEnvironment.transform.parent = null;

        if(rc.RoadType == RoadManager.RoadType.FORK && rc.IsForkStart)
        {
            forkStartPool.Enqueue(rc.gameObject);
        }
        else
        {
            // insert the road into the pool
            Queue<GameObject> pooledTypeRoadQueue;
            roadPoolsByLanesCount[rc.LaneCount].TryGetValue(rc.RoadType, out pooledTypeRoadQueue);

            pooledTypeRoadQueue.Enqueue(rc.gameObject);
        }

        envPool.Enqueue(rc.LeftEnvironment);
        envPool.Enqueue(rc.RightEnvironment);
    }

    public GameObject GetEnvironment()
    {
        return GetOrCreateEnvironment();
    }

    public bool HasRoadObject(RoadManager.RoadType roadType, int laneCount)
    {
        if(roadPrefabsByLaneCount.Length < laneCount || roadPrefabsByLaneCount[laneCount] == null)
        {
            return false;
        }

        return roadPrefabsByLaneCount[laneCount].ContainsKey(roadType);
    }

    public GameObject GetRoad(RoadManager.RoadType roadType, int laneCount)
    {
        return GetOrCreateRoad(roadType, laneCount);
    }

    private GameObject GetOrCreateEnvironment()
    {
        if(specialEnvTimer < 0)
        {
            // reset
            specialEnvTimer = Time.time;
        }

        Queue<GameObject> pooledQueue = envPool;
        /*if(specialEnvPool.Count > 0 && Time.time - specialEnvTimer >= SPECIAL_ENV_TIMER_EXPIRY)
        {
            pooledQueue = specialEnvPool;
            specialEnvTimer = Time.time;
        }*/
        
        if(pooledQueue.Count <= 0)
        {
            BumperCopsDebug.Log("Instantiating");
            Queue<GameObject> newObjectsQueue = InstantiatePrefab(envPrefabs[UnityEngine.Random.Range(0, envPrefabs.Count)], 3);
            GameObject result = newObjectsQueue.Dequeue();
            for(int i = 0; i < newObjectsQueue.Count; i++)
            {
                pooledQueue.Enqueue(newObjectsQueue.Dequeue());
            }

            return result;
        }  

        GameObject obj = pooledQueue.Dequeue();
        obj.GetComponent<EnvironmentMeshAndMaterial>().Init();
        return obj;
    }

    private GameObject GetOrCreateRoad(RoadManager.RoadType roadType, int laneCount)
    {
        //BumperCopsDebug.Log($"RoadController: Looking for type {roadType}");
        Queue<GameObject> pooledQueue;
        // if we are starting the fork. 
        if(nextRoadStartFork)
        {
            if(forkStartPool.Count <= 0)
            {
                BumperCopsDebug.LogWarning("Fork start pool is empty. Should not be");
            }
            else
            {
                nextRoadStartFork = false;
                return forkStartPool.Dequeue();
            }
        }

        // BumperCopsDebug.Log($"Getting queue for {roadType} for lane count {laneCount}");
        if(roadPoolsByLanesCount.Length < laneCount || roadPoolsByLanesCount[laneCount] == null)
        {
            throw new ArgumentException("Lane count road pools do not exist");
        }

        if(!roadPoolsByLanesCount[laneCount].TryGetValue(roadType, out pooledQueue))
        {
            throw new ArgumentException($"Unable to find pooled queue for {roadType} and laneCount {laneCount}");
        }

        if(pooledQueue.Count <= 0)
        {
            Queue<GameObject> newObjectsQueue = InstantiatePrefab(GetPrefabForRoadType(roadType, laneCount), 3);

            // remove the first one for result. queue the rest
            GameObject result = newObjectsQueue.Dequeue();
            for(int i = 0; i < newObjectsQueue.Count; i++)
            {
                pooledQueue.Enqueue(newObjectsQueue.Dequeue());
            }

            return result;
        }

        return pooledQueue.Dequeue();
    }

    private GameObject GetPrefabForRoadType(RoadManager.RoadType roadType, int laneCount)
    {
        // BumperCopsDebug.Log($"Looking for type {roadType} with alne count {laneCount}");
        if(roadPrefabsByLaneCount.Length < laneCount || roadPrefabsByLaneCount[laneCount] == null)
        {
            throw new ArgumentException("Invalid roadtype or lane count for prefab");
        }

        GameObject prefab = null;
        if(!roadPrefabsByLaneCount[laneCount].TryGetValue(roadType, out prefab))
        {
            throw new KeyNotFoundException(roadType.ToString());
        }

        return prefab;
    }
}