using UnityEngine; 
using System.Collections.Generic;
using TMPro;
public class RoadCanvas : MonoBehaviour
{
    [SerializeField]
    private TMP_Text text;

    [SerializeField]
    private List<ParticleSystem> effects;

    private bool isActive;
    private void Awake()
    {
        this.gameObject.SetActive(false);
        ToggleParticleSystems(false);
    }

    public void Show(Transform newParent, PlayerController player)
    {
        this.transform.SetParent(newParent);
        this.transform.localPosition = new Vector3(0, player.transform.position.y + 1, 0);

        LaneController playerLane = player.MoveController.Lane;
        if(playerLane != null)
        {
            this.transform.localPosition = new Vector3(playerLane.transform.localPosition.x, player.transform.position.y + 1, 0);
        }
        
        this.gameObject.SetActive(true);
        ToggleParticleSystems(true);
    }

    private void OnDisable() 
    {
        ToggleParticleSystems(false);
        this.gameObject.SetActive(false);
    }

    private void ToggleParticleSystems(bool enable)
    {
        foreach(ParticleSystem e in effects)
        {
            if(enable)
            {
                e.Play();
            }
            else
            {
                e.Stop();
            }
        }
    }
}