﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

public class RoadController : MonoBehaviour
{
    [SerializeField]
    private GameObject pathEnd;

    [SerializeField]
    private GameObject pathLeftBottom;

    [SerializeField]
    private bool moveRoad;

    [SerializeField]
    private RoadManager.RoadType myType;
    public RoadManager.RoadType RoadType { get { return myType; }}

    [SerializeField]
    [Tooltip("For merge lane lane count will be lower number of lanes. The merging lane should have highest number")]
    private int laneCount;

    [SerializeField]
    private GameObject leftEnvironmentModel;

    [SerializeField]
    private GameObject rightEnvironmentModel;

    [Header("Game Elements")]
    private ScoreBehaviour scoreBehaviour;

    [Header("Fork road")]
    [SerializeField]
    private bool isForkStart;
    public bool IsForkStart { get { return isForkStart; }}
    public int LaneCount { get { return laneCount; }}
    public Vector3 WorldEnd { get { return this.pathEnd.transform.position; }}
    public Vector3 LocaLEnd { get { return this.pathEnd.transform.localPosition; }}
    public Vector3 LeftBottomLocalStart { get { return this.pathLeftBottom.transform.localPosition; }}
    public Vector3 LeftBottomWorldStart { get { return this.pathLeftBottom.transform.position; }}
    private List<LaneController> sameDirectionLanes = new List<LaneController>();
    public int LaneCountSameDirection { get { return sameDirectionLanes.Count; }}
    private List<LaneController> oppositeDirectionLanes = new List<LaneController>();
    public int LaneCountOppositeDirection { get { return oppositeDirectionLanes.Count; }}
    private Dictionary<int, LaneController> allLanes = new Dictionary<int, LaneController>();
    public LaneController CurrentLane { get; private set; }

    public long MyPathNumber { get; private set; }
    
    private RoadManager roadManager;

    public GameObject LeftEnvironment { get; private set; }
    public GameObject RightEnvironment { get; private set; }
    public LaneController MiddleLane 
    {
        get 
        {
            return allLanes[allLanes.Count/2]; 
        }
    }
    // Use this for initialization
    void Awake()
    {
        Transform lanesParent = transform.Find("lanecolliders");
        List<LaneController> controllers = lanesParent.GetComponentsInChildren<LaneController>().ToList();
        foreach (LaneController c in controllers)
        {
            if (c == null)
            {
                BumperCopsDebug.LogWarning("lane controller is null");
                continue;
            }

            c.Init(this); 

            allLanes.Add(c.Id, c);

            if(c.LaneDirection == LaneDirection.OPPOSITE)
            {
                this.oppositeDirectionLanes.Add(c);
            } else 
            {
                this.sameDirectionLanes.Add(c);
            }
        } 
    }

    private void Update() 
    {
        if(!moveRoad)
        {
            return;
        }
        
        int worldSpeed = this.roadManager == null ? Constants.WORLD_SPEED : this.roadManager.WorldSpeed;
        this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 10), Time.deltaTime * worldSpeed);
    }
    
    public Vector3? GetLanePosition(int laneIdx)
    {
        if(laneIdx >= this.LaneCount)
        {
            BumperCopsDebug.LogError("Lane idx is greater than lane count");
            return null;
        }

        BumperCopsDebug.Log($"Looking for idx {laneIdx}");
        return this.allLanes[laneIdx].transform.position;
    }

    public LaneDirection GetLaneDirection(int laneIdx)
    {
        if(laneIdx >= this.LaneCount)
        {
            BumperCopsDebug.LogError("Lane idx is greater than lane count");
            return LaneDirection.NONE;
        }
        
        BumperCopsDebug.Log($"Looking for idx {laneIdx}");
        return this.allLanes[laneIdx].LaneDirection;
    }

    public LaneController GetEdgeLaneClosestToMe(LaneController lane)
    {
        if(this.myType == RoadManager.RoadType.SINGLE)
        {
            return lane.Id < allLanes.Count / 2 ? allLanes[0] : allLanes[allLanes.Count - 1];
        }
        else if(lane.LaneDirection == LaneDirection.OPPOSITE)
        {
            return allLanes[0];
        }
        else
        {
            return allLanes[allLanes.Count - 1];
        }
    }

    public virtual int GetRandomLaneId(bool allLaneSelectionEqual)
    {
        if(this.RoadType == RoadManager.RoadType.SINGLE || this.oppositeDirectionLanes.Count <= 0 || allLaneSelectionEqual)
        {
            return UnityEngine.Random.Range(0, this.LaneCount);
        }
        
        // for double lanes if lane selection is unequal as determined by obstaclemanager
        // we give more chance for opposite direction to be selected.
        if(UnityEngine.Random.value <= 0.5f)
        {
            // select from opposite lane
            return UnityEngine.Random.Range(0, this.oppositeDirectionLanes.Count);
        }
        
        return UnityEngine.Random.Range(0, this.sameDirectionLanes.Count);
    }

    public Tuple<int, int> GetLaneIdsInDirection(LaneDirection direction)
    {
        if(direction == LaneDirection.SAME)
        {
            return new Tuple<int, int>(sameDirectionLanes[0].Id, sameDirectionLanes[sameDirectionLanes.Count - 1].Id);
        } else 
        {
            return new Tuple<int, int>(oppositeDirectionLanes[0].Id, oppositeDirectionLanes[oppositeDirectionLanes.Count - 1].Id);
        }
    }

    public int GetRandomLaneIdInDirection(LaneDirection laneDirection)
    {
        int idx;
        switch(laneDirection)
        {
            case LaneDirection.SAME:
                if(this.sameDirectionLanes.Count == 0)
                {
                    BumperCopsDebug.LogWarning("No same direction lane");
                    return -1;
                }

                idx = UnityEngine.Random.Range(0, this.sameDirectionLanes.Count);
                return this.sameDirectionLanes[idx].Id;

            case LaneDirection.OPPOSITE:
                if(this.oppositeDirectionLanes.Count == 0)
                {
                    BumperCopsDebug.LogWarning("No same direction lane");
                    return -1;
                }

                idx = UnityEngine.Random.Range(0, this.oppositeDirectionLanes.Count);
                return this.oppositeDirectionLanes[idx].Id;

            default:
                BumperCopsDebug.LogError("Lane direction not supported");
                return -1;
        }
    }

    public void Init(
        long pathNumber, 
        RoadManager roadManager, 
        GameObject leftEnvironment,
        GameObject rightEnvironment,
        bool moveRoad = false)
    {
        this.MyPathNumber = pathNumber;
        this.moveRoad = moveRoad;
        this.roadManager = roadManager;
        this.LeftEnvironment = leftEnvironment;
        this.RightEnvironment = rightEnvironment;

        AdjustEnvironment(leftEnvironment, leftEnvironmentModel);
        AdjustEnvironment(rightEnvironment, rightEnvironmentModel);
    }

    private void AdjustEnvironment(GameObject environment, GameObject referenceModel)
    {
        environment.transform.parent = this.transform;
        environment.transform.localPosition = referenceModel.transform.localPosition;
        environment.transform.localRotation = referenceModel.transform.localRotation;
        environment.SetActive(true);
    }
}
