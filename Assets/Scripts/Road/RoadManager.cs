﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using AmazingAssets.CurvedWorld;

public class RoadManager : MonoBehaviour
{
    public event Action<RoadtypeChangedEventArgs> RoadTypeChanged;

    [Header("Game elements")]
    [SerializeField]
    private PlayerController player;
    
    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private GameManager gameManager;

    [Header("UI elements")]
    [SerializeField]
    private BonusBarBehaviour bonusBarBehaviour;

    [Header("Road Canvas")]
    [SerializeField]
    private GameObject roadCanvasPrefab;
    private RoadCanvas roadCanvas;
    private bool highScoreDisplayedAlready;

    private RoadGenerator roadGenerator;
    private EnemyController enemy;
    private int initialPathNumber = 9;
    private Queue<RoadController> pathQueue = new Queue<RoadController>();
    private Dictionary<long, RoadController> activeRoadDictionary = new Dictionary<long, RoadController>();
    private long playersPreviousPathNumber = -2;
    private int currentPathNumber = 1;
    private Vector3 nextPathPosition;
    public RoadController lastRoad { get; private set; }

    public int WorldSpeed { get; private set; }
    // Use this for initialization
    private bool isGameStarted;
    private RoadLaneAndType roadLaneAndType;
    private GameLevelManager gameLevelManager;
    private float roadSwitchTimer = 0;
    private float roadSwitchTimeLimit = 0;
    private bool roadTypeIncreasing = true;
    private bool isTutorialActive = false;

    // TODO
    private void Awake() 
    {
        roadLaneAndType = new RoadLaneAndType();
        this.gameManager.GameInitialized += OnGameTitlePage;
        this.gameManager.StartPlaying += OnGameSessionInitialized;    
        this.gameManager.GameTutorialSessionInitialized += (args) => OnGameSessionInitialized();   
        this.roadGenerator = this.GetComponent<RoadGenerator>();
    }

    void Start()
    {
        nextPathPosition = Vector3.zero;
        
        for (int i = currentPathNumber; i < initialPathNumber; i++)
        {
            CreateNewPath();
        }

        GameObject go = Instantiate(roadCanvasPrefab);
        go.name = "roadCanvas";
        roadCanvas = go.GetComponent<RoadCanvas>() ?? throw new MissingReferenceException(nameof(RoadCanvas));
  
        enemyManager.EnemySpawned += (args) => 
        {
            enemy = args.enemy;
        };

        enemyManager.EnemyHealthOver += OnEnemyHealthOver;

        this.player.AttackController.PlayerEnteredAttackZone += () =>
        {
            // increase the world speed
            this.WorldSpeed = Constants.WORLD_SPEED + 2;
        };

        this.player.AttackController.PlayerExitAttackZone += () =>
        {
            // increase the world speed
            this.WorldSpeed = Constants.WORLD_SPEED;
        };

        this.player.healthController.HealthOver += () => this.WorldSpeed = 0;

        this.bonusBarBehaviour.MegaSirenTurnedOn += () => roadLaneAndType.CanSwitchRoadType = false;
        this.bonusBarBehaviour.MegaSirenTurnedOff += () => roadLaneAndType.CanSwitchRoadType = true;
        this.gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
        this.gameManager.TutorialManager.TutorialEnd += OnTutorialEnded;
        this.gameManager.ScoreBehaviour.ScoreUpdated += OnScoreUpdated;    

        this.gameLevelManager = this.gameManager.GameLevelManager;
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        roadLaneAndType.CanSwitchRoadType = false;
        isTutorialActive = true;
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        roadLaneAndType.CanSwitchRoadType = true;
        isTutorialActive = false;
    }

    private void OnScoreUpdated(ScoreUpdatedEventArgs args)
    {
        if(!args.IsHighScore || highScoreDisplayedAlready || isTutorialActive)
        {
            return;
        }

        RoadController roadForHighScorer = GetRoadForEnemy(Constants.SEC_DISTANCE_BETWEEN_ENEMY_PLAYER_SPAWN);
        roadCanvas.Show(roadForHighScorer.transform, this.player);
        highScoreDisplayedAlready = true;
    }

    private void OnGameTitlePage()
    {
        this.WorldSpeed = Constants.WORLD_SPEED * 2;
    }

    private void OnGameSessionInitialized()
    {
        isGameStarted = true;
        this.WorldSpeed = Constants.WORLD_SPEED;
        roadSwitchTimeLimit = UnityEngine.Random.Range(20, 30);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        long enemyPath = this.enemy != null && this.enemy.movementController.isActive ? this.enemy.movementController.PathNumber : long.MinValue;
        float pathToLookForwardFrom  = Mathf.Max(enemyPath, this.player.MoveController.PathNumber);

        if(this.player.MoveController.PathNumber != -1)
        {
            playersPreviousPathNumber = this.player.MoveController.PathNumber;
        }

        if (this.currentPathNumber - pathToLookForwardFrom < Constants.ROAD_AHEAD_PLAYER_OR_NPC)
        {
            //BumperCopsDebug.Log($"RoadManager: currentPathNumber {currentPathNumber}, pathToLookForwardFrom: {pathToLookForwardFrom}");
            // rehydrate the pool first            
            while(this.pathQueue.Peek().MyPathNumber < this.player.MoveController.PathNumber - 2)
            {
                RoadController rc = pathQueue.Dequeue();
                roadGenerator.RecycleRoad(rc);
                activeRoadDictionary.Remove(rc.MyPathNumber);
            }

            //BumperCopsDebug.Log($"currentPathNumber: {this.currentPathNumber}, pathToLookForwardFrom: {pathToLookForwardFrom}, diff: {this.currentPathNumber - pathToLookForwardFrom}");
            for(float i = pathToLookForwardFrom; i <= pathToLookForwardFrom + Constants.ROAD_AHEAD_PLAYER_OR_NPC; i++)
            {
                CreateNewPath();
            }
        }

        //SwitchRoadLanes();
        if(this.gameLevelManager.CurrentRecommendedLevel >= GameLevel.MEDIUM)
        {
            SwitchRoadType();    
        }
    }   

    private void OnEnemyHealthOver(EnemyArgs e)
    {
        if(roadLaneAndType.CurrentRoadType == RoadType.FORK)
        {
            // max the timer so we switch to non fork
            roadSwitchTimer = roadSwitchTimeLimit;
            SwitchRoadType();
            this.enemy = null;
        }
    }

    private void SwitchRoadLanes(bool forceChange = false)
    {
        if(this.gameManager.GameLevelManager.CurrentRecommendedLevel < GameLevel.HARD && !forceChange)
        {
            return;
        }

        // create an object with different bools.
            // - current lane number
            // - did lane number change
            // - current and previous lane number
            // - merge lane gneerated

        // when to change the lane
        // Difficulty medium - 3 lanes
        // diffculty insane - 2 lanes ( not sure )

        // change the lane only 3 right now
        if(this.gameManager.GameLevelManager.CurrentRecommendedLevel == GameLevel.MEDIUM && 
            roadLaneAndType.CurrentLaneCount > 3)
        {
            roadLaneAndType.CurrentLaneCount = 3;
            roadLaneAndType.CurrentRoadType = RoadType.MERGE;
        }

        BumperCopsDebug.Log($"New lane count is {roadLaneAndType.CurrentLaneCount}");
    }

    private void SwitchRoadType()
    {
        if((!roadLaneAndType.CanSwitchRoadType || !isGameStarted))
        {
            return;
        }

        roadSwitchTimer += Time.deltaTime;

        // after merge lane start always single - for now
        if(roadLaneAndType.CurrentRoadType == RoadType.MERGE && roadLaneAndType.MergeLaneGenerated)
        {
            BumperCopsDebug.Log("Switching from merge to single");
            roadLaneAndType.CurrentRoadType = RoadType.SINGLE;
            return;
        }
        
        // road changing logic
        // single -> double -> opposite
        // fork and merge are special
        // if lane change has occurred, enter merge road first.
        // then normal road continues
        // fork road is only for limited time. If the user misses the fork road, we want to punish them similar to the time penalty
        if(roadSwitchTimer >= roadSwitchTimeLimit || 
            (roadLaneAndType.CurrentRoadType == RoadType.FORK && 
            roadSwitchTimer >= TimerBehaviour.TIME_PENALTY_HIT))
        {
            roadSwitchTimer = 0;
            RoadType possibleRoadType = roadLaneAndType.CurrentRoadType == RoadType.FORK ? 
                roadLaneAndType.PreviousRoadType : 
                roadLaneAndType.CurrentRoadType;
                
            if(roadLaneAndType.CurrentRoadType == RoadType.SINGLE || 
                roadLaneAndType.CurrentRoadType == RoadType.DOUBLE)
            {
                //BumperCopsDebug.Log("Switching road type");
                possibleRoadType = RoadType.FORK;
            }
            else
            {
                ++possibleRoadType;
                if(possibleRoadType > RoadType.OPPOSITE)
                {
                    possibleRoadType = RoadType.SINGLE;
                }

                //BumperCopsDebug.Log($"Road type now is {roadLaneAndType.CurrentRoadType}");
                roadTypeIncreasing = !roadTypeIncreasing;
            }
            
            if(!this.roadGenerator.HasRoadObject(possibleRoadType, roadLaneAndType.CurrentLaneCount))
            {
                BumperCopsDebug.LogWarning($"Cannot find road of type {possibleRoadType} and lane count {roadLaneAndType.CurrentLaneCount}");
                return;
            }

            BumperCopsDebug.Log($"Switching to {possibleRoadType} and lane count {roadLaneAndType.CurrentLaneCount}");
            roadLaneAndType.CurrentRoadType = possibleRoadType;
            RoadTypeChanged?.Invoke(
                new RoadtypeChangedEventArgs(
                    roadLaneAndType.PreviousRoadType, 
                    roadLaneAndType.CurrentRoadType, 
                    this.lastRoad.GetComponent<RoadController>()));
        }
    }

    private void CreateNewPath()
    {        
        Vector3 myPosition = Vector3.zero;
        GameObject thePath = roadGenerator.GetRoad(roadLaneAndType.CurrentRoadType, 
            roadLaneAndType.CurrentLaneCount);

        if(roadLaneAndType.CurrentRoadType == RoadType.MERGE)
        {
            roadLaneAndType.MergeLaneGenerated = true;
            SwitchRoadType();
        }

        //BumperCopsDebug.Log($"Path queue size {pathQueue.Count}");

        if(pathQueue.Any())
        {
            Vector3 lastPathPosition = pathQueue.Last().transform.position;
            myPosition = new Vector3(this.lastRoad.transform.position.x, this.lastRoad.transform.position.y, this.lastRoad.transform.position.z + this.lastRoad.LocaLEnd.z);
            thePath.transform.position = myPosition;
        } else 
        {
            thePath.transform.parent = this.transform;
            thePath.transform.localPosition = myPosition;
        }

        // assign variables
        thePath.name = $"{currentPathNumber}";
        RoadController road = thePath.GetComponent<RoadController>();
        road.Init(
            currentPathNumber++, 
            this, 
            roadGenerator.GetEnvironment(), 
            roadGenerator.GetEnvironment(), 
            moveRoad: true);

        // set active
        thePath.SetActive(true);
        pathQueue.Enqueue(road);
        activeRoadDictionary.Add(road.MyPathNumber, road);

        this.lastRoad = road;
    }

    public RoadController GetRoadForEnemy(float distanceBetweenEnemy)
    {
        // get road that is N seconds away from player
        float zDistance = this.player.SpeedController.MaxSpeed * 
            distanceBetweenEnemy;
            
        RoadController requiredRoadController;
        for(long i = this.playersPreviousPathNumber; i < currentPathNumber; i++)
        {
            // find the roads end that is less than required z
            if(this.activeRoadDictionary.TryGetValue(i, out requiredRoadController) && 
                (requiredRoadController.transform.position.z - this.player.RigidBody.transform.position.z >= zDistance))
            {
                //BumperCopsDebug.Log($"RoadManager: returning road number ${requiredRoadController.MyPathNumber} and player road is {this.playersPreviousPathNumber}");
                return requiredRoadController;
            }
        }

        // or default to road that is 2 roads away from the player
        BumperCopsDebug.Log("Getting default road");
        return GetRoad(2, 2);
    }

    public RoadController GetRoadForObstacle(int minRoadDistance, int maxRoadDistance)
    {
        return GetRoad(minRoadDistance, minRoadDistance);
    }

    public RoadController GetRoadForBonuses()
    {
        RoadController road = GetRoad(Constants.ROAD_DISTANCE_BETWEEN_BONUS_PLAYER_SPAWN - 1, Constants.ROAD_DISTANCE_BETWEEN_BONUS_PLAYER_SPAWN);
        RoadController nextRoad;
        for(long i = road.MyPathNumber; i < road.MyPathNumber + Constants.BONUS_ROAD_COUNT_TO_REMOVE_OBSTACLES; i++)
        {
            if(!this.activeRoadDictionary.TryGetValue(this.playersPreviousPathNumber + i, out nextRoad))
            {
                continue;
            }
        }

        return road;
    }

    public RoadController GetRoadByNumber(long roadNumber)
    {
        RoadController road;
        activeRoadDictionary.TryGetValue(roadNumber, out road);
        return road;
    }
    
    private RoadController GetRoad(int minRoadDistanceBetweenPlayer, int maxRoadDistanceBetweenPlayer)
    {
        RoadController lastRoadController = this.lastRoad.GetComponent<RoadController>();
        if(this.playersPreviousPathNumber < 0)
        {
            //BumperCopsDebug.Log("RoadManager: Road number for obstacle is " + lastRoadController.MyPathNumber);
            return lastRoadController;
        }

        RoadController road;
        for(int i = maxRoadDistanceBetweenPlayer; i >= minRoadDistanceBetweenPlayer; i--)
        {
            if(this.activeRoadDictionary.TryGetValue(this.playersPreviousPathNumber + i, out road))
            {
                //BumperCopsDebug.Log($"RoadManager: Road number for obstacle is {road.MyPathNumber} and player is {this.playersPreviousPathNumber}");
                return road;
            }
        }

        BumperCopsDebug.LogWarning("RoadManager: Player road not found");
        return lastRoadController;
    }

    public enum RoadType
    {
        SINGLE,
        DOUBLE,
        OPPOSITE,
        FORK,
        MERGE
    }

    private class RoadLaneAndType
    {
        private RoadType currentRoadType;
        public RoadType CurrentRoadType 
        {
            get { return currentRoadType; } 
            set
            {
                PreviousRoadType = currentRoadType;
                currentRoadType = value;
            }
        }

        public RoadType PreviousRoadType { get; private set; }

        private int currentLaneCount;
        public int CurrentLaneCount 
        {
            get { return currentLaneCount; }
            set
            {
                PreviousLaneCount = currentLaneCount;
                currentLaneCount = value;
                MergeLaneGenerated = false;
            }
        }
        public int PreviousLaneCount { get; private set; }
        public bool MergeLaneGenerated;
        public bool CanSwitchRoadType = true;

        public RoadLaneAndType()
        {
            this.currentLaneCount = 4;
        }
    }
}
