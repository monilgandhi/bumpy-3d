using UnityEngine;

public class GamePlayCanvasManager : MonoBehaviour 
{
    [Header("Game play elements")]
    [SerializeField]
    private GameManager gameManager;

    [Header("Interactive Canvas")]
    [SerializeField]
    private Canvas GamePlayInteractiveCanvas;

    [SerializeField]
    private bool shouldInteractiveHideOnPause;
    
    [Header("Non Interactive Canvas")]
    [SerializeField]
    private Canvas GamePlayNonInteractiveCanvas;
    [SerializeField]
    private bool shouldNonInteractiveHideOnPause;
    private bool enableOnActive;
    private void Awake() 
    {
        Hide();
    }

    private void Start() 
    {
        if(gameManager != null)
        {
            RegisterEvents();
        }
    }

    public void Init(GameManager gameManager)
    {
        this.gameManager = gameManager;
        RegisterEvents();
        Show();
    }

    private void RegisterEvents()
    {
        this.gameManager.GameStarted += OnGameStart;

        this.gameManager.GamePaused += OnGamePaused;
        this.gameManager.GameOver += (args) => Hide();
    }

    private void OnGameStart(GameEventArgs args)
    {
        Show();
    }

    private void OnGamePaused()
    {
        ToggleCanvas(this.GamePlayInteractiveCanvas, !this.shouldInteractiveHideOnPause);
        ToggleCanvas(this.GamePlayNonInteractiveCanvas, !this.shouldNonInteractiveHideOnPause);
    }

    private void Hide()
    {
        ToggleCanvas(this.GamePlayInteractiveCanvas, false);
        ToggleCanvas(this.GamePlayNonInteractiveCanvas, false);
    }
    
    private void Show()
    {
        ToggleCanvas(this.GamePlayInteractiveCanvas, true);
        ToggleCanvas(this.GamePlayNonInteractiveCanvas, true);
    }

    private void ToggleCanvas(Canvas canvas, bool enable)
    {
        if(canvas != null)
        {
            canvas.enabled = enable;
        }
    }
}