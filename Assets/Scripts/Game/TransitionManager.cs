using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TransitionManager : MonoBehaviour 
{
    public event Action TransitionComplete;
    public event Action TransitionStarted;
    public event Action GameStartTransitionComplete;
    
    [SerializeField]
    private Image transitionPanel;    

    [SerializeField]
    private RatingDialog ratingDialog;

    [SerializeField]
    private ScoreAndRewardTransitionBehaviour scoreAndRewardTransitionBehaviour;

    [SerializeField]
    private TitleManager titleManager;

    private GameManager gameManager;
    private StartTransitionState startTransitionState;
    public EndTransitionState GameOverTransitionState { get; private set; }
    private void Awake() 
    {
        gameManager = this.GetComponent<GameManager>();
        this.gameManager.GameSessionInitialized += (args) => StartGameInitialTransition();
        this.gameManager.GameTutorialSessionInitialized += (args) => StartGameInitialTransition(true);
        this.gameManager.GameOverTransitions += () => 
        {
            GameOverTransitionState = EndTransitionState.NONE;
            this.scoreAndRewardTransitionBehaviour.EndTransitionOver += SwitchEndState;
            TransitionStarted?.Invoke();
            SwitchEndState();
        };
    }
    
    private IEnumerator onPowerupCollectedTransition()
    {
        this.gameManager.Pause(false);
        bool transitionComplete = false;
        Action handler = () =>
        {
            //BumperCopsDebug.Log("Gamemanager: Transition complete");
            transitionComplete = true;
        };

        this.TransitionComplete += handler;

        StartCoroutine(FadeInTransitionPanel());
        yield return new WaitUntil(() => transitionComplete);
        StartCoroutine(FadeoutTransitionPanel());
        this.TransitionComplete -= handler;
        this.gameManager.ContinueGame(false);
    }

    private void SwitchEndState()
    {
        ++GameOverTransitionState;
        switch(GameOverTransitionState)
        {
            case EndTransitionState.SCORE_REWARD:
                BumperCopsDebug.Log("Showing score and reward");
                this.scoreAndRewardTransitionBehaviour.Show();
                break;

            case EndTransitionState.SHOP:
                BumperCopsDebug.Log("Moving to shop");
                this.gameManager.InGameShopManager.DisplayShop();
                break;
            
            case EndTransitionState.END:
                BumperCopsDebug.Log("End transition");
                TransitionComplete?.Invoke();
                this.scoreAndRewardTransitionBehaviour.EndTransitionOver -= SwitchEndState;
                break;
        }
    }

    public void OnContinueClicked()
    {
        if(this.GameOverTransitionState == EndTransitionState.SHOP)
        {
            // call shop to close
            SwitchEndState();
        }
    }
    private void ClearForPlaying()
    {
        if(GameStartTransitionComplete != null)
        {
            BumperCopsDebug.Log("Transition complete");
            GameStartTransitionComplete();
        }
    }

    private void StartGameInitialTransition(bool isTutorial = false)
    {   
        if(isTutorial)
        {
            GameStartTransitionComplete?.Invoke();
        }
        else
        {
            switch(startTransitionState)
            {
                case StartTransitionState.NONE:
                    ++startTransitionState;
                    StartGameInitialTransition();
                    break;

                case StartTransitionState.MISSION_DIALOGUE:
                    this.scoreAndRewardTransitionBehaviour.ShowMissionExpanded();
                    ++startTransitionState;
                    this.scoreAndRewardTransitionBehaviour.ScoreRewardCanvasClosed += OnScoreCanvasClosed;
                    break;

                case StartTransitionState.END:
                    GameStartTransitionComplete?.Invoke();
                    startTransitionState = StartTransitionState.NONE;
                    break;
            }
        }
    }

    private void OnScoreCanvasClosed()
    {
        this.scoreAndRewardTransitionBehaviour.ScoreRewardCanvasClosed -= OnScoreCanvasClosed;
        StartGameInitialTransition(false);
    }

    public IEnumerator FadeoutTransitionPanel()
    {
        //BumperCopsDebug.Log("TransitionManager: Fading out");
        TransitionStarted?.Invoke();
        this.transitionPanel.gameObject.SetActive(true);

        float alpha = 1;
        while(alpha > 0)
        {
            alpha -= (Time.unscaledDeltaTime * 1f) ;
            transitionPanel.color = new Color(transitionPanel.color.r, transitionPanel.color.g, transitionPanel.color.b, alpha);
            yield return new WaitForSecondsRealtime(0.01f);
        }

        TransitionComplete?.Invoke();
        //BumperCopsDebug.Log("TransitionManager: Fading out complete");
        this.transitionPanel.gameObject.SetActive(false);
    }

    public IEnumerator FadeInTransitionPanel()
    {
        //BumperCopsDebug.Log("TransitionManager: Fade in started");
        TransitionStarted?.Invoke();
        this.transitionPanel.gameObject.SetActive(true);
        float alpha = 0;
        while(alpha < 1)
        {
            alpha += (Time.unscaledDeltaTime * 2);
            transitionPanel.color = new Color(transitionPanel.color.r, transitionPanel.color.g, transitionPanel.color.b, alpha);
            yield return new WaitForSecondsRealtime(0.01f);
        }

        TransitionComplete?.Invoke();
    }

    public enum EndTransitionState
    {
        NONE,
        SCORE_REWARD,
        SHOP,
        END
    }

    public enum StartTransitionState
    {
        NONE,
        MISSION_DIALOGUE,
        END
    }
}