using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using BumperCops.Models;
using TMPro;

public class ScoreAndRewardTransitionBehaviour : MonoBehaviour 
{
    public event Action ScoreRewardCanvasOpened;
    public event Action ScoreRewardCanvasClosed;
    public event Action EndTransitionOver;
    public event Action<RewardEventArgs> RewardGiven;

    [Header("Game elements")]
    [SerializeField]
    private ScoreBehaviour scoreBehaviour;
    [SerializeField]
    private MissionAndScorePanelBehaviour scoreMenuBehaviourV4;

    [SerializeField]
    private RewardManager rewardManager;

    [SerializeField]
    private DailyRewardManager dailRewardManager;

    [SerializeField]
    private LeaderboardBehaviour leaderboardBehaviour;

    [SerializeField]
    private Camera rewardUICamera;

    [SerializeField]
    private HighScorePanelBehaviour highScorePanelBehaviour;

    [SerializeField]
    private ChallengeMainPageBehaviours challengeMainPageBehaviours;

    [Header("UI")]
    [SerializeField]
    private Button continueButton;
    
    private TransitionStep currentTransitionStep;
    private AnimationCompletedBehaviour animationCompletedBehaviour;
    private long currentScore;
    private bool transitionInProgress;
    private long coinsEarned;

    private void Awake() 
    {
        coinsEarned = 0;
        currentTransitionStep = TransitionStep.NONE;
        this.continueButton.onClick.AddListener(OnContinueClicked);
        animationCompletedBehaviour = this.GetComponent<AnimationCompletedBehaviour>();
        animationCompletedBehaviour.AnimationOver += OnAnimationOver;
        scoreBehaviour.ScoreFinalized += (args) => 
        {
            currentScore = args.NewScore;
            coinsEarned = args.CoinsCollected;
        };

        scoreMenuBehaviourV4.RewardGiven += OnRewardGiven;
        rewardManager.RewardGiven += OnRewardGiven;
        dailRewardManager.DailyRewardGiven += OnRewardGiven;
    }

    private void Start()
    {
        Disable();
    }

    private void Disable()
    {
        rewardUICamera.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void OnRewardGiven(RewardEventArgs args)
    {
        if(args == null || args.Rewards == null)
        {
            BumperCopsDebug.LogWarning("Arguments for reward given are null");
            return;
        }

        foreach(Reward r in args.Rewards)
        {
            if(r.Amount == 0)
            {
                BumperCopsDebug.LogWarning("Amount is 0");
                continue;
            }

            if(r.Type.Equals(CollectibleType.BAG_COINS))
            {
                coinsEarned += r.Amount;
            }

            PlayerStats.Instance.CollectibleInventory.AddCollectible(r.Type, r.Amount);
        }
        
        RewardGiven?.Invoke(args);
    }

    private void OnAnimationOver(AnimationCompleteEventArgs args)
    {
        BumperCopsDebug.Log($"Animation complete with trigger {args.Key}");
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Args of animation completed event is null");
            return;
        }

        if(!string.IsNullOrEmpty(args.Key) && 
            args.Key.Equals("hide"))
        {
            Disable();

            if(transitionInProgress)
            {
                EndTransitionOver?.Invoke();
                transitionInProgress = false;
            }

            ScoreRewardCanvasClosed?.Invoke();
        }
    }

    private void OnContinueClicked()
    {
        if(!transitionInProgress)
        {
            // for now we are assuming that only mission is active
            if(this.scoreMenuBehaviourV4.TreasurechestBehaviour.IsTreasurechestActive)
            {
                this.scoreMenuBehaviourV4.TreasurechestBehaviour.OnTreasureChestClicked();
                return;
            }

            this.scoreMenuBehaviourV4.Hide();
            Hide();
        }
        else
        {
            BumperCopsDebug.Log("Continue clicked");
            switch(currentTransitionStep)
            {
                case TransitionStep.REWARD:
                    if(this.rewardManager.TreasureChest.IsTreasurechestActive)
                    {
                        BumperCopsDebug.Log("Opening treasure chest");
                        this.rewardManager.TreasureChest.OnTreasureChestClicked();
                        break;
                    }
                    
                    BumperCopsDebug.Log("treasure chest openend");
                    StepOverToNextTransition(false);
                    break;

                case TransitionStep.HIGHSCORE:
                    StartCoroutine(HideHighScoreAndSwitchState());
                    break;

                case TransitionStep.SCORE:
                    if(this.scoreMenuBehaviourV4.TreasurechestBehaviour.IsTreasurechestActive)
                    {
                        BumperCopsDebug.Log("opening mission chest");
                        this.scoreMenuBehaviourV4.TreasurechestBehaviour.OnTreasureChestClicked();
                        break;
                    }

                    BumperCopsDebug.Log("mission chest compelte");

                    StepOverToNextTransition(false);
                    break;

                case TransitionStep.DAILY_REWARD:
                    if(this.dailRewardManager.IsRewardClaimed)
                    {
                        StepOverToNextTransition(false);
                        break;
                    }
                    
                    this.dailRewardManager.OnRewardClaimed();
                    break;

                case TransitionStep.CHALLENGE:
                    Hide();
                    break;

                case TransitionStep.LEADERBOARD:
                    StepOverToNextTransition(false);
                    break;
            }
        }
    }

    private IEnumerator HideHighScoreAndSwitchState()
    {
        StartCoroutine(this.highScorePanelBehaviour.Hide());
        yield return new WaitWhile(() => this.highScorePanelBehaviour.gameObject.activeSelf);
        StepOverToNextTransition(false);
    }

    public void ShowMissionExpanded()
    {
        Enable();

        // disable leaderboard panel
        this.leaderboardBehaviour.Hide();
        this.scoreMenuBehaviourV4.DisplayMissionPanelExpanded();
    }

    public void GiveVideoReward()
    {
        Enable();

        // disable leaderboard panel
        this.leaderboardBehaviour.Hide();
        this.rewardManager.GiveRewardsForVideoAd();
    }

    public void Show()
    {
        BumperCopsDebug.Log("Showing the score and reward transition");
        currentTransitionStep = TransitionStep.NONE;
        Enable();
        transitionInProgress = true;
        StepOverToNextTransition(true);
    }

    private void Enable()
    {
        BumperCopsDebug.Log("Enabling the score and reward transition");
        rewardUICamera.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
        this.gameObject.SetActive(true);

        // hide all the panels
        this.rewardManager.Hide();
        this.scoreMenuBehaviourV4.Hide();
        this.leaderboardBehaviour.Hide();
        this.dailRewardManager.Hide();
        this.challengeMainPageBehaviours.Hide();

        ScoreRewardCanvasOpened?.Invoke();
    }

    private void Hide()
    {
        this.animationCompletedBehaviour.StartAnimationWithTrigger("hide", false, false);
    }

    private void StepOverToNextTransition(bool isFirstStep)
    {
        ++currentTransitionStep;
        if(!isFirstStep)
        {
            this.animationCompletedBehaviour.StartAnimationWithTrigger("flip", false, false);
        }

        switch(currentTransitionStep)
        {
            case TransitionStep.HIGHSCORE:
                BumperCopsDebug.Log("Starting highscore");
                if(!this.highScorePanelBehaviour.ShowHighScore())
                {
                    StepOverToNextTransition(isFirstStep);
                }
                break;
    
            case TransitionStep.REWARD:
                BumperCopsDebug.Log("Starting reward");
                bool success = this.rewardManager.GiveGameOverRewardWhenDue();
                if(!success)
                {
                    StepOverToNextTransition(isFirstStep);
                }
                break;

            case TransitionStep.DAILY_REWARD:
                this.rewardManager.Hide();
                BumperCopsDebug.Log("Starting daily reward");
                if(!this.dailRewardManager.GiveDailyRewardIfDue())
                {
                    StepOverToNextTransition(isFirstStep);
                }
                break;
                
            case TransitionStep.SCORE:
                this.dailRewardManager.Hide();
                BumperCopsDebug.Log("Starting score");
                this.scoreMenuBehaviourV4.Display(this.currentScore, coinsEarned);
                break;

            case TransitionStep.CHALLENGE:
                this.scoreMenuBehaviourV4.Hide();
                BumperCopsDebug.Log("Starting challenge");
                if(!this.challengeMainPageBehaviours.ShowEndScreen(this.currentScore))
                {
                    StepOverToNextTransition(isFirstStep);
                }

                break;

            case TransitionStep.LEADERBOARD:
                this.scoreMenuBehaviourV4.Hide();
                BumperCopsDebug.Log("Starting leaderboard");
                this.leaderboardBehaviour.OnLeaderboardOpened();
                break;
            
            case TransitionStep.DONE:
                BumperCopsDebug.Log("All completed");
                Hide();
                break;    
        }
    }

    private enum TransitionStep
    {
        NONE,
        HIGHSCORE,
        REWARD,
        DAILY_REWARD,
        SCORE,
        CHALLENGE,
        LEADERBOARD,
        DONE
    }
}