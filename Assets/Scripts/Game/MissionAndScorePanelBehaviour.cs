using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using BumperCops.Models;
public class MissionAndScorePanelBehaviour : MonoBehaviour 
{
    public event Action<RewardEventArgs> RewardGiven;
    public event Action<MissionSkippedEventArgs> MissionSkipped;

    [Header("Game Components")]
    [SerializeField]
    private GameplayAssignmentManager assignmentManager;

    [SerializeField]
    private RewardManager rewardManager;

    [Header("Score UI")]
    [SerializeField]
    private TMP_Text scoreText;

    [Header("Coins UI")]
    [SerializeField]
    private TMP_Text coinText;

    [Header("Missions UI")]
    [SerializeField]
    private TMP_Text missionTitle;
    [SerializeField]
    private TreasurechestBehaviour treasurechestBehaviour;

    [SerializeField]
    private TMP_Text fillPercentage;

    [SerializeField]
    private List<MissionPanelBehaviour> missionPanels;

    [SerializeField]
    private TMP_Text comboNumberTxt;

    [Header("Expand mission")]
    [SerializeField]
    private Button expandButton;

    [SerializeField]
    private Image treasureChestEmptyImage;

    [SerializeField]
    private GameObject expandedMission;

    [Header("Rewards")]
    [SerializeField]
    private RewardContainerBehaviour rewardContainer;

    [Header("New missions")]
    [SerializeField]
    private GameObject comingSoonMissionPanel;

    private Assignment currentAssignment;
    private long coinsEarned;

    public TreasurechestBehaviour TreasurechestBehaviour { get { return treasurechestBehaviour; }}
    private AnimationCompletedBehaviour animationCompletedBehaviour;
    private void Awake() 
    {  
        animationCompletedBehaviour = this.GetComponent<AnimationCompletedBehaviour>();
        animationCompletedBehaviour.AnimationOver += OnAnimationOver;
        expandButton.onClick.AddListener(delegate { OnExpandClicked(); });
        expandedMission.gameObject.SetActive(false);
        comingSoonMissionPanel.gameObject.SetActive(false);

        foreach(MissionPanelBehaviour mpb in missionPanels)
        {
            mpb.ObjectiveSkipped += OnObjectiveSkipped;
        }
    }

    private void OnObjectiveSkipped(string objectiveId)
    {
        BumperCopsDebug.Log($"Skipping objective id {objectiveId}");
        MissionSkipped?.Invoke(new MissionSkippedEventArgs(objectiveId, currentAssignment.Id));
    }

    private void OnAnimationOver(AnimationCompleteEventArgs args)
    {
        if(args == null || args.Key == null)
        {
            return;
        }

        if(args.Key.Equals("expand"))
        {
            expandedMission.gameObject.SetActive(true);
        } else if(args.Key.Equals("collapse"))
        {
            expandedMission.gameObject.SetActive(false);
        }
    }

    public void Hide()
    {
        // reset
        this.expandButton.gameObject.SetActive(true);
        
        this.rewardContainer.HideRewards();
        this.gameObject.SetActive(false);
        if(this.expandedMission.activeSelf)
        {
            OnExpandClicked();
        }

        ToggleScorePanel(true);
    }

    public void Display(long currentScore, long coins)
    {
        this.gameObject.SetActive(true);
        scoreText.text = currentScore.ToString("n0");
        this.coinsEarned = coins;
        coinText.text = coinsEarned.ToString("n0");
        SetMissionPanel();
     }

    private void ToggleScorePanel(bool enable)
    {
        this.scoreText.transform.parent.gameObject.SetActive(enable);
    }

     public void DisplayMissionPanelExpanded()
     {
        this.gameObject.SetActive(true);
        if(SetMissionPanel())
        {
            OnExpandClicked();
        }

        // we do not want to show the expand button
        this.expandButton.gameObject.SetActive(false);
        ToggleScorePanel(false);
     }

    private bool SetMissionPanel()
    {
        currentAssignment =  assignmentManager.CurrentAssignment;
        if(currentAssignment == null)
        {
            SetupMoreMissions();
            return false;
        }

        float assignmentCompletedPercentage = LoadObjectivesAndGetCompletionRate(currentAssignment);

        assignmentCompletedPercentage =  assignmentCompletedPercentage / currentAssignment.Objectives.Count;
        if(assignmentCompletedPercentage >= 1)
        {
            treasureChestEmptyImage.enabled = false;
            treasurechestBehaviour.TreasurechestOpened += OnTreasureChestOpened;
        }
        else
        {
            treasureChestEmptyImage.enabled = true;
        }

        treasurechestBehaviour.Show(assignmentCompletedPercentage, assignmentCompletedPercentage >= 1.0f);
        fillPercentage.gameObject.SetActive(true);
        fillPercentage.text = string.Format("{0}%", (assignmentCompletedPercentage * 100).ToString("n0"));
        return true;
    }

    private void SetupMoreMissions()
    {
        comingSoonMissionPanel.gameObject.SetActive(true);
        expandButton.gameObject.SetActive(false);
        treasurechestBehaviour.transform.parent.gameObject.SetActive(false);
        missionTitle.gameObject.SetActive(false);
        comboNumberTxt.transform.parent.gameObject.SetActive(false);
        fillPercentage.gameObject.SetActive(false);

        BumperCopsDebug.Log($"Mission panel parent name {missionPanels[0].transform.parent.name}");
        expandedMission.SetActive(false);
    }

    private int GetComboIncrement(RewardsConfig config)
    {
        foreach(RewardChanceConfig r in config.RewardChance)
        {
            if(r.Type == CollectibleType.COMBO_INCREMENT)
            {
                return r.MaximumAmount;
            }
        }

        return 0;
    }

    private float LoadObjectivesAndGetCompletionRate(Assignment currentAssignment)
    {
        missionTitle.text = GameplayAssignmentManager.GetAssignmentName(currentAssignment.AssignmentNumber);
        comboNumberTxt.text = $"x{PlayerStats.Instance.GetCurrentBaseCombo() + GetComboIncrement(currentAssignment.Reward)}";
        float assignmentCompletedPercentage = 0.0f;
        for(int i = 0; i < currentAssignment.Objectives.Count; i++)
        {
            if(i >= missionPanels.Count)
            {
                BumperCopsDebug.LogWarning("count mismatch with mission panels and objectives");
                break;
            }

            MissionPanelBehaviour m = missionPanels[i];
            AssignmentObjectives o = currentAssignment.Objectives[i];
            BumperCopsDebug.Log($"Player count {o.PlayerCount} required count {o.Count}");
            float completedPercent = (float)o.PlayerCount / (float)o.Count;

             m.Load(o.Description, o.PlayerCount, o.Id, o.Count, o.SkipCost);
            assignmentCompletedPercentage += completedPercent;
            m.gameObject.SetActive(true);
        }

        return assignmentCompletedPercentage;
    }

    private void OnTreasureChestOpened()
    {
        assignmentManager.MarkAssignmentComplete();
        missionTitle.text = "Rewards";
        fillPercentage.gameObject.SetActive(false);
        List<RewardChanceConfig> rewardsChanceConfig = currentAssignment.Reward != null && 
            currentAssignment.Reward.RewardChance != null ?
            currentAssignment.Reward.RewardChance :
            null;

        List<Reward> rewards = rewardManager.GenerateRewardsForMission(rewardsChanceConfig);

        foreach(Reward r in rewards)
        {
            if(r.Type == CollectibleType.BAG_COINS)
            {
                coinsEarned += r.Amount;
            }
        }

        coinText.text = coinsEarned.ToString("n0");

        RewardGiven?.Invoke(new RewardEventArgs(rewards));
        BumperCopsDebug.Log("Displaying rewards");
        rewardContainer.DisplayRewards(rewards, true);
    }

    private void OnExpandClicked(string state = null)
    {
        BumperCopsDebug.Log("expanding mission");
        if(state == null)
        {
            state = !this.expandedMission.gameObject.activeSelf ? "expand" : "collapse";
        }

        animationCompletedBehaviour.StartAnimationWithTrigger(state, false, false);
    }
}

public class MissionSkippedEventArgs
{
    public string ObjectiveId;
    public string AssignmentId;

    public MissionSkippedEventArgs(string objectiveId, string assignmentId)
    {
        this.ObjectiveId = objectiveId;
        this.AssignmentId = assignmentId;
    }
}