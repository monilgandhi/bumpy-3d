﻿using System;
using System.Diagnostics;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(TransitionManager))]
public class GameManager : MonoBehaviour
{
    public event Action<GameOverReason> GameOver;

    public event Action<GameEventArgs> GameStarted;

    /// <summary>
    /// This event is helpful for some systems which need additional time to setup.
    /// For example virutal camera to start paning to player position
    /// </summary>
    public event Action GamePreStart;
    public event Action StartPlaying;
    public event Action GameOverTransitions;
    public event Action GameInitialized;
    public event Action ShopClosed;
    public event Action<GameEventArgs> GameTutorialSessionInitialized;
    public event Action<GameEventArgs> GameSessionInitialized;
    public event Action GamePaused;
    public event Action TitlePageLoaded;

    [Header("Game elements")]
    [SerializeField]
    private PlayerController playerController;
    public PlayerController Player { get { return this.playerController; }}

    [SerializeField]
    private EnemyManager enemyManager;
    public EnemyManager EnemyManager { get { return this.enemyManager; }}

    [SerializeField]
    private BumperCopsGameplayAnalytics analytics;
    public BumperCopsGameplayAnalytics Analytics { get { return analytics; }}
    [SerializeField]
    [Tooltip("Main panel of title page")]
    private TitleManager titleManager;

    [Header("Start UI")]
    [SerializeField]
    [Tooltip("The start game timer that appears  before the game starts")]
    private CanvasGroup startGameTimerCanvasGroup;

    [Header("Game play UI")]
    [SerializeField]
    [Tooltip("Pause menu")]
    private PauseMenuBehaviour pauseMenu;

    [SerializeField]
    [Tooltip("Enemy catching timer")]
    private TimerBehaviour timerBehaviour;
    public TimerBehaviour TimerBehaviour { get { return this.timerBehaviour; }}

    public ScoreBehaviour ScoreBehaviour { get; private set; }

    [SerializeField]
    private TimerExtendBehaviour timerExtendBehaviour;

    [Header("Shop")]
    [SerializeField]
    private InGameShopManager inGameShopManager;
    public InGameShopManager InGameShopManager { get { return inGameShopManager; }}

    public TimerExtendBehaviour TimerExtendBehaviour { get { return timerExtendBehaviour; }}
    public SlowMotionBehaviour SlowMotion { get; private set; }
    private bool isPaused;
    private bool hasInitialized;
    public TransitionManager TransitionManager { get; private set; }
    public CoinCollectibleBehaviour CoinCollectibleManager { get; private set; }
    public TutorialManager TutorialManager { get; private set; }
    public PowerupManager PowerupManager { get; private set; }
    public CoinBonusBehaviour BonusBehaviour { get; private set; }
    public bool IsTutorial { get; private set; }
    private GameState gameState;
    public GameMode GameMode { get; private set; }
    public PrefabManager PrefabManager { get; private set; }
    private long currentSessionNumber;
    public GameLevelManager GameLevelManager { get; private set; }
    private void Awake()
    {
        this.playerController.gameObject.SetActive(false);
        PlayerStats.Instance.LoadCity(Constants.CURRENT_CITY);
        this.playerController.gameObject.SetActive(true);
        Application.targetFrameRate = 60;

        TransitionManager = this.GetComponent<TransitionManager>();
        CoinCollectibleManager = this.GetComponent<CoinCollectibleBehaviour>();
        PowerupManager = this.GetComponentInChildren<PowerupManager>();
        ScoreBehaviour = this.GetComponent<ScoreBehaviour>();
        SlowMotion = this.GetComponentInChildren<SlowMotionBehaviour>();
        TutorialManager = this.GetComponentInChildren<TutorialManager>();
        BonusBehaviour = this.GetComponent<CoinBonusBehaviour>();
        PrefabManager = this.GetComponentInChildren<PrefabManager>();
        GameLevelManager = this.GetComponentInChildren<GameLevelManager>();
        
        RemoteConfig.ReloadConfig(this);
        ShopInventory.Instance.LoadCityInventory(Constants.CURRENT_CITY);

        // load the shaders
        ShaderVariantCollection collection = Resources.Load<ShaderVariantCollection>("shaders/gameplay_outline_curved");
        if ( collection != null )
        {
            collection.WarmUp();
            Resources.UnloadAsset(collection);
        }
    }

    // Use this for initialization
    void Start()
    {
        this.TransitionManager.GameStartTransitionComplete += GameInitialTransitionComplete;
     
        this.TransitionManager.TransitionComplete += OnTransitionComplete;
        this.timerBehaviour.TimeUp += OnTimeUp;
        this.EnemyManager.AllEnemiesDestroyed += OnAllEnemiesDestroyed;
        this.titleManager.ExitTitlePage += StartNewGame;
        this.EnemyManager.EnemySpawned += (args) => 
        {
            args.enemy.EnemyEscaped += OnEnemyEscaped;
        };
        
        this.timerExtendBehaviour.TimeExtended += OnTimeExtended;
        this.timerExtendBehaviour.TimeExtendedDialogueDisplayed += OnTimeExtendedDialogueShowed;
        this.InGameShopManager.ShopClosed += OnContinueClickedFromShop;
    }
    
    private void OnTransitionComplete()
    {
        if(gameState < GameState.OVER)
        {
            return;
        }

        StartCoroutine(TransitionWhiteScreen());
    }
    private void OnContinueClickedFromShop()
    {
        if(this.gameState == GameState.TITLE)
        {
            // play
            ShopClosed?.Invoke();
            gameState = GameState.TITLE;
        }
        else if (this.gameState > GameState.GAME)
        {
            TransitionManager.OnContinueClicked();
        }
    }

    private void OnTimeExtended(TimeExtensionArgs args)
    {
        if(!args.TimeExtended)
        {
            return;
        }

        if(args.Cost > 0)
        {
            PlayerStats.Instance.ReduceCoins(args.Cost);
        }

        BumperCopsDebug.LogFormat("GameManager: Continuing");
        ContinueGame();
    }

    private void OnTimeExtendedDialogueShowed(TimeExtensionArgs args)
    {
        Pause(false);
    }

    private IEnumerator TransitionWhiteScreen()
    {
        bool transitionComplete = false;
        Action handler = () =>
        {
            //BumperCopsDebug.Log("Gamemanager: Transition complete");
            transitionComplete = true;
        };

        this.TransitionManager.TransitionComplete += handler;

        if(gameState == GameState.OVER)
        {
            StartCoroutine(TransitionManager.FadeInTransitionPanel());
            yield return new WaitUntil(() => transitionComplete);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else if(gameState == GameState.TITLE)
        {
            // TODO hack. Fix this to rather have when car is moving or road started to move.
            yield return new WaitForSeconds(0.3f);
            //BumperCopsDebug.Log("Gamemanager: Fading out");
            StartCoroutine(TransitionManager.FadeoutTransitionPanel());
            yield return new WaitUntil(() => transitionComplete);
            TitlePageLoaded?.Invoke();
        }

        // remove the handler
        this.TransitionManager.TransitionComplete -= handler;
    }

    private void LateUpdate() 
    {
        if(!hasInitialized && this.playerController.character != null)
        {
            BumperCopsDebug.Log("Inititlizing game");
            GameInitialized?.Invoke();
            gameState = GameState.TITLE;
            hasInitialized = true;
            StartCoroutine(TransitionWhiteScreen());
        }
    }
    
    private void GameInitialTransitionComplete()
    {
        StartPlaying?.Invoke();
        // start the timer to start the game
        StartCoroutine(StartGameTimer());
    }

    private void OnEnemyEscaped()
    {
        EndGame(GameOverReason.ENEMY_ESCAPED);
    }

    private void OnTimeUp()
    {
        EndGame(GameOverReason.TIME_UP);
    }

    private void OnAllEnemiesDestroyed()
    {
        EndGame(GameOverReason.ALL_ENEMIES_DESTROYED);
    }

    private void EndGame(GameOverReason reason)
    {
        BumperCopsDebug.Log("Game over");
        GameOver?.Invoke(reason);
        ++gameState;
        StartCoroutine(ShowGameOverReason(reason));
        Pause(false);
    }

    private IEnumerator ShowGameOverReason(GameOverReason reason)
    {
        Text timerText = startGameTimerCanvasGroup.GetComponentInChildren<Text>();
        timerText.text = "Game Over";
        
        startGameTimerCanvasGroup.alpha = 0;

        while(startGameTimerCanvasGroup.alpha < 1)
        {
            startGameTimerCanvasGroup.alpha += Time.unscaledDeltaTime;
        }

        yield return new WaitForSecondsRealtime(1f);

        while(startGameTimerCanvasGroup.alpha > 0)
        {
            startGameTimerCanvasGroup.alpha -= Time.unscaledDeltaTime;
        }

        GameOverTransitions?.Invoke();
    }

    public void Pause(bool showDialogue = true)
    {
        if(gameState < GameState.GAME || isPaused || gameState > GameState.GAME)
        {
            return;
        }

        GamePaused?.Invoke();

        isPaused = true;
        //BumperCopsDebug.Log("Pausing");
        if(showDialogue)
        {
            //BumperCopsDebug.Log("Showing dialogue");
            pauseMenu.Show();
        }
    }

    public void ContinueGame(bool runTimer = true)
    {
        BumperCopsDebug.Log("Unpausing the game");
        if(!isPaused || gameState > GameState.GAME)
        {
            return;
        }

        StartCoroutine(StartGameTimer(runTimer));
    }

    private void StartNewGame(GameEventArgs args)
    {
        gameState = GameState.GAME_START_CLICKED;
        //BumperCopsDebug.Log("GameManager: Starting a new sessions");
        currentSessionNumber = PlayerStats.Instance.IncrementSessionNumber();
        
        if(args.IsTutorial)
        {
            BumperCopsDebug.Log("GameManager: Starting tutorial");
            this.TutorialManager.TutorialEnd += OnTutorialEnded;
            IsTutorial = true;

            // make sure the current session is 0
            GameTutorialSessionInitialized(new GameEventArgs(true, currentSessionNumber));
        }
        else
        {
            GameSessionInitialized?.Invoke(new GameEventArgs(true, currentSessionNumber));
        }
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        PlayerStats.Instance.MarkTutorialCompleted();
        this.TutorialManager.TutorialEnd -= OnTutorialEnded;
        IsTutorial = false;
    }
    
    public void EndGameAndGoHome()
    {
        if(isPaused)
        {
            this.pauseMenu.ResetAndHide();
            isPaused = false;
        }

        EndGame(GameOverReason.USER_INITIATED);
    }

    private IEnumerator StartGameTimer(bool runTime = true)
    {   
        // need to disable the canvas group because the buttons are getting triggered
        BumperCopsDebug.Log("Unpausing time");
        if(isPaused && pauseMenu.gameObject.activeSelf)
        {
            pauseMenu.ResetAndHide();
        }
        
        if(runTime)
        {
            startGameTimerCanvasGroup.alpha = 1;
            Text timerText = startGameTimerCanvasGroup.GetComponentInChildren<Text>();
            Stopwatch timerSw = Stopwatch.StartNew();
            
            for(int i = 3; i >= 1; i--)
            {
                if(i == 1)
                {
                    // send the prestart.
                    GamePreStart?.Invoke();
                }

                timerText.text = i.ToString();
                while(timerSw.Elapsed.Seconds < 1) yield return null;

                timerSw.Restart();
            }
            
            startGameTimerCanvasGroup.alpha = 0;
        }

        BumperCopsDebug.Log("Game started");
        GameStarted(new GameEventArgs(!isPaused, currentSessionNumber));
        gameState = GameState.GAME;
        isPaused = false;
    }

    private void OnApplicationPause(bool pauseStatus) 
    {
#if UNITY_EDITOR
    return;
#endif
        Pause();
    }

    private void OnApplicationFocus(bool focusStatus) 
    {
#if UNITY_EDITOR
    return;
#endif
        Pause();
    }

    private enum GameState
    {
        INIT,
        TITLE,
        GAME_START_CLICKED,
        ASSIGNMENT_PANEL,
        GAME,
        OVER
    }
}
