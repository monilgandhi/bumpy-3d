using System;
using UnityEngine;
using System.Collections.Generic;
using BumperCops.Models.Leveling;
public class GameLevelManager : MonoBehaviour
{   
    public event Action<GameLevelChangedEventArgs> GameLevelChanged;

    public float TrafficDensity 
    {
        get 
        {
            ILevelConfig trafficConfig = null;
            if(TryGetConfig(GameLeverType.TRAFFIC_DENSITY, out trafficConfig) && trafficConfig.Values != null)
            {
                return trafficConfig.Values[0];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.TRAFFIC_DENSITY));
        }
    }

    public float EnemyHealth
    {
        get 
        {
            ILevelConfig enemyConfig = null;
            if(TryGetConfig(GameLeverType.ENEMY, out enemyConfig) && enemyConfig.Values != null)
            {
                return enemyConfig.Values[(int)EnemyStats.HEALTH];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.ENEMY));
        }
    }

    public float EnemySpeed
    {
        get 
        {
            ILevelConfig enemyConfig = null;
            if(TryGetConfig(GameLeverType.ENEMY, out enemyConfig) && enemyConfig.Values != null)
            {
                return enemyConfig.Values[(int)EnemyStats.SPEED];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.ENEMY));
        }
    }

    public float EnemyBounty
    {
        get 
        {
            ILevelConfig enemyConfig = null;
            if(TryGetConfig(GameLeverType.ENEMY, out enemyConfig) &&
                 enemyConfig.Values != null)
            {
                return enemyConfig.Values[(int)EnemyStats.BOUNTY];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.ENEMY));
        }
    }

    public float EnemyDistanceOnSpawn
    {
        get 
        {
            ILevelConfig enemyConfig = null;
            if(TryGetConfig(GameLeverType.ENEMY, out enemyConfig) &&
                 enemyConfig.Values != null)
            {
                return enemyConfig.Values[(int)EnemyStats.DISTANCE];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.ENEMY));
        }
    }

    public float GameSpeed
    {
        get 
        {
            ILevelConfig gameSpeedConfig = null;
            if(TryGetConfig(GameLeverType.GAME_SPEED, out gameSpeedConfig) && gameSpeedConfig.Values != null)
            {
                return gameSpeedConfig.Values[0];
            }

            throw new KeyNotFoundException(nameof(GameLeverType.GAME_SPEED));
        }
    }

    public GameLevel CurrentRecommendedLevel { get { return gameLevelingConfig.DetermineNextLevel(gameTime.PlaySessionTime); }}
    
    [SerializeField]
    private ObstacleManager obstacleManager;
    private GameManager gameManager;
    private EnemyManager enemyManager;
    private GameTime gameTime;
    private float lastLevelChangeTime;
    private bool isActive;
    private bool changeTraffic;
    private const float LEVEL_CHANGE_MINIMUM_SPREAD = 10;
    private ILevelConfig[] levelConfigs;
    private GameLevelingConfig gameLevelingConfig;

    public GameLevel GetCurrentLevel(GameLeverType lever)
    {
        int idx = (int) lever;
        if(idx >= levelConfigs.Length)
        {
            throw new ArgumentNullException(nameof(lever));
        }

        return levelConfigs[idx].CurrentLevel;
    }

    private void Awake() 
    {
        isActive = true;
        this.gameTime = this.GetComponent<GameTime>();
        this.gameManager = this.GetComponentInParent<GameManager>();
        changeTraffic = UnityEngine.Random.value >= 0.5f;
        
        InitLevelConfigs();
    }

    // TODO this should not destroy on load
    private void InitLevelConfigs()
    {
        levelConfigs = new ILevelConfig[Enum.GetNames(typeof(GameLeverType)).Length - 1];
        ILevelConfig config = new EnemyLevelConfig();
        levelConfigs[(int)config.GameLeverType] = config;

        config = new TrafficDensityConfig();
        levelConfigs[(int)config.GameLeverType] = config;

        config = new GameSpeedConfig();
        levelConfigs[(int)config.GameLeverType] = config;

        gameLevelingConfig = new GameLevelingConfig();
        gameLevelingConfig.Load();

        foreach(ILevelConfig c in levelConfigs)
        {
            c.Load();
        }
    }

    private void Start() 
    {
        this.enemyManager = this.gameManager.EnemyManager;
        this.enemyManager.EnemyHealthOver += OnEnemyDestroyed;
        this.gameManager.TutorialManager.TutorialStarted += (args) => isActive = false;
        this.gameManager.TutorialManager.TutorialEnd += (args) => isActive = true;
    }

    private void OnEnemyDestroyed(EnemyArgs args)
    {
        IncreaseDifficulty(new GameLeverType[1] { GameLeverType.ENEMY } );
    }

    // TODO move most of this logic to gamespeed config
    private void ChangeTrafficOrGameSpeed()
    {
        if(!changeTraffic)
        {
            //BumperCopsDebug.Log("Changing game speed");
            IncreaseDifficulty(new GameLeverType[2] { GameLeverType.GAME_SPEED, GameLeverType.TRAFFIC_DENSITY } );
        }
        else
        {
            //BumperCopsDebug.Log("Changing traffic");
            IncreaseDifficulty(new GameLeverType[2] { GameLeverType.TRAFFIC_DENSITY, GameLeverType.GAME_SPEED } );
        }

        ILevelConfig gameSpeedConfig = levelConfigs[(int)GameLeverType.GAME_SPEED];
        if(this.GameSpeed >= 15 && this.obstacleManager.TrafficGenerationTime > 0.5 || !gameSpeedConfig.CanChangeLevel())
        {
            // increase traffic
            changeTraffic = true;
        }
        else
        {
            changeTraffic = !changeTraffic;
        }
    }

    private void LateUpdate() 
    {
        if(gameTime.PlaySessionTime - this.lastLevelChangeTime > LEVEL_CHANGE_MINIMUM_SPREAD)
        {
            //BumperCopsDebug.Log("Change game speed level or traffic");
            ChangeTrafficOrGameSpeed();
            this.lastLevelChangeTime = gameTime.PlaySessionTime;
        }
    }

    private bool TryGetConfig(GameLeverType type, out ILevelConfig levelConfig)
    {
        levelConfig = null;
        int idx = (int)type;
        if(idx >= levelConfigs.Length)
        {
            BumperCopsDebug.LogWarning("length of idx greater than level config");
            return false;
        }

        levelConfig = levelConfigs[idx];
        return true;
    }

    private void IncreaseDifficulty(GameLeverType[] gameLeverPriority, int numberToChangeLevel = 1)
    {
        if(!isActive)
        {
            return;
        }

        GameLevel recommendedLevel = gameLevelingConfig.DetermineNextLevel(gameTime.PlaySessionTime);
        BumperCopsDebug.Log($"New recommended level is {recommendedLevel}");

        if(gameLeverPriority.Length >= Enum.GetNames(typeof(GameLeverType)).Length)
        {
            BumperCopsDebug.LogError($"Priority array contains invalid number {gameLeverPriority.Length}");
        }
        
        foreach(GameLeverType t in gameLeverPriority)
        {
            int tIdx = (int)t;
            ILevelConfig selectedConfig = levelConfigs[(int)t];
            if(selectedConfig.CanChangeLevel())
            {
                selectedConfig.ChangeLevel(recommendedLevel);
                GameLevelChanged?.Invoke(new GameLevelChangedEventArgs(t));
                --numberToChangeLevel;
            }

            if(numberToChangeLevel <= 0)
            {
                break;
            }
        }
    }

    public class GameLevelChangedEventArgs
    {
        public readonly GameLeverType GameLever;
        public GameLevelChangedEventArgs(GameLeverType gameLever)
        {
            this.GameLever = gameLever;
        }
    }
}

public enum GameLeverType
{
    ENEMY,
    GAME_SPEED,
    TRAFFIC_DENSITY,
    // this always has to be last
    GAME_LEVEL
}