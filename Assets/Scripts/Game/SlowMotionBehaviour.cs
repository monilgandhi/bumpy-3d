using System.Collections;
using UnityEngine;

public class SlowMotionBehaviour : MonoBehaviour 
{
    public delegate void SlowMotionEffectEventHandler();
    public event SlowMotionEffectEventHandler SlowMotionStarted;
    public event SlowMotionEffectEventHandler SlowMotionEnded;
    private GameManager gameManager;
    private float slowMotionLength = -1;

    [SerializeField]
    private TutorialManager tutorialManager;

    [SerializeField]
    private float originalTimeScale = 1.0f;

    [SerializeField]
    private float originalFixedDeltaTime = 0.02f;
    
    [SerializeField]
    private TutorialBehaviour tutorialBehaviour;

    private const float SLOWMOTION_TIME_SCALE = 0.1f;
    
    // this is required so that we unpause for the right step. Else we will unpause if the events come out of order
    private TutorialStep currentTutorialStep;

    private SlowMoState currentState;
    private SlowMoState CurrentState
    {
        get { return currentState; }
        set 
        {
            //BumperCopsDebug.Log($"Before: Current State {currentState}, previous state {previousState}");
            this.currentState = value;
            //BumperCopsDebug.Log($"After: Current State {CurrentState}, previous state {previousState}");
        }
    }

    private SlowMoState previousState;
    private bool tutorialInProgress;

    private float slowMotionStartTime;
    private void Awake() 
    {
        Time.timeScale = originalTimeScale;
        Time.fixedDeltaTime = originalFixedDeltaTime;
        this.gameManager = this.transform.root.GetComponent<GameManager>();
    }

    private void Start() 
    {
        // this is used so that in case if we pause when slowmotion is active we immediately
        // this.gameManager.GamePauseInititated += OnGamePausedInitiated; 
        this.gameManager.GamePaused += () => GamePaused(); 
        this.gameManager.GameStarted += OnGameStarted; 
        this.gameManager.EnemyManager.EnemySpawned += (args) =>
        {
            args.enemy.DamageController.EnemyDestroyed += ResetTimeScale;
        };

        this.tutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;

        //this.gameManager.Player.AttackController.AttackReady += () => StartCoroutine(OnAttackReady());
        this.gameManager.Player.AttackController.Bumped += OnPlayerAttacked;
    }

    private IEnumerator OnAttackReady()
    {
        yield return new WaitForSeconds(3f);
        StartSlowMotion();
    }

    private void OnPlayerAttacked(BumpEventArgs e)
    {
        // TODO this is not working in tutorial
        previousState = CurrentState;
        CurrentState = SlowMoState.NONE;
        ResetTimeScale();
        slowMotionStartTime = 0.0f;
        tutorialBehaviour.ToggleSwipeAnimation(false);
    }

    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        this.tutorialInProgress = args.IsTutorialActive;

        //BumperCopsDebug.Log($"SlowMotionBehaviour: {args}");
        this.currentTutorialStep = args.TutorialStep;

        // This is a special casing which is bad. The reason is that both, tutorial pause for next step and slow mo depend
        // on attack bar being full. There could be a race condition and either could be the winner causing in undesired experience.
        // Hence attackbar slowmo is specifically triggered here to gurantee desired result which is pause for tutorial followed by slowmo
        /*if(this.currentTutorialStep == TutorialStep.HIT)
        {
            if(CurrentState != SlowMoState.ATTACK_SLOW_MO)
            {
                CurrentState = SlowMoState.ATTACK_SLOW_MO;
                StartSlowMotion();
            }

            return;
        }*/

        if(args.IsTutorialStepActive && args.PauseGame && CurrentState != SlowMoState.TUTORIAL_PAUSE)
        {
            previousState = CurrentState;
            CurrentState = SlowMoState.TUTORIAL_PAUSE;
        }
        else
        {
            //BumperCopsDebug.Log($"Before: Current State {CurrentState}, previous state {previousState}");
            CurrentState = previousState;
        }

        ResetTimeScale();
    }

    private void Update() 
    {
        if(slowMotionStartTime <= 0)
        {
            return;
        }

        tutorialBehaviour.ToggleSwipeAnimation(true);
    }

    private void GamePaused()
    {
        //BumperCopsDebug.Log("SlowMotionBehaviour: Pausing game");
        previousState = CurrentState;
        CurrentState = SlowMoState.PAUSE;
        ResetTimeScale();
    }

    private void UnpauseGame()
    {
        if(CurrentState != SlowMoState.TUTORIAL_PAUSE)
        {
            CurrentState = previousState;
            previousState = SlowMoState.NONE;
            //BumperCopsDebug.Log($"SlowMotionBehaviour: OnGameStarted current state is {CurrentState}");
        }

        ResetTimeScale();
    }

    private void OnGameStarted(GameEventArgs args)
    {
        UnpauseGame();
    }
    
    public void StartSlowMotion(float length)
    {
        if(length <= 0)
        {
            return;
        }

        // no slow motion during tutorial
        if(tutorialInProgress)
        {
            return;
        }
        
        if(SlowMotionStarted != null)
        {
            SlowMotionStarted();
        }

        CurrentState = SlowMoState.ATTACK_SLOW_MO;
        slowMotionLength = length;
        ResetTimeScale();
    }

    private void StartSlowMotion()
    {
        if(CurrentState == SlowMoState.TUTORIAL_PAUSE)
        {
            return;
        }
        
        if(SlowMotionStarted != null)
        {
            SlowMotionStarted();
        }

        CurrentState = SlowMoState.ATTACK_SLOW_MO;
        slowMotionStartTime = Time.unscaledTime;
        ResetTimeScale();
    }

    private void ResetTimeScale()
    {
        switch(CurrentState)
        {
            case SlowMoState.NONE:
                // original
                Time.timeScale = originalTimeScale;
                Time.fixedDeltaTime = originalFixedDeltaTime;

                if(previousState == SlowMoState.ATTACK_SLOW_MO && SlowMotionEnded != null)
                {
                    previousState = SlowMoState.NONE;
                    SlowMotionEnded();
                }

                break;

            case SlowMoState.TUTORIAL_PAUSE:
            case SlowMoState.PAUSE:
                Time.timeScale = 0;
                Time.fixedDeltaTime = originalFixedDeltaTime;
                break;
            
            case SlowMoState.ATTACK_SLOW_MO:
                Time.timeScale = SLOWMOTION_TIME_SCALE;
                Time.fixedDeltaTime = originalFixedDeltaTime * Time.timeScale;
                break;  
        }
    }

    private enum SlowMoState
    {
        NONE,
        PAUSE,
        TUTORIAL_PAUSE,
        ATTACK_SLOW_MO,    
    }
}