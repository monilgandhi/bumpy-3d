using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
public class LoadingManager : MonoBehaviour 
{
    private void Start() 
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);

        #if UNITY_IOS && !UNITY_EDITOR
            BumperCopsDebug.Log("Waiting for permission");
            iOSPlatform.Init();
        #endif
    }
}