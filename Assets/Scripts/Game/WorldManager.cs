using UnityEngine;
using System.Collections;
using AmazingAssets.CurvedWorld;
public class WorldManager : MonoBehaviour 
{
    [SerializeField]
    private CurvedWorldController curvedWorldController;

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private float speed = 0.2f;

    private float curveTime;
    private CurveDirection currentDirection;
    private bool isTutorial;
    private bool wentLeft;
    private const float CURVE_START_TIME_TRIGGER = 15.0f;
    private const float BEND_VALUE = 0.15f;
    private enum CurveDirection
    {
        NONE,
        LEFT,
        RIGHT
    }

    private void Start() 
    {
        gameManager.GameStarted += OnGameStarted;
        gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;
        gameManager.TutorialManager.TutorialEnd += OnTutorialEnd;
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        isTutorial = true;
    }

    private void OnTutorialEnd(TutorialArgs args)
    {
        isTutorial = false;
        curveTime = Time.time;
    }

    private void OnGameStarted(GameEventArgs args)
    {
        curveTime = Time.time;
    }

    private void Update() 
    {
        if(isTutorial)
        {
            return;
        }

        if(currentDirection != CurveDirection.NONE && curveTime < 0)
        {
            currentDirection = CurveDirection.NONE;
            curveTime = Time.time;
            return;
        }

        if(currentDirection == CurveDirection.NONE && 
            Time.time - curveTime >= CURVE_START_TIME_TRIGGER)
        {
            wentLeft = !wentLeft;
            currentDirection = wentLeft ? CurveDirection.LEFT : CurveDirection.RIGHT;
            curveTime = Time.time;
            StartCoroutine(Curve());
        }
    }

    private IEnumerator Curve()
    {
        int direction = currentDirection == CurveDirection.LEFT ? -1 : 1;
        // half time
        float bendValue = 0;
        while(Mathf.Abs(bendValue) < BEND_VALUE)
        {
            bendValue += (Time.deltaTime * direction * speed);
            curvedWorldController.bendHorizontalSize = bendValue;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(3f);

        while((currentDirection == CurveDirection.LEFT && bendValue < 0) ||
                (currentDirection == CurveDirection.RIGHT && bendValue > 0))
        {
            //BumperCopsDebug.Log($"Before reducing {bendValue}");
            bendValue -= (Time.deltaTime * direction * speed);
            //BumperCopsDebug.Log($"After reducing {bendValue}");
            curvedWorldController.bendHorizontalSize = bendValue;
            yield return new WaitForEndOfFrame();
        }

        // curvedWorldController.bendHorizontalSize = 0;
        curveTime = -1;
    }
}