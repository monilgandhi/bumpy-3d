using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicBars : MonoBehaviour 
{
    public delegate void CinematicBarEventHandler();
    public event CinematicBarEventHandler CinematicBarsHidden;

    [SerializeField]
    private GameManager gameManager;

    private RectTransform bottomBar;
    private RectTransform topBar;
    private bool areCinematicbarsShown = true;
    private float topBarPositionY;
    private float bottomBarPositionY;
    private void Awake() 
    {
        gameManager.StartPlaying += () => OnShowCinematicBars();
        gameManager.GameTutorialSessionInitialized += (args) => OnShowCinematicBars();
        gameManager.GameStarted += (args) => OnHideCinematicBars();    
        //gameManager.GameOverTransitions += (args) => OnShowCinematicBars();
    }

    private void Start() 
    {
        RectTransform[] allRectTransforms = GetComponentsInChildren<RectTransform>();
        foreach(RectTransform b in allRectTransforms)
        {
        //BumperCopsDebug.Log("position " + b.anchoredPosition.y);
           if(!b.sizeDelta.Equals(Vector2.zero) && b.anchoredPosition.y < 0)
           {
               // top bar
               topBar = b;
               topBarPositionY = b.anchoredPosition.y;
           } 
           else if (!b.sizeDelta.Equals(Vector2.zero) && b.anchoredPosition.y > 0)
           {
               bottomBar = b;
               bottomBarPositionY = b.anchoredPosition.y;
           }
        }

        if(topBar == null || bottomBar == null)
        {
            throw new MissingComponentException(nameof(RectTransform));
        }

        OnHideCinematicBars();
    }

    private void OnShowCinematicBars()
    {
        if(areCinematicbarsShown)
        {
            return;
        }

        areCinematicbarsShown = true;
        StartCoroutine(Animate(true));
    }

    private void OnHideCinematicBars()
    {
        if(!areCinematicbarsShown)
        {
            return;
        }

        areCinematicbarsShown = false;
        StartCoroutine(Animate(false));
    }

    private IEnumerator Animate(bool show)
    {
        //BumperCopsDebug.Log($"CinematicBars: bottom bar position {bottomBarPositionY} and top bar position {topBarPositionY}");
        //BumperCopsDebug.Log("sizedelta " + bars[0].sizeDelta + " and target size is " + targetSize + " and show is " + show + " changesize is " + changesizeAmount);
        // increase
        float changesizeAmount = bottomBarPositionY / 0.3f;
        while((show && bottomBar.anchoredPosition.y < bottomBarPositionY) || (!show && bottomBar.anchoredPosition.y > (-1) * bottomBarPositionY))
        {
            float yPosition = show ? changesizeAmount * Time.unscaledDeltaTime * -1 : changesizeAmount * Time.unscaledDeltaTime;
            float topBarCalculatedPosition = topBar.anchoredPosition.y + yPosition;
            float bottomBarCalculatedPosition = bottomBar.anchoredPosition.y - yPosition;

            bottomBar.anchoredPosition = new Vector2(bottomBar.anchoredPosition.x, bottomBarCalculatedPosition);
            topBar.anchoredPosition = new Vector2(topBar.anchoredPosition.x, topBarCalculatedPosition);
            yield return new WaitForSecondsRealtime(0.01f);
        }

        if(!show)
        {
            if(CinematicBarsHidden != null)
            {
                CinematicBarsHidden();
            }
            
            bottomBar.anchoredPosition = new Vector2(bottomBar.anchoredPosition.x, -1 * bottomBarPositionY);
            topBar.anchoredPosition = new Vector2(topBar.anchoredPosition.x, -1 * topBarPositionY);
        }
        else
        {
            bottomBar.anchoredPosition = new Vector2(bottomBar.anchoredPosition.x, bottomBarPositionY);
            topBar.anchoredPosition = new Vector2(topBar.anchoredPosition.x, topBarPositionY);
        }
    }

}