﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleSceneManager : MonoBehaviour
{
    public void OnShopUpgradeClicked()
    {
        BumperCopsDebug.Log("Shop clicked");
        SceneManager.LoadScene("Shop", LoadSceneMode.Single);
    }

    public void OnResetData()
    {
        PlayerPrefs.DeleteAll();
    }
}
