using UnityEngine;

public class GameTime : MonoBehaviour 
{
    private GameManager gameManager;
    public float PlaySessionTime { get; private set; }

    private float startTime; 
    private bool captureTime = false;
    public bool IsCapturing { get { return captureTime; }}
    private void Awake() 
    {
        gameManager = this.GetComponentInParent<GameManager>();
        PlaySessionTime = 0f;
    }

    private void Start() 
    {
        gameManager.GamePaused += OnGamePaused;
        gameManager.GameStarted += OnGameStarted;
        gameManager.GameOver += OnGameOver;
        gameManager.TutorialManager.TutorialEnd += OnTutorialEnd;
    }

    private void OnTutorialEnd(TutorialArgs args)
    {
        PlaySessionTime = 0;
    }
    
    private void OnGameStarted(GameEventArgs args)
    {
        captureTime = true;
    }

    private void OnGamePaused()
    {
        captureTime = false;
    }

    private void OnGameOver(GameOverReason reason)
    {
        captureTime = false;
    }

    private void LateUpdate() 
    {
        if(!captureTime)
        {
            return;
        }

        PlaySessionTime += Time.unscaledDeltaTime;

        if(Input.GetKeyDown(KeyCode.T))
        {
            BumperCopsDebug.Log($"Play time is {PlaySessionTime} seconds");
        }
    }
    
    private void OnApplicationPause(bool pauseStatus) 
    {
        if(pauseStatus)
        {
            captureTime = false;
        }    
    }
}