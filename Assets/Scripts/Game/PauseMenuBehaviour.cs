using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

[RequireComponent(typeof(Animator))]
public class PauseMenuBehaviour : MonoBehaviour 
{
    [SerializeField]
    private GameplayAssignmentManager assignmentManager;

    [SerializeField]
    private BumperCopsGameplayAnalytics analytics;

    [Header("UI Elements")]
    [SerializeField]
    private List<MissionPanelBehaviour> missionPanels;

    [SerializeField]
    private TMP_Text missionTitle;
    private AnimationCompletedBehaviour animationCompletedBehaviour;
    private bool missionsLoaded;
    
    private void Awake() 
    {
        animationCompletedBehaviour = this.GetComponent<AnimationCompletedBehaviour>();
        ForceHideNoAnimation();
    }    

    private void LoadMissionDetails()
    {
        for(int i = 0; i < missionPanels.Count; i++)
        {
            MissionPanelBehaviour currentMissionPanel = missionPanels[i];
            if(assignmentManager.CurrentAssignment == null || 
                assignmentManager.CurrentAssignment.Objectives == null || 
                assignmentManager.CurrentAssignment.Objectives.Count == 0)
                {
                    currentMissionPanel.Hide();
                    continue;
                }

            AssignmentObjectives o = assignmentManager.CurrentAssignment.Objectives[i];
            currentMissionPanel.Show();
            currentMissionPanel.Load(o.Description, o.PlayerCount, o.Id, o.Count, 0);
        }

        if(assignmentManager.CurrentAssignment != null)
        {
            missionTitle.text = GameplayAssignmentManager.GetAssignmentName(assignmentManager.CurrentAssignment.AssignmentNumber);
        }
        missionsLoaded = true;
    }

    public void Show()
    {
        LoadMissionDetails();
        this.gameObject.SetActive(true);
        analytics.RecordPauseButtonClick();
    }
    
    public void ResetAndHide()
    {
        BumperCopsDebug.Log("PauseMenu: Resetting and hiding");
        if(this.gameObject.activeSelf)
        {
            StartCoroutine(Hide());
        }
    }

    private void ForceHideNoAnimation()
    {
        BumperCopsDebug.Log("Hiding pause menu");
        this.gameObject.SetActive(false);
    }

    private IEnumerator Hide()
    {
        animationCompletedBehaviour.StartAnimationWithTrigger("hide", false, false);
        yield return new WaitForSecondsRealtime(0.5f);
        ForceHideNoAnimation();
    }
}