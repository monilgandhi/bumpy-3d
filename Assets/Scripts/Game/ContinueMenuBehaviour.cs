using UnityEngine;
using UnityEngine.UI;

public class ContinueMenuBehaviour : MonoBehaviour 
{
    public delegate void ContinuePopupEventHandler();
    public event ContinuePopupEventHandler ContinueClicked;

    [SerializeField]
    private Text coinValue;

    [SerializeField]
    private Text playerCoins;

    private int continueCount = 0;
    private const int MAX_CONTINUE_COUNT = 1;
    private void Awake() 
    {
        this.gameObject.SetActive(false);    
    }    

    public void Show(int coinsThisSession)
    {
        this.gameObject.SetActive(true);
        this.playerCoins.text = (PlayerStats.Instance.CoinCount + coinsThisSession).ToString();
        this.coinValue.text = 300.ToString();
    }

    public void OnContinue()
    {
        ++continueCount;
        if(ContinueClicked != null)
        {
            ContinueClicked();
        }
    }

    public void OnHide()
    {
        this.gameObject.SetActive(false);
    }
}