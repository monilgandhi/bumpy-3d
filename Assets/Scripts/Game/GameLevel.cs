public enum GameLevel
{
    EASY = 0,
    MEDIUM,
    MEDIUM_HARD,
    HARD,
    INSANE
}