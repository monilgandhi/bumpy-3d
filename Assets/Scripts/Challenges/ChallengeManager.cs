using System;
using BumperCops.Models;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class ChallengeManager : MonoBehaviour 
{    
    public event Action ChallengeManagerOpened;
    public event Action ChallengeManagerClosed; 
    public event Action ChallengeNotCreated;
    public event Action PlayNowClicked;
    public event Action<Challenge> ChallengeCreated;
    public event Action ChallengeWon;
    
    [SerializeField]
    private CoinStatsNonGameplayBehaviour coinStats;
    [SerializeField]
    private GameObject canvas;

    [SerializeField]
    private AnimationCompletedBehaviour animator;

    [SerializeField]
    private ChallengeMainPageBehaviours mainPage;

    [SerializeField]
    private ChallengeCreationManager challengeCreator;

    [SerializeField]
    private Camera challengeCamera;

    [Header("Admin")]
    [SerializeField]
    private int submitBotScore;

    [SerializeField]
    private bool deleteChallenge;

    [SerializeField]
    private bool endChallenge;
    private bool scoreFinalized;

    // this is linked with social stats levels
    private static OrderedDictionary BetToScore = new OrderedDictionary()
    {
        { ChallengeCreationManager.DEFAULT_BET_INCREMENT, 1000 },
        { ChallengeCreationManager.DEFAULT_BET_INCREMENT * 2, 50000 },
        { ChallengeCreationManager.DEFAULT_BET_INCREMENT * 3, 250000L },
        { ChallengeCreationManager.MAX_BET, 1000000L }
    };

    private static int MINIMUM_ACTIVE_CHALLENGE_DURATION_MINUTES = 4;
    public bool IsChallengeActive 
    {
        get
        {
            return ChallengeStats.Instance.ActiveChallenge != null;
        }
    }

    private void Awake() 
    {
        animator.AnimationOver += OnAnimationOver;
        mainPage.StartCreateNewChallenge += OnStartNewChallengeCreation;
        challengeCreator.ChallengeCreated += OnChallengeCreated;
        mainPage.PlayClicked += OnPlayClicked;
        mainPage.RewardClaimed += OnRewardClaimed;
        mainPage.ChallengeNotCreated += OnChallengeNotCreated;
        scoreFinalized = false;
        Hide(false, false);
    }

    private void OnPlayClicked()
    {
        Hide(true, false);
        PlayNowClicked?.Invoke();
    }
    
    private void OnStartNewChallengeCreation()
    {
        // delete the previous challegne if any
        CompleteChallenge();

        animator.StartAnimationWithTrigger("flip", false, false);
        mainPage.Hide();
        challengeCreator.Show();
    }

    private void OnChallengeCreated(Challenge c)
    {
        if(c == null || c.Users == null || c.Users.Count == 0)
        {
            throw new ArgumentException(nameof(c));
        }

        PlayerStats.Instance.ReduceCoins(c.BuyIn);
        mainPage.OnNewChallengeCreated(c);
        animator.StartAnimationWithTrigger("flip", false, false);
        challengeCreator.Hide();
        mainPage.Show();
        ChallengeCreated?.Invoke(c);
    }

    private void OnAnimationOver(AnimationCompleteEventArgs args)
    {
        if(args == null)
        {
            throw new ArgumentNullException(nameof(args));
        }

        if(args.IsAnimationComplete && args.Key == "hide")
        {
            canvas.gameObject.SetActive(false);
        }   
    }

    public bool Show(bool force)
    {
        if(deleteChallenge && CarUtils.IsUnityDevelopmentBuild())
        {
            ChallengeStats.Instance.DeleteChallenge();
        }

        // when to open
        // if forced by button clicked
        // if never shown - feature intro
        // if new score submitted

        bool hasScoreBeenSubmitted = false;
        if(IsChallengeActive)
        {
            Challenge activeChallenge = ChallengeStats.Instance.ActiveChallenge;
            TimeSpan durationElapsed = DateTime.Now - activeChallenge.StartTime;
            if((DateTime.Now > activeChallenge.EndTime && !activeChallenge.WinnerDeclared) ||
              (CarUtils.IsUnityDevelopmentBuild() && endChallenge))
            {
                EndChallenge(activeChallenge);
                hasScoreBeenSubmitted = true;
            }
            else if(durationElapsed.TotalMinutes > MINIMUM_ACTIVE_CHALLENGE_DURATION_MINUTES || 
                (CarUtils.IsUnityDevelopmentBuild() && submitBotScore > 0))
            {
                BumperCopsDebug.Log($"totalminutes: {Convert.ToInt32(durationElapsed.TotalMinutes)}");
                // number of submitted scores
                int numberOfSubmittedScoresExpected = Convert.ToInt32(durationElapsed.TotalMinutes) / MINIMUM_ACTIVE_CHALLENGE_DURATION_MINUTES;

                if(CarUtils.IsUnityDevelopmentBuild() && submitBotScore > 0)
                {
                    numberOfSubmittedScoresExpected = submitBotScore;
                }

                int actualSubmittedScores = activeChallenge.GetCountBotsSubmittedScores();

                BumperCopsDebug.Log($"expected: {numberOfSubmittedScoresExpected}, actual {actualSubmittedScores}");
                if(numberOfSubmittedScoresExpected > actualSubmittedScores)
                {
                    BumperCopsDebug.Log($"Submitting score for bot");
                    SetBotsScore(numberOfSubmittedScoresExpected - actualSubmittedScores);
                    hasScoreBeenSubmitted = true;
                }
            }
        }

        BumperCopsDebug.Log($"has score bbeen submitted {hasScoreBeenSubmitted}");
        if(hasScoreBeenSubmitted ||
            ChallengeStats.Instance.CanShowChallengeDialogue() || 
            force)
        {
            canvas.gameObject.SetActive(true);
            mainPage.Show();
            challengeCreator.Hide();    
            ChallengeStats.Instance.FeatureChallengeDialogueShown();
            ChallengeManagerOpened?.Invoke();
            challengeCamera.gameObject.SetActive(true);
            return true;
        }

        return false;
    }

    private void EndChallenge(Challenge activeChallenge)
    {
        BumperCopsDebug.Log("Ending Challenge");
        // see remaning bots
        int actualSubmittedScores = activeChallenge.GetCountBotsSubmittedScores();
        if(activeChallenge.Users.Count - 1 > actualSubmittedScores)
        {
            BumperCopsDebug.Log($"Submitting scores for {(activeChallenge.Users.Count - 1) - actualSubmittedScores}");
            SetBotsScore((activeChallenge.Users.Count - 1) - actualSubmittedScores);
        }

        BumperCopsDebug.Log("Marking the challenge compelte");
        activeChallenge.MarkChallengeComplete();
    }

    private void SetBotsScore(int numberOfbots)
    {
        if(numberOfbots <= 0)
        {
            return;
        }

        Challenge activechallenge = ChallengeStats.Instance.ActiveChallenge;
        numberOfbots = Math.Min(numberOfbots, activechallenge.Users.Count);

        BumperCopsDebug.Log($"Submitting score for bots count {numberOfbots}");

        for(int i = 0; i < numberOfbots; i++)
        {
            Tuple<float,long> winningChanceAndRequiredScore = GetChanceOfWinning(activechallenge);
            BumperCopsDebug.Log($"chance of winning {winningChanceAndRequiredScore.Item1}, requiredscore {winningChanceAndRequiredScore.Item2}");

            if(UnityEngine.Random.value < winningChanceAndRequiredScore.Item1)
            {
                // user wins
                // 10% less to user score - 1
                long botScore = Mathf.RoundToInt(
                    UnityEngine.Random.Range(activechallenge.User.ChallengeHighScore / 3, activechallenge.User.ChallengeHighScore));
                BumperCopsDebug.Log($"bot losing score is {botScore}");
                activechallenge.BotsNewScore(botScore);
            }
            else
            {
                long startScore = activechallenge.User.ChallengeHighScore > 0 ? 
                    activechallenge.User.ChallengeHighScore / 2 : 
                    winningChanceAndRequiredScore.Item2;
                    
                // user score to 10% more
                long botScore = Mathf.RoundToInt(
                    UnityEngine.Random.Range(
                        startScore, winningChanceAndRequiredScore.Item2 + (winningChanceAndRequiredScore.Item2 * 0.1f)));
                BumperCopsDebug.Log($"bot winning score is {botScore}");
                activechallenge.BotsNewScore(botScore);
            }
        }
    }

    private Tuple<float,long> GetChanceOfWinning(Challenge activeChallenge)
    {
        // chance of winning
        // bet levels 100, 300, 500, 700
        // 100 - chance 75 if score is greater than Sergeant
        // 300 - chance 75 if score is greater than Lieutenant
        // 500 - chance 75 if score is greater than Commander
        // 700 - chance 75 if score is greater than Deputy

        // logic - check submitted score and bet
        // if submitted score is in level (+/- 10) with bet, mark it at 75% 
        // for every level above current increase chance by 5% with max of 85% chance of winning
        // if submitted score is less (+/- 10) with bet reduce chance to 25% and bot scorre between range of (100 - 1000) more

        long requiredScore = 0;
        foreach(DictionaryEntry bts in BetToScore)
        {
            int bet = (int)bts.Key;
            if(activeChallenge.BuyIn < bet)
            {
                requiredScore = Convert.ToInt64(bts.Value);
                break;
            }
        } 

        BumperCopsDebug.Log($"Required score is {requiredScore}");

        // higher chance of winning if first player is bot and has required score same as mentioned in dictionary
        // OR if the user has higher score than required

        long minimumScore = Mathf.RoundToInt(requiredScore - (requiredScore * 0.1f));

        // user wins if less than 3 challenges won
        if(activeChallenge.User.ChallengeHighScore > 0 && ChallengeStats.Instance.Wins < 3)
        {
            return new Tuple<float, long> (1.0f, minimumScore);
        }

        // we want the user to come second atleast
        if(!activeChallenge.Users[0].IsUser && activeChallenge.Users[0].ChallengeHighScore > 0)
        {
            return new Tuple<float, long> (1.0f, minimumScore);
        }

        bool higherChanceOfWinning = activeChallenge.User.ChallengeHighScore >= minimumScore;

        BumperCopsDebug.Log($"Higher chance of winning {higherChanceOfWinning} and minimum score {minimumScore}");
        return higherChanceOfWinning ? 
            new Tuple<float, long> (0.75f, minimumScore) : 
            new Tuple<float, long> (0.25f, minimumScore);
    }

    private void OnRewardClaimed()
    {
        Reward r = new Reward((uint)ChallengeStats.Instance.ActiveChallenge.BuyIn, CollectibleType.COIN, RewardReason.CHALLENGE);
        coinStats.OnRewardGiven(new RewardEventArgs(new List<Reward>() { r }));
        PlayerStats.Instance.IncreaseCoins(ChallengeStats.Instance.ActiveChallenge.BuyIn * ChallengeStats.Instance.ActiveChallenge.Users.Count - 1);
        ChallengeWon?.Invoke();
        ++ChallengeStats.Instance.Wins;
        CompleteChallenge();
        mainPage.Show();
    }

    private void OnChallengeNotCreated()
    {
        ChallengeNotCreated?.Invoke();
        Hide(true, true);
        CompleteChallenge();
    }

    private void CompleteChallenge()
    {
        ChallengeStats.Instance.DeleteChallenge();
    }

    public void Hide(bool animate, bool sendEvent)
    {
        if(animate)
        {
            animator.StartAnimationWithTrigger("hide", false, false);
        }
        else
        {
            canvas.gameObject.SetActive(false);
            challengeCamera.gameObject.SetActive(false);
        }
        
        if(sendEvent)
        {
            ChallengeManagerClosed?.Invoke();
        }
    }
}