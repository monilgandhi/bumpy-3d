using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;
public class ChallengeCreationManager : MonoBehaviour 
{
    public Action<Challenge> ChallengeCreated;

    [Header("Challenge Dialog")]
    [SerializeField]
    private GameObject challengeSetupDialog;

    [SerializeField]
    private Button increaseBet;

    [SerializeField]
    private Button decreaseBet;

    [SerializeField]
    private TMP_Text betText;

    [SerializeField]
    private TMP_Text maxWinningsText;

    [Header("User Dialog")]
    
    [SerializeField]
    private GameObject userSelectionDialog;

    [SerializeField]
    private GameObject userPanelPrefab; 

    [SerializeField]
    private GameObject userContent;

    [SerializeField]
    private TMP_Text userSelectionText;

    [Header("General")]
    [SerializeField]
    private Button nextButton;

    [SerializeField]
    private AnimationCompletedBehaviour animationCompletedBehaviour;

    private List<ChallengeUserPanelBehaviour> userPanelBehaviours;
    private IReadOnlyList<ChallengeUser> users;
    int usersSelected = 0;

    private int currentBet;
    private int CurrentBet
    {
        get { return currentBet; }
        set
        {
            currentBet = value;
            betText.text = currentBet.ToString("n0");
        }
    }

    public const int DEFAULT_BET_INCREMENT = 250;
    public const int MAX_BET = 1000;
    private const int MAX_NUMBER_USERS = 3;
    private void Awake() 
    {
        increaseBet.onClick.AddListener(delegate { OnBetChanged (true); });
        decreaseBet.onClick.AddListener(delegate { OnBetChanged (false); });    
        users = ChallengeStats.Instance.GetUsersForChallenge();
        nextButton.onClick.AddListener(OnUserSelectionClicked);
        userPanelBehaviours = new List<ChallengeUserPanelBehaviour>();
        for(int i = 0; i < 5; i++)
        {
            GameObject go = Instantiate(userPanelPrefab, this.userContent.transform);
            ChallengeUserPanelBehaviour userPanelBehaviour = go.GetComponent<ChallengeUserPanelBehaviour>();
            userPanelBehaviour.Init(users[i]);
            userPanelBehaviour.UserSelected += OnUserSelected;
            userPanelBehaviours.Add(go.GetComponent<ChallengeUserPanelBehaviour>());
        }

        Hide();
    }

    private void OnUserSelectionClicked()
    {
        animationCompletedBehaviour.StartAnimationWithTrigger("flip", false, false);
        BumperCopsDebug.Log("On User selection clicked");
        userSelectionDialog.gameObject.SetActive(true);
        challengeSetupDialog.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(false);
    }

    private void OnBetChanged(bool increased)
    {
        if(increased)
        {
            CurrentBet += DEFAULT_BET_INCREMENT;
        }
        else
        {
            CurrentBet -= DEFAULT_BET_INCREMENT;
        }

        maxWinningsText.text = $"Max winnings: {CurrentBet * MAX_NUMBER_USERS}";
        increaseBet.interactable = CurrentBet + DEFAULT_BET_INCREMENT <= PlayerStats.Instance.CoinCount && CurrentBet < MAX_BET;
        decreaseBet.interactable = !CurrentBet.Equals(DEFAULT_BET_INCREMENT);
    }

    private void OnUserSelected(ChallengeUser u)
    {
        if(u == null)
        {
            throw new ArgumentNullException("challengeUser");
        }

        foreach(ChallengeUser user in users)
        {
            if(user.Id.Equals(u.Id))
            {
                ++usersSelected;
                user.IsSelected = true;
                break;
            }
        }

        userSelectionText.text = $"Select <color=#FFC200>{MAX_NUMBER_USERS - usersSelected}</color> users.";
        if(usersSelected.Equals(MAX_NUMBER_USERS))
        {
            List<ChallengeUser> selectedUsers = new List<ChallengeUser>();

            foreach(ChallengeUser selectedUser in users)
            {
                if(!selectedUser.IsSelected)
                {
                    continue;
                }

                selectedUsers.Add(selectedUser);
            }

            Device userDevice = Application.platform == RuntimePlatform.Android ? Device.ANDROID : Device.iOS;
            selectedUsers.Add(new ChallengeUser("currentUser", "You", userDevice, true));
            // create the challenge
            Challenge c = new Challenge(Guid.NewGuid(), selectedUsers, currentBet, DateTime.Now);
            ChallengeCreated?.Invoke(c);
        }
    }

    public void Show()
    {
        if(PlayerStats.Instance.CoinCount < DEFAULT_BET_INCREMENT)
        {
            // show need more coins
        }
        else
        {
            decreaseBet.interactable = false;
            CurrentBet = DEFAULT_BET_INCREMENT;
            increaseBet.interactable = PlayerStats.Instance.CoinCount >= (DEFAULT_BET_INCREMENT * 2);
        }

        BumperCopsDebug.Log("showing challenge setup and hiding user selection");
        maxWinningsText.text = $"Max winnings: {CurrentBet * MAX_NUMBER_USERS}";
        this.gameObject.SetActive(true);
        challengeSetupDialog.gameObject.SetActive(true);
        userSelectionDialog.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(true);
        usersSelected = 0;
    }    


    public void Hide()
    {
        this.gameObject.SetActive(false);
        challengeSetupDialog.gameObject.SetActive(false);
        userSelectionDialog.gameObject.SetActive(false);
    }    
}