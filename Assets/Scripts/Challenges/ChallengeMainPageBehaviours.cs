using UnityEngine;
using UnityEngine.UI;
using System;
using BumperCops.Models;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Events;
public class ChallengeMainPageBehaviours : MonoBehaviour 
{
    public event Action StartCreateNewChallenge;
    public event Action PlayClicked;
    public event Action RewardClaimed;
    public event Action ChallengeNotCreated;
    
    [Header("UI Elements")]
    [Header("New challenge")]
    [SerializeField]
    private Button createNewChallenge;

    [SerializeField]
    private TMP_Text description;

    [Header("Existing challenge")]
    [SerializeField]
    private GameObject currentChallengeContentHolder;

    [SerializeField]
    private GameObject currentChallengeUserPanel;

    [SerializeField]
    private TMP_Text timeRemaining;

    [SerializeField]
    private GameObject currentChallengeParent;

    [SerializeField]
    private TMP_Text activeChallengeText;

    [Header("Optional")]
    [SerializeField]
    private Button exitButton;
    [SerializeField]
    private ParticleSystem winningEffect;
    [SerializeField]
    private GameObject moreCoins;

    private Challenge ActiveChallenge
    {
        get { return ChallengeStats.Instance.ActiveChallenge; }
        set
        {
            if(ChallengeStats.Instance.ActiveChallenge == null)
            {
                return;
            }

            for(int i = 0; i < ChallengeStats.Instance.ActiveChallenge.Users.Count; i++)
            {
                if(challengePanelUserBehaviour.Count < i)
                {
                    CreateChallengePanelUser(1);
                }

                //BumperCopsDebug.Log($"Initing now for user with id {ChallengeStats.Instance.ActiveChallenge.Users[i].Name}");
                challengePanelUserBehaviour[i].Init(ChallengeStats.Instance.ActiveChallenge.Users[i]);
                if(ChallengeStats.Instance.ActiveChallenge.Users[i].IsUser)
                {
                    userPanel = challengePanelUserBehaviour[i];
                }
            }
        }
    }

    private ChallengeUserPanelBehaviour userPanel; 
    private List<ChallengeUserPanelBehaviour> challengePanelUserBehaviour;

    private void Awake() 
    {
        if(challengePanelUserBehaviour == null)
        {
            challengePanelUserBehaviour = new List<ChallengeUserPanelBehaviour>();
            CreateChallengePanelUser(4);
        }

        if(exitButton != null)
        {
            exitButton.onClick.AddListener(OnExit);
        }
    }

    private void CreateChallengePanelUser(int count = 1)
    {
        for(int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(currentChallengeUserPanel, currentChallengeContentHolder.transform);
            ChallengeUserPanelBehaviour cpb = go.GetComponent<ChallengeUserPanelBehaviour>();
            challengePanelUserBehaviour.Add(cpb);
        }
    }

    private void OnCreateNewChallenge()
    {
        StartCreateNewChallenge?.Invoke();
    }

    public void OnNewChallengeCreated(Challenge challenge)
    {
        ActiveChallenge = challenge;
    }

    private void Update() 
    {
        if(ActiveChallenge == null)
        {
            return;
        }    

        //BumperCopsDebug.Log($"end time is {ActiveChallenge.EndTime} and now is {DateTime.Now}");
        TimeSpan timeRemainingDt = ActiveChallenge.EndTime - DateTime.Now;
        if(timeRemainingDt.TotalSeconds <= 0)
        {
            return;
        }

        timeRemaining.text = $"Time remaining {timeRemainingDt.ToString(@"hh\:mm\:ss")}";
    }

    public bool ShowEndScreen(long newScore)
    {
        if(ChallengeStats.Instance.ActiveChallenge == null || newScore <= 0 || ChallengeStats.Instance.ActiveChallenge.WinnerDeclared)
        {
            return false;
        }

        ActiveChallenge.OnSubmitNewHighScore(newScore);
        ActiveChallenge = ChallengeStats.Instance.ActiveChallenge;
        BumperCopsDebug.Log("Updating users score");
        userPanel.UpdateScore(newScore);
        Show();
        return true;
    }

    public void Show()
    {
        if((ActiveChallenge == null && ChallengeStats.Instance.ActiveChallenge == null))
        {
            BumperCopsDebug.Log("No active challenge");
            // show create new challenge
            ToggleNewChallengeContent(true);
            ToggleExistingChallengeContent(false);
            
            ShowbuttonsForNewChallenge();
        }
        else
        {
            ActiveChallenge = ChallengeStats.Instance.ActiveChallenge;
            // display current challenge
            ToggleNewChallengeContent(false);
            ToggleExistingChallengeContent(true);

            if(ActiveChallenge.WinnerDeclared)
            {
                BumperCopsDebug.Log("Winner declared");
                ShowForCompleteChallenge();
            }
            else
            {
                BumperCopsDebug.Log("Challenge active");
                ShowForActiveChallenge();
            }
        }

        this.gameObject.SetActive(true);
    }

    private bool CanStartChallenge()
    {
        return PlayerStats.Instance.CoinCount >= ChallengeCreationManager.DEFAULT_BET_INCREMENT;
    }

    private void ShowbuttonsForNewChallenge()
    {
        if(!CanStartChallenge())
        {
            SetupNewChallengeButton(true, "Play Now", OnPlayClicked);
        }
        else
        {
            SetupNewChallengeButton(true, "New Challenge", OnCreateNewChallenge);
        }   
    }

    private void OnExit()
    {
        ChallengeNotCreated?.Invoke();
    }

    private void SetupNewChallengeButton(bool enabled, string text, UnityAction action)
    {
        if(createNewChallenge == null)
        {
            return;
        }

        createNewChallenge.gameObject.SetActive(enabled);
        if(!enabled)
        {
            return;
        }

        createNewChallenge.GetComponentInChildren<TMP_Text>().text = text;
        createNewChallenge.onClick.RemoveAllListeners();
        createNewChallenge.onClick.AddListener(action);
    }

    private void ShowForCompleteChallenge()
    {
        timeRemaining.gameObject.SetActive(false);
        activeChallengeText.gameObject.SetActive(true);
        ChallengeUser winner = ActiveChallenge.Users[0];

        if(winner.IsUser)
        {
            activeChallengeText.color = Color.yellow;
            activeChallengeText.text = "You Won!!";
            SetupNewChallengeButton(true, "Claim Reward", OnRewardClaimed);
    
            if(winningEffect != null)
            {
                BumperCopsDebug.Log("Playing winning effect");
                winningEffect.gameObject.SetActive(true);
            }
        }
        else
        {
            activeChallengeText.color = Color.red;
            activeChallengeText.text = $"{winner.Name} Won.";
            ShowbuttonsForNewChallenge();
            return;
        }
    }

    private void ShowForActiveChallenge()
    {
        timeRemaining.gameObject.SetActive(true);
        activeChallengeText.gameObject.SetActive(true);
        activeChallengeText.color = Color.white;
        activeChallengeText.text = "Active Challenge";
        
        SetupNewChallengeButton(true, "Play now", OnPlayClicked);
    }

    public void Hide()
    {
        if(winningEffect != null)
        {
            winningEffect.gameObject.SetActive(false);
        }
    
        this.gameObject.SetActive(false);
    }

    private void ToggleNewChallengeContent(bool enable)
    {
        bool moreCoinsEnabled = false;
        bool descriptionEnabled = false;

        if(enable)
        {
            if(!CanStartChallenge())
            {
                moreCoinsEnabled =true;
                descriptionEnabled = false;
            }
            else
            {
                descriptionEnabled = true;
                moreCoinsEnabled = false;
            }
        }

        BumperCopsDebug.Log($"Description: {descriptionEnabled}, more coins: {moreCoinsEnabled}");
        description.gameObject.SetActive(descriptionEnabled);

        if(moreCoins != null)
        {
            moreCoins.gameObject.SetActive(moreCoinsEnabled);
        }
    }

    private void ToggleExistingChallengeContent(bool enable)
    {
        currentChallengeParent.gameObject.SetActive(enable);
    }

    private void OnRewardClaimed()
    {
        RewardClaimed?.Invoke();
        BumperCopsDebug.Log("Reward claimed");
    }

    private void OnPlayClicked()
    {
        PlayClicked?.Invoke();
    }
}