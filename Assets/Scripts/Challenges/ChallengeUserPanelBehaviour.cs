using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ChallengeUserPanelBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image bgImage;
    [SerializeField]
    private Button challengeButton;

    [SerializeField]
    private TMP_Text nameField;

    [SerializeField]
    private TMP_Text highScoreField;

    [SerializeField]
    private Image deviceIcon;

    [SerializeField]
    private Sprite iosIcon;

    [SerializeField]
    private Sprite androidIcon;

    public Action<ChallengeUser> UserSelected;
    private bool isSelected;
    private ChallengeUser user;

    private void Awake() 
    {
        if(challengeButton != null)
        {
            challengeButton.onClick.AddListener(OnUserChallenged);
        }
    }

    public void Init(ChallengeUser u)
    {
        this.user = u;

        //BumperCopsDebug.Log($"Name of the user is {u.Name}");
        this.nameField.text = u.Name;

        UpdateScore(u.ChallengeHighScore);
        if(u.Device == Device.ANDROID)
        {
            deviceIcon.sprite = androidIcon;
        } else if (u.Device == Device.iOS)
        {
            deviceIcon.sprite = iosIcon;
        }

        if(u.IsUser)
        {
            bgImage.color = Constants.BLUE_BACKGROUND;
        }
        else
        {
            bgImage.color = Constants.USER_RANK_NOT_ACHIEVED_COLOR;
        }
    }

    public void UpdateScore(long newScore)
    {
        BumperCopsDebug.Log($"New score is {newScore}");
        if(this.highScoreField != null)
        {
            if(newScore <= 0)
            {
                this.highScoreField.text = "Not Submitted";
            }
            else
            {
                this.highScoreField.text = newScore.ToString("n0");
            }
        }
    }

    private void OnUserChallenged()
    {
        isSelected = true;
        if(challengeButton != null)
        {
            challengeButton.gameObject.SetActive(false);
        }
        
        bgImage.color = Constants.USER_RANK_ACHIEVED_COLOR;
        BumperCopsDebug.Log("Users selected");
        UserSelected?.Invoke(user);
    }
}