using System;
using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;

public class ChallengeStats
{
    private static List<string> userNames;
    private Challenge activeChallenge;
    public Challenge ActiveChallenge
    {
        get
        {
            if(activeChallenge == null)
            {
                string serializedString = PlayerPrefs.GetString("active_challenge");
                activeChallenge = JsonConvert.DeserializeObject<Challenge>(serializedString);
            }

            return activeChallenge; 
        }

        private set { activeChallenge = value; }
    }

    public int Wins
    {
        get
        {
            return PlayerPrefs.GetInt("player_wins", 0);
        }

        set
        {
            PlayerPrefs.SetInt("player_wins", value);
            PlayerPrefs.Save();
        }
    }
    
    private static ChallengeStats instance;

    /// <summary>
    /// Call this to get the instance of current city or load a new city or load a new instance.null
    /// At a given point only 1 city can be active
    /// </summary>
    /// <returns>PlayerStats</returns>
    public static ChallengeStats Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new ChallengeStats();
            }

            return instance;
        }
    }

    private ChallengeStats()
    {
        ConfigCache.TryGetConfig<List<string>>("users", "game", out userNames);
        if(userNames == null || userNames.Count <= 0)
        {
            userNames = new List<string>()
            {
                "aoe324",
                "mglite45",
                "deerhnt3r",
                "bullzzzzi67",
                "zzzzZz",
                "portichi24"
            };
        }
    }

    public bool CanShowChallengeDialogue()
    {
        if(!PlayerStats.Instance.IsTutorialComplete())
        {
            return false;
        }

        long shown = Convert.ToInt64(PlayerPrefs.GetFloat("show_feature_dialogue", 0));
        DateTimeOffset now = DateTime.Now;

        BumperCopsDebug.Log($"diff is {now.ToUnixTimeSeconds() - shown}");
        return (now.ToUnixTimeSeconds() - shown) > 14400;
    }

    public void FeatureChallengeDialogueShown()
    {
        DateTimeOffset offset = DateTime.Now;
        PlayerPrefs.SetFloat("show_feature_dialogue", offset.ToUnixTimeSeconds());
        PlayerPrefs.Save();
    }

    public void StoreOrUpdateChallenge(Challenge c)
    {
        if(c == null)
        {
            throw new ArgumentNullException("challenge");
        }

        ActiveChallenge = c;
        string serializedChallenge = JsonConvert.SerializeObject(c);
        BumperCopsDebug.Log($"challenge is {serializedChallenge}");
        PlayerPrefs.SetString("active_challenge", serializedChallenge);
        PlayerPrefs.Save();
    }

    public void DeleteChallenge()
    {
        PlayerPrefs.DeleteKey("active_challenge");
        ActiveChallenge = null;
        PlayerPrefs.Save();
    }

    public IReadOnlyList<ChallengeUser> GetUsersForChallenge()
    {
        List<ChallengeUser> cUsers = new List<ChallengeUser>();
        for(int i = 0; i < 5; i++ )
        {
            string name = userNames[UnityEngine.Random.Range(0, userNames.Count)];
            string id = $"u{i}";
            
            Device d = UnityEngine.Random.value < 0.5 ? Device.ANDROID : Device.iOS;
            cUsers.Add(new ChallengeUser(id, name, d, false));
        }

        return cUsers;
    }
}