using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using BumperCops.Models;
public class DailyRewardManager : MonoBehaviour 
{
    public Action<RewardEventArgs> DailyRewardGiven;
    [Header("Daily reward")]
    [SerializeField]
    private GameObject dailyrewardPrefab;

    [SerializeField]
    private GridLayoutGroup contentHolder;
    private List<DailyRewardCellBehaviour> rewardObjects;

    [Header("Consecutive Panel")]
    [SerializeField]
    private TMP_Text consecutiveDays;

    [SerializeField]
    private TMP_Text consecutiveDaysHelpText;

    [SerializeField]
    private TMP_Text consecutiveAmountText;

    [SerializeField]
    private TMP_Text resetDate;

    [Header("Admin")]

    [SerializeField]
    private int advanceDay;

    private const string GAME_CONFIG_FOLDER = "game";
    private List<RewardChanceConfig> configs;
    private RewardChanceConfig eligibleReward;
    private DailyRewardCellBehaviour eligibleDcb;
    private bool isRewardDue;
    private bool isConsecutive;
    private const int DEFAULT_CONSECUTIVE_AMOUNT = 100;
    private int consecutiveRewardAmount = 0;

    public bool IsRewardClaimed { get { return eligibleReward == null; }}
    private void Awake() 
    {
        if(!ConfigCache.TryGetConfig<List<RewardChanceConfig>>("daily-rewards", GAME_CONFIG_FOLDER, out configs))
        {
            BumperCopsDebug.LogError("Unable to load reward config");
            return;
        }

        if(!CarUtils.IsUnityDevelopmentBuild())
        {
            advanceDay = 0;
        }

        DateTime? lastDailyRewardDateTime = PlayerStats.Instance.DailyRewardDate;
        
        if(!lastDailyRewardDateTime.HasValue)
        {
            isRewardDue = true;
            isConsecutive = false;
        }
        else
        {
            BumperCopsDebug.Log($"Last daily reward time is {lastDailyRewardDateTime.Value}");
            DateTime nextRewardDate = lastDailyRewardDateTime.Value.AddDays(1);
            DateTime currentTime = DateTime.Now.AddDays(advanceDay);

            BumperCopsDebug.Log($"next reward day {nextRewardDate} and current time is {currentTime} and consecutive count {PlayerStats.Instance.DailyRewardConsecutiveCount}");
            isRewardDue = currentTime >= nextRewardDate;
            isConsecutive = (nextRewardDate.Date.Equals(currentTime.Date) && 
                PlayerStats.Instance.DailyRewardConsecutiveCount > 0);
        }

        SetupDialogue();
        BumperCopsDebug.Log("daily rewards setup complete");
    }

    private void SetupDialogue()
    {
        if(!isRewardDue)
        {
            return;
        }

        rewardObjects = new List<DailyRewardCellBehaviour>();
        BumperCopsDebug.Log($"daily reward size is {this.configs.Count}");
        BumperCopsDebug.Log($"daily reward count is  {PlayerStats.Instance.DailyRewardDayCount}");
        for(int i = 0; i < configs.Count; i++)
        {
            RewardChanceConfig r = configs[i];
            GameObject go = Instantiate(dailyrewardPrefab, this.contentHolder.transform);
            DailyRewardCellBehaviour dcb = go.GetComponent<DailyRewardCellBehaviour>();
            Sprite icon = CarUtils.GetCollectibleSprite(r.Type);
            bool canBeClaimed = PlayerStats.Instance.DailyRewardDayCount == i;
            if(canBeClaimed)
            {
                eligibleReward = r;
                eligibleDcb = dcb;
                dcb.ClaimButton.onClick.AddListener(OnRewardClaimed);
            }

            dcb.Init(icon, r.MaximumAmount, PlayerStats.Instance.DailyRewardDayCount > i, canBeClaimed, i + 1);
            rewardObjects.Add(go.GetComponent<DailyRewardCellBehaviour>());    
        }

        resetDate.text = $"Resets after {configs.Count} days";
    }

    public void OnRewardClaimed()
    {
        Debug.Log("reward claimed");
        PlayerStats.Instance.DailyRewardGiven(configs.Count, isConsecutive);
        eligibleDcb.RewardClaimed();

        int finalRewardAmount = eligibleReward.MaximumAmount + consecutiveRewardAmount;
        Reward r = new Reward((uint)finalRewardAmount, eligibleReward.Type, RewardReason.DAILY_REWARD);

        eligibleReward = null;
        eligibleDcb = null;
        DailyRewardGiven?.Invoke(new RewardEventArgs(new List<Reward>() { r }));
    }

    public bool GiveDailyRewardIfDue()
    {
        if(!isRewardDue)
        {
            return false;
        }

        // determine which day
        int dayCount = PlayerStats.Instance.DailyRewardDayCount;
        int consecutiveDayCount = PlayerStats.Instance.DailyRewardConsecutiveCount;

        if(isConsecutive)
        {
            consecutiveRewardAmount = (DEFAULT_CONSECUTIVE_AMOUNT * (PlayerStats.Instance.DailyRewardConsecutiveCount + 1));
            consecutiveDays.text = $"{consecutiveDayCount + 1} days in a row";
            consecutiveAmountText.text = consecutiveRewardAmount.ToString();
            consecutiveAmountText.transform.parent.gameObject.SetActive(true);
            consecutiveDaysHelpText.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            consecutiveAmountText.transform.parent.gameObject.SetActive(false);
            consecutiveDaysHelpText.transform.parent.gameObject.SetActive(false);
            if(dayCount <= configs.Count)
            {
                consecutiveDaysHelpText.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                consecutiveDaysHelpText.transform.parent.gameObject.SetActive(true);
                consecutiveDaysHelpText.text = $"Bonus tomorrow for {consecutiveDayCount + 2} days in a row";
            }
        }

        Show();
        return true;
    }

    private void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}