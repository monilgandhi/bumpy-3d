using System;
using UnityEngine;
using System.Collections.Generic;
using BumperCops.Models;

public class RewardContainerBehaviour : MonoBehaviour 
{
    [SerializeField]
    private GameObject rewardPanelPrefab;

    private Dictionary<CollectibleType, RewardPanelBehaviour> rewardPanelMap;
    private void Awake() 
    {
        rewardPanelMap = new Dictionary<CollectibleType, RewardPanelBehaviour>();
        foreach(CollectibleType t in Enum.GetValues(typeof(CollectibleType)))
        {
            if(t == CollectibleType.NONE)
            {
                continue;
            }

            GameObject go = Instantiate(rewardPanelPrefab, this.transform);
            rewardPanelMap.Add(t, go.GetComponent<RewardPanelBehaviour>());
        }        
    }

    public void DisplayRewards(List<Reward> rewards, bool enableBackground)
    {
        if(rewards == null || rewards.Count <= 0)
        {
            BumperCopsDebug.LogWarning("No rewarsd to load in container");
            return;
        }

        foreach(Reward r in rewards)
        {
            if(r.Type == CollectibleType.COMBO_INCREMENT)
            {
                PlayerStats.Instance.IncreaseCurrentBaseCombo(r.Amount);
                continue;
            }
            
            RewardPanelBehaviour rewardPanelBehaviour;
            if(!rewardPanelMap.TryGetValue(r.Type, out rewardPanelBehaviour))
            {
                BumperCopsDebug.LogWarning($"Could not find prefab of type {r.Type}");
                GameObject go = Instantiate(rewardPanelPrefab, this.transform);
                rewardPanelBehaviour = go.GetComponent<RewardPanelBehaviour>();
                rewardPanelMap.Add(r.Type, rewardPanelBehaviour);
            }

            BumperCopsDebug.Log($"Showing image to type {r.Type}");
            rewardPanelBehaviour.Init(CarUtils.GetCollectibleSprite(r.Type), r.Amount, CollectibleInventory.GetDescription(r.Type));
            rewardPanelBehaviour.Show(enableBackground);
        }
    }

    public void HideRewards()
    {
        foreach(RewardPanelBehaviour rp in rewardPanelMap.Values)
        {
            rp.Hide();
        }
    }
}