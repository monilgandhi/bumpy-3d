using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DailyRewardCellBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image rewardIcon;

    [SerializeField]
    private TMP_Text rewardAmount;

    [SerializeField]
    private Image claimedImage;

    [SerializeField]
    private Button claimButton;

    [SerializeField]
    private TMP_Text dayNumberText;
    public Button ClaimButton { get { return claimButton; }}
    

    public void Init(Sprite rewardIconSprite, int amount, bool claimed, bool canClaim, int daynumber)
    {
        BumperCopsDebug.Log($"claimed {claimed} and canClaim {canClaim}");
        rewardIcon.sprite = rewardIconSprite;
        rewardAmount.text = amount.ToString("n0");
        claimedImage.gameObject.SetActive(claimed);
        claimButton.gameObject.SetActive(!claimed && canClaim);
        dayNumberText.text = $"Day {daynumber}";
    }

    public void RewardClaimed()
    {
        claimButton.gameObject.SetActive(false);
        claimedImage.gameObject.SetActive(true);
    }
}