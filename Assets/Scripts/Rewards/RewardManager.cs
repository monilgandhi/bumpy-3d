using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BumperCops.Models;
using TMPro;
public class RewardManager : MonoBehaviour
{
    public event Action<RewardEventArgs> RewardGiven;
    [Header("Game components")]
    [SerializeField]
    private ScoreBehaviour score;

    [SerializeField]
    private AdsManager adsManager;

    [SerializeField]
    private GameManager gameManager;

    [Header("UI")]
    [SerializeField]
    private GameObject rewardPanel;
    [SerializeField]
    private TMP_Text title;

    [SerializeField]
    private RewardContainerBehaviour rewardContainer;

    [SerializeField]
    private TreasurechestBehaviour treasurechestBehaviour;

    [SerializeField]
    private Button videoAdReward;

    [Header("Admin")]
    [SerializeField]
    private bool showReward;

    private IList<Reward> pendingRewards;
    private long sessionScore;
    private long currentSession;
    private const long MINIMUM_SCORE = 10000;

    public TreasurechestBehaviour TreasureChest { get { return treasurechestBehaviour; } }
    private Dictionary<RewardReason, RewardsConfig> configMap;
    private const string GAME_CONFIG_FOLDER = "game";
    private void Awake() 
    {
        pendingRewards = new List<Reward>();
        score.ScoreFinalized += OnScoreFinalized;
        configMap = new Dictionary<RewardReason, RewardsConfig>();
        this.gameManager.GameSessionInitialized += OnGameSessionInitialized;
        List<RewardsConfig> configs;
        //load the config
        if(!ConfigCache.TryGetConfig<List<RewardsConfig>>("reward", GAME_CONFIG_FOLDER, out configs))
        {
            BumperCopsDebug.LogError("Unable to load reward config");
            return;
        }

        foreach(RewardsConfig r in configs)
        {
            if(configMap.ContainsKey(r.Reason))
            {
                BumperCopsDebug.LogWarning($"Key collision for {r.Reason}");
                configMap[r.Reason] = r;
            }
            else
            {
                configMap.Add(r.Reason, r);
            }
        }

        treasurechestBehaviour.TreasurechestOpened += OnTreasureChestClicked;
        videoAdReward.onClick.AddListener(OnAdRewardClicked);

        if(showReward)
        {
            showReward = CarUtils.IsUnityDevelopmentBuild();
        }

        Hide();
    }

    private void OnGameSessionInitialized(GameEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("game event args are null");
            return;
        }

        currentSession = args.CurrentSessionNumber;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
    
    private void OnAdRewardClicked()
    {
        this.rewardContainer.HideRewards();
        this.adsManager.AdEvent += OnAdEvent;
        this.adsManager.ShowRewardedAd(true, AdsPlacement.REWARD_DIALOGUE_AD);
    }

    private void OnAdEvent(AdEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("args for adevent are null");
            return;
        }

        if(args.AdsPlacement != AdsPlacement.REWARD_DIALOGUE_AD || !args.AdComplete)
        {
            return;
        }

        this.adsManager.AdEvent -= OnAdEvent;
        if(args.GiveReward)
        {
            GiveRewardsForVideoAd();
        }
    }

    private void OnTreasureChestClicked()
    {
        BumperCopsDebug.Log("Treasure chest opened clicked");
        // play animation or effects
        List<Reward> rewardRollUps = RollupRewards();
        pendingRewards = new List<Reward>();
        BumperCopsDebug.Log("Displaying rewards");
        RewardGiven?.Invoke(new RewardEventArgs(rewardRollUps));
        rewardContainer.DisplayRewards(rewardRollUps, false);

        videoAdReward.gameObject.SetActive(
                adsManager.CanShowAd(AdsPlacement.REWARD_DIALOGUE_AD));
    }

    private List<Reward> RollupRewards()
    {
        Dictionary<CollectibleType, Reward> rewardRollUps = new Dictionary<CollectibleType, Reward>();
        foreach(Reward r in pendingRewards)
        {
            if(!rewardRollUps.ContainsKey(r.Type))
            {
                rewardRollUps.Add(r.Type, r);
            }
            else
            {
                rewardRollUps[r.Type].IncrementAmount(r.Amount);
            }

            BumperCopsDebug.Log($"reward type {r.Type} and amount is {r.Amount}");
        }

        return rewardRollUps.Values.ToList();
    }

    private void Show(bool showMoreAdButton)
    {
        BumperCopsDebug.Log("Setting active");
        this.gameObject.SetActive(true);
        videoAdReward.gameObject.SetActive(false);
        treasurechestBehaviour.Show();
    }

    public bool GiveGameOverRewardWhenDue()
    {
        BumperCopsDebug.Log("Checking for reward");

        if(showReward)
        {
            pendingRewards.Add(new Reward(100, CollectibleType.BAG_COINS, RewardReason.HIGH_SCORE));
        }

        if(pendingRewards.Count == 0 && !GiveRewardForRun())
        {
            return false;
        }
        
        Show(true);
        return true;
    }

    public bool GiveRewardsForVideoAd()
    {
        RewardsConfig config = null;
        if(!configMap.TryGetValue(RewardReason.REWARD_VIDEO, out config))
        {
            BumperCopsDebug.LogWarning("Unable to find config for video ads");
            GiveRewardForRun(true);
            return false;
        }
        
        pendingRewards = pendingRewards.Concat(GenerateRewards(config.Reason, config.RewardChance)).ToList();
        Show(false);
        
        BumperCopsDebug.Log($"Giving reward {pendingRewards.Count}");
        return pendingRewards.Count > 0;
    }

    private bool GiveRewardForRun(bool forceReward = false)
    {
        if(sessionScore < MINIMUM_SCORE && !forceReward)
        {
            return false;
        }
        
        RewardsConfig config = null;
        if(!configMap.TryGetValue(RewardReason.RUN, out config))
        {
            return false;
        }

        if(UnityEngine.Random.value < config.ChanceForGivingReward || this.currentSession < 5)
        {
            pendingRewards = pendingRewards.Concat(GenerateRewards(config.Reason, config.RewardChance)).ToList();
            return pendingRewards.Count > 0;
        }
        
        return false;
    }

    private void OnScoreFinalized(ScoreUpdatedEventArgs args)
    {
        sessionScore = args.NewScore;
        if(args.IsHighScore)
        {
            // give high score reward here
            pendingRewards = pendingRewards.Concat(GenerateRewards(RewardReason.HIGH_SCORE)).ToList();
            BumperCopsDebug.Log($"Pending reward size is {pendingRewards.Count}");
        }
    }

    public List<Reward> GenerateRewardsForMission(List<RewardChanceConfig> config)
    {
        return GenerateRewards(RewardReason.MISSION_COMPLETION, config);
    }

    private List<Reward> GenerateRewards(RewardReason r, List<RewardChanceConfig> config = null)
    {
        List<Reward> rewards = new List<Reward>();
        RewardsConfig c = null;
        if(config == null && !configMap.TryGetValue(r, out c))
        {
            BumperCopsDebug.LogError($"Unable to find the reward reason with key {r.ToString()}");
            return rewards;
        }

        List<RewardChanceConfig> rcs = config != null ? config : c.RewardChance;
        HashSet<CollectibleType> rewardTypesGiven = new HashSet<CollectibleType>();
        foreach(RewardChanceConfig rc in rcs)
        {
            if(rewardTypesGiven.Contains(rc.Type))
            {
                continue;
            }
            
            float randomNumber = UnityEngine.Random.value;
            BumperCopsDebug.Log($"Random number is {randomNumber} and chance is {rc.Chance}");
            if(randomNumber > rc.Chance)
            {
                continue;
            }

            if(rc.MinimumAmount == rc.MaximumAmount)
            {
                BumperCopsDebug.Log($"Adding amount of {rc.MaximumAmount} with conversion {Convert.ToUInt32(rc.MaximumAmount)}");
                // return here
                rewards.Add(new Reward(Convert.ToUInt32(rc.MaximumAmount), rc.Type, r));
                continue;
            }

            BumperCopsDebug.Log($"Minimum amount is {rc.MinimumAmount} and max amount is {rc.MaximumAmount}");
            float prizeAmount = UnityEngine.Random.Range(rc.MinimumAmount, rc.MaximumAmount);
            uint roundUpPrizeAmount = Convert.ToUInt32((Mathf.CeilToInt(prizeAmount / rc.Multiple)) * rc.Multiple);
            BumperCopsDebug.Log($"round up is {roundUpPrizeAmount}, prize amount: {prizeAmount}, multiple: {rc.Multiple}");
            rewards.Add(new Reward(roundUpPrizeAmount, rc.Type, r));
            rewardTypesGiven.Add(rc.Type);
        }

        BumperCopsDebug.Log($"Reward count is {rewards.Count}");
        return rewards;
    }
}

public enum RewardReason
{
    NONE,
    HIGH_SCORE,
    MISSION_COMPLETION,
    RUN,
    REWARD_VIDEO,
    DAILY_REWARD,
    CHALLENGE
}

public class RewardEventArgs
{
    public IReadOnlyList<Reward> Rewards;
    public RewardEventArgs(List<Reward> rewardsGiven)
    {
        if(rewardsGiven == null || rewardsGiven.Count <= 0)
        {
            throw new ArgumentException("rewards given is null");
        }

        this.Rewards = rewardsGiven;
    }
}