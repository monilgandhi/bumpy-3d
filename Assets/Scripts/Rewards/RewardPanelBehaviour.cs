using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RewardPanelBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image rewardImage;

    [SerializeField]
    private TMP_Text rewardQuantity;

    [SerializeField]
    private TMP_Text descriptionTxt;

    private bool isInited;

    private void Awake() 
    {
        Hide();
    }

    public void Init(Sprite image, uint quantity, string description)
    {
        isInited = true;
        rewardImage.sprite = image;
        rewardQuantity.text = $"x{quantity}";
        descriptionTxt.text = description;
        Hide();
    }

    public void Show(bool enableBackgroundImage = true)
    {
        this.gameObject.SetActive(true);
        this.GetComponent<Image>().enabled = true;
        isInited = false;
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);  
    }
}