﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBumpTest : MonoBehaviour
{
    private Rigidbody rigidBody;
    [Header("Bump effects")]
    [SerializeField]
    private ForceMode effectForce;

    [SerializeField]
    private Vector3 forceVector;

    [SerializeField]
    private float forceMagnitude = 5000f;

    [SerializeField]
    private float explosionSeconds = 1f;

    private bool explosionIneffect;
    private float explosionTimer = -1f;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody>();
    }

    private void FixedUpdate() {
        BumpEffect();
    }

    void BumpEffect()
    {
        if(Input.GetKeyDown(KeyCode.B) && !explosionIneffect)
        {
            BumperCopsDebug.Log("Bumping now");
            StartCoroutine(BumpImpulse());
            explosionIneffect = true;
        }
    }

    IEnumerator BumpImpulse()
    {
        explosionTimer = Time.time;

        BumperCopsDebug.Log("Applying for of " + forceMagnitude);
        while((Time.time - explosionTimer) < explosionSeconds)
        {
            this.rigidBody.AddForce(forceVector.normalized * forceMagnitude, ForceMode.Impulse);
            yield return new WaitForFixedUpdate();
        }

        explosionIneffect = false;
    }

    IEnumerator BumpTorque()
    {
        explosionTimer = Time.time;

        BumperCopsDebug.Log("Applying for of " + forceMagnitude);
        while((Time.time - explosionTimer) < explosionSeconds)
        {
            this.rigidBody.AddTorque(forceVector.normalized * forceMagnitude, ForceMode.Impulse);
            yield return new WaitForFixedUpdate();
        }

        explosionIneffect = false;
    }
}
