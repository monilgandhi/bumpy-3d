﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class CarPhysicsTest : MonoBehaviour
{
    private Rigidbody rigidBody;

    [Header("Explosion effects")]
    [SerializeField]
    private ForceMode effectForce;

    [SerializeField]
    private Vector3 forceVector;

    [Header("Movement")]
    [SerializeField]
    private bool withWheelCollider;

    [SerializeField]
    private bool withForce;

    [SerializeField]
    private bool withVelocity;

    [SerializeField]
    private bool withForceAndVelocity;

    [SerializeField]
    private ForceMode forceType;

    [SerializeField]
    private float maxSpeed = 45f;

    [SerializeField]
    private float laneChangeSensitvity = 0.1f;

    [Header("Wheel collider")]
    [SerializeField]
    private WheelCollider[] powerWheels;

    [SerializeField]
    private WheelCollider[] turnWheels;

    [SerializeField]
    private GameObject centerOfMass;

    [SerializeField]
    private AnimationCurve turnCurve;
    // Start is called before the first frame update

    private bool explosionIneffect;
    private float explosionTimer = -1f;

    private Vector3 velocityDirectionVector = Vector3.forward;

    private float targetLocation;

    private void Awake() 
    {
        foreach(WheelCollider w in turnWheels)
        {
            w.steerAngle = 0;
        }
    }
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody>();
        rigidBody.centerOfMass = centerOfMass.transform.position;

        if(this.withWheelCollider)
        {
            foreach(WheelCollider w in powerWheels)
            {
                w.motorTorque = 1000f;
            }
        }

        if(this.withVelocity)
        {
            this.rigidBody.velocity = Vector3.forward * maxSpeed;
        }

        if(this.withForce)
        {
            this.rigidBody.AddForce(Vector3.forward * maxSpeed, forceType);
        }

        if(withForceAndVelocity)
        {
           this.rigidBody.AddForce(Vector3.forward * maxSpeed, ForceMode.Acceleration); 
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //BumperCopsDebug.Log(velocityDirectionVector);
        if(this.withForce)
        {
            AccelerateForce();
            return;
        }
        else if(this.withVelocity)
        {
            AccelerateVelocity();
            return;
        }
        else if(this.withWheelCollider)
        {
            AccelerateWheelCollider();
            return;
        } else if (this.withForceAndVelocity)
        {
            AccelerationForce();
            return;
        }

        /*if(Input.GetKeyDown(KeyCode.E))
        {
            if(explosionTimer < 0)
            {
                explosionTimer = Time.time;
            }

            rigidBody.constraints = RigidbodyConstraints.None;
            explosionIneffect = true;
            BumperCopsDebug.Log("Exploding");
            //rigidBody.AddExplosionForce(500000, Vector3.up, 50, 30);
            rigidBody.AddRelativeTorque(Vector3.up * 5000, ForceMode.Impulse);
        }
        
        float rotationLimit = Mathf.Clamp(Input.GetAxis("Mouse X"), -15, 15);
        transform.Rotate(new Vector3(0, rotationLimit, 0));*/
    }

    void AccelerateWheelCollider()
    {
        if(this.rigidBody.velocity.magnitude >= maxSpeed)
        {
            foreach(WheelCollider w in powerWheels)
            {
                BumperCopsDebug.Log("adjusting torque");
                w.motorTorque = 0.0001f;
            }
        }

        foreach(WheelCollider w in turnWheels)
        {
            w.steerAngle = 15  * Input.GetAxis("Horizontal");
            //BumperCopsDebug.Log(w.steerAngle);
        }
    }

    void AccelerateVelocity()
    {
        if(this.rigidBody.velocity.magnitude >= maxSpeed)
        {
            this.rigidBody.velocity = velocityDirectionVector.normalized * maxSpeed;
        } else 
        {
            this.rigidBody.velocity = velocityDirectionVector * maxSpeed;
        }
    }

    void AccelerateForce()
    {
        velocityDirectionVector = new Vector3(Input.GetAxis("Mouse X") * laneChangeSensitvity, 0, 0);
        if(this.rigidBody.velocity.magnitude < maxSpeed)
        {
            this.rigidBody.AddForce(Vector3.forward * maxSpeed, forceType);
        }
        else
        {
            this.rigidBody.AddForce(velocityDirectionVector.normalized, forceType);
        }
    }

    void AccelerationForce()
    {
        velocityDirectionVector = transform.TransformDirection(Vector3.forward);
        if (this.rigidBody.velocity.magnitude < maxSpeed)
        {
            this.rigidBody.AddForce(Vector3.forward * maxSpeed, ForceMode.Acceleration);
        }
        else if(Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(TurnForPedestrians());
        }
        else if(Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(Swerve());
        }
    }

    IEnumerator Swerve()
    {
        // get the center of the lane
        WheelCollider w = turnWheels.FirstOrDefault();
        WheelHit hit;
        w.GetGroundHit(out hit);

        targetLocation = hit.collider.GetComponent<LaneController>().NextLane.transform.position.x;
        Vector3 currentLocationVector = new Vector3(transform.position.x, 0, 0);
        Vector3 targetLocationVector = new Vector3(targetLocation, 0, 0);

        // first get to target location
        Vector3 direction = targetLocationVector - transform.position;
        this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));
        yield return new WaitWhile(() => this.transform.position.x < targetLocation);
        
        // now back to previous lane
        BumperCopsDebug.Log("now to previous lane");
        direction = currentLocationVector - transform.position;
        this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));
        yield return new WaitWhile(() => this.transform.position.x < currentLocationVector.x);

        transform.rotation = Quaternion.identity;
    }

    private AnimationCurve CreateKeyFrames()
    {
        List<Keyframe> keyframe = new List<Keyframe>();
        keyframe.Add(new Keyframe(0, transform.position.x));
        keyframe.Add(new Keyframe(0.25f, transform.position.x + 6.89f));
        keyframe.Add(new Keyframe(0.50f, transform.position.x));
        keyframe.Add(new Keyframe(1, transform.position.x + 6.89f));

        return new AnimationCurve(keyframe.ToArray());
    }

    IEnumerator TurnForPedestrians()
    {
        // get the center of the lane
        WheelCollider w = turnWheels.FirstOrDefault();
        WheelHit hit;
        w.GetGroundHit(out hit);

        targetLocation = hit.collider.GetComponent<LaneController>().NextLane.transform.position.x;
        
        Vector3 targetLocationVector = new Vector3(targetLocation, 0, 0);
        //this.rigidBody.AddForce(targetLocationVector.normalized * maxSpeed, ForceMode.Acceleration);
        Vector3 direction = targetLocationVector - transform.position;
        // sort of working
        this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));

        yield return new WaitWhile(() => this.transform.position.x < targetLocation);

        transform.rotation = Quaternion.identity;
    }
}
