using System;
using System.Linq;
using System.Collections;
using UnityEngine;

public class EnemyCarWithWheelColliders : MonoBehaviour {
    [SerializeField]
    private WheelCollider[] turnWheels;

    [SerializeField]
    private AnimationCurve turnForce;

    private Rigidbody rigidBody;
    private void Awake() {
        rigidBody = this.GetComponent<Rigidbody>();
    }
    
    private void FixedUpdate() 
    {
        AccelerationForce();
    }

    void AccelerationForce()
    {
        //velocityDirectionVector = transform.TransformDirection(Vector3.forward);
        if (this.rigidBody.velocity.magnitude < 65f)
        {
            this.rigidBody.AddForce(Vector3.forward * 30f, ForceMode.Acceleration);
        }
        else if(Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(DrunkDriving());
        }
        else if(Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(TurnWithForce());
        }
    }

    IEnumerator TurnWithForce()
    {
        rigidBody.constraints &= ~RigidbodyConstraints.FreezeRotationY;
        // get the center of the lane
        WheelCollider w = turnWheels.FirstOrDefault();
        BumperCopsDebug.Log(w.motorTorque);
        WheelHit hit;
        w.GetGroundHit(out hit);

        float targetLocation = hit.collider.GetComponent<LaneController>().NextLane.transform.position.x;
        float startTime = Time.time;
        while(transform.position.x < targetLocation)
        {
            Vector3 targetVector = new Vector3(targetLocation, transform.position.y, transform.position.z);
            Vector3 direction = targetVector - transform.position;
            this.rigidBody.AddForce(direction.normalized * turnForce.Evaluate(Time.time - startTime));
            yield return new WaitForFixedUpdate();
        }
        
        BumperCopsDebug.Log("current position is " + transform.position.x + " required is " + targetLocation);
        transform.rotation = Quaternion.identity;
        /*while(transform.rotation.y != 0)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 10);
            yield return new WaitForFixedUpdate();
        }*/

        rigidBody.constraints |= RigidbodyConstraints.FreezeRotationY;
    }

    IEnumerator TurnForPedestrians()
    {
        // get the center of the lane
        WheelCollider w = turnWheels.FirstOrDefault();
        WheelHit hit;
        w.GetGroundHit(out hit);

        float targetLocation = hit.collider.GetComponent<LaneController>().NextLane.transform.position.x;
        
        //this.rigidBody.AddForce(targetLocationVector.normalized * maxSpeed, ForceMode.Acceleration);

        while(this.transform.position.x < targetLocation)
        {
            Vector3 targetLocationVector = new Vector3(targetLocation, transform.position.y, transform.position.z);
            Vector3 direction = targetLocationVector - transform.position;
            this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));
            yield return new WaitForFixedUpdate();
        }

        transform.rotation = Quaternion.identity;
    }

    IEnumerator DrunkDriving()
    {
        BumperCopsDebug.Log("Start");
        WheelCollider w = turnWheels.FirstOrDefault();
        BumperCopsDebug.Log(w.motorTorque);
        WheelHit hit;
        w.GetGroundHit(out hit);
        float targetLocation = hit.collider.GetComponent<LaneController>().NextLane.transform.position.x - 1.5f;
        
        Vector3 targetLocationVector = new Vector3(targetLocation, 0, 0);
        Vector3 currentLocationVector = new Vector3(transform.position.x, 0, 0);
 
        BumperCopsDebug.Log("Current " + transform.position.x + " target " + targetLocationVector);
        while(this.rigidBody.transform.position.x < targetLocation)
        {
            yield return new WaitForFixedUpdate();
            Vector3 minuscleTarget = new Vector3(this.rigidBody.transform.position.x + (Time.deltaTime * 2), transform.position.y, transform.position.z);
            Vector3 direction = minuscleTarget - this.rigidBody.transform.position;
            this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));

           // BumperCopsDebug.Log(this.rigidBody.transform.position.x);
        }

        BumperCopsDebug.Log("Complete");
        while(transform.rotation.y > 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 5);
            yield return new WaitForFixedUpdate();
        }

        transform.rotation = Quaternion.identity;
        BumperCopsDebug.Log("Turning again");
        // now move to actual position
        while(this.rigidBody.transform.position.x > targetLocation)
        {
            yield return new WaitForFixedUpdate();
            Vector3 minuscleTarget = new Vector3(this.rigidBody.transform.position.x - Time.deltaTime, transform.position.y, transform.position.z);
            Vector3 direction = minuscleTarget - this.rigidBody.transform.position;
            this.rigidBody.transform.Rotate(new Vector3(0, direction.x, 0));

            BumperCopsDebug.Log(this.rigidBody.transform.position.x);
        }

        transform.rotation = Quaternion.identity;
    }
}