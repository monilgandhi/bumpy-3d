using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusBarBehaviour : MonoBehaviour 
{
    public event Action MegaSirenTurnedOn;
    public event Action MegaSirenTurnedOff;
    
    [Header("Game elements")]
    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private GameLevelManager gameLevelManager;

    [SerializeField]
    private ComboBehaviour comboBehaviour;

    [SerializeField]
    private CoinBonusBehaviour bonusBehaviour;

    [SerializeField]
    private PowerupManager powerupManager;

    [Header("Multiplier UI Elements")]

    [SerializeField]
    private Image multiplierFillBar;

    [Tooltip("Left followed by right")]
    [SerializeField]
    private List<BonusLightsBehaviour> lightsBehaviours;

    [SerializeField]
    private Image flashBang;

    [Tooltip("Admin")]
    [SerializeField]
    private bool enableBonusBar;
    
    public int BonusCompletionRate { get { return Mathf.RoundToInt(fillAmount * 100f); }}
    private int currentActionCount;
    private float fillAmount
    {
        get { return 0; }
        //get { return (float)currentActionCount / (float)this.gameLevelManager.BonusRequiredActionCount; }
    }
    private bool bonusComplete = true;
    private void Awake() 
    {
        bonusBehaviour.BonusEnded += () => bonusComplete = true;

        if(enableBonusBar)
        {
            comboBehaviour.ComboChanged += OnComboChanged;
        }
        
        powerupManager.PowerupCollected += OnPowerupCollected;
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args.Type != PowerupType.INSTANT_BONUS)
        {
            return;
        }

        StartCoroutine(PowerupCollected());
    }

    private IEnumerator PowerupCollected()
    {
        MegaSirenTurnedOn?.Invoke();
        StartCoroutine(FlashBang());
        this.player.ShakeCamera(10, 0.1f);

        yield return new WaitForSeconds(1f);
        MegaSirenTurnedOff?.Invoke();        
    }

    private void OnComboChanged(ComboEventArgs args)
    {
        if(args.NewCombo <= args.PreviousCombo || !bonusComplete || !enableBonusBar)
        {
            return;
        }

        // /BumperCopsDebug.Log($"new required count is {this.gameLevelManager.BonusRequiredActionCount}");
        // calculate the fill amount
        ++currentActionCount;
        StartCoroutine(AdjustBonusBar());

        if(fillAmount >= 1)
        {
            StartCoroutine(PlayMegaSirenForBonusBar());
        }
    }
    
    private IEnumerator FlashBang()
    {
        Color originalColor = flashBang.color;
        flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0);

        while(flashBang.color.a < 0.65)
        {
            flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, flashBang.color.a + (Time.deltaTime * 5));
            yield return new WaitForEndOfFrame();
        }

        while(flashBang.color.a > 0)
        {
            flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, flashBang.color.a - (Time.deltaTime * 5));
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator PlayMegaSirenForBonusBar()
    {
        bonusComplete = false;
        currentActionCount = 0;
        // turn off the fill bar
        this.multiplierFillBar.enabled = false;

        //MegaSirenTurnedOn?.Invoke();

        float startTime = Time.time;
        // play the bump effect
        if(this.player.BonusBodySlam != null)
        {
            this.player.BonusBodySlam.Play();
            yield return new WaitForSeconds(0.2f);
        }

        StartCoroutine(FlashBang());
        this.player.ShakeCamera(12, 0.2f);

        // play the wheel trails
        this.player.character.PlayWheelTrails();

        if(enableBonusBar)
        {
            lightsBehaviours.ForEach(l => l.BonusRoundTurnedOn());
        }
    

        yield return new WaitUntil(() => bonusComplete);

        BumperCopsDebug.Log("Readjusting the fillamount");
        StartCoroutine(AdjustBonusBar());

        if(enableBonusBar)
        {
            lightsBehaviours.ForEach(l => l.BonusRoundTurnedOff());
        }
    
        MegaSirenTurnedOff?.Invoke();
        this.multiplierFillBar.enabled = true;
    }   

    private IEnumerator AdjustBonusBar()
    {
        if(!this.multiplierFillBar.enabled || !this.gameObject.activeInHierarchy || !enableBonusBar)
        {
            yield break;
        }

        bool deduct = multiplierFillBar.fillAmount >= fillAmount;

        while((deduct && multiplierFillBar.fillAmount > fillAmount) || (!deduct && multiplierFillBar.fillAmount < fillAmount))
        {
            float delta = Mathf.Abs(multiplierFillBar.fillAmount - fillAmount) < Time.deltaTime ? Mathf.Abs(multiplierFillBar.fillAmount - fillAmount) : Time.deltaTime;
            delta = deduct ? delta * -1 : delta;

            // BumperCopsDebug.Log($"Delta is {delta}");
            multiplierFillBar.fillAmount += delta;
            yield return new WaitForEndOfFrame();
        }

        multiplierFillBar.fillAmount = fillAmount;
    }
}