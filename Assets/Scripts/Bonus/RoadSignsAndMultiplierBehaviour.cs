using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoadSignsAndMultiplierBehaviour : MonoBehaviour 
{
    public delegate void MegaSireEventHandler();
    public event MegaSireEventHandler MegaSirenTurnedOn;
    public event MegaSireEventHandler MegaSirenTurnedOff;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private CoinBonusBehaviour bonusBehaviour;

    [Header("Roadsign UI Elements")]
    [SerializeField]
    private Image forkLeft;
    
    [SerializeField]
    private Image forkRight;

    [SerializeField]
    private Image forkPanelImage;

    [Header("Multiplier UI Elements")]
    [SerializeField]
    private GameObject multiplierParentObject;

    [SerializeField]
    private TMP_Text multiplier;

    [SerializeField]
    private TMP_Text multiplierReasonText;

    [SerializeField]
    private Image multiplierFillBar;

    [Tooltip("Left followed by right")]
    [SerializeField]
    private List<BonusLightsBehaviour> lightsBehaviours;

    [SerializeField]
    private Image flashBang;
    
    private bool playerApproachingForkRoad;
    private ShakeText shakeText;
    private const string MULTIPLIER_SUFFIX_TEXT = "<size=50%>X</size>{0}";
    private const string MULTIPLIER_PREFIX_TEXT = "{0}<size=50%></size>X";

    //TODO this animation value is hardcoded. Would be good to figure out dynamically
    private const float MULTIPLIER_CAMERA_SHAKE_TIME = 0.8f;
    private float fillAmount = 0.0f;
    private bool bonusComplete = true;

    private void Hideall()
    {
        forkPanelImage.gameObject.SetActive(false);
    }

    private void Awake() 
    {
        multiplier.text = string.Format(MULTIPLIER_SUFFIX_TEXT, 1);
        shakeText = multiplier.GetComponent<ShakeText>();
        multiplierReasonText.transform.parent.gameObject.SetActive(false);
        bonusBehaviour.BonusEnded += () => bonusComplete = true;
    }

    private void Start() 
    {
        Hideall();
        player.MoveController.ApproachingForkRoad += () => 
        {
            playerApproachingForkRoad = true; 
            StartCoroutine(ShowForkSign());
        };

        player.MoveController.EnteredForkRoad += () => playerApproachingForkRoad = false; 
    }    

    private IEnumerator ShowForkSign()
    {
        // hide the multiplier parent object
        multiplierParentObject.SetActive(false);
        forkPanelImage.gameObject.SetActive(true);

        while(playerApproachingForkRoad)
        {
            forkPanelImage.gameObject.SetActive(!forkPanelImage.gameObject.activeSelf);
            yield return new WaitForSeconds(0.3f);
        }

        forkPanelImage.gameObject.SetActive(false);
        multiplierParentObject.SetActive(true);
    }

    public void NewMultiplier(int nextMultipler)
    {
        //BumperCopsDebug.Log("RoadSignsAndMultiplierBehaviour: Updating multiplier");
        StartCoroutine(NewMultiplierUIUpdate(nextMultipler));
        UpdateMultiplierBar(0);
    }

    public void UpdateMultiplierBar(int completionRate)
    {
        if(completionRate < 0)
        {
            BumperCopsDebug.LogWarning("Got a invalid completionRate");
            return;
        }

        // calculate the fill amount
        //BumperCopsDebug.Log($"Completion rate is  is {completionRate}");
        fillAmount = (float)completionRate * 0.01f;
        //BumperCopsDebug.Log($"Fill amount is {fillAmount}");
        StartCoroutine(IncreaseMultiplierBar());
        shakeText.IncreaseScale((int)(fillAmount * 100));
    }

    public void UpdateMultiplierText(string reason, int multiplier = 1)
    {
        if(string.IsNullOrEmpty(reason))
        {
            BumperCopsDebug.LogWarning("txt for multiplier text is empty");
            return;
        }

        string txt = reason;
        if(multiplier > 1)
        {
            txt = string.Format(MULTIPLIER_PREFIX_TEXT, multiplier) + $" {reason}";
        }

        StartCoroutine(UpdateMultiplierTextInternal(txt));
    }
    
    private IEnumerator NewMultiplierUIUpdate(int nextMultiplier)
    {
        this.multiplier.text = string.Format(MULTIPLIER_SUFFIX_TEXT, nextMultiplier);
        // play the animation
        Animator animator = this.multiplier.GetComponent<Animator>();
        animator.SetTrigger("newMultiplier");
        // on 0.4 seconds remaining with animation play camera shake

        yield return new WaitForSeconds(MULTIPLIER_CAMERA_SHAKE_TIME);
        StartCoroutine(PlayMegaSiren());
    }

    private IEnumerator FlashBang()
    {
        Color originalColor = flashBang.color;
        flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0);

        while(flashBang.color.a < 0.65)
        {
            flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, flashBang.color.a + (Time.deltaTime * 5));
            yield return new WaitForEndOfFrame();
        }

        while(flashBang.color.a > 0)
        {
            flashBang.color = new Color(originalColor.r, originalColor.g, originalColor.b, flashBang.color.a - (Time.deltaTime * 5));
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator PlayMegaSiren()
    {
        bonusComplete = false;

        // turn off the fill bar
        this.multiplierFillBar.enabled = false;

        if(MegaSirenTurnedOn != null)
        {
            MegaSirenTurnedOn();    
        }

        float startTime = Time.time;
        // play the bump effect
        if(this.player.BonusBodySlam != null)
        {
            this.player.BonusBodySlam.Play();
            yield return new WaitForSeconds(0.2f);
        }

        StartCoroutine(FlashBang());
        this.player.ShakeCamera(12, 0.2f);

        // play the wheel trails
        this.player.character.PlayWheelTrails();
        lightsBehaviours.ForEach(l => l.BonusRoundTurnedOn());

        yield return new WaitUntil(() => bonusComplete);
        lightsBehaviours.ForEach(l => l.BonusRoundTurnedOff());

        if(MegaSirenTurnedOff != null)
        {
            MegaSirenTurnedOff();    
        }

        this.multiplierFillBar.enabled = true;

        // fill up everything up until now
        UpdateMultiplierBar((int)fillAmount * 100);
    }   

    private IEnumerator IncreaseMultiplierBar()
    {
        if(!this.multiplierFillBar.enabled || !this.gameObject.activeInHierarchy)
        {
            yield break;
        }

        bool deduct = multiplierFillBar.fillAmount > fillAmount;
        while(multiplierFillBar.fillAmount != fillAmount)
        {
            float delta = Mathf.Abs(multiplierFillBar.fillAmount - fillAmount) < Time.deltaTime ? Mathf.Abs(multiplierFillBar.fillAmount - fillAmount) : Time.deltaTime;
            if(deduct)
            {
                delta *= -1;
            }

            //BumperCopsDebug.Log($"Delta is {delta}");
            multiplierFillBar.fillAmount += delta;
            yield return new WaitForEndOfFrame();
        }

        multiplierFillBar.fillAmount = fillAmount;
    }

    private IEnumerator UpdateMultiplierTextInternal(string txt)
    {
        if(!multiplierReasonText.gameObject.activeInHierarchy)
        {
            yield break;
        }
        
        multiplierReasonText.transform.parent.gameObject.SetActive(true);
        this.multiplierReasonText.text = txt;
        yield return new WaitForSeconds(0.5f);
        multiplierReasonText.transform.parent.gameObject.SetActive(false);
    }
}