using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinBonusBehaviour : MonoBehaviour 
{
    public event Action<CoinBonusEventArgs> BonusStarted;
    public event Action BonusEnded;
    private List<Texture2D> bonusMaps;
    private int bonusMapIndex;

    [SerializeField]
    private BonusBarBehaviour bonusBarBehaviour;

    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private RoadManager roadManager;

    [SerializeField]
    private ObstacleManager obstacleManager;

    private Color[] nextPixelMap;

    private PowerupManager PowerupManager;
    private GameManager gameManager;
    private bool bonusPending;

    private const float UNIT_PER_PIXEL_LENGTH = 15f;
    private const float UNIT_PER_PIXEL_WIDTH = 1.53f;
    private const int TEXTURE_MAP_SIZE = 20;
    private HashSet<GameObject> activeBarrierPool = new HashSet<GameObject>();
    private const string BONUS_MAP_FOLDER = "bonusmap";
    private const int POOL_SIZE = 100;

    private void Awake() 
    {
        bonusMaps = LoadBonusMaps();
        bonusMaps.Shuffle();
    }

    private void Start() 
    {
        gameManager = this.GetComponent<GameManager>();
        PowerupManager = this.gameManager.PowerupManager;
        // NOTE when using bonus bar
        //bonusBarBehaviour.MegaSirenTurnedOn += OnMultiplierIncreased;
        enemyManager.EnemyHealthOver += (args) => OnMultiplierIncreased();
    }    

    private void Update() 
    {
        if(Input.GetKeyDown(KeyCode.B))
        {
            bonusPending = true;
        }

        if(!bonusPending)
        {
            return;
        }    

        RoadController startRoad = roadManager.GetRoadForBonuses();
        if(startRoad != null)
        {
            bonusPending = false;
            //BumperCopsDebug.Log("Placing bonus");
            PlaceBonus(startRoad);
        }
    }

    private void OnMultiplierIncreased() 
    {
        bonusPending = true;
    }

    private void LateUpdate() 
    {
        if(activeBarrierPool.Count == 0)
        {
            return;
        }    

        List<GameObject> itemsToDeactivate = new List<GameObject>();
        foreach(GameObject a in activeBarrierPool)
        {
            // if the player has crossed 1 road
            if(a.transform.position.z < this.gameManager.Player.transform.position.z - 50)
            {
                a.SetActive(false);
                itemsToDeactivate.Add(a);
            }
        }

        foreach(GameObject a in itemsToDeactivate)
        {
            activeBarrierPool.Remove(a);
        }
    }

    private void PlaceBonus(RoadController startRoad)
    {
        nextPixelMap = bonusMaps[bonusMapIndex++].GetPixels();
        if(bonusMapIndex >= bonusMaps.Count)
        {
            bonusMapIndex = 0;
            bonusMaps.Shuffle();
        }
        
        PlaceBonus(startRoad, nextPixelMap);
    }

    public void PlaceBonus(RoadController startRoad, Color[] pixelMap)
    {
        Vector3 referencePosition = startRoad.LeftBottomWorldStart;
        int x = 0;
        int y = 0;

        float zPosition = startRoad.transform.position.z;
        int coinCount = 0;
        for(int i = 0; i < pixelMap.Length; i++)
        {
            zPosition = referencePosition.z + (y * UNIT_PER_PIXEL_LENGTH);
            //BumperCopsDebug.Log($"BonusBehaviour: x :{referencePosition.x + (x * UNIT_PER_PIXEL_WIDTH)}");
            Vector3 position = new Vector3(referencePosition.x + (x * UNIT_PER_PIXEL_WIDTH), this.gameManager.Player.RigidBody.transform.position.y, zPosition);
            ++x;
            
            if(x >= TEXTURE_MAP_SIZE)
            {
                x = 0;
                ++y;
            } 
            
            if (pixelMap[i] == Color.yellow)
            {
                ++coinCount;
                PowerupManager.GeneratePowerup(PowerupType.COINS, position, true);
            }
        }

        BonusStarted?.Invoke(new CoinBonusEventArgs(startRoad.MyPathNumber + 1, coinCount));
        StartCoroutine(EndBonus(zPosition));
    }

    private IEnumerator EndBonus(float lastConeZ)
    {
        yield return new WaitUntil(() => this.gameManager.Player.transform.position.z + 20 > lastConeZ);
        
        if(BonusEnded != null)
        {
            //BumperCopsDebug.Log("Ending bonus");
            BonusEnded();
        }
    }

    public static List<Texture2D> LoadBonusMaps(string folder = BONUS_MAP_FOLDER)
    {
        Texture2D[] textureMaps = Resources.LoadAll<Texture2D>(folder);
        return new List<Texture2D>(textureMaps);
    }
}

public class CoinBonusEventArgs
{
    public readonly long EndRoadNumber;
    public readonly int TotalCoins;
    public CoinBonusEventArgs(long endRoadNumber, int totalCoins)
    {
        this.EndRoadNumber = endRoadNumber;
        this.TotalCoins = totalCoins;
    }
}