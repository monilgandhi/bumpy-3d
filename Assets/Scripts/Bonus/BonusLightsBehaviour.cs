using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BonusLightsBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image screenPanels;

    [SerializeField]
    private Image lightWithGlow;
    
    [SerializeField]
    private float frequency = 2;
    
    [SerializeField]
    private float amplitude= 4;
    private float phase = 0.0f; // start point inside on wave cycle
    private float baseStart = 0.0f; // start 
    private bool areLightsFlashing;
    private Color originalColor;
    private List<ParticleSystem> wheelTrails;

    private void Awake() 
    {
        screenPanels.enabled = false;
        if(lightWithGlow != null)
        {
            lightWithGlow.enabled = false;
        }
        
        originalColor = screenPanels.color;
    }

    public void ToggleScreenPanels(bool enable)
    {
        screenPanels.enabled = enable;
        if(enable)
        {
            StartCoroutine(FlashLights());
        }
    }

    public void BonusRoundTurnedOn()
    {
        areLightsFlashing = true;
        lightWithGlow.enabled = true;
        screenPanels.enabled = true;
        StartCoroutine(FlashLights());
    }

    public void BonusRoundTurnedOff()
    {
        //BumperCopsDebug.Log("Turning off bonus round");
        areLightsFlashing = false;
        lightWithGlow.enabled = false;
        screenPanels.enabled = false;
    }

    private IEnumerator FlashLights()
    {
        while(areLightsFlashing)
        {
            float waveParam = EvalWave();

            if(lightWithGlow.enabled)
            {
                lightWithGlow.color = Color.white * waveParam;
            }
            
            screenPanels.color = new Color(originalColor.r * waveParam, originalColor.g * waveParam, originalColor.b * waveParam, originalColor.a);
            yield return new WaitForEndOfFrame();
        }
    }

    private float EvalWave()
    {
        float x = (Time.time + phase) * frequency;
        float y;
        x = x - Mathf.Floor(x); // normalized value (0..1)
        
        // Tri wave from LightBehaviour
        if (x < 0.5f)
            y = 4.0f * x - 1.0f;
        else
            y = -4.0f * x + 3.0f;
        return (y * amplitude) + baseStart;
    }
}
