using UnityEngine;

public abstract class BumperCopsAnalyticsPlatformAbstract
{
 
    protected int totalCollisions { get; private set;}
    protected bool isTutorialActive { get; private set;}
    protected float sessionStartTime { get; private set;}
    protected long currentSessionNumber { get; private set;}
    protected int extensionAdsInThisSession { get; private set;}
    protected int totalTimeExtensionsThisSession { get; private set;}
    protected bool isAnalyticsOn { get; private set;}
    protected float donutsUsed { get; private set;}
    protected float donutsGenerated { get; private set;}
    protected float donutsPickedup { get; private set;}
    protected float playTimeSeconds { get; private set; }

    protected float donutsPickedupPercent { get; private set; }
    protected float donutUsedPercent { get; private set; }

    protected const string IN_GAME_PURCHASE = "in_game_purchase";
    protected const string TIME_EXTENSION = "time_extension";
    protected const string TUTORIAL_DESIGN_EVENT_P1 = "tutorial";
    protected const string EARNED_COINS = "coins_earned";
    protected const string NEAR_MISS_COINS = "near_miss_coins";
    protected const string NEAR_MISS = "nearmiss";
    protected const string ENEMY_COINS = "enemy_coins";
    protected const string ENEMY_ARRESTED = "enemies_arrested";
    protected const string GAME_SESSION = "game_session";

    protected const string DONUTS_PICKED_UP = "donuts";
    protected const string DONUTS_USED = "donuts_used";

    protected const string OBJECTIVE_COMPLETE = "objective_complete";
    protected const string GENERIC_ID = "id";
    protected const string ITEM_ID = "itemId";
    protected const string ITEM_TYPE = "item_type";
    
    protected const string RANK = "rank";
    protected const string REWARD = "reward";

    protected const string OBSTACLE_COLLISION = "obstacle_collision";
    protected const string SESSION_TIME = "session_time";
    protected const string TUTORIAL_STEP_START = "tutorial_step_start";
    protected const string TUTORIAL_STEP_END= "tutorial_step_end";
    protected const string UTILITY_BELT = "ub";
    protected const string UTILITY_BELT_PAID = "ub_paid";
    protected const string CHALLENGE ="challenge";

    public virtual void Init()
    {
        donutsGenerated = 0;
        donutUsedPercent = 0;
        donutsPickedup = 0;
        playTimeSeconds = 0;
    }

    public virtual void OnPowerupSpawned(PowerupType powerupType)
    {
        if (powerupType == PowerupType.DONUT)
        {
            ++donutsGenerated;
        }
    }

    public virtual void OnPowerupCollected(PowerupType powerupType)
    {
        if (powerupType == PowerupType.DONUT)
        {
            ++donutsPickedup;
        } else if (powerupType == PowerupType.WEAPON || powerupType == PowerupType.BIG_BOOST || powerupType == PowerupType.SIREN || powerupType == PowerupType.SHIELD)
        {
            ++donutsUsed;
        }
    }

    public virtual void OnEnemyHealthOver()
    {
    }

    public virtual void OnPlayerCollidedWithObstacles()
    {
        ++totalCollisions;
    }

    protected void GameOver()
    {
        playTimeSeconds = Time.time - sessionStartTime;
        // record play time

        if(donutsGenerated > 0)
        {
            donutsPickedupPercent = Mathf.RoundToInt(donutsPickedup/donutsGenerated * 100);
            if(donutsPickedup > 0)
            {
                donutUsedPercent = Mathf.RoundToInt(donutsUsed/donutsPickedup * 100);
            }
        }
    }

    public virtual void OnGameStarted(long sessionNumber)
    {
        sessionStartTime = Time.time;
        currentSessionNumber = sessionNumber;

        string sessionDimension = $"session_{sessionNumber}";
        if(sessionNumber == 2)
        {
            sessionDimension = "session_2";
        } else if(sessionNumber == 3)
        {
            sessionDimension = "session_3";
        } else if (sessionNumber > 3)
        {
            sessionDimension = "session_gt_3";
        }
    }

    public virtual void OnTimeExtended(bool isAds, int cost, int extensionTime)
    {
        ++totalTimeExtensionsThisSession;
        if(isAds)
        {
            ++extensionAdsInThisSession;
        }
    }

    public virtual void OnTutorialStarted()
    {
        this.isTutorialActive = true;
    }

    public virtual void OnTutorialEnded()
    {
        isTutorialActive = false;
    }
}