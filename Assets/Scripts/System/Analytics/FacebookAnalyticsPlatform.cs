using UnityEngine.Purchasing.Security;
using System.Collections.Generic;
using Facebook.Unity;
using BumperCops.Models;

public class FacebookAnalyticsPlatform : BumperCopsAnalyticsPlatformAbstract, IAnalyticsPlatform
{
    public bool HasInited { get; private set; }
    public override void Init()
    {
        base.Init();
        if (!FB.IsInitialized) {
            // Initialize the Facebook SDK
            FB.Init(FBInitCallback);
        } else {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
            HasInited = true;
        }
    }

    private void FBInitCallback ()
    {
        if (FB.IsInitialized) {
            // Signal an app activation App Event
            FB.ActivateApp();
            HasInited = true;
        } else {
            BumperCopsDebug.LogWarning("Failed to Initialize the Facebook SDK");
        }
    } 

    public void ReportCoinsCollected(long totalCoins, long enemyCoins, long nearMissCoins)
    {
        if(totalCoins <= 0)
        {
            return;
        }

        FB.LogAppEvent (
            EARNED_COINS,
            totalCoins
        );

        FB.LogAppEvent (
            NEAR_MISS_COINS,
            nearMissCoins
        );

        FB.LogAppEvent (
            ENEMY_COINS,
            enemyCoins
        );
    }

    public void OnButtonClicked(string eventName)
    {
        FB.LogAppEvent (
            eventName,
            1
        );
    }

    public void ReportGameStats(int nearMisses, int enemiesArrested)
    {
        FB.LogAppEvent (
            NEAR_MISS,
            nearMisses
        );

        FB.LogAppEvent (
            ENEMY_ARRESTED,
            enemiesArrested
        );
    }

    public override void OnGameStarted(long sessionNumber)
    {
        base.OnGameStarted(sessionNumber);
        FB.LogAppEvent(
            GAME_SESSION,
            1,
            new Dictionary<string, object>()
            {
                { GAME_SESSION, currentSessionNumber }
            }
        );
    }

    public void OnGameOver()
    {
        base.GameOver();
        if(donutsGenerated > 0)
        {
            FB.LogAppEvent(
                DONUTS_PICKED_UP,
                donutsPickedupPercent
            );
        }

        if(donutsPickedup > 0)
        {
            FB.LogAppEvent(
                DONUTS_USED,
                donutUsedPercent
            );
        }

        FB.LogAppEvent(
            OBSTACLE_COLLISION,
            totalCollisions
        );

        FB.LogAppEvent(
            SESSION_TIME,
            playTimeSeconds,
            new Dictionary<string, object>()
            {
                { GAME_SESSION, currentSessionNumber }
            }
        );
    }

    public override void OnTutorialEnded()
    {
        base.OnTutorialEnded();
        FB.LogAppEvent (
            AppEventName.CompletedTutorial
        );

    }

    public void OnAdEvent(AdEventType type)
    {}
    
    public override void OnTimeExtended(bool isAds, int cost, int extensionTime)
    {
        if(isTutorialActive)
        {
            FB.LogAppEvent(
                $"{TIME_EXTENSION}_tutorial",
                1
            );

            return;
        }

        base.OnTimeExtended(isAds, cost, extensionTime);
        // record how many times is the user extending time per session
        if(isAds)
        {
            // record how many times the user is selecting ads per session
            FB.LogAppEvent (
                TIME_EXTENSION,
                1,
                parameters: new Dictionary<string, object>() 
                {
                    { GAME_SESSION, currentSessionNumber },
                    { "item", "extension" },
                    { "extensionCount", totalTimeExtensionsThisSession - 1},
                    { "adCount", extensionAdsInThisSession - 1 }
                }
            );
        }
        else
        {
            // rercord the sing in coins
            FB.LogAppEvent (
                IN_GAME_PURCHASE,
                1,
                parameters: new Dictionary<string, object>() 
                {
                    { "item_type", "extension" }
                }
            );
        }
    }

    public void OnTutorialStepUpdate(bool isStepActive, TutorialStep step, bool isStepComplete)
    {
        if(!this.isTutorialActive)
        {
            return;
        }

        if(isStepComplete)
        {
            FB.LogAppEvent (
                TUTORIAL_STEP_END,
                parameters: new Dictionary<string, object>() 
                {
                    { "name", step.ToString() }
                }
            );
        }
        else if(isStepActive)
        {
            FB.LogAppEvent (
                TUTORIAL_STEP_START,
                parameters: new Dictionary<string, object>() 
                {
                    { "name", step.ToString() }
                }
            );
        }
    }
    
    public void OnScoreFinalized(long newScore, bool isHighScore, string newRank)
    {
        if(string.IsNullOrEmpty(newRank))
        {
            return;
        }
        
        if(isHighScore)
        {
            FB.LogAppEvent (
                $"{RANK}_{newRank}",
                1
            );
        }
    }

    public void OnCoinsSpent(long amountSpent, string itemId, string reason)
    {
        if(amountSpent <= 0)
        {
            return;
        }

        FB.LogAppEvent(
            "coins_spent",
            amountSpent,
            new Dictionary<string, object>()
            {
                { GAME_SESSION, currentSessionNumber },
                { "itemId", ITEM_ID },
            }
        );
    }

    public void OnPurchaseMade(int itemcost, ShopItemType itemType, string itemId)
    {
        if(string.IsNullOrEmpty(itemId) || itemType == ShopItemType.NONE)
        {
            BumperCopsDebug.LogWarning($"Invalid itemid {itemId} or itemtype {itemType}");
            return;
        }

        FB.LogAppEvent (
            IN_GAME_PURCHASE,
            1,
            parameters: new Dictionary<string, object>() 
            {
                { ITEM_TYPE, itemType.ToString().ToLower() },
                { ITEM_ID, itemId}
            }
        );
    }

    public void OnIAPEvent(IAPProduct product, 
        string receipt, 
        IPurchaseReceipt PurchaseReceipt,
        string isoCurrencyCode,
        decimal localizedPrice)
    {
        return;
    }

    public void OnObjectiveComplete(string completedObjectiveId)
    {
        if(string.IsNullOrEmpty(completedObjectiveId))
        {
            return;
        }

        FB.LogAppEvent (
            OBJECTIVE_COMPLETE,
            1,
            new Dictionary<string, object>()
            {
                { GENERIC_ID, completedObjectiveId }
            }
        );
    }

    public void OnRewardGiven(IReadOnlyList<Reward> rewards)
    {
        if(rewards == null || rewards.Count <= 0)
        {
            return;
        }

        foreach(Reward r in rewards)
        {
            FB.LogAppEvent(
                $"{REWARD}_{r.Type}",
                r.Amount
            );
        }
    }

    public void OnUtilitBeltPowerupActived(PowerupType type, int cost)
    {
        if(cost > 0)
        {
            FB.LogAppEvent(
                $"{UTILITY_BELT_PAID}_{type}",
                1
            );
        }
        else
        {
            FB.LogAppEvent(
                $"{UTILITY_BELT}_{type}",
                1
            );
        }
    }

    public void OnChallengeCreated(Challenge c)
    {
        FB.LogAppEvent(
            $"{CHALLENGE}_bet",
            c.BuyIn
        );
    }

    public void OnChallengeManagerOpened()
    {
        FB.LogAppEvent(
            $"{CHALLENGE}_open",
            1
        );
    }

    public void OnChallengeNotCreated()
    {
        FB.LogAppEvent(
            $"{CHALLENGE}_create_cancel",
            1
        );
    }

    public void OnChallengeWon()
    {
        FB.LogAppEvent(
            $"{CHALLENGE}_wins",
            1
        );
    }
}