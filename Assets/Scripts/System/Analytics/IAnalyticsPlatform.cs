using System.Collections.Generic;
using BumperCops.Models;
using UnityEngine.Purchasing.Security;
public interface IAnalyticsPlatform
{
    bool HasInited { get; }
    void Init();
    void OnGameStarted(long sessionNumber);
    void OnGameOver();
    void OnTutorialEnded();
    void OnTutorialStarted();
    void OnTimeExtended(bool isAds, int cost, int extensionTime);
    void OnPlayerCollidedWithObstacles();
    void OnEnemyHealthOver();
    void OnPowerupSpawned(PowerupType powerupType);
    void OnPowerupCollected(PowerupType powerupType);
    void OnScoreFinalized(long newScore, bool isHighScore, string newRank);
    void OnCoinsSpent(long amount, string itemId, string reason);
    void OnPurchaseMade(int itemcost, ShopItemType itemType, string itemId);
    void OnIAPEvent(IAPProduct product, 
        string receipt, 
        IPurchaseReceipt PurchaseReceipt,
        string isoCurrencyCode,
        decimal localizedPrice);
    void OnButtonClicked(string eventName);
    void OnObjectiveComplete(string objectiveId);
    void OnRewardGiven(IReadOnlyList<Reward> rewards);
    void OnUtilitBeltPowerupActived(PowerupType type, int cost);
    void OnTutorialStepUpdate(bool isStepActive, TutorialStep step, bool isStepComplete);
    void ReportCoinsCollected(long totalCoins, long enemyCoins, long nearMissCoins);
    void ReportGameStats(int nearMisses, int enemiesArrested);
    void OnAdEvent(AdEventType eventType);
    void OnChallengeCreated(Challenge c);
    void OnChallengeManagerOpened();
    void OnChallengeNotCreated();
    void OnChallengeWon();
}
