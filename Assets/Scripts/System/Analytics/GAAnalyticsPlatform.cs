using System;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using BumperCops.Models;

public class GAAnalyticsPlatform : BumperCopsAnalyticsPlatformAbstract, IAnalyticsPlatform 
{
    public bool HasInited { get; private set; }
    public override void Init()
    {
        GameAnalytics.Initialize();   
        HasInited = true;
    }

    public override void OnGameStarted(long sessionNumber)
    {
        base.OnGameStarted(sessionNumber);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, GAME_SESSION, currentSessionNumber.ToString());
    }

    public void OnGameOver()
    {
        base.GameOver();
        GameAnalytics.NewDesignEvent(SESSION_TIME, playTimeSeconds);
        
        if(donutsGenerated > 0)
        {
            GameAnalytics.NewDesignEvent(DONUTS_PICKED_UP, donutsPickedupPercent);
            
            if(donutsPickedup > 0)
            {
                GameAnalytics.NewDesignEvent(DONUTS_USED, donutUsedPercent);
            }
        }

        GameAnalytics.NewDesignEvent(OBSTACLE_COLLISION, totalCollisions);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, GAME_SESSION, currentSessionNumber.ToString());
    }

    public void ReportCoinsCollected(long totalCoins, long enemyCoins, long nearMissCoins)
    {
        GameAnalytics.NewDesignEvent(EARNED_COINS, totalCoins);
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "coins", nearMissCoins, NEAR_MISS_COINS, NEAR_MISS_COINS);
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "coins", enemyCoins, ENEMY_COINS, ENEMY_COINS);
    }

    public void ReportGameStats(int nearMisses, int enemiesArrested)
    {
        GameAnalytics.NewDesignEvent(NEAR_MISS, nearMisses);
        GameAnalytics.NewDesignEvent(ENEMY_ARRESTED, enemiesArrested);
    }

    public override void OnTutorialEnded()
    {
        base.OnTutorialEnded();
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, TUTORIAL_DESIGN_EVENT_P1, TutorialStep.OVER.ToString());
    }

    public void OnTutorialStepUpdate(bool isStepActive, TutorialStep step, bool isStepComplete)
    {
        if(!isTutorialActive)
        {
            return;
        }

        if(isStepComplete)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, TUTORIAL_DESIGN_EVENT_P1, step.ToString());
        }
        else if(isStepActive)
        {
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, TUTORIAL_DESIGN_EVENT_P1, step.ToString());
        }
    }

    public override void OnTimeExtended(bool isAds, int cost, int extensionTime)
    {
        base.OnTimeExtended(isAds, cost, extensionTime);
        GameAnalytics.NewDesignEvent($"{TIME_EXTENSION}", totalTimeExtensionsThisSession);

        if(isAds)
        {
            // record how many times the user is selecting ads per session
            GameAnalytics.NewDesignEvent($"{TIME_EXTENSION}:ads", extensionAdsInThisSession);
        }
        else
        {
            // rercord the sing in coins
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "coins", cost, "extension", extensionTime.ToString());
        }
    }

    public void OnButtonClicked(string eventName)
    {
        return;
    }
    
    public void OnScoreFinalized(long newScore, bool isHighScore, string newRank)
    {
        if(string.IsNullOrEmpty(newRank) || !isHighScore)
        {
            return;
        }
        
        GameAnalytics.SetCustomDimension03(newRank);
    }
    public void OnCoinsSpent(long amount, string itemId, string reason)
    {
        if(amount <= 0 || string .IsNullOrEmpty(itemId) || string .IsNullOrEmpty(reason))
        {
            BumperCopsDebug.LogWarning($"Invalid arguments amount: {amount}, itemId: {itemId}, reason: {reason}");
            return;
        }

        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "coins", amount, reason, itemId);
    }

    public void OnPurchaseMade(int itemcost, ShopItemType itemType, string itemId)
    {
        if(itemcost <= 0 || itemType == ShopItemType.NONE || string.IsNullOrEmpty(itemId))
        {
            BumperCopsDebug.LogWarning($"Invalid arguments itemcost: {itemcost}, itemId: {itemId}, type: {itemType}");
            return;
        }

        SetPurchaseDimension();
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "coins", itemcost, itemType.ToString(), itemId);
    }

    public void OnIAPEvent(
        IAPProduct product, 
        string receipt, 
        IPurchaseReceipt PurchaseReceipt,
        string isoCurrencyCode,
        decimal localizedPrice)
    {
        if(product == null || ((string.IsNullOrEmpty(receipt) || PurchaseReceipt == null || product.Definition == null) && !Application.isEditor))
        {
            BumperCopsDebug.LogError("Purchase event args are invalid");
            return;
        }

        string currencyCode = isoCurrencyCode;
        int amountInCents = Convert.ToInt32(localizedPrice * 100m);
        string itemType = "coins";

#if UNITY_EDITOR
        if(Application.isEditor)
        {
            GameAnalytics.NewBusinessEvent(currencyCode, amountInCents, itemType, product.Definition.id, "shop");
        } 

#elif UNITY_ANDROID && !UNITY_EDITOR
        if (StandardPurchasingModule.Instance().appStore == AppStore.GooglePlay)
        {
            GooglePlayReceipt gpReceipt = PurchaseReceipt as GooglePlayReceipt;
            if(gpReceipt == null)
            {
                BumperCopsDebug.LogError("Google play receipt is null");
                return;
            }

            GameAnalytics.NewBusinessEventGooglePlay(currencyCode, amountInCents, itemType, product.Definition.id, "shop", receipt, gpReceipt.purchaseToken);
        }
#endif
    }

    public void OnAdEvent(AdEventType type)
    {
        switch(type)
        {
            case AdEventType.AD_FAILED:
                GameAnalytics.NewAdEvent(GAAdAction.FailedShow, 
                    GAAdType.RewardedVideo, 
                    "admob", 
                    Enum.GetName(typeof(AdEventType), type));
                break;

            case AdEventType.AD_CLOSED:
                GameAnalytics.NewAdEvent(GAAdAction.Show, 
                    GAAdType.RewardedVideo, 
                    "admob",
                    Enum.GetName(typeof(AdEventType), type));
                break;
            
            case AdEventType.AD_REWARD_EARNED:
                GameAnalytics.NewAdEvent(GAAdAction.RewardReceived, 
                    GAAdType.RewardedVideo, 
                    "admob", 
                    Enum.GetName(typeof(AdEventType), type));
                break;
        }
    }

    public void OnObjectiveComplete(string objectiveId)
    {
        if(string.IsNullOrEmpty(objectiveId))
        {
            BumperCopsDebug.LogWarning("objective id is null");
            return;
        }

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "objective", objectiveId);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "objective", objectiveId);
    }

    public void OnRewardGiven(IReadOnlyList<Reward> rewards)
    {
        if(rewards == null)
        {
            BumperCopsDebug.LogWarning("rewards is null");
            return;
        }

        foreach(Reward r in rewards)
        {
            if(r.Type == CollectibleType.BAG_COINS || r.Type == CollectibleType.COIN)
            {
                GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "coins", r.Amount, "reward", r.Reason.ToString());
            }
            else
            {
                GameAnalytics.NewDesignEvent($"reward:{r.Reason}:{r.Type}", r.Amount);   
            }
        }
    }

    public void OnUtilitBeltPowerupActived(PowerupType type, int cost)
    {
        GameAnalytics.NewDesignEvent($"{UTILITY_BELT}:{type}", 1);
        if(cost > 0)
        {
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "coins", cost, "utility-belt", type.ToString());
        }
    }

    private void SetPurchaseDimension()
    {
        bool hasMadeCarPurchase = PlayerStats.Instance.HasPurchasedCar();
        bool hasMadeWeaponPurchase = PlayerStats.Instance.HasPurchasedWeapon();
        bool hasMadeSkinPurchase = PlayerStats.Instance.HasPurchasedSkin();

        // only car
        if(hasMadeCarPurchase && !hasMadeSkinPurchase && !hasMadeWeaponPurchase)
        {
            GameAnalytics.SetCustomDimension01("car");
        } else if (!hasMadeCarPurchase && hasMadeSkinPurchase && !hasMadeWeaponPurchase)
        {
            // only skin
            GameAnalytics.SetCustomDimension01("skin");
        } else if (!hasMadeCarPurchase && !hasMadeSkinPurchase && hasMadeWeaponPurchase)
        {
            // only weapon
            GameAnalytics.SetCustomDimension01("weapon");
        } else if (hasMadeCarPurchase && hasMadeSkinPurchase && !hasMadeWeaponPurchase)
        {
            // car & skin
            GameAnalytics.SetCustomDimension01("car_skin");
        } else if (hasMadeCarPurchase && !hasMadeSkinPurchase && hasMadeWeaponPurchase)
        {
            // car & weapon
            GameAnalytics.SetCustomDimension01("car_weapon");
        } else if (!hasMadeCarPurchase && hasMadeSkinPurchase && hasMadeWeaponPurchase)
        {
            // weapon & skin
            GameAnalytics.SetCustomDimension01("weapon_skin");
        } else if (hasMadeCarPurchase && hasMadeSkinPurchase && hasMadeWeaponPurchase)
        {
            // all others
            GameAnalytics.SetCustomDimension01("car_weapon_skin");
        }
    }

    public void OnChallengeCreated(Challenge c)
    {
        GameAnalytics.NewDesignEvent($"{CHALLENGE}:create:bet", c.BuyIn);
    }

    public void OnChallengeManagerOpened()
    {
        GameAnalytics.NewDesignEvent($"{CHALLENGE}:open", 1);
    }

    public void OnChallengeNotCreated()
    {
        GameAnalytics.NewDesignEvent($"{CHALLENGE}:create-cancel", 1);
    }

    public void OnChallengeWon()
    {
        GameAnalytics.NewDesignEvent($"{CHALLENGE}:wins", 1);
    }
}