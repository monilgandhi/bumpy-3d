using System.Collections.Generic;
using UnityEngine;

public class BumperCopsGameplayAnalytics : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private ChallengeManager challengeManager;

    [SerializeField]
    private GameplayAssignmentManager gameplayAssignmentManager;

    [SerializeField]
    private ScoreAndRewardTransitionBehaviour scoreAndRewardTransitionBehaviour;

    [SerializeField]
    private AdsManager adsManager;

    [SerializeField]
    private ObstacleManager obstacleManager;

    [SerializeField]
    private UtilityBeltBehaviour utilityBeltBehaviour;

    private bool isAnalyticsOn;
    private List<IAnalyticsPlatform> analyticsPlatforms;

    public void ReportCoinsCollected(long coinAmount, long nearMissCoins, long enemyCoins)
    {
        if(coinAmount <= 0 || !isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            a.ReportCoinsCollected(coinAmount, nearMissCoins, enemyCoins);
        }
    }

    private bool CanUsePlatform(IAnalyticsPlatform a)
    {
        return a.HasInited;
    }

    public void ReportStats(int nearMissCount, int enemyCount)
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            a.ReportGameStats(nearMissCount, enemyCount);
        }
    }

    private void Awake() 
    {
        isAnalyticsOn = !CarUtils.IsUnityDevelopmentBuild() || Application.isEditor;

        BumperCopsDebug.Log($"Is analytics on {isAnalyticsOn}, dev build {CarUtils.IsUnityDevelopmentBuild()}");
        if(!isAnalyticsOn)
        {
            this.gameObject.SetActive(false);
            return;
        }

        analyticsPlatforms = new List<IAnalyticsPlatform>();
        analyticsPlatforms.Add(new FacebookAnalyticsPlatform());
        analyticsPlatforms.Add(new GAAnalyticsPlatform());
        analyticsPlatforms.Add(new FirebaseAnalyticsPlatform());
    }

    private void Start() 
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        // initialize the analytics
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            a.Init();
        }

        gameManager.GameSessionInitialized += OnGameStarted;
        gameManager.GameTutorialSessionInitialized += OnGameStarted;
        gameManager.GameOver += OnGameOver;
        gameManager.TutorialManager.TutorialEnd += OnTutorialEnded;
        gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
        gameManager.TutorialManager.TutorialStarted += OnTutorialStarted;

        gameManager.TimerExtendBehaviour.TimeExtended += OnTimeExtended;
        gameManager.Player.healthController.PlayerObstacleCollided += OnPlayerCollidedWithObstacles;
        
        gameManager.EnemyManager.EnemyHealthOver += OnEnemyHealthOver;
        gameManager.PowerupManager.PowerupGenerated += OnPowerupSpawned;
        gameManager.PowerupManager.PowerupCollected += OnPowerupCollected;

        // score events
        gameManager.ScoreBehaviour.ScoreFinalized += OnScoreFinalized;
        // shop events
        gameManager.InGameShopManager.CoinsSpent += OnCoinsSpent;
        gameManager.InGameShopManager.CarPurchased += OnPurchaseMade;
        gameManager.InGameShopManager.SkinPurchased += OnPurchaseMade;
        gameManager.InGameShopManager.WeaponPurchased += OnPurchaseMade;
        gameManager.InGameShopManager.IAPurchaseManager.ProductPurchased += OnIAPEvent;
        
        // missions
        gameplayAssignmentManager.ObjectiveComplete += OnObjectiveComplete;

        //rewards
        scoreAndRewardTransitionBehaviour.RewardGiven += OnRewardGiven;

        // utility belt
        utilityBeltBehaviour.PowerupActivated += OnPowerupActivated;

        // challenge manager;
        challengeManager.ChallengeCreated += OnChallengeCreated;
        challengeManager.ChallengeManagerOpened += OnChallengeManagerOpened;
        challengeManager.ChallengeNotCreated += OnChallengeNotCreated;
        challengeManager.ChallengeWon += OnChallengeWon;
        adsManager.AdEvent += OnAdEvent;
    }    

    private void OnChallengeWon()
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }

            a.OnChallengeWon();
        }
    }

    private void OnChallengeCreated(Challenge c)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            a.OnChallengeCreated(c);
        }
    }

    private void OnChallengeManagerOpened()
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnChallengeManagerOpened();
        }
    }

    private void OnChallengeNotCreated()
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnChallengeNotCreated();
        }
    }

    
    private void OnPowerupActivated(UtilityBeltEventArgs args)
    {
        if(args == null || args.Type == PowerupType.NONE)
        {
            BumperCopsDebug.LogWarning("Invalid utility blet args");
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnUtilitBeltPowerupActived(args.Type, args.CoinCost);
        }
    }

    private void OnRewardGiven(RewardEventArgs args)
    {
        if(args == null || args.Rewards == null || args.Rewards.Count == 0)
        {
            BumperCopsDebug.Log("Rewards missing on rewards given");
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnRewardGiven(args.Rewards);
        }
    }

    private void OnObjectiveComplete(ObjectiveCompleteEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("objective complete args are null");
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnObjectiveComplete(args.CompletedObjective.Id);
        }
    }

    public void RecordChallengeButtonClick()
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("challenge_button_clicked");
        }
    }

    public void RecordPauseButtonClick()
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("pause_button_clicked");
        }
    }

    public void RecordShopButtonClick()
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("shop_button_clicked");
        };
    }

    public void RecordRatingbuttonClosed()
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("rating_dialogue_shown");
        };

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("rating_dialogue_close");
        };
    }

    public void RecordRatingbuttonOk()
    {
        if(!isAnalyticsOn)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("rating_dialogue_shown");
        };

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnButtonClicked("rating_dialogue_ok");
        };
    }

    private void OnScoreFinalized(ScoreUpdatedEventArgs args)
    {
        if(args == null)
        {
            return;
        }

        if(args.IsHighScore)
        {
            string rank = SocialStats.GetGenericScoreDimension(args.NewScore);
            foreach(IAnalyticsPlatform a in analyticsPlatforms)
            {
                if(!CanUsePlatform(a))
                {
                    continue;
                }
            
                a.OnScoreFinalized(args.NewScore, args.IsHighScore, rank);
            }
        }
    }

    private void OnPowerupSpawned(PowerupSpawnedEventArgs args)
    {
        if(args == null || args.Reward == null)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }

            a.OnPowerupSpawned(args.Reward.Type);
        }
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args == null)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnPowerupSpawned(args.Type);
        }
    }

    private void OnEnemyHealthOver(EnemyArgs args)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnEnemyHealthOver();
        }
    }

    private void OnAdEvent(AdEventArgs args)
    {
        if(args == null)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnAdEvent(args.AdEventType);
        }
    }

    private void OnPlayerCollidedWithObstacles(PlayerObstacleCollisionEventArgs args)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnPlayerCollidedWithObstacles();
        }
    }

    private void OnGameOver(GameOverReason reason)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnGameOver();
        }
    }

    private void OnCoinsSpent(CoinSpentEventArgs args)
    {
        if(args == null || args.AmountSpent <= 0 || string.IsNullOrEmpty(args.SpendReason))
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnCoinsSpent(args.AmountSpent, args.ItemId, args.SpendReason);
        }
    }

    private void OnGameStarted(GameEventArgs args)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnGameStarted(args.CurrentSessionNumber);
        }
    }

    private void OnTimeExtended(TimeExtensionArgs args)
    {
        if(!args.TimeExtended)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnTimeExtended(args.IsAds, args.Cost, args.TimeExtension);
        }
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnTutorialStarted();
        }
    }

    private void OnTutorialStepUpdate(TutorialArgs args)
    {
        if(!args.IsTutorialActive)
        {
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnTutorialStepUpdate(args.IsTutorialStepActive, args.TutorialStep, args.IsTutorialStepComplete);
        }
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnTutorialEnded();
        }
    }

    private void OnIAPEvent(object o, ProductPurchaseEventArgs args)
    {
        if(args == null || args.Product == null || ((string.IsNullOrEmpty(args.Receipt) || args.PurchaseReceipt == null) && !Application.isEditor))
        {
            BumperCopsDebug.LogError("Purchase event args are invalid");
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnIAPEvent(
                args.Product, 
                args.Receipt, 
                args.PurchaseReceipt, 
                args.Product.Metadata.isoCurrencyCode, 
                args.Product.Metadata.localizedPrice);
        }
    }

    private void OnPurchaseMade(ShopItemEventArgs args)
    {
        if(args == null || string.IsNullOrEmpty(args.SelectedItem))
        {
            BumperCopsDebug.LogWarning("Invalid CarPurchase Args");
            return;
        }

        foreach(IAnalyticsPlatform a in analyticsPlatforms)
        {
            if(!CanUsePlatform(a))
            {
                continue;
            }
            
            a.OnPurchaseMade(args.ItemCost, args.ItemType, args.SelectedItem);
        }
    }

}