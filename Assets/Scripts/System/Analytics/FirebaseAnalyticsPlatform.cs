using System;
using System.Collections.Generic;
using BumperCops.Models;
using Firebase;
using Firebase.Analytics;
using UnityEngine.Purchasing.Security;

public class FirebaseAnalyticsPlatform : BumperCopsAnalyticsPlatformAbstract, IAnalyticsPlatform 
{
    public bool HasInited { get; private set; }
    private FirebaseApp app;
    public override void Init()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
        var dependencyStatus = task.Result;
        if (dependencyStatus == Firebase.DependencyStatus.Available) 
        {
            // Create and hold a reference to your FirebaseApp,
            // where app is a Firebase.FirebaseApp property of your application class.
            app = Firebase.FirebaseApp.DefaultInstance;
            HasInited = true;

            // Set a flag here to indicate whether Firebase is ready to use by your app.
        } else 
        {
            BumperCopsDebug.LogWarning(System.String.Format(
            "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            // Firebase Unity SDK is not safe to use here.
        }
        });
    }

    public void ReportCoinsCollected(long totalCoins, long enemyCoins, long nearMissCoins)
    {
        if(totalCoins <= 0)
        {
            return;
        }

        Parameter[] coinsEarnedParameters = 
        {
            new Parameter(EARNED_COINS, totalCoins),
            new Parameter(NEAR_MISS_COINS, nearMissCoins),
            new Parameter(ENEMY_COINS, enemyCoins)
        };

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEarnVirtualCurrency, coinsEarnedParameters);
    }

    public void OnAdEvent(AdEventType type)
    {
        switch(type)
        {
            case AdEventType.AD_REWARD_EARNED:
                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventAdImpression, "admob", Enum.GetName(typeof(AdEventType), type));
                break;
        }
    }
    
    public void OnIAPEvent(IAPProduct product, 
        string receipt, 
        IPurchaseReceipt PurchaseReceipt,
        string isoCurrencyCode,
        decimal localizedPrice)
    {
        return;
    }

    public void OnButtonClicked(string eventName)
    {
        return;
    }

    public void ReportGameStats(int nearMisses, int enemiesArrested)
    {
        Parameter[] gameStatsParams = 
        {
            new Parameter(NEAR_MISS, nearMisses),
            new Parameter(ENEMY_ARRESTED, enemiesArrested)
        };

        FirebaseAnalytics.LogEvent("game_stats", gameStatsParams);
    }

    public override void OnGameStarted(long sessionNumber)
    {
        base.OnGameStarted(sessionNumber);
        FirebaseAnalytics.LogEvent(GAME_SESSION);
    }

    public void OnGameOver()
    {
        base.GameOver();
        if(donutsGenerated > 0)
        {
            FirebaseAnalytics.LogEvent("game_stats", DONUTS_PICKED_UP, donutsPickedupPercent);
        }

        if(donutsPickedup > 0)
        {
            FirebaseAnalytics.LogEvent("game_stats", DONUTS_USED, donutUsedPercent);
        }

        FirebaseAnalytics.LogEvent("game_stats", OBSTACLE_COLLISION, totalCollisions);
        FirebaseAnalytics.LogEvent("game_stats", SESSION_TIME, playTimeSeconds);
    }

    public override void OnTutorialEnded()
    {
        base.OnTutorialEnded();
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialComplete);
    }

    public override void OnTutorialStarted()
    {
        base.OnTutorialEnded();
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialBegin);
    }

    public override void OnTimeExtended(bool isAds, int cost, int extensionTime)
    {
        if(isTutorialActive)
        {
            FirebaseAnalytics.LogEvent(GAME_SESSION, $"{TIME_EXTENSION}_tutorial", 1);
            return;
        }

        base.OnTimeExtended(isAds, cost, extensionTime);
        // record how many times is the user extending time per session
        if(isAds)
        {
            // record how many times the user is selecting ads per session
            FirebaseAnalytics.LogEvent(GAME_SESSION, $"{TIME_EXTENSION}_ads", 1);
        }
        else
        {
            FirebaseAnalytics.LogEvent(GAME_SESSION, $"{TIME_EXTENSION}", 1);
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPurchase, TIME_EXTENSION, 1);
        }
    }

    public void OnTutorialStepUpdate(bool isStepActive, TutorialStep step, bool isStepComplete)
    {
        if(!this.isTutorialActive)
        {
            return;
        }

        if(isStepComplete)
        {
            FirebaseAnalytics.LogEvent("tutorial", TUTORIAL_STEP_END, step.ToString());
        }
        else if(isStepActive && step != TutorialStep.START)
        {
            FirebaseAnalytics.LogEvent("tutorial", TUTORIAL_STEP_START, step.ToString());
        }
    }

    public void OnScoreFinalized(long newScore, bool isHighScore, string newRank)
    {
        if(string.IsNullOrEmpty(newRank))
        {
            return;
        }
        
        if(isHighScore)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPostScore, "rank", newRank);
        }

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventPostScore, "score", newScore);
    }

    public void OnCoinsSpent(long amountSpent, string itemId, string reason)
    {
        if(amountSpent <= 0)
        {
            return;
        }

        Parameter[] spendParameters = 
        {
            new Parameter(FirebaseAnalytics.ParameterItemName, itemId),
            new Parameter(FirebaseAnalytics.ParameterVirtualCurrencyName, "coins"),
            new Parameter(FirebaseAnalytics.ParameterValue, amountSpent)
        };

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSpendVirtualCurrency, spendParameters);
    }

    public void OnPurchaseMade(int itemcost, ShopItemType itemType, string itemId)
    {
        if(string.IsNullOrEmpty(itemId) || itemType == ShopItemType.NONE)
        {
            BumperCopsDebug.LogWarning($"Invalid itemid {itemId} or itemtype {itemType}");
            return;
        }

       Parameter[] spendParameters = 
        {
            new Parameter(FirebaseAnalytics.ParameterItemName, itemId),
            new Parameter(FirebaseAnalytics.ParameterVirtualCurrencyName, "coins"),
            new Parameter(FirebaseAnalytics.ParameterValue, itemcost)
        };

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSpendVirtualCurrency, spendParameters);
    }

    public void OnObjectiveComplete(string completedObjectiveId)
    {
        if(string.IsNullOrEmpty(completedObjectiveId))
        {
            return;
        }

        List<Parameter> levelParameters = new List<Parameter>
        {
            new Parameter(FirebaseAnalytics.ParameterLevelName, completedObjectiveId)
        };

        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, levelParameters.ToArray());

        levelParameters.Add(new Parameter(FirebaseAnalytics.ParameterSuccess, 1));
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, levelParameters.ToArray());
    }

    public void OnRewardGiven(IReadOnlyList<Reward> rewards)
    {
        if(rewards == null || rewards.Count <= 0)
        {
            return;
        }

        foreach(Reward r in rewards)
        {
            if(r.Type == CollectibleType.COIN || r.Type == CollectibleType.BAG_COINS)
            {
                Parameter[] rewardParameters = 
                {
                    new Parameter(FirebaseAnalytics.ParameterVirtualCurrencyName, "coin"),
                    new Parameter(FirebaseAnalytics.ParameterValue, r.Amount),
                };

                FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventEarnVirtualCurrency, rewardParameters);
            }
            else
            {
                Parameter[] rewardParameters = 
                {
                    new Parameter("reason", r.Reason.ToString()),
                    new Parameter("type", r.Type.ToString()),
                    new Parameter("amount", r.Amount.ToString())
                };

                FirebaseAnalytics.LogEvent(REWARD, rewardParameters);
            }
        }
    }


    public void OnUtilitBeltPowerupActived(PowerupType type, int cost)
    {
        if(cost > 0)
        {
            FirebaseAnalytics.LogEvent(GAME_SESSION, "paid", 1);
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventSpendVirtualCurrency, type.ToString(), cost);
        }
        else
        {
            FirebaseAnalytics.LogEvent(GAME_SESSION, "use", 1);
        }
    }

    public void OnChallengeCreated(Challenge c)
    {
        Parameter[] rewardParameters = 
        {
            new Parameter("bet", c.BuyIn)
        };

        FirebaseAnalytics.LogEvent(CHALLENGE, rewardParameters);
    }

    public void OnChallengeManagerOpened()
    {
        FirebaseAnalytics.LogEvent(CHALLENGE, "open", 1);
    }

    public void OnChallengeNotCreated()
    {
        FirebaseAnalytics.LogEvent(CHALLENGE, "create-cancel", 1);
    }

    public void OnChallengeWon()
    {
        FirebaseAnalytics.LogEvent(CHALLENGE, "wins", 1);
    }
}