using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TreasurechestBehaviour : MonoBehaviour 
{
    public event Action TreasurechestOpened;
    [Header("VFX")]
    [SerializeField]
    private List<ParticleSystem> lockedTreasureChest;

    [SerializeField]
    private List<ParticleSystem> treasureChestOnOpen;

    [Header("UI")]
    [SerializeField]
    private Button treasureChestButton;

    [SerializeField]
    private Image fillImage;

    public bool IsTreasurechestActive { get; private set; }

    private void Awake() 
    {
        treasureChestButton.onClick.AddListener(OnTreasureChestClicked);
        treasureChestButton.gameObject.SetActive(false);   
        treasureChestButton.gameObject.SetActive(false);    
        treasureChestButton.interactable = false;
    }

    public void Show(float fillAmount = 1.0f, bool activate = true)
    {
        BumperCopsDebug.Log("TreasureChest show");
        // get screen position
        treasureChestButton.interactable = activate;
        fillImage.fillAmount = fillAmount;
        treasureChestButton.gameObject.SetActive(true);

        IsTreasurechestActive = activate;
        if(!activate)
        {
            return;
        }

        this.GetComponent<Animator>().SetTrigger("ready");
        foreach(ParticleSystem p in lockedTreasureChest)
        {
            p.Play();
        }
    }

    public void OnTreasureChestClicked()
    {
        BumperCopsDebug.Log("TreasureChest show");
        IsTreasurechestActive = false;
        float maxPlayTime = 0f;
        // play animation or effects
        foreach(ParticleSystem p in treasureChestOnOpen)
        {
            p.Play();
            maxPlayTime = Mathf.Max(maxPlayTime, p.main.duration);
        } 

        foreach(ParticleSystem p in lockedTreasureChest)
        {
            p.Stop();
        }
        
        StartCoroutine(HideTreasureOnClick(maxPlayTime));
    }

    private IEnumerator HideTreasureOnClick(float maxPlayTime)
    {
        yield return new WaitForSecondsRealtime(maxPlayTime * 0.5f);
        this.treasureChestButton.gameObject.SetActive(false);

        TreasurechestOpened?.Invoke();
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}