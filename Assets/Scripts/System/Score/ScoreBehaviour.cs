using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameManager))]
[RequireComponent(typeof(CoinCollectibleBehaviour))]
[RequireComponent(typeof(GameplayAssignmentManager))]
public class ScoreBehaviour : MonoBehaviour 
{
    public event Action<ScoreUpdatedEventArgs> ScoreUpdated;
    public event Action<ScoreUpdatedEventArgs> ScoreFinalized;
    private EnemyManager enemyManager;
    private PlayerController player;
    private GameplayAssignmentManager assignmentManager;

    [SerializeField]
    private ScreenTextAnimation screenTextAnimation;

    [Header("In game UI Elements")]
    [SerializeField]
    private TopPanelAnimationBehaviour topPanelAnimationBehaviour;
    [SerializeField]
    private BonusBarBehaviour bonusBarBehaviour;

    [SerializeField]
    private ComboBehaviour comboBehaviour;
    public ComboBehaviour ComboBehaviour { get { return comboBehaviour; }}

    [SerializeField]
    private ObstacleManager obstacleManager;

    private TimerBehaviour timer;
    private GameManager gameManager;
    private bool playerEnteredAttackZone = false;
    private bool startScoreCalculation;
    private Score score;
    private long highScore;
    private TutorialStep currentTutorialStep;
    private EnemyController enemy;
    private int totalCoins;
    private int obstacleHitCurrentEnemy;

    private void Start() 
    {
        totalCoins = -1;
        this.gameManager = this.GetComponent<GameManager>();
        this.assignmentManager = this.GetComponent<GameplayAssignmentManager>();
        this.enemyManager = this.gameManager.EnemyManager;
        this.player = this.gameManager.Player;

        // start the score counting
        this.gameManager.GameStarted += OnGameStarted;
        this.gameManager.GameSessionInitialized += OnGameSessionInitialized;
        this.gameManager.GameTutorialSessionInitialized += OnGameSessionInitialized;
    } 

    private void OnBonusStarted(CoinBonusEventArgs args)
    {
        totalCoins = args.TotalCoins;
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        obstacleHitCurrentEnemy = 0;
        this.enemy = args.enemy;
    }

    private void OnPlayerCharacterChanged()
    {
        if(this.player.character == null)
        {
            return;
        }

        foreach(NearMissBehaviour b in this.player.NearMiss)
        {
            b.NearMiss += OnNearMiss;
        }
    }

    private void OnTutorialStepUpdate(TutorialArgs a)
    {
        currentTutorialStep = a.TutorialStep;
    }

    private void OnPlayerHitObstacle(PlayerObstacleCollisionEventArgs args)
    {
        if(!args.IsHitCounted || this.player.IsBackupCharacter)
        {
            return;
        }

        ++obstacleHitCurrentEnemy;
        comboBehaviour.ResetCombo();
    }

    private void OnPlayerEnteredAttackZone()
    {
        playerEnteredAttackZone = true;
    }

    private void OnPlayerExitAttackZone()
    {
        playerEnteredAttackZone = false;
    }

    private void OnObjectiveComplete(ObjectiveCompleteEventArgs args)
    {
        comboBehaviour.IncrementCombo(ComboMultiplier.OBJECTIVE_COMPLETE);
        int multiplicand = score.AddObjectiveCompleteAndGetMultiplicand(comboBehaviour.CurrentCombo);
        UpdateCurrentScore("Obective Complete", multiplicand);
    }

    private void OnBumped(BumpEventArgs args)
    {
        comboBehaviour.IncrementCombo(ComboMultiplier.BUMPED);
        int multiplicand = score.AddEnemyHitScoreAndGetMultiplicand(comboBehaviour.CurrentCombo);
        UpdateCurrentScore("Critical hit", multiplicand);
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        int increment = 1;
        if(args.Type == PowerupType.COINS)
        {
            increment = 0;
            if(totalCoins > 0)
            {
                --totalCoins;
            }

            if(totalCoins == 0)
            {
                // give the bonus here
                increment = 5;
                totalCoins = -1;
            }

            return;
        }

        comboBehaviour.IncrementCombo(increment);
        int multiplicand = score.AddPowerupPickupAndGetMuiltiplicand(comboBehaviour.CurrentCombo);
        UpdateCurrentScore("Pickup", multiplicand);
    }

    private void OnEnemyDestroyed(EnemyArgs args)
    {
        //BumperCopsDebug.Log("ScoreBehaviour: Timer for enemy is " + timer.EnemyDestroyedSecondsRemaining);
        //StartCoroutine(scoreStatsUI.IncreaseScore(ENEMY_DESTROYED_MULTIPLIER, "Busted"));
        screenTextAnimation.PlayAnimation("Arrested");

        int currentCount = score.EnemyCount;
        
        if(obstacleHitCurrentEnemy <= 0)
        {
            comboBehaviour.TwoxCombo();
        }

        int multiplicand = score.AddEnemyArrestedScoreAndGetMultiplicand(comboBehaviour.CurrentCombo);
        UpdateCurrentScore("Arrested", multiplicand);

        // health Difference
        int timeRemainingBonus = Math.Max(1, args.TimeRemaining / 5);
        BumperCopsDebug.Log($"Increasing the combo by time {timeRemainingBonus}");
        comboBehaviour.IncrementCombo(ComboMultiplier.ENEMY_ARRESTED + timeRemainingBonus);
        int timeMultiplicand = score.AddTimeScoreAndGetMultiplicand(args.TimeRemaining, comboBehaviour.CurrentCombo);
        UpdateCurrentScore("Time Bonus", timeMultiplicand);

        playerEnteredAttackZone = false;
    }
    
    private void OnNearMiss(NearMissEventArgs npc)
    {
        //BumperCopsDebug.Log("ScoreBehaviour: near miss for " + npc.NPCName);
        if(playerEnteredAttackZone || !startScoreCalculation)
        {
            // the player is invincible here to other traffic. Hence we do not count near miss here
            return;
        }

        GiveNearMissReward(npc.Reward, npc.LaneDirection);
    }

    private void GiveNearMissReward(PowerupReward reward, LaneDirection npcLaneDirection, bool countScore = true)
    {
        if(reward == null || reward.Amount <= 0)
        {
            return;
        }
        
        this.gameManager.CoinCollectibleManager.IncreaseNearMissCoins(reward.Amount);

        if(!countScore)
        {
            return;
        }

        comboBehaviour.IncrementCombo(npcLaneDirection.Equals(LaneDirection.OPPOSITE) ? 
            ComboMultiplier.NEAR_MISS_OPPOSITE_DIRECTION : 
            ComboMultiplier.NEAR_MISS_SAME_DIRECTION);

        int multiplicand = score.RecordNearMissAndGetMultiplicand(npcLaneDirection, comboBehaviour.CurrentCombo);
        UpdateCurrentScore(npcLaneDirection.Equals(LaneDirection.OPPOSITE) ? "Opposite lane" : "Near miss", multiplicand);
    }

    private void OnCoinAwarded(int coinReward, string reason)
    {
        this.topPanelAnimationBehaviour.PlayCoinAnimation(coinReward);
    }

    private void RegisterListeners()
    {
        // score events
        this.enemyManager.EnemyHealthOver += OnEnemyDestroyed;
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        //this.player.AttackController.PerfectAttack += OnPerfectAttack;

        // this is so that we do not count near miss when player has just exit the attack zone
        this.player.AttackController.PlayerEnteredAttackZone += OnPlayerEnteredAttackZone;
        this.player.AttackController.PlayerEnteredAttackZone += OnPlayerExitAttackZone;
        this.player.AttackController.Bumped += OnBumped;
        OnPlayerCharacterChanged();

        // stop the score counting
        this.player.healthController.HealthOver += OnPlayerHealthOver;
        this.player.healthController.PlayerObstacleCollided += OnPlayerHitObstacle;
        
        this.gameManager.CoinCollectibleManager.CoinAwarded += OnCoinAwarded;
        this.gameManager.BonusBehaviour.BonusStarted += OnBonusStarted;
        this.gameManager.PowerupManager.PowerupCollected += OnPowerupCollected;

        // start the score counting
        this.gameManager.TutorialManager.TutorialStepUpdate += OnTutorialStepUpdate;
        this.gameManager.GameOver += OnGameOver;

        this.assignmentManager.ObjectiveComplete += OnObjectiveComplete;
    }

    private void DeregisterListeners()
    {
        // score events
        this.enemyManager.EnemyHealthOver -= OnEnemyDestroyed;
        this.enemyManager.EnemySpawned -= OnEnemySpawned;
        //this.player.AttackController.PerfectAttack += OnPerfectAttack;

        // this is so that we do not count near miss when player has just exit the attack zone
        this.player.AttackController.PlayerEnteredAttackZone -= OnPlayerEnteredAttackZone;
        this.player.AttackController.PlayerEnteredAttackZone -= OnPlayerExitAttackZone;
        this.player.AttackController.Bumped -= OnBumped;

        // stop the score counting
        this.player.healthController.HealthOver -= OnPlayerHealthOver;
        this.player.healthController.PlayerObstacleCollided -= OnPlayerHitObstacle;
        
        this.gameManager.CoinCollectibleManager.CoinAwarded -= OnCoinAwarded;
        this.gameManager.BonusBehaviour.BonusStarted -= OnBonusStarted;
        this.gameManager.PowerupManager.PowerupCollected -= OnPowerupCollected;

        // start the score counting
        this.gameManager.GameStarted -= OnGameStarted;
        this.gameManager.TutorialManager.TutorialStepUpdate -= OnTutorialStepUpdate;
        this.gameManager.GameOver -= OnGameOver;

        this.assignmentManager.ObjectiveComplete -= OnObjectiveComplete;
    }

    private void OnGameOver(GameOverReason args)
    {
        DeregisterListeners();
        PlayerStats.Instance.IncreaseCoins(this.gameManager.CoinCollectibleManager.CoinsCollected);
        score.AddCoins(this.gameManager.CoinCollectibleManager.CoinsCollected);
        score.IsHighScore = PlayerStats.Instance.SaveHighScore(score.TotalScore);
        score.DidArrestMaxEnemies = PlayerStats.Instance.SetMaxEnemiesCaught(score.EnemyCount);
        PlayerStats.Instance.AddEnemiesCaughtCount(score.EnemyCount);

        ScoreFinalized?.Invoke(
            new ScoreUpdatedEventArgs(
                score.TotalScore, 
                score.IsHighScore, 
                comboBehaviour.CurrentCombo, 
                comboBehaviour.IsHighComboCurrentSession,
                this.gameManager.CoinCollectibleManager.CoinsCollected));

        this.gameManager.Analytics.ReportCoinsCollected(
            coinAmount: this.gameManager.CoinCollectibleManager.CoinsCollected,
            nearMissCoins: this.gameManager.CoinCollectibleManager.NearMissCollected,
            enemyCoins: this.gameManager.CoinCollectibleManager.BountiesCollected);

        this.gameManager.Analytics.ReportStats(score.NearMissCount, score.EnemyCount);
    }

    private void OnPlayerHealthOver()
    {
        startScoreCalculation = false;
    }

    private void OnGameSessionInitialized(GameEventArgs args)
    {
        RegisterListeners();
    }

    private void OnGameStarted(GameEventArgs args)
    {
        if(args.NewSessionStarted)
        {
            ResetValues();
        }
        
        startScoreCalculation = true;
    }

    private void ResetValues()
    {
        score = new Score();
        highScore = PlayerStats.Instance.GetHighScore();
        comboBehaviour.ResetCombo();
    }

    private void UpdateCurrentScore(string reason, int multiplicand)
    {
        if(string.IsNullOrEmpty(reason))
        {
            throw new ArgumentNullException(nameof(reason));
        }

        //BumperCopsDebug.Log($"ScoreBehaviour: Updating current score {reason}");
        BumperCopsDebug.Log($"ScoreBehaviour - Updating score for {reason}");
        ScoreUpdated?.Invoke(new ScoreUpdatedEventArgs(
            score.TotalScore, 
            reason, 
            comboBehaviour.CurrentCombo, 
            score.PreviousScore, 
            score.TotalScore > highScore, multiplicand));
    }

    private static class ComboMultiplier
    {
        public const int NEAR_MISS_SAME_DIRECTION = 1;
        public const int NEAR_MISS_OPPOSITE_DIRECTION = 2;
        public const int BUMPED = 2;
        public const int OBJECTIVE_COMPLETE = 3;
        public const int MISSION_COMPLETE = 6;
        public const int ENEMY_TIME_REMAINING = 2;
        public const int ENEMY_ARRESTED = 2;
    }
}