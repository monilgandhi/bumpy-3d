using System;
using UnityEngine;
using System.Collections;
using TMPro;

public class HighScorePanelBehaviour : MonoBehaviour 
{    

    [SerializeField]
    private AnimationCompletedBehaviour highScoreAnimator;

    [SerializeField]
    private ScoreBehaviour scoreBehaviour;

    [SerializeField]
    private TMP_Text scoreText;

    private long highScore;
    private bool isHighScore;
    private HighScoreAnimationState highScoreAnimationState;
    private bool isAnimationComplete;
    private void Awake() 
    {
        this.scoreBehaviour.ScoreFinalized += OnScoreFinalized;    
        this.highScoreAnimator.AnimationOver += OnAnimationOver;
        StartCoroutine(Hide(true));
    }

    private void OnAnimationOver(AnimationCompleteEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("animation complete args are null");
            return;
        }

        isAnimationComplete = true;
    }

    private void OnScoreFinalized(ScoreUpdatedEventArgs args)
    {
        isHighScore = false;
        highScoreAnimationState = HighScoreAnimationState.NONE;

        if(args == null)
        {
            BumperCopsDebug.LogWarning("ScoreUpdatedEventArgs are null");
            return;
        }

        BumperCopsDebug.Log($"Is highscore {args.IsHighScore}");
        if(!args.IsHighScore)
        {
            return;
        }

        highScore = args.NewScore;
        isHighScore = true;
    }

    public bool ShowHighScore()
    {
        if(!isHighScore)
        {
            return false;
        }

        this.gameObject.SetActive(true);
        StartCoroutine(StartHighScoreAnimation());
        scoreText.text = highScore.ToString("n0");
        return true; 
    }

    public IEnumerator Hide(bool instant = false)
    {
        if(highScoreAnimationState == HighScoreAnimationState.NONE && !instant)
        {
            yield break;
        }

        if(!instant)
        {
            isAnimationComplete = false;
            highScoreAnimator.StartAnimationWithInteger("state", (int)++highScoreAnimationState); 
            yield return new WaitWhile(() => !isAnimationComplete); 
        }

        highScoreAnimator.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private IEnumerator StartHighScoreAnimation()
    {
        scoreText.gameObject.SetActive(true);
        for(;highScoreAnimationState <= HighScoreAnimationState.MID; highScoreAnimationState++)
        {
            highScoreAnimator.StartAnimationWithInteger("state", (int)highScoreAnimationState);  
            if(highScoreAnimationState == HighScoreAnimationState.START)
            {
                yield return new WaitWhile(() => !isAnimationComplete);
            }
        }
    }

    private enum HighScoreAnimationState
    {
        NONE,
        START,
        MID,
        END
    }
}