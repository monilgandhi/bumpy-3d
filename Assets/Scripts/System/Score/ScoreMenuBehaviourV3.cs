using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreMenuBehaviourV3 : MonoBehaviour 
{
    public event Action ScoreDialogueComplete;
    [Header("Game Elements")]
    [SerializeField]
    private GameManager gameManager;

    [Header("Score stats")]
    [SerializeField]
    private TMP_Text currentScore;

    [SerializeField]
    private TMP_Text highScore;

    [Header("Coin stats")]
    [SerializeField]
    private TMP_Text coins;

    [SerializeField]
    private TMP_Text totalCoins;

    [Header("Enemies Arrested Stats")]
    [SerializeField]
    private TMP_Text arrestedThisSession;

    [SerializeField]
    private TMP_Text bestArrestedScore;

    [Header("Continue button")]
    [SerializeField]
    private Button continueButton;

    [Header("Leaderboard")]
    [SerializeField]
    private LeaderboardBehaviour leaderboardBehaviour;

    private Animator animator;
    private bool isCurrentAnimationComplete;
    private ScoreState currentScoreState;

    private void Awake() 
    {
        animator = this.GetComponent<Animator>();
        continueButton.onClick.AddListener(OnContinueButtonClicked);
    }

    private void Start() 
    {
        this.gameObject.SetActive(false);    
        currentScoreState = ScoreState.NONE;
    }

    private void OnContinueButtonClicked()
    {
        BumperCopsDebug.Log($"Current scorer state is {currentScoreState}");
        if(currentScoreState == ScoreState.SCORE)
        {
            StartCoroutine(ShowLeaderboard());
        } 
        else if (currentScoreState == ScoreState.LEADERBOARD)
        {
            // hide
            StartCoroutine(ResetAndHide());
        }
    }

    private IEnumerator ShowLeaderboard()
    {
        this.animator.SetTrigger("flip");
        yield return new WaitWhile(() => !this.isCurrentAnimationComplete);

        BumperCopsDebug.Log("Animation complete");
        currentScoreState = ScoreState.LEADERBOARD;
        leaderboardBehaviour.gameObject.SetActive(true);
        this.leaderboardBehaviour.OnLeaderboardOpened();
    }

    public void DisplayScoreMenu(Score score, PlayerCharacter playerCharacter)
    {
        leaderboardBehaviour.gameObject.SetActive(false);
        currentScoreState = ScoreState.SCORE;
        this.gameObject.SetActive(true);
        DisplayScore(score);
        DisplayEnemiesArrested(score);
        // get total coins
        long totalCoinsCount = PlayerStats.Instance.CoinCount - score.CoinsCollected;
        
        // add the details of coins
        totalCoins.text = totalCoinsCount.ToString();
        coins.text = $"+ {score.CoinsCollected.ToString()}";
        StartCoroutine(IncreaseScore(score.CoinsCollected, totalCoinsCount));
    }

    private void DisplayEnemiesArrested(Score score)
    {
        int maxEnemyCount = PlayerStats.Instance.GetMaxEnemiesCaught();

        if(score.DidArrestMaxEnemies)
        {
            bestArrestedScore.text = "New Best";
            StartCoroutine(PulsateText(bestArrestedScore));
        }
        else
        {
            bestArrestedScore.text = maxEnemyCount.ToString();
        }
    
        arrestedThisSession.text = score.EnemyCount.ToString();
    }

    private IEnumerator PulsateText(TMP_Text text)
    {
        RectTransform transform = text.GetComponent<RectTransform>();
        float currentLocalScale = 1;
        int multiplier = 1;
        while(true)
        {
            currentLocalScale += (Time.unscaledDeltaTime * multiplier);
            transform.localScale = new Vector2(currentLocalScale, currentLocalScale);

            if(multiplier > 0 && currentLocalScale >= 1.6f)
            {
                multiplier = -1;
            } else if (multiplier < 0 && currentLocalScale <= 1.0f)
            {
                multiplier = 1;
            }

            yield return new WaitForSecondsRealtime(0.03f);
        }
    }

    private void DisplayScore(Score score)
    {
        long currentHighScore = PlayerStats.Instance.GetHighScore();
          // add the details of the score
        currentScore.text = score.TotalScore.ToString("n0");

        if(currentHighScore < score.TotalScore)
        {
            highScore.text = "New High Score";
            StartCoroutine(PulsateText(highScore));
        }
        else
        {
            highScore.text = currentHighScore.ToString("n0");
        }

        highScore.text = currentHighScore.ToString("n0");
    }

    private IEnumerator IncreaseScore(long currentCoins, long previousTotal)
    {
        long coinJump = 50;
        //BumperCopsDebug.Log($"ScoreMenuBehaviourV2: Total coins is {totalCoins}");
        //BumperCopsDebug.Log($"ScoreMenuBehaviourV2: Previous coins is {previousCoin} with coins collected this session {coinsCollected}");
        long requiredTotal = previousTotal + currentCoins;
        while(previousTotal < requiredTotal)
        {
            coinJump = requiredTotal - previousTotal < 50 ? requiredTotal - previousTotal : 50;
            previousTotal += coinJump;
            coinJump = requiredTotal - previousTotal > coinJump ? requiredTotal - previousTotal  : coinJump;
            totalCoins.text = previousTotal.ToString();
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator ResetAndHide()
    {
        currentScoreState = ScoreState.NONE;
        isCurrentAnimationComplete = false;
        animator.SetTrigger("hide");
        yield return new WaitWhile(() => !this.isCurrentAnimationComplete);
        BumperCopsDebug.Log("Animation complete 2");
        ScoreDialogueComplete?.Invoke();
        StopAllCoroutines();
        this.gameObject.SetActive(false);
    }

    public void AnimationComplete()
    {
        isCurrentAnimationComplete = true;
    }

    private enum ScoreState
    {
        NONE,
        SCORE,
        LEADERBOARD
    }
}