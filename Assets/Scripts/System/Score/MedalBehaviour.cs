using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MedalBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image medalNotAchieved;

    [SerializeField]
    private Image medalAchieved;

    [SerializeField]
    private PlayerController player;
    private void Update() 
    {
        if(!player)
        {
            return;
        }
    }

    public IEnumerator OnShowAchieved()
    {
        this.medalAchieved.gameObject.SetActive(true);
        // fadein effect. Followed by camera shake.
        this.GetComponent<Animator>().SetTrigger("medalAwarded");

        this.medalNotAchieved.gameObject.SetActive(false);
        yield return null;
    }

    public void Show(bool achieved)
    {
        this.GetComponent<Animator>().enabled = !achieved;
        this.medalAchieved.gameObject.SetActive(achieved);
        this.medalNotAchieved.gameObject.SetActive(!achieved);

        if(!achieved)
        {
            // reset and make sure alpha is 1. In animation we reduce the alpha
            this.medalNotAchieved.color = new Color(0, 0, 0, 1);
            this.medalAchieved.color = new Color(this.medalAchieved.color.r, this.medalAchieved.color.g, this.medalAchieved.color.b, 0);
        }
    }
}