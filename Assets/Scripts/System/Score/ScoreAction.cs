public enum ScoreAction
{
    NEAR_MISS,
    ENEMY_HIT,
    PERFECT_HIT,
    ENEMY_CAPTURED
}