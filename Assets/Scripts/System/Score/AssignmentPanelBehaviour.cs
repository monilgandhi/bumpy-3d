using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Animator))]
public class AssignmentPanelBehaviour : MonoBehaviour 
{
    [SerializeField]
    private List<TMP_Text> assignmentText;

    [SerializeField]
    private List<MedalBehaviour> medals;

    [SerializeField]
    private Material completedTaskMaterial;

    [SerializeField]
    private TMP_Text titleText;

    [SerializeField]
    private List<AudioSource> objectiveCompletionSound;

    private Gradient gradient = new Gradient();
    private Animator animatorComponent;

    public bool AnimationSequenceComplete { get; private set; }

    private void Awake() 
    {
        SetupTextGradient();
        animatorComponent = this.GetComponent<Animator>();
    }

    private void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void ShowEndScreen(Assignment currentAssignment)
    {
        if(currentAssignment == null || string.IsNullOrEmpty(currentAssignment.Id) || currentAssignment.Objectives == null || currentAssignment.Objectives.Count <= 0)
        {
            throw new ArgumentNullException(nameof(currentAssignment));
        }

        this.GetComponentInChildren<Button>(true).gameObject.SetActive(!currentAssignment.IsAssignmentComplete);
        medals.ForEach((m) => m.gameObject.SetActive(true));
        this.gameObject.SetActive(true);
        PlayCompletedAnimation(currentAssignment, false);
    }

    public void ShowStartScreen(Assignment currentAssignment, bool showButton = false, bool immediately = false)
    {
        if(currentAssignment == null || string.IsNullOrEmpty(currentAssignment.Id) || currentAssignment.Objectives == null || currentAssignment.Objectives.Count <= 0)
        {
            throw new ArgumentNullException(nameof(currentAssignment));
        }

        Button b = this.GetComponentInChildren<Button>(true);
        // hide the button
        if(b != null)
        {
            this.GetComponentInChildren<Button>(true).gameObject.SetActive(showButton);
        }
        
        medals.ForEach((m) => m.gameObject.SetActive(false));
        this.gameObject.SetActive(true);
        PlayStartAnimation(currentAssignment, true);
    }

    public IEnumerator HideScreen()
    {
        if(AnimationSequenceComplete)
        {
            yield break;
        }

        //BumperCopsDebug.Log($"normalize time before: {animatorComponent.GetCurrentAnimatorStateInfo(0).normalizedTime}");
        animatorComponent.SetTrigger("hide");
        //BumperCopsDebug.Log($"normalize time: {animatorComponent.GetCurrentAnimatorStateInfo(0).normalizedTime}");

        //bool completeState = animatorComponent.GetCurrentAnimatorStateInfo(0).IsName("assignment_complete");
        //bool previousState = animatorComponent.GetCurrentAnimatorStateInfo(0).IsName("assignment_new");
        //BumperCopsDebug.Log($"completeState: {completeState}, previousState: {previousState}");
        
        yield return new WaitUntil(() => animatorComponent.GetCurrentAnimatorStateInfo(0).IsName("hide"));
        //BumperCopsDebug.Log($"normalize time: {animatorComponent.GetCurrentAnimatorStateInfo(0).normalizedTime}");
        this.gameObject.SetActive(false);
        AnimationSequenceComplete = true;
    }

    private void PlayStartAnimation(Assignment currentAssignment, bool showReward)
    {
        AnimationSequenceComplete = false;
        //BumperCopsDebug.Log($"AssignmentPanelBehaviour: Running PlayStartAnimation {this.gameObject.activeSelf}");
        if(titleText != null)
        {
            titleText.text = currentAssignment.IsTutorial ? "Training" : $"mission {currentAssignment.AssignmentNumber}";
        }
        
        //BumperCopsDebug.Log("AssignmentPanelBehaviour: Total objectives " + currentAssignment.Objectives.Count);
        for(int i = 0; i < currentAssignment.Objectives.Count; i++)
        {
            AssignmentObjectives o = currentAssignment.Objectives[i];
            //BumperCopsDebug.Log("AssignmentPanelBehaviour: Description is " + o.Description);
            TMP_Text currentObjectiveText = assignmentText[i];
            
            if(o == null || string.IsNullOrEmpty(o.Id))
            {
                BumperCopsDebug.LogWarning("AssignmentPanelBehaviour: Empty assignment objective found");
                continue;
            }

            currentObjectiveText.text = FormatDescription(i + 1, o);
            // write the texts of ones which are complete and strike them out
            currentObjectiveText.transform.parent.gameObject.SetActive(true);
            if(o.IsComplete)
            {
                currentObjectiveText.color = gradient.Evaluate(1.0f);
            }

            //ToggleReward(currentObjectiveText.transform.parent, showReward && !o.IsComplete, o.CoinPayout);
        }
    }

    private void PlayCompletedAnimation(Assignment currentAssignment, bool showReward)
    {
        AnimationSequenceComplete = false;
        // leave space here for style purpose
        titleText.text = $"mission {currentAssignment.AssignmentNumber}";
        int medalsIdx = 0;
        // loop through each of the objectives

        if(currentAssignment.IsTutorial)
        {
            // show all the medals
            for(int i = 0; i < 3; i++)
            {
                // animate sidemedals
                StartCoroutine(medals[i].OnShowAchieved());
            }
        }

        for(int i = 0; i < currentAssignment.Objectives.Count; i++)
        {
            AssignmentObjectives o = currentAssignment.Objectives[i];
            TMP_Text currentObjectiveText = assignmentText[i];

            if(o == null || string.IsNullOrEmpty(o.Id))
            {
                BumperCopsDebug.LogWarning("AssignmentPanelBehaviour: Empty assignment objective found");
                continue;
            }

            StringBuilder description = new StringBuilder(o.Description);
            BumperCopsDebug.Log($"{this.GetType().ToString()}: player count is {o.PlayerCount}");
            if(!o.IsComplete)
            {
                description.Append($"<space=1em><color=white>({o.PlayerCount}/{o.Count})");
            }

            // write the texts of ones which are complete and strike them out
            currentObjectiveText.text = FormatDescription(i + 1, o);

            // first deal with side medals for non tutorial
            if(o.IsComplete && !currentAssignment.IsTutorial)
            {
                if(o.CompletedInThisSession)
                {
                    // now change the color
                    StartCoroutine(CompleteObjective(currentObjectiveText));
                    if(medalsIdx < medals.Count)
                    {
                        // animate sidemedals
                        StartCoroutine(medals[medalsIdx++].OnShowAchieved());
                    }
                }
                else
                {
                    if(medalsIdx < medals.Count)
                    {
                        // animate sidemedals
                        medals[medalsIdx++].Show(true);
                    }

                    //currentObjectiveText.GetComponent<RectTransform>().localScale = new Vector2(0.9f, 0.9f);
                }

                // force this for both the scenarios;
                currentObjectiveText.text = $"<s>{currentObjectiveText.text}</s>";
                currentObjectiveText.color = gradient.Evaluate(0.5f);
                BumperCopsDebug.Log($"AssignmentPanelBehaviour: Color is {currentObjectiveText.color}");
                currentObjectiveText.fontSharedMaterial = completedTaskMaterial;
            }

            //ToggleReward(currentObjectiveText.transform.parent, showReward && !o.IsComplete, o.CoinPayout);
        }
    }

    private void ToggleReward(Transform rewardParent, bool show, int reward)
    {
        if(rewardParent == null)
        {
            BumperCopsDebug.LogWarning($"{nameof(rewardParent)} is null");
            return;
        }

        if(reward <= 0 && show)
        {
            BumperCopsDebug.LogWarning($"{nameof(reward)} is 0");
            return;
        }
        
        GameObject rewardGO = rewardParent.Find("reward").gameObject;
        rewardGO.gameObject.SetActive(show);

        if(!show)
        {
            return;
        }

        rewardGO.GetComponentInChildren<TMP_Text>().text = reward.ToString();
    }

    private IEnumerator CompleteObjective(TMP_Text currentObjectiveText)
    {
        float startTime = 0;
        float maxTime = gradient.colorKeys.Last().time;
        RectTransform rectTransform = currentObjectiveText.GetComponent<RectTransform>();
        while(startTime < maxTime)
        {
            startTime += Time.fixedUnscaledDeltaTime;
            currentObjectiveText.color = gradient.Evaluate(startTime);   
            float newScale = startTime < maxTime/2 ? rectTransform.localScale.x + Time.fixedUnscaledDeltaTime : rectTransform.localScale.x - Time.fixedUnscaledDeltaTime;
            rectTransform.localScale = new Vector2(newScale, newScale);

            //BumperCopsDebug.Log($"AssignmentPanelBehaviour: Running complete objective with color {currentObjectiveText.color} and scale {newScale}");
            yield return new WaitForSecondsRealtime(0.02f);
        }
    }

    private void SetupTextGradient()
    {
        GradientColorKey[] colorKeys = new GradientColorKey[2];
        GradientColorKey startColor = new GradientColorKey();
        startColor.color = new Color(1, 0.4f, 0.1f);;
        startColor.time = 0.0f;
        colorKeys[0] = startColor;

        GradientColorKey endColor = new GradientColorKey();
        endColor.color = Color.white;
        endColor.time = 0.5f;
        colorKeys[1] = endColor;

        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
        GradientAlphaKey startAlpha = new GradientAlphaKey();
        startAlpha.time = 0.0f;
        startAlpha.alpha = 1;
        alphaKeys[0] = startAlpha;

        GradientAlphaKey endAlpha = new GradientAlphaKey();
        endAlpha.time = 0.5f;
        endAlpha.alpha = 1;
        alphaKeys[1] = startAlpha;

        gradient.SetKeys(colorKeys, alphaKeys);
    }

    private String FormatDescription(int index, AssignmentObjectives objective)
    {
        if(objective.IsComplete)
        {
            return String.Format("<s>{0}.<indent=10%>{1}</indent>", index, objective.Description);
        }

        // completed amount
        // completed percent
        int percentageCompleted = Mathf.RoundToInt(((float)objective.PlayerCount/(float)objective.Count) * 100);
        string completionRate = $"<color=white>({percentageCompleted}%)";
        return $"{index}.<indent=15%>{objective.Description} \n {completionRate}</indent>";
    }
}