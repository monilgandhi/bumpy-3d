using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GoalInformationBehaviour : MonoBehaviour 
{
    [Header("Game elements")]
    [SerializeField]
    private ScoreBehaviour scoreBehaviour;

    [SerializeField]
    private TutorialManager tutorialManager;

    [Header("UI Elements")]
    [SerializeField]
    private Image fillImage;

    [SerializeField]
    private Image playerProfileImage;

    [SerializeField]
    private GameObject targetPosition;

    [SerializeField]
    private Image targetProfilePicture;

    [SerializeField]
    private TMP_Text rankTitle;

    private float distanceToCover;
    private RectTransform playerProfileTransform;
    private Tuple<string, long> nextScore;
    private long scoreToCover;
    private Queue<float> movePercentQueue;
    private bool isAnimationRunning;
    private Animator animator;
    private float originalYPosition;
    private float profilePictureTargetPosition;
    private bool isTutorial;
    private void Awake() 
    {
        playerProfileTransform = playerProfileImage.GetComponent<RectTransform>();
        animator = this.GetComponent<Animator>();
        tutorialManager.TutorialStarted += OnTutorialStarted;
        tutorialManager.TutorialEnd += OnTutorialEnded;
    
        originalYPosition = playerProfileTransform.anchoredPosition.y;
        profilePictureTargetPosition = targetPosition.GetComponent<RectTransform>().anchoredPosition.y;
        distanceToCover = profilePictureTargetPosition - originalYPosition;
        this.scoreBehaviour.ScoreUpdated += OnScoreUpdated;

        RefreshTargetScore();
        movePercentQueue = new Queue<float>();
    }

    private void OnTutorialStarted(TutorialArgs args)
    {
        isTutorial = true;
        Hide();
    }

    private void OnTutorialEnded(TutorialArgs args)
    {
        BumperCopsDebug.Log("Tutorial off");
        RefreshTargetScore();
        this.gameObject.SetActive(true);
        isTutorial = false;
    }   

    private void Update() 
    {
        if(isAnimationRunning || isTutorial)
        {
            return;
        }

        if(movePercentQueue.Count > 0)
        {
            StartCoroutine(Increase());
        }
    }

    private void OnScoreUpdated(ScoreUpdatedEventArgs args)
    {
        if(isTutorial)
        {
            return;
        }

        BumperCopsDebug.Log("Adding to the queue");
        long changeInScore = args.NewScore - args.PreviousScore;
        float movePercent = (float)changeInScore/(float)scoreToCover;

        movePercentQueue.Enqueue(movePercent);
    }

    private void Hide()
    {
        this.gameObject.SetActive(false);
    }

    private IEnumerator Increase()
    {
        animator.SetBool("enter", true);
        isAnimationRunning = true;
        while(movePercentQueue.Count > 0)
        {
            float movePercent = movePercentQueue.Dequeue();
            Debug.Log($"move percent is {movePercent}");
            float newFillAmount = fillImage.fillAmount + movePercent;
            if(newFillAmount > 1)
            {
                movePercentQueue.Clear();
                newFillAmount = 1;
            }

            float newYPosition = playerProfileTransform.anchoredPosition.y + (distanceToCover * movePercent);
            
            BumperCopsDebug.Log($"new y position is {newYPosition}");
            if(newYPosition > profilePictureTargetPosition)
            {
                BumperCopsDebug.Log("Reached max with yposition");
                newYPosition = profilePictureTargetPosition;
            }

            while(fillImage.fillAmount < newFillAmount)
            {
                fillImage.fillAmount += Time.deltaTime;

                float profilePictureTarget = playerProfileTransform.anchoredPosition.y + newYPosition;
                if(playerProfileTransform.anchoredPosition.y < profilePictureTarget)
                {
                    playerProfileTransform.anchoredPosition = 
                        new Vector2(playerProfileTransform.anchoredPosition.x, 
                            playerProfileTransform.anchoredPosition.y + Time.deltaTime);
                }

                yield return new WaitForEndOfFrame();
            }
            
            fillImage.fillAmount = newFillAmount;
            playerProfileTransform.anchoredPosition = 
                new Vector2(playerProfileTransform.anchoredPosition.x, newYPosition);
        }  

        if(fillImage.fillAmount >= 1)
        {
            animator.SetBool("enter", false);
            RefreshTargetScore();
            fillImage.fillAmount = 0;
            playerProfileTransform.anchoredPosition = 
                new Vector2(playerProfileTransform.anchoredPosition.x, originalYPosition);
        }

        Debug.Log("Animation is not running anymore");
        isAnimationRunning = false; 
    }

    private void RefreshTargetScore()
    {
        long previousTarget = nextScore == null ? 0 : nextScore.Item2;
        nextScore = SocialStats.GetNextTargetScore(previousTarget + 1);

        if(nextScore == null)
        {
            Hide();
            this.scoreBehaviour.ScoreUpdated -= OnScoreUpdated;
            return;
        }

        BumperCopsDebug.Log($"New score is {nextScore.Item1}");
        rankTitle.text = nextScore.Item1;
        scoreToCover = nextScore.Item2 - previousTarget;
        targetProfilePicture.sprite = SocialStats.GetProfileImage(nextScore.Item1);
    }
}