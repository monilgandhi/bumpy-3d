using System;
using System.Collections;
using UnityEngine;
using TMPro;

public class ComboBehaviour : MonoBehaviour 
{
    public Action<ComboEventArgs> ComboChanged;
    public Action<ComboEventArgs> ComboReset;

    [SerializeField]
    private TMP_Text comboValue;

    [SerializeField]
    private Animator panelAnimator;

    [SerializeField]
    private ParticleSystem twoXComboParticleSystem;

    private int currentCombo;
    private float comboStartTime;

    public bool IsHighComboCurrentSession { get; private set; }
    
    public int CurrentCombo
    {
        get { return currentCombo; }
        private set
        {
            currentCombo = value;
            
            if(currentCombo <= baseCombo)
            {
                comboStartTime = -1;
            }
            else
            {
                comboStartTime = Time.time;
            }

            comboValue.text = $"<size=%75>x</size>{currentCombo.ToString("n0")}";
            if(value > PlayerStats.Instance.CurrentHighCombo)
            {
                PlayerStats.Instance.CurrentHighCombo = value;
                IsHighComboCurrentSession = true;
            }
        }
    }

    private ShakeText shakeText;
    private int baseCombo;
    private const int MAX_COMBO = 10000;
    private const float COMBO_ALIVE_TIME_SECONDS = 20.0f;
    private void Awake() 
    {
        IsHighComboCurrentSession = false;
        this.shakeText = this.comboValue.GetComponent<ShakeText>();
        baseCombo = PlayerStats.Instance.GetCurrentBaseCombo();
        ResetCombo();
    }

    public void TwoxCombo()
    {
        IncrementCombo(CurrentCombo);
        twoXComboParticleSystem.Play();
    }

    public void IncrementCombo(int increment = 1)
    {
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }

        if(!this.panelAnimator.GetBool("show"))
        {
            this.panelAnimator.SetBool("show", true);
        }

        int previousCombo = CurrentCombo;

        if(CurrentCombo >= 1)
        {
            StartCoroutine(PlayIncrementAnimation());
        }

        CurrentCombo = Math.Min(CurrentCombo + increment, MAX_COMBO);
        ComboChanged?.Invoke(new ComboEventArgs(previousCombo, CurrentCombo));
    }

    private IEnumerator PlayIncrementAnimation()
    {
        this.panelAnimator.SetTrigger("increment");
        yield break;
    }

    public void ResetCombo()
    {
        int previousCombo = CurrentCombo;
        CurrentCombo = baseCombo;
        ComboChanged?.Invoke(new ComboEventArgs(previousCombo, CurrentCombo));
    }

    private void LateUpdate() 
    {
        if(comboStartTime < 0)
        {
            return;
        }

        if(Time.time - comboStartTime >= COMBO_ALIVE_TIME_SECONDS)
        {
            ResetCombo();
            return;
        }

        // convert the shake to percent
        float shakeValue = 200 * (1 - ((Time.time - comboStartTime) / COMBO_ALIVE_TIME_SECONDS));
        //BumperCopsDebug.Log($"New shake value is {shakeValue}");
        this.shakeText.IncreaseScale(shakeValue);
    }
}

public class ComboEventArgs
{
    public readonly int PreviousCombo;
    public readonly int NewCombo;

    public ComboEventArgs(int previousCombo, int newCombo)
    {
        this.PreviousCombo = previousCombo;
        this.NewCombo = newCombo;
    }
}