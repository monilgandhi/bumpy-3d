using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ScoreMenuBehaviourV4 : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text currentScoreTxt;

    public void DisplayScoreMenu(long currentScore)
    {
        BumperCopsDebug.Log($"Current score is {currentScore}");
        this.currentScoreTxt.text = currentScore.ToString("n0");
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}