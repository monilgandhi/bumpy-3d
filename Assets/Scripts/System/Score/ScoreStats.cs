using System;
using System.Text;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreStats : MonoBehaviour 
{
    [Header("Game Elements")]
    [SerializeField]
    private ScoreBehaviour scoreBehaviour;
    [SerializeField]
    private TMP_Text scoreStats;

    [SerializeField]
    private TMP_Text scoreReason;

    [SerializeField]
    private TMP_Text scoreMultiplied;

    private long currentScore;
    private long targetScore;

    private Animator scoreReasonAnimator;
    private Animator scoreMultipliedAnimator;
    private Gradient gradient;

    private void Awake() 
    {
        scoreReasonAnimator = scoreReason.gameObject.GetComponent<Animator>();
        scoreMultipliedAnimator = scoreMultiplied.gameObject.GetComponent<Animator>();
        HideReason();
    }

    private void Start() 
    {
        scoreStats.text = currentScore.ToString("n0");
        scoreBehaviour.ScoreUpdated += OnScoreUpdated;
        SetupGradient();
    }

    private void OnScoreUpdated(ScoreUpdatedEventArgs args)
    {
        if(args == null)
        {
            return;
        }

        if(args.PreviousScore > args.NewScore)
        {
            throw new InvalidOperationException($"Previous score {args.PreviousScore} greater than newScore {args.NewScore}");
        }

        //BumperCopsDebug.Log("ScoreStats: Score updated ");
        if(args.NewScore > targetScore)
        {
            targetScore = args.NewScore;
        }

        /*if(!string.IsNullOrEmpty(args.Reason))
        {
            string mulipliedScore = $"{args.Multiplicand} x {args.Combo}"; 
            ModifyReasonAndMultiplier(args.Reason, mulipliedScore);
        }*/

        if(this.gameObject.activeInHierarchy)
        {
            StartCoroutine(UpdateScoreStats());
        }
    }

    private IEnumerator UpdateScoreStats()
    {
        this.scoreStats.color = gradient.Evaluate(0);
        while(this.currentScore < this.targetScore)
        {
            currentScore += GetJump();
            this.scoreStats.text = currentScore.ToString("n0");
            yield return new WaitForEndOfFrame();
        }

        
        this.scoreStats.color = gradient.Evaluate(1);
    }

    private long GetJump()
    {
        long diff = this.targetScore - this.currentScore;
        return diff  > 10 ? Mathf.RoundToInt(diff / 10) : diff;
    }

    private void HideReason()
    {
        scoreReason.gameObject.SetActive(false);
        scoreMultiplied.gameObject.SetActive(false);
    }

    private void ModifyReasonAndMultiplier(string reason, string multiplied)
    {
        if(string.IsNullOrEmpty(reason) || string.IsNullOrEmpty(multiplied))
        {
            BumperCopsDebug.LogWarning("reason or mulitiplied text is null");
            return;
        }

        HideReason();
        scoreReason.gameObject.SetActive(true);
        scoreMultiplied.gameObject.SetActive(true);

        scoreReason.text = reason;
        scoreMultiplied.text = multiplied;
    }

    private void SetupGradient()
    {
        GradientColorKey[] colorKeys =  new GradientColorKey[2];
        GradientColorKey startColorKey = new GradientColorKey();
        startColorKey.color = new Color(1, 0.39f, 0f);
        startColorKey.time = 0.0f;
        colorKeys[0] = startColorKey;

        GradientColorKey endColorKey = new GradientColorKey();
        endColorKey.color = Color.white;
        endColorKey.time = 1.0f;
        colorKeys[1] = endColorKey;
        
        // alpha
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
        GradientAlphaKey startAlphaKey = new GradientAlphaKey();
        startAlphaKey.alpha = 1.0f;
        startAlphaKey.time = 0.0f;
        alphaKeys[0] = startAlphaKey;

        GradientAlphaKey endAlphaKey = new GradientAlphaKey();
        endAlphaKey.alpha = 1.0f;
        endAlphaKey.time = 1.0f;

        alphaKeys[1] = endAlphaKey;
        gradient = new Gradient();
        gradient.SetKeys(colorKeys, alphaKeys);
    }
}