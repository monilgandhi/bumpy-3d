using System;
using UnityEngine;
using TMPro;
public class ScoreSection : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text scoreNameText;

    [SerializeField]
    private TMP_Text scoreDisplayText;

    public void Init(string scoreName, string scoreDisplay, bool isTotal = false)
    {
        if(string.IsNullOrEmpty(scoreName))
        {
            throw new ArgumentNullException(nameof(scoreName));
        }

        if(string.IsNullOrEmpty(scoreDisplay))
        {
            throw new ArgumentNullException(nameof(scoreDisplay));
        }

        if(isTotal)
        {
            scoreNameText.color = Color.white;
            scoreDisplayText.color = Color.white;
        }

        scoreNameText.text = scoreName;
        scoreDisplayText.text = scoreDisplay;
    }     
}