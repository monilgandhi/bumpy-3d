using UnityEngine;

public class RotationBehaviour : MonoBehaviour 
{
    [SerializeField]
    private bool rotateX;

    [SerializeField]
    private bool rotateY;

    [SerializeField]
    private bool rotateZ;

    [SerializeField]
    private bool clockWiseRotation;

    [SerializeField]
    private float speedPerSecond;

    [SerializeField]
    public bool StartRotationImmediately;
    private float directionMultiplier;

    private void Awake() 
    {
        directionMultiplier = clockWiseRotation ? -1 : 1;
    }

    private void Update() 
    {
        if(!StartRotationImmediately)
        {
            return;
        }
        
        if(!rotateX && !rotateY && !rotateZ)
        {
            return;
        }    

        float xRotation = rotateX ? Time.deltaTime * speedPerSecond * directionMultiplier : 0;
        float yRotation = rotateY ? Time.deltaTime * speedPerSecond * directionMultiplier : 0;
        float zRotation = rotateZ ? Time.deltaTime * speedPerSecond * directionMultiplier : 0;

        this.transform.Rotate(xRotation, yRotation, zRotation, Space.Self);
    }
}