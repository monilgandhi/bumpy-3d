using System;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ExceptionHandlingBehaviour : MonoBehaviour 
{
    [SerializeField]
    private CanvasGroup exceptionCanvasGroup;
    private void Awake() 
    {
#if !UNITY_EDITOR
        Application.logMessageReceived += HandleException;
#endif
        this.exceptionCanvasGroup.gameObject.SetActive(false);
        this.exceptionCanvasGroup.alpha = 0;
    }    

    public void Restart()
    {
        this.exceptionCanvasGroup.gameObject.SetActive(false);
        this.exceptionCanvasGroup.alpha = 0;
        Scene s = SceneManager.GetActiveScene();
        SceneManager.LoadScene(s.name);
    }

    // TODO this should probably get uploaded to some server
    private void HandleException(string condition, string stackTrace, LogType type)
     {
         if (type == LogType.Exception)
         {
            BumperCopsDebug.LogException(new Exception(stackTrace));
            this.exceptionCanvasGroup.gameObject.SetActive(true);
            this.exceptionCanvasGroup.alpha = 1;
         }
     }
}