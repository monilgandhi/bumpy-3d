using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TimerBehaviour : MonoBehaviour 
{
    public delegate void TimerOverEventHandler();
    public event TimerOverEventHandler TimeUp;

    [Header("UI elements")]
    [Tooltip("The text element where this timer will be displayed")]
    [SerializeField]
    private TMP_Text clockTextTimer;

    [SerializeField]
    [Tooltip("The clock panel where this timer will be displayed. Should be in topPanel")]
    private CanvasGroup clockPanel;

    [SerializeField]
    [Tooltip("The deduct text that shows up in the top panel")]
    private TMP_Text deductText;

    [Header("Game Components")]
    [SerializeField]
    private EnemyManager enemyManager;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private TimerExtendBehaviour timerExtendBehaviour;

    [SerializeField]
    private PowerupManager powerupManager;

    [SerializeField]
    private TopPanelAnimationBehaviour topPanelAnimationBehaviour;

    [SerializeField]
    private ScreenTextAnimation screenTextAnimation;

    private bool waitingForTimeExtension;
    private int timeTocatchTheEnemy = 60;
    // represents the enemy that was destroyed if the current one is not destroyed.
    // resets when new enemy is spawned
    
    private float enemyTimer;
    private int remainingTime;
    public int RemainingTime  
    {
        get 
        {
            return remainingTime;
        }
    }

    public float RemainingPercentTime  
    {
        get 
        {
            if(this.timeTocatchTheEnemy > 0)
            {
                return (float)remainingTime / (float)this.timeTocatchTheEnemy;    
            }

            return 0;
        }
    }

    public float TimePassedCurrentEnemy { get; private set; }
    private EnemyController enemy;
    private Gradient gradient;
    private GradientColorKey[] gradientColorKey;
    private GradientAlphaKey[] alphaKey;
    public const float TIME_PENALTY_HIT = 10f;
    private bool isTimeExtensionDialogueDisplayed = false;
    private float previousHitTime;
    private CoinStatsBehaviour coinStatsBehaviour;
    private bool IsDeductAnimationRunning;

    private const int TIME_EXTENSION_ON_HIT = 5;
    private void Awake() 
    {
        ResetTime();
        timerExtendBehaviour.TimeExtended += OnTimeExtended;
        coinStatsBehaviour = this.GetComponent<CoinStatsBehaviour>();

        powerupManager.PowerupCollected += OnPowerupCollected;
    }

    private void Start() 
    {    
        this.enemyManager.EnemySpawned += OnEnemySpawned;
        this.player.healthController.PlayerObstacleCollided += OnPlayerObstacleCollided;
    }

    private void OnPlayerObstacleCollided(PlayerObstacleCollisionEventArgs args)
    {
        BumperCopsDebug.Log($"timer: {enemyTimer}, isActive: {this.gameObject.activeInHierarchy}, hit counted: {args.IsHitCounted}");
        if(enemyTimer <= 0 || !this.gameObject.activeInHierarchy || !args.IsHitCounted)
        {
            return;
        }
        
        //BumperCopsDebug.Log("TimeBehaviour: Giving the penalty");

        float timeToDeduct = remainingTime < TIME_PENALTY_HIT ? this.remainingTime : TIME_PENALTY_HIT;
        //BumperCopsDebug.Log("TimeBehaviour: Penalty given");

        this.topPanelAnimationBehaviour.PlayClockAnimation($"{timeToDeduct}", Color.red);
        StartCoroutine(Deduct(Mathf.RoundToInt(timeToDeduct)));
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        // reset
        SetupGradient();
        this.enemy = args.enemy;
        timeTocatchTheEnemy = this.enemy.EnemyCharacteristics.Time;
        remainingTime = timeTocatchTheEnemy;
        this.enemy.Health.EnemyHit += OnEnemyHit;

        // set the timer
        enemyTimer = Time.deltaTime;
        previousHitTime = Time.time;
        this.enemyManager.currentEnemy.DamageController.EnemyDestructionBegin += OnEnemyDestroyed;

        // start the timer
        clockPanel.alpha = 1;
        TimePassedCurrentEnemy = 0;
    }

    private void OnEnemyHit(AttackArgs args)
    {
        if(args.RemainingHealth <= 0)
        {
            //enemy destroyed. Nothing to do
            OnEnemyDestroyed();
            return;
        }
        
        return;
        
        int timeExtension = Math.Min(Mathf.RoundToInt(Time.time - previousHitTime), TIME_EXTENSION_ON_HIT);
        previousHitTime = Time.time;
        if(timeExtension <= 0)
        {
            return;
        }

        screenTextAnimation.PlayAnimation($"Time Extended by {timeExtension}");
        ExtendTime(timeExtension);
    }

    private IEnumerator AnimationSequenceOnHit(int timeExtension)
    {
        screenTextAnimation.PlayAnimation($"Time Extended");
        yield return new WaitUntil(() => screenTextAnimation.IsAnimatorPlaying);

        AnimationSequenceOnTimeExtension(timeExtension);
    }

    private void OnTimeExtended(TimeExtensionArgs args)
    {
        if(!args.TimeExtended)
        {
            TimeUp();
            return;
        }

        isTimeExtensionDialogueDisplayed = false;
        BumperCopsDebug.Log($"Extending time by {args.TimeExtension}");
        ExtendTime(args.TimeExtension);
    }

    // there may be a race condition where player just extended the time but also crashed right before the dialogue
    // the collision may arrive later on and we may penalize the player after extension which is a horrific experience
    private IEnumerator StartPenalizingAfterTimeExtension()
    {
        yield return new WaitForSeconds(3f);
        //canGetPenalized = true;
    }

    // this is the animation that rurns from the player car to the top panel.
    // we want to sync it so that it looks better and player knows we increased time
    private void AnimationSequenceOnTimeExtension(float timeExtension)
    {
        ExtendTime(timeExtension);
        this.topPanelAnimationBehaviour.PlayClockAnimation($"+ {timeExtension}", Color.green);
        // now change the color to green
        //StartCoroutine(Deduct($"+ {timeExtension}", Color.green));
    }

    private void OnPowerupCollected(CollectiblePickedEventArgs args)
    {
        if(args == null)
        {
            BumperCopsDebug.LogWarning("Argument is null");
            return;
        }
        
        if(args.Type == PowerupType.AIR_SUPPORT || 
            args.Type == PowerupType.INSTANT_BONUS ||
            args.Type == PowerupType.BACKUP && this.enemy != null && this.enemy.Health.RemainingHits < 2)
        {
            OnEnemyDestroyed();
            return;
        }

        if(args.Type != PowerupType.CLOCK)
        {
            return;
        }

        if(args.Value <= 0)
        {
            BumperCopsDebug.LogWarning("Value for time power up is 0");
            return;
        }

        AnimationSequenceOnTimeExtension(args.Value);
    }

    private void ExtendTime(float timeExtension)
    {
        if(remainingTime + timeExtension > timeTocatchTheEnemy)
        {
            // cap it to timeToCatchTheEnemy
            timeExtension = timeTocatchTheEnemy - remainingTime;
        }

        //BumperCopsDebug.Log($"Enemy timer before {enemyTimer}");
        enemyTimer -= timeExtension;
        if(enemyTimer < 0)
        {
            enemyTimer = 0.1f;
        }
        //BumperCopsDebug.LogFormat($"TimerBehaviour: Time extended by {timeExtension}, enemy timer {enemyTimer}");
    }
    
    private IEnumerator Deduct(int delta)
    {
        BumperCopsDebug.Log("Deduction started");
        IsDeductAnimationRunning = true;
        float targetValue = enemyTimer + delta;
        int enemyTimerInt = Mathf.RoundToInt(enemyTimer);
        while(enemyTimerInt < Mathf.RoundToInt(targetValue))
        {
            enemyTimer += Time.deltaTime * 8;
            enemyTimerInt = Mathf.RoundToInt(enemyTimer);
            yield return new WaitForEndOfFrame();
        }

        IsDeductAnimationRunning = false;
        BumperCopsDebug.Log("Deduction over");
    }

    private void ResetTime() 
    {
        clockPanel.alpha = 0;
        enemyTimer = -1;
        clockTextTimer.text = Mathf.RoundToInt(timeTocatchTheEnemy).ToString();
    }
    
    private void OnEnemyDestroyed()
    {
        // reset
        ResetTime();
    }
    
    private bool IsCollisionAnimationRunning()
    {
        return this.IsDeductAnimationRunning && !this.topPanelAnimationBehaviour.IsClockAnimationComplete;
    }

    private void Update() 
    {
        if(enemyTimer <= 0 || isTimeExtensionDialogueDisplayed)
        {
            return;
        }

        // in case the time is less than what we deduct on collision, it may seem wierd to player that we showed popup.
        // may seem like a bug. Hence we wait till the animation is over
        if(remainingTime <= 0 && IsCollisionAnimationRunning())
        {
            return;
        }

        TimePassedCurrentEnemy += Time.deltaTime;
        enemyTimer += Time.deltaTime;

        if(enemyTimer > timeTocatchTheEnemy)
        {
            remainingTime = 0;
        }
        else
        {
            remainingTime = Mathf.RoundToInt(timeTocatchTheEnemy - enemyTimer);
        }
       
        clockTextTimer.text = remainingTime.ToString();
        clockTextTimer.color = gradient.Evaluate((float)remainingTime/timeTocatchTheEnemy);
       // BumperCopsDebug.Log($"TimerBehaviour: color {clockTextTimer.color} for timr {remainingTime}");

        // it is possible that the control is here when the event is triggered. 
        if(remainingTime <= 0 && !IsCollisionAnimationRunning())
        {
            // hide the deduct
            Color deductColorNonAlpha = new Color(this.deductText.color.r, this.deductText.color.g, this.deductText.color.b, 0);
            this.deductText.color = deductColorNonAlpha;
            isTimeExtensionDialogueDisplayed = timerExtendBehaviour.ShowIfPossible();
            
            // it is not possible to show the dialog. 
            if(!isTimeExtensionDialogueDisplayed)
            {
                enemyTimer = 0;
                TimeUp();
            }
        }

        //BumperCopsDebug.LogFormat("TimerBehaviour: fill amount " + this.clockFillImage.fillAmount + " current timer " + currentTimer.Elapsed.Seconds);
    }

    private void SetupGradient()
    {
        gradientColorKey = new GradientColorKey[4];

        // we just want two alpha since we will always show
        alphaKey = new GradientAlphaKey[2];

        // start with white
        gradientColorKey[0].color = new Color(0.26f, 0.91f, 0.5f, 1);
        gradientColorKey[0].time = 1.0f;
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = timeTocatchTheEnemy;

        // move to yellowish
        gradientColorKey[1].color = new Color(0.745f, 0.745f, 0.024f);
        gradientColorKey[1].time = 0.5f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 0.0f;


        // move to orange
        gradientColorKey[2].color = new Color(1, 0.22f, 0);
        gradientColorKey[2].time = 0.3f;

        // move to red
        gradientColorKey[3].color = new Color(1, 0, 0);
        gradientColorKey[3].time = 0.16f;

        gradient = new Gradient();
        gradient.SetKeys(gradientColorKey, alphaKey);
    }
}