using UnityEngine;

public class ScrollTexture : MonoBehaviour 
{
    private const float ScrollY = 0.7f;

    private Renderer myRenderer;
    private void Awake() 
    {
        this.myRenderer = this.GetComponent<Renderer>();
    }
    private void Update() 
    {
        float offSetY = Time.time * ScrollY * -1;
        myRenderer.material.mainTextureOffset = new Vector2(0, offSetY);
    }  
}