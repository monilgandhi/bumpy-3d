using UnityEngine;
using UnityEngine.UI;
public class SoundBehaviour : MonoBehaviour 
{
    [SerializeField]
    private Image SoundOn;

    [SerializeField]
    private Image SoundOff;

    private void OnEnable() 
    {
        SetSound();
    }

    public void ToggleSound()
    {
        // update the sound
        PlayerStats.Instance.ToggleSound();
        SetSound();
    }

    private void SetSound()
    {
        bool isSoundTurnedOn = PlayerStats.Instance.IsSoundTurnedOn();
        AudioListener.volume = isSoundTurnedOn ? 1.0f : 0.0f;
        SoundOn.gameObject.SetActive(isSoundTurnedOn);
        SoundOff.gameObject.SetActive(!isSoundTurnedOn);
    }
}