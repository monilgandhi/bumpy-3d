using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScreenTextAnimation : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text screenText;
    private Animator animator;

    public bool IsAnimatorPlaying { get; private set; }
    private Queue<string> queuedAnimation = new Queue<string>();
    private void Awake() 
    {
        screenText.gameObject.SetActive(false);
        animator = screenText.GetComponent<Animator>();
    }

    public void PlayAnimation(string str)
    {
        queuedAnimation.Enqueue(str);
    }

    private void Update() 
    {
        if(queuedAnimation.Count == 0)
        {
            return;
        }    

        if(!IsAnimatorPlaying)
        {
            IsAnimatorPlaying = true;
            StartCoroutine(PlayAnimation());
        }
    }

    private IEnumerator PlayAnimation()
    {
        screenText.gameObject.SetActive(true);
        while(queuedAnimation.Count > 0)
        {
            screenText.text = queuedAnimation.Dequeue();
            animator.SetTrigger("show");

            yield return new WaitForSeconds(0.5f);
            yield return new WaitWhile(() => animator.GetCurrentAnimatorStateInfo(0).IsName("show") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);
        }

        IsAnimatorPlaying = false;
        screenText.gameObject.SetActive(true);
    }
}