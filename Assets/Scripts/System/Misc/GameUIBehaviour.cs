using UnityEngine;

public class GameUIBehaviour : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;
    public GameManager GameManager { get { return gameManager; }}
    private CanvasGroup canvasGroup;

    private void Awake() {
        gameManager.GameStarted += (args) => Show();
        gameManager.GamePaused += () => Hide();
        gameManager.GameOver += (r) => Hide();
        Hide();
    }   

    public void Show()
    {
        this.transform.parent.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.transform.parent.gameObject.SetActive(false);
    }
}