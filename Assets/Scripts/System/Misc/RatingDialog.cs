using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_ANDROID
using Google.Play.Review;
#elif UNITY_IOS && !UNITY_EDITOR
using UnityEngine.iOS;
#endif

public class RatingDialog : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private bool testRatingDialog;

    [SerializeField]
    private BumperCopsGameplayAnalytics analytics;

    private long currentSessionNumber;

    public bool HasFlowFinished { get; private set; }
    private static readonly HashSet<int> SESSION_FOR_REVIEW = new HashSet<int>()
    {
        3,
        7,
        11
    };

    private const int MIN_SESSION_NUMBER = 2;

    private bool isRatingDialogAvailable;
    public bool ShouldShowRating { get ; private set; }

    private void Start() 
    {
        currentSessionNumber = PlayerStats.Instance.CurrentSessionNumber;
    }

    public void PrepRatingDialog()
    {
        if(PlayerStats.Instance.ReviewProvided)
        {
            BumperCopsDebug.Log($"Review already provided");
            ShouldShowRating = false;
            return;
        }
        
        long nowSeconds =  Convert.ToInt64((DateTime.UtcNow - Constants.EPOCH).TotalSeconds);
        long lastReviewTime = PlayerStats.Instance.LastReviewTime();

        BumperCopsDebug.Log($"current sessions {currentSessionNumber}, lastreview {lastReviewTime}");
        //BumperCopsDebug.Log($"checking if Rating dialog is available now: {nowSeconds}, lastReviewTime: {lastReviewTime}, {this.gameManager.CurrentSessionNumber}");
        // if current session is greater than 3 & never reviewed, this is for existing users
        // or if we have tried to review with this user before then do it in the given session if not asked in last 24 hours
        
        ShouldShowRating = (currentSessionNumber > MIN_SESSION_NUMBER && lastReviewTime == 0) ||
            (SESSION_FOR_REVIEW.Contains((int)this.currentSessionNumber) && 
                nowSeconds - lastReviewTime > 86400);

        #if UNITY_ANDROID && !UNITY_EDITOR
            StartCoroutine(GooglePlay());
        #else
            isRatingDialogAvailable = true;
        #endif

    }

    public bool TakeUserToRateApp()
    {
        PlayerStats.Instance.ReviewProvided = true;
        string uriToOpen = null;

        #if UNITY_ANDROID && !UNITY_EDITOR
            uriToOpen = $"market://details?id={Application.identifier}";
        #elif UNITY_IOS && !UNITY_EDITOR
            uriToOpen = "itms://itunes.apple.com/app/apple-store/id1519832037";
        #else 
            return true;
        #endif

        // android
        Application.OpenURL (uriToOpen);
        return true;
    }

    public bool IsInbuiltDialogueAvailable()
    {
        return isRatingDialogAvailable || (Application.isEditor && testRatingDialog);
    }

    public bool ShowInbuiltRatingDialogue()
    {
        BumperCopsDebug.Log("checking for rating");

        if(Application.isEditor && testRatingDialog)
        {
            HasFlowFinished = false;
            //BumperCopsDebug.Log("Showing editor version rating dialog");
            StartCoroutine(TestEditor());
            return true;
        }

        //BumperCopsDebug.Log($"Is review dialog available {isRatingDialogAvailable}");
        #if UNITY_ANDROID && !UNITY_EDITOR
            if(isRatingDialogAvailable)
            {    
                HasFlowFinished = false;
                BumperCopsDebug.Log("Showing the review dialog now");
                StartCoroutine(Show());
                return true;
            }
            
        #elif UNITY_IOS && !UNITY_EDITOR
            if(isRatingDialogAvailable)
            {
                BumperCopsDebug.Log("Asking for rating now");
                HasFlowFinished = true;
                return UnityEngine.iOS.Device.RequestStoreReview();
            }
            
        #endif
        return false;
    }

    private IEnumerator TestEditor()
    {
        if(!testRatingDialog)
        {
            yield break;
        }

        yield return new WaitForSecondsRealtime(3f);
        HasFlowFinished = true;
    }

#if UNITY_ANDROID
    private PlayReviewInfo playReviewInfo;
    private ReviewManager reviewManager;
    private IEnumerator GooglePlay()
    {
        reviewManager = new ReviewManager();
        BumperCopsDebug.Log("Called to initialize playReviewInfo");
        var requestFlowOperation = reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            BumperCopsDebug.LogWarning($"requestFlowOperation error {requestFlowOperation.Error.ToString()}");
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }

        playReviewInfo = requestFlowOperation.GetResult();

        if(playReviewInfo == null)
        {
            BumperCopsDebug.Log("Player review is null");
        }

        isRatingDialogAvailable = playReviewInfo != null;
    }

    private IEnumerator Show()
    {
        BumperCopsDebug.Log("Launching review flow now");
        var launchFlowOperation = reviewManager.LaunchReviewFlow(playReviewInfo);
        // BumperCopsDebug.Log("yielding launch flow operation");
        yield return launchFlowOperation;

        BumperCopsDebug.Log($"is review complete {launchFlowOperation.IsDone}, is successful {launchFlowOperation.IsSuccessful}");
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            BumperCopsDebug.LogWarning($"Review dialog error: {launchFlowOperation.Error.ToString()}");
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }

        BumperCopsDebug.Log("Review flow complete");
        HasFlowFinished = true;
        isRatingDialogAvailable = false;
        playReviewInfo = null;
    }

#endif
}