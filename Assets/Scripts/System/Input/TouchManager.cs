using UnityEngine;
using DigitalRubyShared;
public class TouchManager : MonoBehaviour 
{
    [SerializeField]
    private GameManager gameManager;
    public delegate void TapEventHandler(TapGestureArgs args);
    public event TapEventHandler Tapped;
    public event TapEventHandler DoubleTapped;
    public event TapEventHandler SwipeUp;
    public event TapEventHandler SwipeLeft;
    public event TapEventHandler SwipeRight;
    private TapGestureRecognizer TapGesture;
    private SwipeGestureRecognizer SwipeGesture;
    private TapGestureRecognizer DoubleTapGesture;
    private void Awake() 
    {
        TapGesture = new TapGestureRecognizer { MaximumNumberOfTouchesToTrack = 10 };
        TapGesture.StateUpdated += TapGestureStateUpdate;
        FingersScript.Instance.AddGesture(TapGesture);
        TapGesture.Enabled = false;

        // swipe up
        SwipeGesture = new SwipeGestureRecognizer();
        SwipeGesture.StateUpdated += SwipeUpdated;
        SwipeGesture.DirectionThreshold = 0;
        SwipeGesture.MinimumSpeedUnits = 1.0f;
        SwipeGesture.MinimumDistanceUnits = 0.1f;
        SwipeGesture.EndMode = SwipeGestureRecognizerEndMode.EndImmediately;
        SwipeGesture.MinimumNumberOfTouchesToTrack = SwipeGesture.MaximumNumberOfTouchesToTrack = 1;
        SwipeGesture.Enabled = true;
        FingersScript.Instance.AddGesture(SwipeGesture);

        FingersScript.Instance.ShowTouches = false;

        gameManager.TitlePageLoaded += OnTitlePageLoaded;
    }

    private void OnTitlePageLoaded()
    {
        TapGesture.Enabled = true;
    }

    private void SwipeUpdated(DigitalRubyShared.GestureRecognizer gesture)
    {
        SwipeGestureRecognizer swipe = gesture as SwipeGestureRecognizer;
        if (swipe.State == GestureRecognizerState.Ended)
        {
            TapGestureArgs args = new TapGestureArgs(gesture.FocusX, gesture.FocusY);
            switch (swipe.EndDirection)
            {
                case SwipeGestureRecognizerDirection.Up:
                    if(SwipeUp != null)
                    {
                        BumperCopsDebug.Log("Swipe up detected");
                        SwipeUp(args);
                    }
                    break;

                case SwipeGestureRecognizerDirection.Left:
                    if(SwipeLeft != null)
                    {
                        SwipeLeft(args);
                    }
                    break;

                case SwipeGestureRecognizerDirection.Right:
                    if(SwipeRight != null)
                    {
                        SwipeRight(args);
                    }
                    
                    break;

                default:
                    break;
            }
        }
    }

    private void TapGestureStateUpdate(DigitalRubyShared.GestureRecognizer gesture)
    {
        if(gesture.State == GestureRecognizerState.Ended)
        {
            TapGestureArgs args = new TapGestureArgs(gesture.FocusX, gesture.FocusY);
            Tapped(args);
        }
    }
}