using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MultiChoiceQuestionBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text question;

    [SerializeField]
    private Image failedValidationImage;

    [SerializeField]
    private TMP_Text[] toolTips;

    [SerializeField]
    private GameObject choiceButtonPrefab;

    [SerializeField]
    private HorizontalLayoutGroup choicePanel;
    
    private Color normalColor;
    private Color selectedColor;
    private List<Button> choiceButtons = new List<Button>();
    private int currentSelectedButtonIdx = -1;
    public FeedbackQuestionConfig Question { get; private set; }
    
    public void Init(FeedbackQuestionConfig config)
    {
        if(config == null)
        {
            BumperCopsDebug.LogError("Multi choice question is empty");
            return;
        }

        if(!config.IsChoice)
        {
            BumperCopsDebug.LogError("Not a multi choice question");
            return;
        }

        this.Question = config;
        if(toolTips.Length > 0)
        {
            toolTips[0].text = config.ToolTips[0];
        }

        if(toolTips.Length > 1)
        {
            toolTips[1].text = config.ToolTips[1];
        }

        foreach(Button b in choiceButtons)
        {
            b.gameObject.SetActive(false);
        }

        // insert buttons
        for(int i = 0; i < config.Choices.Count; i++)
        {
            Button b;
            if(choiceButtons.Count <= i)
            {
                GameObject g = Instantiate(choiceButtonPrefab, choicePanel.transform);
                b = g.GetComponent<Button>();
                b.name = i.ToString();
                b.onClick.AddListener(() => OnButtonClicked(b));
                choiceButtons.Add(b);

                if(choiceButtons.Count <= 1)
                {
                    normalColor = b.colors.normalColor;
                    selectedColor = b.colors.selectedColor;
                }
            }
            else
            {
                b = choiceButtons[i];
            }

            TMP_Text t = b.GetComponentInChildren<TMP_Text>();
            t.text = config.Choices[i];
            b.gameObject.SetActive(true);
        }

        if(currentSelectedButtonIdx >= 0)
        {
            ChangeSelectionButton(false);
        }

        BumperCopsDebug.Log($"MultiChoiceQuestionBehaviour: question is {question.text}");
        question.text = config.Question;
        failedValidationImage.enabled = false;
        currentSelectedButtonIdx = -1;
    }

    private void ChangeSelectionButton(bool isSelected)
    {
        if(isSelected)
        {
            ColorBlock newButtonColorBlock = choiceButtons[currentSelectedButtonIdx].colors;
            newButtonColorBlock.normalColor = selectedColor;
            choiceButtons[currentSelectedButtonIdx].colors = newButtonColorBlock;
        }
        else
        {
            ColorBlock oldButtonColorBlock = choiceButtons[currentSelectedButtonIdx].colors;
            oldButtonColorBlock.normalColor = normalColor;
            choiceButtons[currentSelectedButtonIdx].colors = oldButtonColorBlock;
        }
    }

    private void OnButtonClicked(Button b)
    {
        if(currentSelectedButtonIdx >= 0)
        {
            ChangeSelectionButton(false);
        }
        
        currentSelectedButtonIdx = Convert.ToInt16(b.name);

        ChangeSelectionButton(true);

        BumperCopsDebug.Log($"MultiChoiceQuestionBehaviour: Selecting index {currentSelectedButtonIdx}");
        this.failedValidationImage.enabled = false;
    }

    public bool Validate()
    {
        if(currentSelectedButtonIdx < 0)
        {
            failedValidationImage.enabled = true;
            return false;
        }

        this.Question.Response = Question.Choices[currentSelectedButtonIdx];
        return true;
    }
}