using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputQuestionBehaviour : MonoBehaviour 
{
    [SerializeField]
    private TMP_Text question;

    [SerializeField]
    private TMP_InputField inputField;

    [SerializeField]
    private Image failedValidationImage;

    public FeedbackQuestionConfig Question { get; private set; }
    public void Init(FeedbackQuestionConfig config)
    {
        if(config == null)
        {
            BumperCopsDebug.LogError("Input question is empty");
            return;
        }

        if(config.IsChoice)
        {
            BumperCopsDebug.LogError("Not a input question");
            return;
        }

        this.Question = config;
        question.text = config.Question;
        inputField.text = "";
        this.failedValidationImage.enabled = false;
    }

    public bool Validate()
    {
        BumperCopsDebug.Log($"InputQuestionBehaviour: input field text length is {this.inputField.text.Length}");
        if(this.inputField.text.Length <= 0)
        {
            StartCoroutine(BlinkError());
            return false;
        }

        this.Question.Response = this.inputField.text;
        this.failedValidationImage.enabled = false;
        return true;
    }

    private IEnumerator BlinkError()
    {
        int loopCount = 0;
        while(loopCount < 4)
        {
            this.failedValidationImage.enabled = true;
            yield return new WaitForSecondsRealtime(0.2f);
            this.failedValidationImage.enabled = false;
            yield return new WaitForSecondsRealtime(0.2f);
            ++loopCount;
        }
    }
}