using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using TMPro;
public class FeedbackFormBehaviour : MonoBehaviour 
{
    public delegate void FeedbackEventHandler(FeedbackEventArgs args);
    public event FeedbackEventHandler FeedbackCompleted;

    [SerializeField]
    private GameObject multiChoicePrefab;

    [SerializeField]
    private GameObject inputTextPrefab;

    [SerializeField]
    private GameObject questionsPanel;

    [SerializeField]
    private TMP_Text continueText;

    [SerializeField]
    private TMP_Text title;

    [SerializeField]
    private TMP_Text description;

    [Header("Editor Only")]
    [SerializeField]
    private bool readLocal;

    private const string FEEDBACK_FOLDER_NAME = "feedback";
    private MultiChoiceQuestionBehaviour multiChoiceQuestion;
    private InputQuestionBehaviour inputTextQuestion;
    private List<FeedbackQuestionConfig> configQuestions = new List<FeedbackQuestionConfig>();

    private int currentQuestionIndex;
    private Animator animator;

    private void Awake() 
    {
        ReadLocal();
        
        GameObject multiChoiceGameObject = Instantiate(multiChoicePrefab, questionsPanel.transform);
        multiChoiceQuestion = multiChoiceGameObject.GetComponent<MultiChoiceQuestionBehaviour>();

        GameObject inputTextGameObject = Instantiate(inputTextPrefab, questionsPanel.transform);
        inputTextQuestion = inputTextGameObject.GetComponent<InputQuestionBehaviour>();

        HideQuestions();

        this.animator = this.GetComponent<Animator>();
        // hide
        this.gameObject.SetActive(false);
    }

    public bool DisplayFeedbackForm()
    {
        return false;
        if(configQuestions.Count == 0)
        {
            BumperCopsDebug.LogWarning("feedback question count is 0");
            return false;
        }

        //BumperCopsDebug.Log($"have we shown feedback form {PlayerStats.Instance.HaveShownFeedbackForm(configQuestions[0].Version)}");
        if(PlayerStats.Instance.HaveShownFeedbackForm(configQuestions[0].Version))
        {
            return false;
        }

        CreateCurrentForm();   
        this.gameObject.SetActive(true);
        return true;     
    }

    private void ReadLocal()
    {
        string filePath = FEEDBACK_FOLDER_NAME + Path.DirectorySeparatorChar + "feedbackform";

        TextAsset feedbackJson = Resources.Load<TextAsset>(filePath);

        if(feedbackJson == null)
        {
            BumperCopsDebug.LogError($"File not found {filePath}");
            return;
        }

        string json = Encoding.UTF8.GetString(feedbackJson.bytes, 0, feedbackJson.bytes.Length);
        configQuestions = JsonConvert.DeserializeObject<List<FeedbackQuestionConfig>>(json);
    }

    private void HideQuestions()
    {
        multiChoiceQuestion.gameObject.SetActive(false);
        inputTextQuestion.gameObject.SetActive(false);
    }

    private void CreateCurrentForm()
    {
        // move to the next two questions
        for(int i = 0; i < 2; i++)
        {
            if(i >= configQuestions.Count)
            {
                break;
            }

            //BumperCopsDebug.Log("FeedbackFormBehaviour: Setting up new question");
            FeedbackQuestionConfig cq = configQuestions[i + currentQuestionIndex];
            if(cq.IsChoice)
            {
                multiChoiceQuestion.Init(cq);
                multiChoiceQuestion.gameObject.SetActive(true);
            }
            else
            {
                inputTextQuestion.Init(cq);
                inputTextQuestion.gameObject.SetActive(true);
            }
        }

        currentQuestionIndex += 2;

        if(currentQuestionIndex < configQuestions.Count - 1)
        {
            int totalPages = Mathf.CeilToInt(configQuestions.Count / 2);
            int currentPage = (currentQuestionIndex + 1) / 2;
            continueText.text = $"Next   ({currentPage}/{totalPages})";
        }
        else
        {
            continueText.text = "Submit";
        }
    }

    public void Next()
    {
        bool isValid = multiChoiceQuestion.Validate() && inputTextQuestion.Validate();

        if(!isValid)
        {
            return;
        }

        if(currentQuestionIndex >= configQuestions.Count)
        {
            //BumperCopsDebug.Log("FeedbackFormBehaviour: hiding");
            StartCoroutine(Hide(true));
        }
        else
        {
            //BumperCopsDebug.Log("FeedbackFormBehaviour: fliping");
            StartCoroutine(PlayFlipAndChangeQuestions());
        }
    }

    private IEnumerator PlayFlipAndChangeQuestions()
    {
            this.animator.SetTrigger("flip");
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("flip") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);
            CreateCurrentForm();
    }

    private IEnumerator Hide(bool feedbackProvided = false)
    {
        // thank the user for providing feedback
        if(feedbackProvided)
        {
            HideQuestions();
            continueText.enabled = false;
            // change the title
            title.text = "Thank you for providing the feedback";
            description.text = "";

            this.animator.SetTrigger("flip");
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("flip") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);

            yield return new WaitForSecondsRealtime(2f);
        }

        //PlayerStats.Instance.MarkFeedbackFormShown(inputTextQuestion.Question.Version);
        if(FeedbackCompleted != null)
        {
            FeedbackCompleted(new FeedbackEventArgs(currentQuestionIndex));
        }

        this.animator.SetTrigger("hide");
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("hide") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1);
    }

    public void Close()
    {
        StartCoroutine(Hide());
    }
}