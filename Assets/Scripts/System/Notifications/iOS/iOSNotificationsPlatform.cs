#if UNITY_IOS
using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity.Notifications.iOS;

namespace BumperCops.Notifications.iOS
{
    /// <summary>
    /// iOS implementation of <see cref="IGameNotificationsPlatform"/>.
    /// </summary>
    public class iOSNotificationsPlatform : IGameNotificationsPlatform<iOSGameNotification>,
        IDisposable
    {
        
        /// <inheritdoc />
        public event Action<IGameNotification> NotificationReceived;

        /// <summary>
        /// Instantiate a new instance of <see cref="iOSNotificationsPlatform"/>.
        /// </summary>
        public iOSNotificationsPlatform()
        {
            iOSNotificationCenter.OnNotificationReceived += OnLocalNotificationReceived;
        }

        /// <inheritdoc />
        public void ScheduleNotification(IGameNotification gameNotification)
        {
            if (gameNotification == null)
            {
                throw new ArgumentNullException(nameof(gameNotification));
            }

            if (!(gameNotification is iOSGameNotification notification))
            {
                throw new InvalidOperationException(
                    "Notification provided to ScheduleNotification isn't an iOSGameNotification.");
            }

            ScheduleNotification(notification);
        }

        /// <inheritdoc />
        public void ScheduleNotification(iOSGameNotification notification)
        {
            if (notification == null)
            {
                throw new ArgumentNullException(nameof(notification));
            }

            iOSNotificationCenter.ScheduleNotification(notification.InternalNotification);
            notification.OnScheduled();
        }

        public void SendNotification(IGameNotification notification)
        {
            iOSGameNotification an = notification as iOSGameNotification;
            iOSNotificationCenter.ScheduleNotification(an.InternalNotification);
        }

        public void SendNotification(iOSGameNotification notification)
        {
            iOSNotificationCenter.ScheduleNotification(notification.InternalNotification);
        }

        /// <inheritdoc />
        /// <summary>
        /// Create a new <see cref="T:NotificationSamples.Android.AndroidNotification" />.
        /// </summary>
        IGameNotification IGameNotificationsPlatform.CreateNotification()
        {
            return CreateNotification();
        }

        /// <inheritdoc />
        /// <summary>
        /// Create a new <see cref="T:NotificationSamples.Android.AndroidNotification" />.
        /// </summary>
        public iOSGameNotification CreateNotification()
        {
            return new iOSGameNotification();
        }

        /// <inheritdoc />
        public void CancelNotification(int notificationId)
        {
            iOSNotificationCenter.RemoveScheduledNotification(notificationId.ToString());
        }

        /// <inheritdoc />
        public void DismissNotification(int notificationId)
        {
            iOSNotificationCenter.RemoveDeliveredNotification(notificationId.ToString());
        }

        /// <inheritdoc />
        public void CancelAllScheduledNotifications()
        {
            iOSNotificationCenter.RemoveAllScheduledNotifications();
        }

        public int GetDeliveredNotificationCount()
        {
            return iOSNotificationCenter.GetScheduledNotifications().Length;
        }

        /// <inheritdoc />
        public void DismissAllDisplayedNotifications()
        {
            iOSNotificationCenter.RemoveAllDeliveredNotifications();
        }

        public IGameNotification GetLastNotificationResponded()
        {
            iOSNotification notification = iOSNotificationCenter.GetLastRespondedNotification();
            if(notification == null)
            {
                return default;
            }

            return new iOSGameNotification(notification);
        }

        /// <summary>
        /// Clears badge count.
        /// </summary>
        public void OnForeground()
        {
            iOSNotificationCenter.ApplicationBadge = 0;
        }

        /// <summary>
        /// Does nothing on iOS.
        /// </summary>
        public void OnBackground() {}

        /// <summary>
        /// Unregister delegates.
        /// </summary>
        public void Dispose()
        {
            iOSNotificationCenter.OnNotificationReceived -= OnLocalNotificationReceived;
        }

        // Event handler for receiving local notifications.
        private void OnLocalNotificationReceived(iOSNotification notification)
        {
            // Create a new AndroidGameNotification out of the delivered notification, but only
            // if the event is registered
            NotificationReceived?.Invoke(new iOSGameNotification(notification));
        }

        public bool IsExplicitAuthorizationGranted()
        {
            return GetAuthorizationStatus() == AuthorizationStatus.Authorized;
        }

        private AuthorizationStatus GetAuthorizationStatus()
        {
            iOSNotificationSettings settings = iOSNotificationCenter.GetNotificationSettings();
            return settings.AuthorizationStatus;
        }

        public IEnumerator RequestAuthorization(bool pushNotifications, bool shouldAskForExplicitPermission)
        {
            AuthorizationStatus authStatus = GetAuthorizationStatus();
            AuthorizationOption options = AuthorizationOption.Alert | AuthorizationOption.Badge;
            if(authStatus == AuthorizationStatus.Denied || authStatus == AuthorizationStatus.Authorized)
            {
                yield break;
            }
            else if(authStatus == AuthorizationStatus.NotDetermined && !shouldAskForExplicitPermission)
            {
                // setup for provisional
                options |= AuthorizationOption.Provisional;
            }

            BumperCopsDebug.Log($"request authorization options are {options}");
            using (var req = new AuthorizationRequest(options, pushNotifications))
            {
                while (!req.IsFinished)
                { 
                    yield return null;
                };

                string result = "\n RequestAuthorization: \n";
                result += "\n finished: " + req.IsFinished;
                result += "\n granted :  " + req.Granted;
                result += "\n error:  " + req.Error;
                result += "\n deviceToken:  " + req.DeviceToken;
                BumperCopsDebug.Log(result);
            }
        }
    }
}
#endif
