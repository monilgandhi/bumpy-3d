using System;
using UnityEngine;
public class NotificationStats
{
    public DateTime InstallTimeUTC => PlayerStats.Instance.InstallTimeUTC;

    private bool? explicitPermissionAsked;
    public bool ExplicitPermissionAsked
    {
        get
        {
            if(!explicitPermissionAsked.HasValue)
            {
                explicitPermissionAsked = PersistenceManager.ExplicitPermissionAsked;
            }

            return explicitPermissionAsked.Value;
        }

        set
        {
            explicitPermissionAsked = PersistenceManager.ExplicitPermissionAsked = value;
        }
    }

    private DateTime? day1NotificationUTC;
    public DateTime? Day1NotificationUTC
    {
        get 
        {
            if(!day1NotificationUTC.HasValue)
            {
                day1NotificationUTC = PersistenceManager.Day1NotificationUTC;
            }

            return day1NotificationUTC;
        }

        set
        {
            PersistenceManager.Day1NotificationUTC = value;
            day1NotificationUTC = value;
        }
    }

    private int notificationContentIdx = -1; 
    public int NotificationContentIndex
    {
        get 
        {
            if(notificationContentIdx < 0)
            {
                notificationContentIdx = PersistenceManager.NotificationContentIdx;
            }

            return notificationContentIdx; 
        }

        set 
        {
            if(value < 0)
            {
                BumperCopsDebug.LogWarning("Invalid value of negative index");
            }

            notificationContentIdx = PersistenceManager.NotificationContentIdx = value;
        }
    }

    private int lastClickedNotificationTime = -1;
    public int LastClickedNotificationTime
    {
        get 
        { 
            if(lastClickedNotificationTime < 0)
            {
                lastClickedNotificationTime = PersistenceManager.LastClickNotificationTime;
            }

            return lastClickedNotificationTime;
        }

        set 
        {
            if(value <= 0)
            {
                return;
            }

            lastClickedNotificationTime = PersistenceManager.LastClickNotificationTime = value;
        }
    }
    
    private static NotificationStats instance;

    public static NotificationStats Instance 
    {
        get
        {
            if(instance == null)
            {
                instance = new NotificationStats();
            }

            return instance;
        }
    }

    private class PersistenceManager
    {
        private const string DAY_1_NOTIFICATION = "day_1_notification";
        private const string NOTIFICATION_CONTENT_IDX = "notification_content_idx";
        private const string LAST_CLICKED_NOTIFICATION = "last_clicked_notification";
        private const string EXPLICIT_PERMISSION_ASKED = "explicit_permission_asked";

        public static bool ExplicitPermissionAsked
        {
            get 
            {
                return PlayerPrefs.GetInt(EXPLICIT_PERMISSION_ASKED, 0) > 0;
            }

            set
            {
                PlayerPrefs.SetInt(EXPLICIT_PERMISSION_ASKED, Convert.ToInt32(value));
                PlayerPrefs.Save();
            }
        }

        public static DateTime? Day1NotificationUTC
        {
            get 
            {
                float secsSinceEpoch = PlayerPrefs.GetFloat(DAY_1_NOTIFICATION, 0);
                BumperCopsDebug.Log($"day 1 notification secs since epoch is {secsSinceEpoch}");

                if(secsSinceEpoch == 0)
                {
                    return null;
                }

                return DateTimeOffset.FromUnixTimeSeconds(Convert.ToInt64(secsSinceEpoch)).DateTime;
            }

            set
            {
                if(!value.HasValue)
                {
                    return;
                }

                DateTimeOffset valueUtc = value.Value.ToUniversalTime();
                BumperCopsDebug.Log($"Storing day 1 notification to utc {valueUtc} & {valueUtc.ToUnixTimeSeconds()}");
                PlayerPrefs.SetFloat(DAY_1_NOTIFICATION, Convert.ToInt64(valueUtc.ToUnixTimeSeconds()));
                PlayerPrefs.Save();
            }
        }

        public static int NotificationContentIdx
        {
            get { return PlayerPrefs.GetInt(NOTIFICATION_CONTENT_IDX, 0); }
            set
            {
                PlayerPrefs.SetInt(NOTIFICATION_CONTENT_IDX, value); 
                PlayerPrefs.Save();
            }      
        }

        public static int LastClickNotificationTime
        {
            get { return PlayerPrefs.GetInt(LAST_CLICKED_NOTIFICATION, 0); }
            set
            {
                PlayerPrefs.SetInt(LAST_CLICKED_NOTIFICATION, value); 
                PlayerPrefs.Save();
            }      
        }
    }
}