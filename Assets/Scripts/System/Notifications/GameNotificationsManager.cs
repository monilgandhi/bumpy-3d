using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

#if UNITY_ANDROID
using Unity.Notifications.Android;
using BumperCops.Notifications.Android;
#elif UNITY_IOS
using BumperCops.Notifications.iOS;
#endif

using UnityEngine;

namespace BumperCops.Notifications
{
    /// <summary>
    /// Global notifications manager that serves as a wrapper for multiple platforms' notification systems.
    /// </summary>
    public class GameNotificationsManager : MonoBehaviour
    {
        [Header("Game Elements")]
        [SerializeField]
        private GameManager gameManager;

        [SerializeField]
        private BumperCopsGameplayAnalytics analytics;

        [Header("Admin")]
        [SerializeField]
        private bool isTestingTurnedOn;

        [SerializeField]
        private bool scheduleNotificationDuringTesting;
        // Default filename for notifications serializer
        private const string DefaultFilename = "bumper-cops-notifications.bin";

        // for testing we just set at exactly the same second. Ideally we want this config driven or more smarter
        private static TimeSpan DEFAULT_TIME = new TimeSpan(18, 0 ,0);

        // for testing we just set at exactly the same second
        private static TimeSpan DEFAULT_TIME_TEST = new TimeSpan(0, 0 ,18);

        [SerializeField, Tooltip(
            "Check to make the notifications manager automatically set badge numbers so that they increment.\n" +
            "Schedule notifications with no numbers manually set to make use of this feature.")]
        private bool autoBadging = true;

        /// <summary>
        /// Event fired when a scheduled local notification is delivered while the app is in the foreground.
        /// </summary>
        public event Action<PendingNotification> LocalNotificationDelivered;

        /// <summary>
        /// Event fired when a queued local notification is cancelled because the application is in the foreground
        /// when it was meant to be displayed.
        /// </summary>
        /// <seealso cref="OperatingMode.Queue"/>
        public event Action<PendingNotification> LocalNotificationExpired;

        /// <summary>
        /// Gets the implementation of the notifications for the current platform;
        /// </summary>
        public IGameNotificationsPlatform Platform { get; private set; }

        /// <summary>
        /// Gets a collection of notifications that are scheduled or queued.
        /// </summary>
        public List<PendingNotification> PendingNotifications { get; private set; }

        /// <summary>
        /// Gets or sets the serializer to use to save pending notifications to disk if we're in
        /// <see cref="OperatingMode.RescheduleAfterClearing"/> mode.
        /// </summary>
        public IPendingNotificationsSerializer Serializer { get; set; }

        /// <summary>
        /// Gets whether this manager has been initialized.
        /// </summary>
        private bool initialized;

        // Flag set when we're in the foreground
        private bool inForeground = true;
        private bool canScheduleNotifications;
        private GameNotificationChannel defaultNotificationChannel;
        private bool explicitPermissionAskedInThisSession = false;
        private List<Tuple<string, string>> respondedNotification = new List<Tuple<string, string>>();
        
        private void Awake() 
        {
            DontDestroyOnLoad(this.gameObject);

            // default game notification channel
            defaultNotificationChannel = new GameNotificationChannel(
                    "bumper_cops_reminder", 
                    "Bumper Cops", 
                    "Bumper Cops Notifications");

            this.Initialize();
            canScheduleNotifications = true;
            
            if(isTestingTurnedOn)
            {
                scheduleNotificationDuringTesting = true;
            }
        }

        private void Start() 
        {
            if(Platform == null)
            {
                return;
            }

            gameManager.GameSessionInitialized += OnGameInitialized;
            CheckAndReportRespondedNotification();

            DateTimeOffset installDate = NotificationStats.Instance.InstallTimeUTC;

            BumperCopsDebug.Log($"Diff between now and install date is {(DateTime.UtcNow - installDate).TotalDays}");
            StartCoroutine(Platform.RequestAuthorization(true, false));
            if(Platform.GetDeliveredNotificationCount() > 0 && (DateTime.UtcNow - installDate).TotalDays >= 3)
            {
                BumperCopsDebug.Log($"Asking for permission since conditions passed");
                AskExplicitPermission();
            }
        }

        private void CheckAndReportRespondedNotification()
        {
            IGameNotification notification = Platform.GetLastNotificationResponded();
            if(notification == null)
            {
                BumperCopsDebug.Log("No notification found");
                return;
            }

            if(!notification.Id.HasValue || string.IsNullOrEmpty(notification.Metadata))
            {
                BumperCopsDebug.LogWarning("Missing metadata information");
                return;
            }

            BumperCopsDebug.Log($"Notification found with id {notification.Id.Value}");
            // check if we have been given explicit permission
            AskExplicitPermission();

            int lastReportedDateTime = NotificationStats.Instance.LastClickedNotificationTime;
            if(notification.Id <= lastReportedDateTime)
            {
                return;
            }

            string id, version;
            if(!NotificationContentItem.TryGetIdAndVersion(notification.Metadata, out id, out version))
            {
                BumperCopsDebug.LogWarning($"Invalid metadata information {notification.Metadata}");
                return;
            }

            // report and log
            //analytics.ReportLastClickedNotification(id, version);
            respondedNotification.Add(new Tuple<string, string>(id, version));
            NotificationStats.Instance.LastClickedNotificationTime = notification.Id.Value;
        }

        private void AskExplicitPermission()
        {
            BumperCopsDebug.Log("Asking for permission");
            if(Platform.IsExplicitAuthorizationGranted() || explicitPermissionAskedInThisSession)
            {
                BumperCopsDebug.Log("Permission already granted");
                return;
            }

            NotificationStats.Instance.ExplicitPermissionAsked = true;
            StartCoroutine(Platform.RequestAuthorization(true, true));
            explicitPermissionAskedInThisSession = true;
        }

        private void OnGameInitialized(GameEventArgs args)
        {
            canScheduleNotifications = true;
            if(CarUtils.IsUnityDevelopmentBuild() && !scheduleNotificationDuringTesting)
            {
                canScheduleNotifications = false;
            }
        }
        
        private void RemoveExpiredNotifications()
        {
            // remove expired ones
            for (var i = PendingNotifications.Count - 1; i >= 0; i--)
            {
                PendingNotification pendingNotification = PendingNotifications[i];
                // If a non-scheduled notification is in the past (or not within our threshold)
                // just remove it immediately
                if (pendingNotification.Notification.DeliveryTime.HasValue &&
                    pendingNotification.Notification.DeliveryTime.Value.Date <= DateTime.Now.Date)
                {
                    // BumperCopsDebug.Log($"Removing notification with delivery date of {pendingNotification.Notification.DeliveryTime}");
                    PendingNotifications.RemoveAt(i);
                    if (pendingNotification.Notification.Scheduled && pendingNotification.Notification.Id.HasValue)
                    {
                        // BumperCopsDebug.Log($"Canceling notification which was scheduled {pendingNotification.Notification.DeliveryTime}");
                        Platform.CancelNotification(pendingNotification.Notification.Id.Value);
                    }
                }
            }
        }

        private List<int> GetNotificationIntervalInMinutes()
        {
            // get the install date
            DateTimeOffset installDate = NotificationStats.Instance.InstallTimeUTC;
            BumperCopsDebug.Log($"install time in local {installDate.LocalDateTime}");
            DateTime utcNow = DateTime.UtcNow;

            List<int> notificationIntervals = new List<int>();
            DateTime? day1NotificationUtc = NotificationStats.Instance.Day1NotificationUTC;
            if((utcNow - installDate) > new TimeSpan(0, 7, 0, 0))
            {
                // less than 3 days and less than 7 days
                notificationIntervals.Add(50 * 60); 
                notificationIntervals.Add(144 * 60); 
            }
            // either we are doing it first time or this is day 3
            else if (!day1NotificationUtc.HasValue || (utcNow - day1NotificationUtc.Value >= new TimeSpan(0, 3, 0, 0)))
            {
                notificationIntervals.Add(14 * 60); 
                notificationIntervals.Add(50 * 60); 
                notificationIntervals.Add(144 * 60);
            }

            return notificationIntervals;
        }

        private List<int> GetNotificationIntervalForChallengeInMinutes()
        {
            List<int> notificationIntervals = new List<int>();
            if(ChallengeStats.Instance.ActiveChallenge != null)
            {
                TimeSpan minutesRemaining = ChallengeStats.Instance.ActiveChallenge.EndTime - DateTime.Now;
                if(minutesRemaining.TotalMinutes > 3)
                {
                    BumperCopsDebug.Log($"Challenge end utc time is {ChallengeStats.Instance.ActiveChallenge.EndTime.ToUniversalTime()} and minutes remaining is {minutesRemaining.TotalMinutes}");
                    notificationIntervals.Add((int)Math.Ceiling(minutesRemaining.TotalMinutes));
                }
            }

            return notificationIntervals;
        }

        private void ScheduleDailyNotifications()
        {
            if(!canScheduleNotifications)
            {
                return;
            }

            // BumperCopsDebug.Log($"Notification interval is of size {notificationInterval.Length}");

            // cancel all notifications
            Platform.CancelAllScheduledNotifications();
            Func<double, DateTime> functionToIncrement = DateTime.Today.AddDays;
            DateTimeOffset installLocal = NotificationStats.Instance.InstallTimeUTC.ToLocalTime();
            BumperCopsDebug.Log($"Install local is {installLocal}");
            DateTime dtToUse = DateTime.Today;
            dtToUse = dtToUse.AddHours(installLocal.Hour).AddMinutes(installLocal.Minute);
            //remove the seconds
            BumperCopsDebug.Log($"dttouse is {dtToUse}");
            
            if(isTestingTurnedOn)
            {
                dtToUse = dtToUse.AddHours(installLocal.Minute);
                // set to default seconds
                functionToIncrement = dtToUse.AddSeconds;
            }
            else
            {
                functionToIncrement = dtToUse.AddMinutes;
            }

            BumperCopsDebug.Log($"final dtToUse is {dtToUse}");

            // Sort notifications by delivery time, if no notifications have a badge number set
            bool noBadgeNumbersSet =
                PendingNotifications.All(notification => notification.Notification.BadgeNumber == null);

            // add notification of missing days
            PendingNotifications.Sort(PendingNotification.CompareByDeliveryTime);

            List<int> notificationIntervalInMinutes = GetNotificationIntervalInMinutes();
            if(notificationIntervalInMinutes == null)
            {
                // nothing to do
                return;
            }

            int intervalIdxDays = 0;
            while(PendingNotifications.Count < notificationIntervalInMinutes.Count)
            {
                IGameNotification newNotification = CreteAndScheduleNotification(
                    functionToIncrement(notificationIntervalInMinutes[intervalIdxDays]),
                    notificationIntervalInMinutes[intervalIdxDays++],
                    false);

                PendingNotifications.Add(new PendingNotification(newNotification));
                if(intervalIdxDays == 1)
                {
                    // BumperCopsDebug.Log("Storing day 1 notification");
                    NotificationStats.Instance.Day1NotificationUTC = newNotification.DeliveryTime.Value.ToUniversalTime();
                }
            }

            List<int> challengeNotificationInterval = GetNotificationIntervalForChallengeInMinutes();
            if(challengeNotificationInterval != null && challengeNotificationInterval.Count > 0)
            {
                // take only the first now
                IGameNotification challengeNotification = CreteAndScheduleNotification(
                    DateTime.Now.AddMinutes(challengeNotificationInterval[0]),
                    challengeNotificationInterval[0],
                    true);

                BumperCopsDebug.Log($"challenge notification now with delivery date {challengeNotification.DeliveryTime}");
                PendingNotifications.Add(new PendingNotification(challengeNotification));
            }   

            if (autoBadging)
            {
                // Set badge numbers incrementally
                var badgeNum = 1;
                foreach (PendingNotification pendingNotification in PendingNotifications)
                {
                    if (pendingNotification.Notification.DeliveryTime.HasValue &&
                        !pendingNotification.Notification.Scheduled)
                    {
                        pendingNotification.Notification.BadgeNumber = badgeNum++;
                    }
                }
            }

            for (int i = PendingNotifications.Count - 1; i >= 0; i--)
            {
                PendingNotification pendingNotification = PendingNotifications[i];
                // Ignore already scheduled ones
                if (pendingNotification.Notification.Scheduled)
                {
                    continue;
                }

                BumperCopsDebug.Log($"Sending the notification now with delivery date {pendingNotification.Notification.DeliveryTime}");
                // Schedule it now
                Platform.ScheduleNotification(pendingNotification.Notification);
                //BumperCopsDebug.Log($"Checcking status of notification with id {pendingNotification.Notification.Id.Value}: {AndroidNotificationCenter.CheckScheduledNotificationStatus(pendingNotification.Notification.Id.Value)}");
            }

            // get list of scheduled notifications
            
        }

        private IGameNotification CreteAndScheduleNotification(
                DateTime? deliveryTime,
                int metadata,
                bool isChallenge
            )
        {
            IGameNotification newNotification = Platform.CreateNotification();                
            newNotification.Metadata = $"{metadata}";
            newNotification.DeliveryTime = deliveryTime;

            DateTimeOffset idDateTime = newNotification.DeliveryTime.Value;
            newNotification.Id = Convert.ToInt32(idDateTime.ToUnixTimeSeconds());
            
            if(!isChallenge)
            {
                NotificationContentManager.FillNotificationContent(ref newNotification);
            }
            else
            {
                NotificationContentManager.FillChallengeNotificationContent(ref newNotification);
            }
            
            BumperCopsDebug.Log($"Adding notification for time {newNotification.DeliveryTime} local time with id {newNotification.Id}");
            return newNotification;
        }

        protected virtual void OnDestroy()
        {
            if (Platform == null)
            {
                return;
            }

            Platform.NotificationReceived -= OnNotificationReceived;
            if (Platform is IDisposable disposable)
            {
                disposable.Dispose();
            }

            inForeground = false;
        }

        /// <summary>
        /// Respond to application foreground/background events.
        /// </summary>
        protected void OnApplicationFocus(bool hasFocus)
        {
            if (Platform == null || !initialized)
            {
                return;
            }

            inForeground = hasFocus;

            if (hasFocus)
            {
                OnForegrounding();
                return;
            }

            Platform.OnBackground();
            ScheduleDailyNotifications();
            SaveToFile();
        }

        private void SaveToFile()
        {
            // Clear badge numbers again (for saving)
            if (autoBadging)
            {
                foreach (PendingNotification pendingNotification in PendingNotifications)
                {
                    if (pendingNotification.Notification.DeliveryTime.HasValue)
                    {
                        pendingNotification.Notification.BadgeNumber = null;
                    }
                }
            }

            // Calculate notifications to save
            var notificationsToSave = new List<PendingNotification>(PendingNotifications.Count);
            foreach (PendingNotification pendingNotification in PendingNotifications)
            {
                if (pendingNotification.Notification.Scheduled &&
                    pendingNotification.Notification.DeliveryTime.HasValue)
                {
                    notificationsToSave.Add(pendingNotification);
                }
            }

            // Save to disk
            Serializer.Serialize(notificationsToSave);
        }
        /// <summary>
        /// Initialize the notifications manager.
        /// </summary>
        /// <param name="channels">An optional collection of channels to register, for Android</param>
        /// <exception cref="InvalidOperationException"><see cref="Initialize"/> has already been called.</exception>
        public void Initialize()
        {
            if (initialized)
            {
                throw new InvalidOperationException("NotificationsManager already initialized.");
            }

            initialized = true;

#if UNITY_ANDROID
            Platform = new AndroidNotificationsPlatform();

            // Register the notification channels
            var doneDefault = false;
            if (!doneDefault)
            {
                doneDefault = true;
                ((AndroidNotificationsPlatform)Platform).DefaultChannelId = defaultNotificationChannel.Id;
            }

            // Wrap channel in Android object
            var androidChannel = new AndroidNotificationChannel(defaultNotificationChannel.Id, defaultNotificationChannel.Name,
                defaultNotificationChannel.Description,
                (Importance)defaultNotificationChannel.Style)
            {
                CanBypassDnd = defaultNotificationChannel.HighPriority,
                CanShowBadge = defaultNotificationChannel.ShowsBadge,
                EnableLights = defaultNotificationChannel.ShowLights,
                EnableVibration = defaultNotificationChannel.Vibrates,
                LockScreenVisibility = (LockScreenVisibility)defaultNotificationChannel.Privacy
            };

            AndroidNotificationCenter.RegisterNotificationChannel(androidChannel);
            
#elif UNITY_IOS && !UNITY_EDITOR
            Platform = new iOSNotificationsPlatform();
#endif

            if (Platform == null)
            {
                BumperCopsDebug.LogWarning("Unable to create platform for notification");
                return;
            }
            
            PendingNotifications = new List<PendingNotification>();
            Platform.NotificationReceived += OnNotificationReceived;

            // Check serializer
            if (Serializer == null)
            {
                Serializer = new DefaultSerializer(Path.Combine(Application.persistentDataPath, DefaultFilename));
            }

            //OnForegrounding();
        }

        /// <summary>
        /// Event fired by <see cref="Platform"/> when a notification is received.
        /// </summary>
        private void OnNotificationReceived(IGameNotification deliveredNotification)
        {
            // Ignore for background messages (this happens on Android sometimes)
            if (!inForeground)
            {
                return;
            }

            // Find in pending list
            int deliveredIndex =
                PendingNotifications.FindIndex(scheduledNotification =>
                    scheduledNotification.Notification.Id == deliveredNotification.Id);
            if (deliveredIndex >= 0)
            {
                LocalNotificationDelivered?.Invoke(PendingNotifications[deliveredIndex]);

                PendingNotifications.RemoveAt(deliveredIndex);
            }
        }

        // Clear foreground notifications and reschedule stuff from a file
        private void OnForegrounding()
        {
            PendingNotifications.Clear();
            Platform.OnForeground();

            // Deserialize saved items
            IList<IGameNotification> loaded = Serializer?.Deserialize(Platform);

            if (loaded == null)
            {
                return;
            }

            foreach (IGameNotification savedNotification in loaded)
            {
                if (savedNotification.DeliveryTime.Value.Date > DateTime.Now.Date)
                {
                    PendingNotifications.Add(new PendingNotification(savedNotification));
                }
            }

            RemoveExpiredNotifications();
        }
    }
}
