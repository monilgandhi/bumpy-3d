using System;

namespace BumperCops.Notifications
{
    /// <summary>
    /// Represents a notification that was scheduled with <see cref="GameNotificationsManager.ScheduleNotification"/>.
    /// </summary>
    public class PendingNotification
    {
        /// <summary>
        /// The scheduled notification.
        /// </summary>
        public readonly IGameNotification Notification;

        /// <summary>
        /// Instantiate a new instance of <see cref="PendingNotification"/> from a <see cref="IGameNotification"/>.
        /// </summary>
        /// <param name="notification">The notification to create from.</param>
        public PendingNotification(IGameNotification notification)
        {
            Notification = notification ?? throw new ArgumentNullException(nameof(notification));
        }

        public static int CompareByDeliveryTime(PendingNotification a, PendingNotification b)
        {
            if (!a.Notification.DeliveryTime.HasValue)
            {
                return 1;
            }

            if (!b.Notification.DeliveryTime.HasValue)
            {
                return -1;
            }

            return a.Notification.DeliveryTime.Value.CompareTo(b.Notification.DeliveryTime.Value);
        }
    }
}
