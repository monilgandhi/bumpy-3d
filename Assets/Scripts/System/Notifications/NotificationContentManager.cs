using System.Collections.Generic;
using UnityEngine;

namespace BumperCops.Notifications
{
    public class NotificationContentManager
    {
        private static List<NotificationContentItem> notificationContentItem;
        private static bool loadFailed;
        private const string NOTIFICATION_FOLDER_NAME = "Notifications";
        public static void FillNotificationContent(ref IGameNotification notification)
        {
            if(notificationContentItem == null || notificationContentItem.Count == 0)
            {
                LoadNotificationContent();
                if(loadFailed)
                {
                    BumperCopsDebug.LogWarning("Failed to load notification content");
                    return;
                }
            }

            if(notification == null)
            {
                BumperCopsDebug.LogWarning("Argument null: notification");
                return;
            }

            int previousIdx = NotificationStats.Instance.NotificationContentIndex;
            ++previousIdx;
            if(previousIdx == notificationContentItem.Count)
            {
                // reset
                previousIdx = 0;
            }

            NotificationContentItem content = notificationContentItem[previousIdx];
            NotificationStats.Instance.NotificationContentIndex = previousIdx;
            notification.Title = content.Title;
            notification.Body = content.Description;
            notification.Metadata = NotificationContentItem.GetMetadata(content.Id, content.Version);
        }

        public static void FillChallengeNotificationContent(ref IGameNotification notification)
        {
            if(notification == null)
            {
                BumperCopsDebug.LogWarning("Argument null: notification");
                return;
            }

            notification.Title = "Your Challenge Over.";
            notification.Body = "Tap now to see who won!!";
            notification.Metadata = "challenge";
        }

        private static void LoadNotificationContent()
        {
            ConfigCache.TryGetConfig<List<NotificationContentItem>>("notifications", NOTIFICATION_FOLDER_NAME, out notificationContentItem);
            if(notificationContentItem == null || notificationContentItem.Count == 0)
            {
                loadFailed = true;
            }
            else
            {
                loadFailed = false;
            }
        }
    }
}
