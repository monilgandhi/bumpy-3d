﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Cinemachine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    [Header("Game components")]
    [SerializeField]
    private GameManager gameManager;

    [Header("Cameras")]
    [SerializeField]
    private CinemachineVirtualCamera playerNonPlayCameras;

    [SerializeField]
    private CinemachineVirtualCamera gamePlayCamera;

    [SerializeField]
    private CinemachineVirtualCamera playerWeaponCamera;

    [SerializeField]
    private CinemachineVirtualCamera boostCameraGameplay;

    [SerializeField]
    private ParticleSystem windLinesParticleSystem;

    [Header("Post-processing")]
    [SerializeField]
    private PostProcessingController postProcessingController;
    private CinemachineVirtualCamera selectedPlayerCamera;
    private PlayerController playerController;

    private CameraShake selectedCameraShake;
    private void Awake() 
    {
        gameManager.GamePreStart += () => OnSelectNonChaseCamera();

        playerNonPlayCameras.gameObject.SetActive(true);
        gamePlayCamera.gameObject.SetActive(false);
        playerWeaponCamera.gameObject.SetActive(false);
        boostCameraGameplay.gameObject.SetActive(false);

        playerController = gamePlayCamera.Follow.GetComponent<PlayerController>();

        if(playerController == null)
        {
            throw new MissingComponentException(nameof(playerController));
        }

        //player.AttackController.PlayerEnteredAttackZone += OnSelectChaseCamera;
        //player.AttackController.Bumped += (o, args) => OnSelectNonChaseCamera();
        playerController.SpeedController.PlayerBoostUpdate += OnBoostUpdate;
        playerController.healthController.PlayerObstacleCollided += OnPlayerCollision;
    }

    private void Start() 
    {
        postProcessingController.Init(playerController, gameManager.EnemyManager, gameManager);    
    }

    private void OnPlayerCollision(PlayerObstacleCollisionEventArgs args)
    {
        if(!args.IsHitCounted || selectedCameraShake == null)
        {
            return;
        }

        StartCoroutine(selectedCameraShake.Shake(3, Constants.SECONDS_OBSTACLE_COLLISION_EFFECT));
    }

    private void OnBoostUpdate(BoostUpdateEventArgs args)
    {
        SwitchCamera(args.IsTurnedOn ? boostCameraGameplay : gamePlayCamera);
        if(args.IsTurnedOn)
        {
            //BumperCopsDebug.Log("Shaking the camera");
            float shakeStrength = 1.5f;

            if(selectedCameraShake != null)
            {
                StartCoroutine(selectedCameraShake.Shake(shakeStrength, args.BoostLength));
            }
            
            windLinesParticleSystem.Play();
        }
        else
        {
            windLinesParticleSystem.Stop();
        }
    }

    private void SwitchCamera(CinemachineVirtualCamera to)
    {
        if(selectedPlayerCamera != null)
        {
            selectedPlayerCamera.gameObject.SetActive(false);
        }

        selectedPlayerCamera = to;
        selectedPlayerCamera.gameObject.SetActive(true);
        selectedCameraShake = selectedPlayerCamera.GetComponent<CameraShake>();
    }

    private void OnSelectNonChaseCamera()
    {
        SwitchCamera(gamePlayCamera);
    }

    public void ToggleWeaponCamera(bool enable)
    {
        selectedPlayerCamera.gameObject.SetActive(!enable);
        playerWeaponCamera.gameObject.SetActive(enable);
    }
}