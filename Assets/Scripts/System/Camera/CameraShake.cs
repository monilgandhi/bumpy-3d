using System.Collections;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraShake : MonoBehaviour 
{
    private CinemachineVirtualCamera virtualCamera;
    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;
    private void Awake() 
    {
        this.virtualCamera = this.GetComponent<CinemachineVirtualCamera>();
        this.virtualCameraNoise = this.virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();

        if(this.virtualCameraNoise == null)
        {
            throw new MissingComponentException(nameof(Cinemachine.CinemachineBasicMultiChannelPerlin));
        }
        
        this.virtualCameraNoise.m_AmplitudeGain = 0;
    }    

    public IEnumerator Shake(float amplitude, float durationInSeconds)
    {
        if(virtualCameraNoise.m_AmplitudeGain > 0)
        {
            //BumperCopsDebug.LogWarning("CameraShake: Camera is still shaking");
            yield break;
        }

        float startTime = Time.time;
        virtualCameraNoise.m_AmplitudeGain = amplitude;

        yield return new WaitForSecondsRealtime(durationInSeconds);
        BumperCopsDebug.Log("CameraShake: Turn off shaking");
        virtualCameraNoise.m_AmplitudeGain = 0;
    }
}