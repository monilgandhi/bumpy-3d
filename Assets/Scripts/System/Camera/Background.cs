using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Cinemachine;
public class Background : MonoBehaviour 
{
    [SerializeField]
    private CinemachineBrain cinemachineBrain;

    private bool backgroundMergeStarted;

    public void NewCameraActive(ICinemachineCamera from, ICinemachineCamera to)
    {
        if(this.gameObject.activeSelf && from != null && from.VirtualCameraGameObject.tag == "player_main_camera" && !backgroundMergeStarted)
        {
            backgroundMergeStarted = false;
            StartCoroutine(ShowBackground());
        }
    }

    private IEnumerator ShowBackground()
    {
        yield return new WaitWhile(() => cinemachineBrain.ActiveBlend != null);
        List<SpriteRenderer> backgroundSprites = this.GetComponentsInChildren<SpriteRenderer>().ToList();

        if(backgroundSprites.Count <= 0)
        {
            yield return null;
        }

        SpriteRenderer firstSprite = backgroundSprites.First();
        
        while(firstSprite.color.a < 0.7)
        {
            backgroundSprites.ForEach(b => b.color = new Color(1, 1, 1, firstSprite.color.a + Time.deltaTime * 0.2f));
            yield return new WaitForFixedUpdate();
        }
    }
}