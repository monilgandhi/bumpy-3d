using UnityEngine;
using Cinemachine;

public class AutoOrbital : MonoBehaviour 
{
    public float speed = 10f;
 
    private CinemachineOrbitalTransposer m_orbital;
    private bool moveLeft;

    private float waitTime;
    private float waitTimer;

    private const float TURN_TIME = 7;
    private float turnTimer;
    void Start()
    {
        var vcam = GetComponent<CinemachineVirtualCamera>();
        if (vcam != null)
        {
            m_orbital = vcam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        }
    }
 
    void Update()
    {
        if(m_orbital == null)
        {
            return;
        }

        if(waitTimer > 0 && waitTimer < waitTime)
        {
            waitTimer += Time.deltaTime;
            return;
        } else if (waitTimer > 0 && waitTimer >= waitTime)
        {
            turnTimer = 0;
            waitTimer = 0;
        }

        turnTimer += Time.deltaTime;
        if (moveLeft)
        {
            m_orbital.m_XAxis.Value += Time.deltaTime * speed;
        }
        else
        {
            m_orbital.m_XAxis.Value -= Time.deltaTime * speed;
        }

        if((m_orbital.m_XAxis.Value < m_orbital.m_XAxis.m_MinValue + 10f) || (m_orbital.m_XAxis.Value > m_orbital.m_XAxis.m_MaxValue - 10f) || turnTimer > TURN_TIME)
        {
            waitTime = UnityEngine.Random.Range(5, 15);
            //BumperCopsDebug.Log($"AutoOrbit: Waiting for {waitTime}");
            waitTimer = Time.deltaTime;
            moveLeft = !moveLeft;
        }
    }    
}