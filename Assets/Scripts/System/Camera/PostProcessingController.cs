using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
public class PostProcessingController : MonoBehaviour 
{
    private VolumeComponent motionBlur;
    private VolumeComponent bloom;
    private VolumeComponent colorAdjustments;
    private Volume postprocessingVolume;

    [SerializeField]
    private Image crackedScreenImage;

    private void Awake() 
    {
        postprocessingVolume = this.GetComponent<Volume>();
        postprocessingVolume.sharedProfile.TryGet(typeof(MotionBlur), out motionBlur);
        postprocessingVolume.sharedProfile.TryGet(typeof(Bloom), out bloom);
        postprocessingVolume.sharedProfile.TryGet(typeof(ColorAdjustments), out colorAdjustments);

        crackedScreenImage.gameObject.SetActive(false);
        if(motionBlur != null)
        {
            motionBlur.active = false;
        }

        if(bloom != null)
        {
            bloom.active = false;
        }

        if(colorAdjustments != null)
        {
            colorAdjustments.active = false;
        }
    }    

    public void Init(PlayerController player, EnemyManager enemyManager, GameManager gameManager) 
    {
        BumperCopsDebug.Log("Initing post processing");
        player.SpeedController.PlayerBoostUpdate += OnPlayerBoostUpdate;
        player.healthController.PlayerObstacleCollided += OnPlayerCollision;
        enemyManager.EnemySpawned += OnEnemySpawned;
        enemyManager.EnemyHealthOver += OnEnemyHealthOver;
        gameManager.GamePaused += () => ToggleColorAdjustment(true);
        gameManager.GameOver += (args) => ToggleColorAdjustment(true);
        gameManager.GameStarted += (args) => ToggleColorAdjustment(false);
    }

    private void ToggleColorAdjustment(bool enable)
    {
        if(colorAdjustments.active == enable)
        {
            return;
        }

        colorAdjustments.active = enable;    
    }

    private void OnEnemySpawned(EnemyArgs args)
    {
        if(bloom != null)
        {
            bloom.active = true;
        }
    }

    private void OnEnemyHealthOver(EnemyArgs args)
    {
        if(bloom != null)
        {
            bloom.active = false;
        }
    }
    

    private void OnPlayerBoostUpdate(BoostUpdateEventArgs args)
    {
        if(motionBlur == null)
        {
            BumperCopsDebug.LogWarning("Could not find motion blur");
            return;
        }

        if(!args.IsPowerup && args.IsTurnedOn)
        {
            return;
        
        }
        
        //BumperCopsDebug.Log("turning motion blur on");
        motionBlur.active = args.IsTurnedOn;
    }

    private void OnPlayerCollision(PlayerObstacleCollisionEventArgs args)
    {
        if(!args.IsHitCounted)
        {
            return;
        }   

        StartCoroutine(PlayHitEffect());
    }

    private IEnumerator PlayHitEffect()
    {
        float start = Time.time;

        Color startColor = crackedScreenImage.color;
        crackedScreenImage.gameObject.SetActive(true);
        while(Time.time - start <= Constants.SECONDS_OBSTACLE_COLLISION_EFFECT || crackedScreenImage.color.a >= 0)
        {
            crackedScreenImage.color = new Color(startColor.r, startColor.g, startColor.b, crackedScreenImage.color.a - Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        crackedScreenImage.color = new Color(startColor.r, startColor.g, startColor.b, startColor.a);

        BumperCopsDebug.Log($"Alpha is {crackedScreenImage.color.a}");
        crackedScreenImage.gameObject.SetActive(false);
    }
}