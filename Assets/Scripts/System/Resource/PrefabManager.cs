using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PrefabManager : MonoBehaviour
{
    public delegate void PrefabLoadEventHandler(PrefabLoadEventArgs a);
    public event PrefabLoadEventHandler CharacterLoadRequestComplete;
    public event PrefabLoadEventHandler PrefabLoadRequestComplete;
    
    private Dictionary<string, GameObject> prefabDictionary = new Dictionary<string, GameObject>();

    public void LoadPrefabAsync(string prefabId, Type caller, string filePath)
    {
        if(string.IsNullOrEmpty(prefabId) || string.IsNullOrEmpty(filePath))
        {
            BumperCopsDebug.LogWarning("prefabId or filepath is null");
            return;
        }

        GameObject c = null;
        if(prefabDictionary.TryGetValue(prefabId, out c))
        {
            OnPrefabLoadComplete(c, prefabId, caller);
            return;
        }

        StartCoroutine(LoadAsync(filePath, prefabId, caller));
    }

    private IEnumerator LoadAsync(string filePath, string prefabId, Type callingClass)
    {
        BumperCopsDebug.Log($"Loading filepath {filePath}");
        ResourceRequest loadRequest = Resources.LoadAsync<GameObject>(filePath);
        yield return new WaitWhile(() => loadRequest.isDone);

        GameObject asset = loadRequest.asset as GameObject;
        if(asset != null)
        {
            asset.SetActive(false);
            GameObject g = GameObject.Instantiate(asset);

            if(g != null)
            {
                // data needs to be loaded
                OnPrefabLoadComplete(g, prefabId, callingClass);
            }
        }
    }

    private void OnPrefabLoadComplete(GameObject loadedGameObject, string prefabId, Type callingClass)
    {
        if(loadedGameObject != null)
        {
            if(!prefabDictionary.ContainsKey(prefabId))
            {
                prefabDictionary.Add(prefabId, loadedGameObject);
            }
        }
        
        BumperCopsDebug.Log($"Load complete for {prefabId} called by {callingClass}");
        PrefabLoadRequestComplete?.Invoke(new PrefabLoadEventArgs(loadedGameObject, callingClass, prefabId));
        return;
        
    }
}
