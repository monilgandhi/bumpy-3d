using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class NPCMovementController : MonoBehaviour
{
    public int Speed 
    {
         get { return this.recommendedSpeed; }
         set 
         {
            //BumperCopsDebug.Log($"Changing speed to {value} for {this.name}");
             this.recommendedSpeed = value; 
         }
    }
    protected WheelCollider[] turnWheels; 
    protected float turnTime;
    protected int recommendedSpeed;
    protected Rigidbody rigidBody;
    protected Vector3 velocityVector;
    protected bool reduceSpeed;
    protected bool changingLanes;
    protected bool canChangeLanes = true;
    private const float RAYCAST_DISTANCE = 500f;
    public bool isActive { get; protected set; }

    private float laneTimer;
    protected Character Character;
    protected Vector3 vectorDirection = Vector3.forward;

    private LaneController lane;
    public virtual LaneController Lane 
    {
        get 
        {
            if(!this.isActive)
            {
                return null;
            }

            laneTimer += Time.deltaTime;

            if(laneTimer < 2f && lane != null)
            {
                //BumperCopsDebug.Log($"returrning lane is road {lane.Road.MyPathNumber} and lanetimer {laneTimer}");
                return lane;
            }   

            laneTimer = 0f;
            // shoot at ray down
            Vector3 rayStartPosition = new Vector3(this.transform.position.x, this.rigidBody.centerOfMass.y, this.transform.position.z);
            Ray downRay = new Ray(rayStartPosition, Vector3.down);
            // Debug.DrawRay(downRay.origin, downRay.direction, Color.red, 500);
            RaycastHit[] hits;
            if(Physics.Raycast(downRay))
            {
                hits = Physics.RaycastAll(downRay, RAYCAST_DISTANCE);
                if(hits.Length > 0 && hits[0].collider.GetComponent<LaneController>() != null)
                {
                    lane = hits[0].collider.GetComponent<LaneController>();
                    return lane;
                }
            }

            WheelHit laneHit;
            foreach(WheelCollider w in this.turnWheels)
            {
                // we should not put a null check here since no null wheel collider should be here
                if(w == null || !w.GetGroundHit(out laneHit))
                {
                    continue;
                }

                lane = laneHit.collider.GetComponent<LaneController>();

                if(lane == null)
                {
                    continue;
                }

                return lane;
            }

            // we will return the old value and hopefully figure out in the next run. Should be okay for NPC
            return lane;
        }
    }

    public long PathNumber
    {
        get
        {
            LaneController l = Lane;
            if(l == null || l.Road == null)
            {
                return -1;
            }
            
            return l.Road.MyPathNumber;
        }
    }

    // Update is called once per frame

    protected virtual void Move()
    {
        if(!isActive)
        {
            return;
        }

        //velocityVector = transform.TransformDirection(Vector3.forward);
        // we use forward vector because we are not changing lanes of the obstacles yet.
        // when hit, if the y is anything but 0, the car will start flying
        // additionally, making them invactive would stop them immediately which looks rather odd
        //velocityVector = Vector3.forward;
        if (this.rigidBody.velocity.magnitude < this.Speed || this.rigidBody.velocity.z < 0 || this.reduceSpeed)
        {
            this.rigidBody.velocity = velocityVector * this.Speed;
            if(this.reduceSpeed && this.rigidBody.velocity.magnitude < this.Speed)
            {
                this.reduceSpeed = false;
                BumperCopsDebug.LogFormat($"Recommended speed is {this.Speed} current speed is {this.rigidBody.velocity.magnitude}");
            }
        }
    }

    protected virtual void ResetValues()
    {
        velocityVector = Vector3.zero;
        changingLanes = false;
        reduceSpeed = false;
        lane = null;
    }

    protected bool TryGetTrafficAheadOfMe(out RaycastHit hit)
    {
        hit = new RaycastHit();
        WheelCollider w = turnWheels.First();
        if(w == null)
        {
            BumperCopsDebug.LogError("wheels are null");
            return false;
        }

        // check if there is a car or vehicle in front of you. If there is make a turn

        Vector3 rayStartPosition = new Vector3(this.rigidBody.transform.position.x, this.rigidBody.transform.position.y + 0.5f, this.rigidBody.transform.position.z);
        Ray straightRay = new Ray(rayStartPosition, this.rigidBody.transform.forward);
        return Physics.Raycast(straightRay, out hit, RAYCAST_DISTANCE);
    }

    protected virtual IEnumerator ReactToTrafficAhead(NPCMovementController vehicleAheadOfMe = null, bool forceLaneChange = false, float turnTime = 3f)
    {
        if(Lane == null && forceLaneChange)
        {
            yield break;
        }

        // gget the next lane to go to first
        LaneController nextLane = CarUtils.GetNextLane(this.Lane);

        if(nextLane == null)
        {
            // if this is a force lane change and there is no next lane change, escape
            yield  break;
        }

        // calculate the distance between vehicle ahead if peresent
        if(forceLaneChange)
        {
            if(vehicleAheadOfMe != null)
            {
                float zDistance = vehicleAheadOfMe.transform.position.z - this.rigidBody.transform.position.z;
                float calculatedTurnTime = zDistance / this.rigidBody.velocity.magnitude;
                turnTime = calculatedTurnTime <= 0 ? 0.5f : calculatedTurnTime;
            }

            //BumperCopsDebug.Log($"NPCMovementController: Making turn with turntime {turnTime}");
            StartCoroutine(MakeTurn(nextLane, turnTime));
            yield break;
        }

        if(UnityEngine.Random.value < 0.0f)
        {
            {
                //BumperCopsDebug.Log("NPCMovementController: Making turn");
                StartCoroutine(MakeTurn(nextLane, turnTime));
            }
        }
        else if(vehicleAheadOfMe != null && this.Speed > vehicleAheadOfMe.Speed)
        {
            // Slow down the speed based on vehicle ahead of you
            // get the speed of vehicle ahead of me
            //BumperCopsDebug.Log($"NPCMovementController: going to slow down from {this.recommendedSpeed} to {vehicleAheadOfMe.Speed - 3} for {this.transform.root.name}");
            this.Speed = 0;//Mathf.FloorToInt(vehicleAheadOfMe.Speed - 3);
            this.reduceSpeed = true;
        }

        yield break;
    }

    public IEnumerator MakeTurn(LaneController nextTargetLane, 
        float turnSpeed,
        bool forceLaneChange = false)
    {
        if(this.Lane == null || 
            !isActive || 
            ((!canChangeLanes || changingLanes) && !forceLaneChange))
        {
            yield break;
        }

        changingLanes = true;
        bool isTurningLeft = this.Lane.transform.position.x > nextTargetLane.transform.position.x;

        if(this.Character != null && this.Character.TurnLights != null)
        {
            this.Character.TurnLights.MakeTurn(isTurningLeft);
        }
        
        // we want the turn to complete in 0.5 seconds
        float distanceToTravel = turnSpeed * this.rigidBody.velocity.magnitude;

        Vector3 targetLocationVector = new Vector3(nextTargetLane.transform.position.x, 0, transform.position.z + (distanceToTravel * vectorDirection.z));
        Vector3 heading = (targetLocationVector - this.rigidBody.position);

        this.rigidBody.transform.rotation = Quaternion.LookRotation(heading, Vector3.up);

        float turnStartTime = Time.time;
        //BumperCopsDebug.Log($"isTurningLeft: {isTurningLeft}, target: {nextTargetLane.transform.position.x}, currentPosition: {this.rigidBody.position.x}");
        yield return new WaitUntil(() => ((isTurningLeft && this.rigidBody.position.x < nextTargetLane.transform.position.x) || 
                (!isTurningLeft && this.rigidBody.position.x > nextTargetLane.transform.position.x) || Time.time - turnStartTime > turnSpeed));

        changingLanes = false;
        if(this.Character != null && this.Character.TurnLights != null)
        {
            this.Character.TurnLights.CompleteTurn();
        }
        
        targetLocationVector = new Vector3(this.transform.position.x, 0, transform.position.z + distanceToTravel);
        heading = (targetLocationVector - this.rigidBody.position);
        this.rigidBody.transform.rotation = Quaternion.LookRotation(heading, Vector3.up);

        // BumperCopsDebug.Log($"NPCMovementController: moved into target lane {this.transform.root.name}");

        turnStartTime = Time.time;
        yield return new WaitUntil(() => Mathf.RoundToInt(this.rigidBody.transform.eulerAngles.y) == 0 || Time.time - turnStartTime > turnSpeed);
        // BumperCopsDebug.Log($"NPCMovementController: Lane change complete for {this.transform.root.name}");
        //BumperCopsDebug.Log("NPCMovementController: Turn complete");
    }

    private void OnDisable() 
    {
        isActive = false;
    }

    public void Deactivate()
    {
        //BumperCopsDebug.Log("Deactivating from npc controller");
        isActive = false;
        if(rigidBody != null)
        {
            rigidBody.velocity = Vector3.zero;
        }
    }

    public void MakeInactive()
    {
        isActive = false;
    }
}