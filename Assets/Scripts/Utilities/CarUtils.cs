﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CarUtils
{
    public static Sprite GetPlayerCarSprite(string itemId)
    {
        if(string.IsNullOrEmpty(itemId))
        {
            throw new ArgumentNullException(nameof(itemId));
        }
        
        string filePath = "player" + Path.DirectorySeparatorChar + PlayerStats.Instance.CurrentCity + Path.DirectorySeparatorChar +
            itemId + Path.DirectorySeparatorChar + "screenshot";

        // Load sprite
        return Resources.Load<Sprite>(filePath);
    }
    
    public static IEnumerator ToggleImage(Image image, int speed = 1)
    {
        bool reducing = image.color.a > 0;
        Color c = image.color;
        
        while(reducing && image.color.a > 0)
        {
            image.color = new Color(c.r, c.g, c.b, image.color.a - (Time.deltaTime * speed));
            yield return new WaitForEndOfFrame();
        }

        while(!reducing && image.color.a < 1)
        {
            image.color = new Color(c.r, c.g, c.b, image.color.a + (Time.deltaTime * speed));
            yield return new WaitForEndOfFrame();
        }
    }

    public static bool IsTruck(ObstacleType type)
    {
        return type == ObstacleType.CONSTRUCTION || type == ObstacleType.FIRE_TRUCK || type == ObstacleType.GARBAGE || type == ObstacleType.TRUCK;
    }

    public static string SanitzeName(string s)
    {
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }

        StringBuilder sb = new StringBuilder(s.ToLower());
        sb[0] = char.ToUpper(sb[0]);

        sb = sb.Replace('_', ' ');
        return sb.ToString();
    }

    public static int DisplaySkillPointsValue(Skill skillType, float points)
    {
        return skillType == Skill.BOOST ? Mathf.RoundToInt(points * 10) : Mathf.RoundToInt(points);
    }
    

    public static long GetCurrentTimeSecondsFromEpoch()
    {
        return Convert.ToInt64((DateTime.UtcNow - Constants.EPOCH).TotalSeconds);
    }

    public static bool IsUnityDevelopmentBuild()
    {
        #if DEBUG || DEVELOPMENT_BUILD
        return true;
        #endif
        
        return false;
    }

    public static int GetLaneWrtPlayer(RoadController roadToGenerate, LaneController playersLane, bool equalSelectionProbabiltyDouble = true)
    {
        if(roadToGenerate == null)
        {
            return -1;
        }
        
        if(roadToGenerate.RoadType != RoadManager.RoadType.FORK || playersLane == null)
        {
            return roadToGenerate.GetRandomLaneId(equalSelectionProbabiltyDouble);
        }

        //BumperCopsDebug.Log($"ObstacleManager: players lane direction is {this.player.MoveController.Lane.LaneDirection}");
        int laneId = roadToGenerate.GetRandomLaneIdInDirection(playersLane.LaneDirection);
        //BumperCopsDebug.Log($"ObstacleManager: selected Lane is {laneId}");
        return laneId;
    }

    public static int GetRandomLane(RoadController road, 
        LaneDirection laneDirection = LaneDirection.NONE, 
        List<int> excludeLaneIds = null)
    {
        int laneId = laneDirection != LaneDirection.NONE ? road.GetRandomLaneIdInDirection(laneDirection) : road.GetRandomLaneId(true);
        if(excludeLaneIds == null)
        {
            return laneId;
        }

        int loopBreaker = 0;
        while(excludeLaneIds.Contains(laneId) && loopBreaker < 3)
        {
            laneId = laneDirection != LaneDirection.NONE ? road.GetRandomLaneIdInDirection(laneDirection) : road.GetRandomLaneId(true);
            ++loopBreaker;
        }

        return laneId;
    }

    // Range is inclsive of both the ends
    public static LaneController GetNextLane(LaneController givenLane)
    {
        LaneController laneController = givenLane;

        if(givenLane == null)
        {
            return laneController;
        }

        if(UnityEngine.Random.value > 0.5f)
        {
            if(givenLane.PreviousLane == null)
            {
                laneController = givenLane.NextLane.GetComponent<LaneController>();
            } 
            else 
            {
                laneController = givenLane.PreviousLane.GetComponent<LaneController>();
            }
        } else
        {   
            if(givenLane.NextLane == null)
            {
                if(givenLane.PreviousLane == null)
                {
                    BumperCopsDebug.Log("null");
                }
                
                laneController = givenLane.PreviousLane.GetComponent<LaneController>();
            }
            else
            {
                laneController = givenLane.NextLane.GetComponent<LaneController>();
            }
        }
 
        return laneController;
    }

    public static bool IsAnimationComplete(Animator animator, string stateName)
    {
        if(animator == null || string.IsNullOrEmpty(stateName))
        {
            BumperCopsDebug.LogWarning("ArgumentNullException: animator or stateName");
            return false;
        }
        
        return animator.GetCurrentAnimatorStateInfo(0).IsName(stateName) && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1;
    }

    public static string GetConfigLocationForShopItem(string id, ShopItemType itemType)
    {
        if(itemType == ShopItemType.NONE)
        {
            throw new ArgumentNullException("id or itemType");
        }

        switch(itemType)
        {
            case ShopItemType.CAR:
                return Constants.PLAYER_FOLDER + 
                    Path.DirectorySeparatorChar + 
                    PlayerStats.Instance.CurrentCity + 
                    Path.DirectorySeparatorChar +
                    "cars" +
                    Path.DirectorySeparatorChar +
                    id;

            case ShopItemType.WEAPON:
                return Constants.PLAYER_FOLDER + 
                    Path.DirectorySeparatorChar + 
                    PlayerStats.Instance.CurrentCity + 
                    Path.DirectorySeparatorChar +
                    "weapons" +
                    Path.DirectorySeparatorChar +
                    id;
            
            case ShopItemType.POWERUP:
                return Constants.COLLECTIBLE_FOLDER;

            default:
                return null;            
        }
    }

    public static Sprite GetCollectibleSprite(CollectibleType type)
    {
        if(type == CollectibleType.NONE)
        {
            return null;
        }

        string filePath = GetConfigLocationForShopItem(null, ShopItemType.POWERUP) + 
                Path.DirectorySeparatorChar + 
                "screenshots" +
                Path.DirectorySeparatorChar +
                type;
                
        BumperCopsDebug.Log($"Loading sprite at location {filePath}");
        Sprite s = Resources.Load<Sprite>(filePath);
        if(s == null)
        {
            BumperCopsDebug.LogWarning($"Sprite not found for collectible {type} at path {filePath}");
        }

        return s;
    }
}
