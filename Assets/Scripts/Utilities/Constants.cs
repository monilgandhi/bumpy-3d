﻿using UnityEngine;
using System;
using System.IO;
using System.Globalization;
public static class Constants
{
    public const int PATROL_SPEED = 45;
    public const int NPC_SPEED = 30;
    public const int TOTAL_LANES = 5;
    public const string PREFAB_BASE_LOCATION = "Prefabs";
    public const string CURRENT_CITY = "city1";
    public const int WORLD_SPEED = 6;
    public const int ROAD_LAYER = 8;
    public const int PLAYER_LAYER = 9;
    public const int ENEMY_LAYER = 11;
    public const int NPC_LAYER = 10;
    public const int SHOP_LAYER = 15;
    public const int ENVIRONMENT_LAYER = 13;
    public const int PROJECTILES_LAYER = 16;
    public const int PLAYER_NPC_LAYER = 17;
    public const int ROAD_AHEAD_PLAYER_OR_NPC = 11;
    public const float SEC_DISTANCE_BETWEEN_ENEMY_PLAYER_SPAWN = 1.5f;
    public const int ROAD_DISTANCE_BETWEEN_NPC_PLAYER_SPAWN = 4;
    public const int ROAD_DISTANCE_BETWEEN_BONUS_PLAYER_SPAWN = 3;
    public const float SECONDS_OBSTACLE_COLLISION_EFFECT = 1;
    public const int DISTANCE_AHEAD_PLAYER_IN_CASE_STUCK = 15;
    public const int ATTACK_ZONE_REACH = 15;
    public const string PLAYER_COLLECTIBLE_TAG = "player_collectible";
    public const string PLAYER_TAG = "player_character";
    public const string CENTER_BARRIER_TAG = "CenterBarrier";
    public const string COLLECTIBLE_FOLDER = "collectible";
    public const int BONUS_ROAD_COUNT_TO_REMOVE_OBSTACLES = 4;
    public const string ENEMY_SEQUENCE_JSON_LOCATION = "enemy";
    private static string PREFAB_ASSET_PATH = 
        "Prefabs" + Path.DirectorySeparatorChar + "police";
    private static string WEAPON_PREFAB_ASSET_PATH = 
        "Prefabs" + Path.DirectorySeparatorChar + "police" + Path.DirectorySeparatorChar + "weapons";

    public const string BACKUP_COIN_SPENT_REASON = "backup";
    public const string CAR_COIN_SPENT_REASON = "car";
    public const string PLAYER_FOLDER = "player";
    
    //BCAC00
    public static Color LEADERBOARD_USER_COLOR = new Color(0.73f, 0.67f, 0);
    //79500E
    public static Color USER_RANK_NOT_ACHIEVED_COLOR = new Color(0.47f, 0.31f, 0.05f);

    //0E7915
    public static Color USER_RANK_ACHIEVED_COLOR = new Color(0.05f, 0.47f, 0.08f);

    //00B6FF
    public static Color BLUE_BACKGROUND = new Color(0, 0.7f, 1f);
    public static readonly DateTime EPOCH = new DateTime(1970, 1, 1).ToUniversalTime();
    public static String GetPlayerPrefabPath(string prefabId)
    {
        if(string.IsNullOrEmpty(prefabId))
        {
            throw new ArgumentNullException(nameof(prefabId));
        }
        
        return PREFAB_ASSET_PATH + Path.DirectorySeparatorChar + prefabId;
    }

    public static String GetWeaponPrefabPath(string prefabId)
    {
        if(string.IsNullOrEmpty(prefabId))
        {
            throw new ArgumentNullException(nameof(prefabId));
        }
        
        return WEAPON_PREFAB_ASSET_PATH + Path.DirectorySeparatorChar + 
            prefabId + Path.DirectorySeparatorChar + 
            prefabId;
    }
}
