Thank you for purchasing Toon Vehicles!

Version 1.2:

The assets are very simple to use, just drag and drop one of the prefabs into the scene.

Each vehicle has the wheels separated, with centered pivot points.

The naming convention uses the “.L” and “.R” suffixes 
for wheels on the left-hand side and right-hand side respectively.
Example: the wheel on the front right of the vehicle uses the name "Wheel_F.R”

The textures used by the material are very small in size: the diffuse 
texture is 128*128 and the ramp texture is just 128*4.

Some of the cars have moving parts, such as animated lights, trunk doors, cranes, pistons, etc.
It's easy to edit their position and rotation, by selecting them from the objects’ hierarchy.

In addition to the cars, there are some props and particle systems included in the 
package, to complement the cars and add to the overall stylized feeling.

If you plan to use the LWPR version of the pack, starting with the 2019.1.1 and later versions of Unity,
just open the LWRP installer that's found in the main Toon Vehicles folder.



For additional info, details or requests, don’t hesitate to contact at:

https://www.facebook.com/SICSproduction/