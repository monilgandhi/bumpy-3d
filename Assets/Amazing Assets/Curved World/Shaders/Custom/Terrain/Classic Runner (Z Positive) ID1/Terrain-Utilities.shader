// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Hidden/Amazing Assets/Curved World/Terrain/Classic Runner (Z Positive) ID 1/Utilities"
{
    SubShader
    {
        Pass
        {
            Name "Picking"
            Tags { "LightMode" = "Picking" }

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard srp library
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            #pragma vertex DepthOnlyVertex
            #pragma fragment DepthOnlyFragment

            #pragma multi_compile_instancing
            #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap

            #define PICKINGPASS
            #include "Packages/com.unity.render-pipelines.universal/Shaders/Terrain/TerrainLitInput.hlsl"


            #define CURVEDWORLD_BEND_TYPE_CLASSICRUNNER_Z_POSITIVE
            #define CURVEDWORLD_BEND_ID_1            

            #include "../TerrainLitPasses.hlsl"
            ENDHLSL
        }

        Pass
        {
            Name "SceneSelectionPass"
            Tags { "LightMode" = "SceneSelectionPass" }

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard srp library
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            #pragma vertex DepthOnlyVertex
            #pragma fragment DepthOnlyFragment

            #pragma multi_compile_instancing
            #pragma instancing_options assumeuniformscaling nomatrices nolightprobe nolightmap

            #define SCENESELECTIONPASS
            #include "Packages/com.unity.render-pipelines.universal/Shaders/Terrain/TerrainLitInput.hlsl"


            #define CURVEDWORLD_BEND_TYPE_CLASSICRUNNER_Z_POSITIVE
            #define CURVEDWORLD_BEND_ID_1

            #include "../TerrainLitPasses.hlsl"
            ENDHLSL
        }
    }
}
