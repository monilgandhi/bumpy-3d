# Readme
This is the source code for project codename bumpy

## Run
- Download the project
- Open the project in Unity
- Look into folder "Scenes"
- Select Gameplay
- Hit run. 

## Controls on computer
- Mouse movement to turn the car
- Left click to attack
- Space for boost.

## Changing the boost for the car
### Requirements
Lets take an example of the car named "SM_Veh_Car_Police_Heist_01"
- Select the prefab named "SM_Veh_Car_Police_Heist_01"
- Scroll down to the character component
- In "boost on bump" select the boost particle system